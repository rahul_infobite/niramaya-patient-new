package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition;
import com.ibt.niramaya.ui.activity.ViewInvoiceDetailActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class BloodRequisitionAdapter extends RecyclerView.Adapter<BloodRequisitionAdapter.MyViewHolder> {

    private List<BloodRequisition> bloodRequisitionList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public BloodRequisitionAdapter(List<BloodRequisition> bloodRequisitionList, Context mContext, View.OnClickListener onClickListener) {
        this.bloodRequisitionList = bloodRequisitionList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View itemView = li.inflate(R.layout.row_blood_requisition, null);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BloodRequisition bloodRequisition = bloodRequisitionList.get(position);
        holder.tvPatientName.setText(bloodRequisition.getPatientName());
        holder.txtDoctorName.setText(bloodRequisition.getRequisitionByDoctorName());
        holder.tvPatientContact.setText(bloodRequisition.getPatientContact());
        holder.requisitionDate.setText(changeDateFormat(bloodRequisition.getRequisitionDob()));

        holder.cvBloodRequisition.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, ViewInvoiceDetailActivity.class);
            intent.putExtra("billLInk", bloodRequisition.getSlipLink());
            intent.putExtra("invoiceNumber", bloodRequisition.getPatientName());
            mContext.startActivity(intent);
        });
    }

    @Override
    public int getItemCount() {
        return bloodRequisitionList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView tvPatientName, txtDoctorName, tvPatientContact, requisitionDate;
        CardView cvBloodRequisition;

        public MyViewHolder(View view) {
            super(view);
            tvPatientName = view.findViewById(R.id.tvPatientName);
            txtDoctorName = view.findViewById(R.id.txtDoctorName);
            tvPatientContact = view.findViewById(R.id.tvPatientContact);
            requisitionDate = view.findViewById(R.id.requisitionDate);
            cvBloodRequisition = view.findViewById(R.id.cvBloodRequisition);
        }
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}

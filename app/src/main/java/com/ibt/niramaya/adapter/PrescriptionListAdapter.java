package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.prescription.OpdList;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class PrescriptionListAdapter extends RecyclerView.Adapter<PrescriptionListAdapter.MyViewHolder> {

    private List<OpdList> vendorLists;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public PrescriptionListAdapter(List<OpdList> vendorLists, Context mContext, View.OnClickListener onClickListener) {
        this.vendorLists = vendorLists;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_prescription_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtDate.setText(changeDateFormat(vendorLists.get(position).getOpdCreatedDate()));
        holder.txtHospitalName.setText(vendorLists.get(position).getHospitalName());
        holder.txtOpdTitle.setText("OPD : " + vendorLists.get(position).getOpdTitle());
        holder.txtDoctorName.setText(vendorLists.get(position).getDoctorName());

        holder.txtOpen.setTag(position);
        holder.txtOpen.setOnClickListener(onClickListener);
        holder.cardViewItem.setTag(position);
        holder.cardViewItem.setOnClickListener(onClickListener);

        holder.btnBloodSlip.setVisibility(View.VISIBLE);
        holder.btnBloodSlip.setTag(position);
        holder.btnBloodSlip.setOnClickListener(onClickListener);


        if (vendorLists.get(position).getTypeOfDischarge().equals("Referred")) {
            holder.btnReferSlip.setVisibility(View.VISIBLE);
        }
        holder.btnReferSlip.setTag(position);
        holder.btnReferSlip.setOnClickListener(onClickListener);

    }

    @Override
    public int getItemCount() {
        return vendorLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtDate, txtHospitalName, txtOpdTitle, txtDoctorName, txtOpen;
        private CardView cardViewItem;
        private Button btnBloodSlip, btnReferSlip;

        public MyViewHolder(View view) {
            super(view);
            txtDate = view.findViewById(R.id.txtDate);
            txtHospitalName = view.findViewById(R.id.txtHospitalName);
            txtDoctorName = view.findViewById(R.id.txtDoctorName);
            txtOpen = view.findViewById(R.id.txtOpen);
            cardViewItem = view.findViewById(R.id.cardViewItem);
            txtOpdTitle = view.findViewById(R.id.txtOpdTitle);
            btnBloodSlip = view.findViewById(R.id.btnBloodSlip);
            btnReferSlip = view.findViewById(R.id.btnReferSlip);
        }
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}

package com.ibt.niramaya.adapter.doctor_opd;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.home.HospitalDatum;
import com.ibt.niramaya.modal.hospital_detail.DoctorDatum;

import java.util.List;

public class DoctorOpdListAdapter extends RecyclerView.Adapter<DoctorOpdListAdapter.MyViewHolder> {

    private List<DoctorDatum> doctorList;
    private Context mContext;
    private View.OnClickListener onClickListener;
    private HospitalDatum hospitalData;

    public DoctorOpdListAdapter(List<DoctorDatum> doctorList, Context mContext,
                                View.OnClickListener onClickListener, HospitalDatum hospitalData) {
        this.doctorList = doctorList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
        this.hospitalData = hospitalData;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_peditrician, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        DoctorDatum doctorData = doctorList.get(position);
        holder.tvDoctorName.setText(doctorData.getName());
        if (doctorData.getDoctorSpecialization().size()>0) {
            holder.tvDoctorSpecialization.setText(doctorData.getDoctorSpecialization().get(0).getSpecializationTitle());
        }
        holder.tvDoctorAddress.setText(hospitalData.getHospitalStreetName());
        holder.tvDoctorRatings.setText(doctorData.getRating());
        holder.btnBookAppointment.setTag(position);
        holder.btnBookAppointment.setOnClickListener(onClickListener);

        Glide.with(mContext)
                .load(doctorData.getProfileImage())
                .placeholder(R.drawable.ic_profile)
                .into(holder.civDoctorProfile);

        StringBuilder specialization = new StringBuilder();
        int sCount = 0;

        for (int i = 0; i < doctorData.getDoctorSpecialization().size(); i++) {
            if (sCount == 0) {
                specialization = new StringBuilder(doctorData.getDoctorSpecialization().get(i).getSpecializationTitle().trim());
                sCount++;
            } else {
                specialization.append(", ").append(doctorData.getDoctorSpecialization().get(i).getSpecializationTitle().trim());
            }
        }
        holder.tvDoctorSpecialization.setText(specialization);

    }

    @Override
    public int getItemCount() {
        return doctorList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDoctorName, tvDoctorSpecialization, tvDoctorAddress, tvDoctorRatings;
        private ImageView civDoctorProfile;
        private LinearLayout llDoctor;
        private Button btnBookAppointment;

        public MyViewHolder(View view) {
            super(view);
            llDoctor = view.findViewById(R.id.llDoctor);

            tvDoctorName = view.findViewById(R.id.tvDoctorName);
            tvDoctorSpecialization = view.findViewById(R.id.tvDoctorSpecialization);
            tvDoctorAddress = view.findViewById(R.id.tvDoctorAddress);
            tvDoctorRatings = view.findViewById(R.id.tvDoctorRatings);

            btnBookAppointment = view.findViewById(R.id.btnBookAppointment);

            civDoctorProfile = view.findViewById(R.id.civDoctorProfile);
        }
    }

}

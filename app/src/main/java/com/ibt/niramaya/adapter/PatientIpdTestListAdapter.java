package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.ipd_test_report.TestList;
import com.ibt.niramaya.utils.MultipleDateTimeFormats;

import java.util.List;

public class PatientIpdTestListAdapter extends RecyclerView.Adapter<PatientIpdTestListAdapter.MyViewHolder> {

    private List<TestList> reportLists;
    private Context mContext;
    private String patientName, patientId, ipdDate;
    private View.OnClickListener onclicktioner;

    public PatientIpdTestListAdapter(List<TestList> reportLists, Context mContext, View.OnClickListener onclicktioner) {
        this.reportLists = reportLists;
        this.mContext = mContext;
        this.onclicktioner = onclicktioner;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View itemView = li.inflate(R.layout.row_report_list, null);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        TestList testList = reportLists.get(position);

        holder.tvHospital.setText(testList.getHospitalName());
        holder.tvDoctorName.setText("Doctor Name: " + testList.getTestReferDoctor());
        holder.tvTestName.setText("Test : " + testList.getPathologyTestName());
        holder.tvPatientName.setText(testList.getPatientName());
        holder.tvTestSample.setText("Sample : " + testList.getTestSample());
        if (!testList.getTestSampleDate().isEmpty()) {
            holder.tvTestDate.setText("Date : " + MultipleDateTimeFormats.dateFormatyyyyMMddToddMMyyyy(testList.getTestSampleDate()));
        }
        holder.tvTestSampleList.setText("Sample List : " + testList.getTestSampleList());
        holder.testPresquistics.setText("Perquisites :" + testList.getTestPrerequisites());
        holder.testSuggestion.setText("Suggestion : " + testList.getIpdTestReportSuggestion());
        holder.testDescription.setText("Description : " + testList.getIpdTestReportDescription());


        if (!testList.getPatientDateOfBirth().isEmpty()) {
            holder.tvPatientAge.setText(MultipleDateTimeFormats.getAge(testList.getPatientDateOfBirth()));
        }

        Glide.with(mContext).load(testList.getHospitalImage()).placeholder(R.drawable.img_c).into(holder.ivHospital);
        if (!testList.getIpdTestReportFileType().isEmpty()) {
            holder.txtView.setTag(position);
            holder.txtView.setOnClickListener(onclicktioner);
        } else {
            holder.txtView.setVisibility(View.GONE);
        }
        holder.cardViewItem.setTag(position);
        holder.cardViewItem.setOnClickListener(onclicktioner);

    }

    @Override
    public int getItemCount() {
        return reportLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView restaurent_name, txtOpen, tvHospital, tvDoctorName, tvPatientAge, tvTestName, txtView, tvPatientName,
                tvTestSample, tvTestSampleList, tvTestDate, testPresquistics, testSuggestion, testDescription;
        public ImageView rc_img, ivHospital;
        private CardView cardViewItem;

        public MyViewHolder(View view) {
            super(view);
            cardViewItem = view.findViewById(R.id.cardViewItem);
            txtOpen = view.findViewById(R.id.txtOpen);
            restaurent_name = view.findViewById(R.id.restaurent_name);
            rc_img = view.findViewById(R.id.restaurent_img);

            txtView = view.findViewById(R.id.txtView);

            ivHospital = view.findViewById(R.id.ivHospital);
            tvHospital = view.findViewById(R.id.tvHospital);
            tvDoctorName = view.findViewById(R.id.tvDoctorName);
            tvTestName = view.findViewById(R.id.tvTestName);
            tvPatientName = view.findViewById(R.id.tvPatientName);
            tvPatientAge = view.findViewById(R.id.tvPatientAge);
            tvTestSample = view.findViewById(R.id.tvTestSample);
            tvTestSampleList = view.findViewById(R.id.tvTestSampleList);
            tvTestDate = view.findViewById(R.id.tvTestDate);
            testPresquistics = view.findViewById(R.id.testPresquistics);
            testSuggestion = view.findViewById(R.id.testSuggestion);
            testDescription = view.findViewById(R.id.testDescription);
        }
    }

}

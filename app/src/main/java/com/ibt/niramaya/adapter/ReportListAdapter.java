package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.report.TestList;
import com.ibt.niramaya.utils.MultipleDateTimeFormats;

import java.util.List;

public class ReportListAdapter extends RecyclerView.Adapter<ReportListAdapter.MyViewHolder> {

    private List<TestList> testReportList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public ReportListAdapter(List<TestList> testReportList, Context mContext, View.OnClickListener onClickListener) {
        this.testReportList = testReportList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_report_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        TestList testList = testReportList.get(position);
        holder.tvHospital.setText(testList.getHospitalName());
        holder.tvDoctorName.setText("Doctor Name: " + testList.getTestReferDoctor());
        holder.tvTestName.setText("Test : " + testList.getPathologyTestName());
        holder.tvPatientName.setText(testList.getPatientName());
        holder.tvTestSample.setText("Sample : " + testList.getTestSample());
        if (!testList.getTestSampleDate().isEmpty()) {
            holder.tvTestDate.setText("Date : " + MultipleDateTimeFormats.dateFormatyyyyMMddToddMMyyyy(testList.getTestSampleDate()));
        }
        holder.tvTestSampleList.setText("Sample List : " + testList.getTestSampleList());
        holder.testPresquistics.setText("Perquisites :" + testList.getTestPrerequisites());
        holder.testSuggestion.setText("Suggestion : " + testList.getTestReportSuggestion());
        holder.testDescription.setText("Description : " + testList.getTestReportDescription());


        if (!testList.getPatientDateOfBirth().isEmpty()) {
            holder.tvPatientAge.setText(MultipleDateTimeFormats.getAge(testList.getPatientDateOfBirth()));
        }

        Glide.with(mContext).load(testList.getHospitalImage()).placeholder(R.drawable.img_c).into(holder.ivHospital);
        if (!testList.getTestReportFileType().isEmpty()) {
            holder.txtView.setTag(position);
            holder.txtView.setOnClickListener(onClickListener);
        } else {
            holder.txtView.setVisibility(View.GONE);
        }
        holder.cardViewItem.setTag(position);
        holder.cardViewItem.setOnClickListener(onClickListener);

    }

    @Override
    public int getItemCount() {
        return testReportList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView restaurent_name, txtOpen, tvHospital, tvDoctorName, tvPatientAge, tvTestName, txtView, tvPatientName,
                tvTestSample, tvTestSampleList, tvTestDate, testPresquistics, testSuggestion, testDescription;
        public ImageView rc_img, ivHospital;
        private CardView cardViewItem;

        public MyViewHolder(View view) {
            super(view);
            cardViewItem = view.findViewById(R.id.cardViewItem);
            txtOpen = view.findViewById(R.id.txtOpen);
            restaurent_name = view.findViewById(R.id.restaurent_name);
            rc_img = view.findViewById(R.id.restaurent_img);

            txtView = view.findViewById(R.id.txtView);

            ivHospital = view.findViewById(R.id.ivHospital);
            tvHospital = view.findViewById(R.id.tvHospital);
            tvDoctorName = view.findViewById(R.id.tvDoctorName);
            tvTestName = view.findViewById(R.id.tvTestName);
            tvPatientName = view.findViewById(R.id.tvPatientName);
            tvPatientAge = view.findViewById(R.id.tvPatientAge);
            tvTestSample = view.findViewById(R.id.tvTestSample);
            tvTestSampleList = view.findViewById(R.id.tvTestSampleList);
            tvTestDate = view.findViewById(R.id.tvTestDate);
            testPresquistics = view.findViewById(R.id.testPresquistics);
            testSuggestion = view.findViewById(R.id.testSuggestion);
            testDescription = view.findViewById(R.id.testDescription);
        }
    }

}

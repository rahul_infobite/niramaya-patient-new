package com.ibt.niramaya.adapter.ipd_prescription_adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.ipd_prescription.details.IpdPreceptionTest;

import java.util.List;

public class IpdPathologyTestDetailAdapter extends RecyclerView.Adapter<IpdPathologyTestDetailAdapter.MyViewHolder> {

    private List<IpdPreceptionTest> prescriptiontList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public IpdPathologyTestDetailAdapter(List<IpdPreceptionTest> prescriptiontList, Context mContext) {
        this.prescriptiontList = prescriptiontList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_prescriptin_detail_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IpdPreceptionTest preception = prescriptiontList.get(position);
        if (preception.getPreception() == 0) {
            holder.llViewPrescriptionImage.setVisibility(View.VISIBLE);
            holder.llViewPrescriptionName.setVisibility(View.GONE);
            Glide.with(mContext).load(preception.getTestName()).into(holder.ivFrMedicineDose);
            //  Glide.with(mContext).load(preception.getMedicineName()).into(holder.ivFrMedicineImage);
        } else {

            holder.llViewPrescriptionName.setVisibility(View.VISIBLE);
            holder.llViewPrescriptionImage.setVisibility(View.GONE);
            holder.tvFrMedicineName.setText(preception.getTestName());
            //   holder.tvFrMedicineDose.setText(preception.getMedicineDose());
        }
    }

    @Override
    public int getItemCount() {
        return prescriptiontList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvFrMedicineName, tvFrMedicineDose;
        public ImageView ivFrMedicineImage, ivFrMedicineDose;
        private LinearLayout llViewPrescriptionImage, llViewPrescriptionName;

        public MyViewHolder(View view) {
            super(view);
            ivFrMedicineImage = view.findViewById(R.id.ivFrMedicineImage);
            ivFrMedicineDose = view.findViewById(R.id.ivFrMedicineDose);
            tvFrMedicineName = view.findViewById(R.id.tvFrMedicineName);
            tvFrMedicineDose = view.findViewById(R.id.tvFrMedicineDose);
            llViewPrescriptionName = view.findViewById(R.id.llViewPrescriptionName);
            llViewPrescriptionImage = view.findViewById(R.id.llViewPrescriptionImage);
        }
    }

}

package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.nurse_ipd_reporting.IpdReportList;
import com.ibt.niramaya.ui.activity.ViewInvoiceDetailActivity;
import com.ibt.niramaya.utils.AppPreference;

import java.util.List;

public class NurseIpdReportListAdapter extends RecyclerView.Adapter<NurseIpdReportListAdapter.MyViewHolder> {

    private List<IpdReportList> reportLists;
    private Context mContext;
    private String patientName, patientId, ipdDate;
    private View.OnClickListener onclicktioner;

    public NurseIpdReportListAdapter(List<IpdReportList> reportLists, Context mContext, String patientId, String patientName, String ipdDate, View.OnClickListener onclicktioner) {
        this.reportLists = reportLists;
        this.mContext = mContext;
        this.onclicktioner = onclicktioner;
        this.patientName = patientName;
        this.patientId = patientId;
        this.ipdDate = ipdDate;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View itemView = li.inflate(R.layout.row_past_reports_list, null);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IpdReportList ipdReportList = reportLists.get(position);
        holder.txtOpen.setOnClickListener(onclicktioner);
        holder.txtOpen.setOnClickListener(view -> mContext.startActivity(new Intent(mContext, ViewInvoiceDetailActivity.class)
                .putExtra("billLInk", ipdReportList.getIpdSlip())
                .putExtra("invoiceNumber", "Report")
        ));

        String patientNumber = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_NUMBER);

        holder.patientName.setText(patientName);
        holder.patientIds.setText("Patient Number : " + patientNumber);
        holder.txtDate.setText(ipdDate);

    }

    @Override
    public int getItemCount() {
        return reportLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtOpen, patientName, patientIds, txtDate;

        public MyViewHolder(View view) {
            super(view);
            txtOpen = view.findViewById(R.id.txtOpen);
            patientName = view.findViewById(R.id.txtHospitalName);
            patientIds = view.findViewById(R.id.txtDoctorName);
            txtDate = view.findViewById(R.id.txtDate);

        }
    }

}

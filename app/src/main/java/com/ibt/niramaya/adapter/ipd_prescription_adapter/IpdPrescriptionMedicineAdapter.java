package com.ibt.niramaya.adapter.ipd_prescription_adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.ipd_prescription.details.IpdPreception;

import java.util.List;

public class IpdPrescriptionMedicineAdapter extends RecyclerView.Adapter<IpdPrescriptionMedicineAdapter.MyViewHolder> {

    private List<IpdPreception> prescriptiontList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public IpdPrescriptionMedicineAdapter(List<IpdPreception> prescriptiontList, Context mContext) {
        this.prescriptiontList = prescriptiontList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_prescriptin_detail_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        IpdPreception preception = prescriptiontList.get(position);
        if (preception.getPreception() == 0) {
            holder.llViewPrescriptionName.setVisibility(View.GONE);
            holder.llViewPrescriptionImage.setVisibility(View.VISIBLE);
            Glide.with(mContext).load(preception.getMedicineDose()).into(holder.ivFrMedicineDose);
            Glide.with(mContext).load(preception.getMedicineName()).into(holder.ivFrMedicineImage);
        } else {
            holder.llViewPrescriptionImage.setVisibility(View.GONE);
            holder.llViewPrescriptionName.setVisibility(View.VISIBLE);
            holder.tvFrMedicineName.setText(preception.getMedicineName());
            holder.tvFrMedicineDose.setText(preception.getMedicineDose());
        }
    }

    @Override
    public int getItemCount() {
        return prescriptiontList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvFrMedicineName, tvFrMedicineDose;
        private ImageView ivFrMedicineImage, ivFrMedicineDose;
        private LinearLayout llViewPrescriptionImage, llViewPrescriptionName;

        public MyViewHolder(View view) {
            super(view);
            ivFrMedicineImage = view.findViewById(R.id.ivFrMedicineImage);
            ivFrMedicineDose = view.findViewById(R.id.ivFrMedicineDose);
            tvFrMedicineName = view.findViewById(R.id.tvFrMedicineName);
            tvFrMedicineDose = view.findViewById(R.id.tvFrMedicineDose);
            llViewPrescriptionName = view.findViewById(R.id.llViewPrescriptionName);
            llViewPrescriptionImage = view.findViewById(R.id.llViewPrescriptionImage);
        }
    }

}

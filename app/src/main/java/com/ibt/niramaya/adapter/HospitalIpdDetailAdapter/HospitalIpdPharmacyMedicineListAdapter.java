package com.ibt.niramaya.adapter.HospitalIpdDetailAdapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pharmacy_detail_modal.IpdPharmacyBillMedicine;

import java.util.List;

public class HospitalIpdPharmacyMedicineListAdapter extends RecyclerView.Adapter<HospitalIpdPharmacyMedicineListAdapter.MyViewHolder> {

    private List<IpdPharmacyBillMedicine> billMedicineList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public HospitalIpdPharmacyMedicineListAdapter(List<IpdPharmacyBillMedicine> billMedicineList, Context mContext) {
        this.billMedicineList = billMedicineList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_account_pharmcy_medicine, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IpdPharmacyBillMedicine medicine = billMedicineList.get(position);

        holder.tvMedicineName.setText(medicine.getMedicineName());
        holder.tvCompanyName.setText(medicine.getMedicineCompanyName());
        holder.tvExpireDate.setText(medicine.getMedicineExpireDate());
        holder.tvMedicineMrp.setText(medicine.getMedicineMrp());
        holder.tvMedicineQuantity.setText(medicine.getMedicineQuantity());
        holder.tvMedicineAmount.setText(medicine.getMedicineAmount());

    }

    @Override
    public int getItemCount() {
        return billMedicineList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvMedicineName, tvExpireDate, tvCompanyName, tvMedicineMrp, tvMedicineQuantity, tvMedicineAmount, tvHospitalName, tvHospitalLocation;
        private ImageView imgHospital;

        public MyViewHolder(View view) {
            super(view);

            tvMedicineName = view.findViewById(R.id.tvMedicineName);
            tvCompanyName = view.findViewById(R.id.tvCompanyName);
            tvExpireDate = view.findViewById(R.id.tvExpireDate);
            tvMedicineQuantity = view.findViewById(R.id.tvMedicineQuantity);
            tvMedicineMrp = view.findViewById(R.id.tvMedicineMrp);
            tvMedicineAmount = view.findViewById(R.id.tvAmount);
            tvHospitalName = view.findViewById(R.id.tvHospitalName);

        }
    }

}

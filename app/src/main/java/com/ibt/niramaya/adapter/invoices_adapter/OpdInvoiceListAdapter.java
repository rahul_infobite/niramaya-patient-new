package com.ibt.niramaya.adapter.invoices_adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.invoice_modal.opd_invoice_modal.BillDatum;
import com.ibt.niramaya.modal.invoice_modal.opd_invoice_modal.HospitalBillInformation;
import com.ibt.niramaya.utils.MultipleDateTimeFormats;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class OpdInvoiceListAdapter extends RecyclerView.Adapter<OpdInvoiceListAdapter.MyViewHolder> {

    private List<BillDatum> opdInformationList;
    private Context mContext;
    private View.OnClickListener onClickListener;
    private HospitalBillInformation hospitalBillInformation;

    public OpdInvoiceListAdapter(List<BillDatum> opdInformationList, Context mContext, View.OnClickListener onClickListener) {
        this.opdInformationList = opdInformationList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pharmacy_invoice_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BillDatum billDatum = opdInformationList.get(position);
        hospitalBillInformation = billDatum.getHospitalBillInformation();

        holder.cardPharmacyBill.setTag(position);
        holder.cardPharmacyBill.setOnClickListener(onClickListener);
        holder.tvInvoiceNumber.setVisibility(View.GONE);

        holder.tvHospitalName.setText(hospitalBillInformation.getHospitalName());
        Glide.with(mContext).load(hospitalBillInformation.getHospialLogo()).placeholder(R.drawable.ic_profile).into(holder.imgHospital);
        holder.tvHospitalLocation.setText(hospitalBillInformation.getHospitalStreetName());

        holder.lblDiscount.setText("OPD Name");

        float amount = Float.parseFloat(billDatum.getAppointmentAmount());
        holder.tvTotalAmount.setText(String.format("%.2f", amount));
        if (billDatum.getOpdInformation() != null) {
            holder.tvDiscountAmount.setText(billDatum.getOpdInformation().getTitle());
        }
        holder.tvBillGenerateDAte.setText(billDatum.getAppointmentCreatedDate());

        holder.tvPatientName.setText(billDatum.getPatientName());
        if (!billDatum.getPatientDob().isEmpty()) {
            holder.tvPatientAge.setText(MultipleDateTimeFormats.getAge(billDatum.getPatientDob()));
        }
        holder.tvPatientGender.setText(billDatum.getPatientGender());
        String strBillStatus = billDatum.getAppointmentPaymentStatus();
        if (!strBillStatus.isEmpty()){
            switch (strBillStatus) {
                case "0":
                    holder.tvBillStatus.setTextColor(mContext.getResources().getColor(R.color.green_dark));
                    strBillStatus = "Paid";
                    break;
                case "1":
                    holder.tvBillStatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                    strBillStatus = "Pay";
                    break;
                case "2":
                    holder.tvBillStatus.setTextColor(mContext.getResources().getColor(R.color.yellow_d));
                    strBillStatus = "Partial Payment";
                    break;
            }
        }
        holder.tvBillStatus.setText(strBillStatus);

    }

    @Override
    public int getItemCount() {
        return opdInformationList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvHospitalName, tvHospitalLocation, lblDiscount, tvPatientName, tvPatientGender, tvInvoiceNumber, tvDiscountAmount, tvPatientAge, tvBillGenerateDAte, tvBillStatus, tvTotalAmount, tvBIllGstNUmber;
        private CircleImageView imgHospital;
        private CardView cardPharmacyBill;
        private RecyclerView recyclerViewInvoice;

        public MyViewHolder(View view) {
            super(view);
            cardPharmacyBill = view.findViewById(R.id.cardPharmacyBill);
            tvHospitalLocation = view.findViewById(R.id.tvHospitalLocation);
            tvHospitalName = view.findViewById(R.id.tvHospitalName);
            tvDiscountAmount = view.findViewById(R.id.tvDiscountAmount);
            tvBillGenerateDAte = view.findViewById(R.id.tvBillGenerateDAte);
            tvPatientName = view.findViewById(R.id.tvPatientName);
            tvBIllGstNUmber = view.findViewById(R.id.tvBIllGstNUmber);
            tvTotalAmount = view.findViewById(R.id.tvTotalAmount);
            tvBillStatus = view.findViewById(R.id.tvBillStatus);
            imgHospital = view.findViewById(R.id.imgHospital);
            recyclerViewInvoice = view.findViewById(R.id.recylerMecidine);
            tvInvoiceNumber = view.findViewById(R.id.tvInvoiceNumber);
            tvPatientAge = view.findViewById(R.id.tvPatientAge);
            tvPatientGender = view.findViewById(R.id.tvPatientGender);
            lblDiscount = view.findViewById(R.id.lblDiscount);
        }
    }

}

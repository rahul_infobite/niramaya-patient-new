package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.ipd_prescription.list.IpdPreception;
import com.ibt.niramaya.ui.activity.ViewInvoiceDetailActivity;
import com.ibt.niramaya.utils.AppPreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class DoctorIpdPrescriptionListAdapter extends RecyclerView.Adapter<DoctorIpdPrescriptionListAdapter.MyViewHolder> {

    private List<IpdPreception> reportLists;
    private Context mContext;
    private String patientName, patientId, ipdDate;
    private View.OnClickListener onclicktioner;

    public DoctorIpdPrescriptionListAdapter(List<IpdPreception> reportLists, Context mContext, String patientId, String patientName, String ipdDate, View.OnClickListener onclicktioner) {
        this.reportLists = reportLists;
        this.mContext = mContext;
        this.onclicktioner = onclicktioner;
        this.patientName = patientName;
        this.patientId = patientId;
        this.ipdDate = ipdDate;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater li = LayoutInflater.from(mContext);
        View itemView = li.inflate(R.layout.row_past_reports_list, null);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IpdPreception ipdReportList = reportLists.get(position);

        holder.patientName.setText(ipdReportList.getStaffName());
        String patientNumber = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_NUMBER);
        holder.patientIds.setText("Patient Number : " + patientNumber);

        holder.txtDate.setText(changeDateFormat(ipdReportList.getIpdCreatedDate()));

            holder.txtOpen.setOnClickListener(v -> mContext.startActivity(new Intent(mContext, ViewInvoiceDetailActivity.class)
                    .putExtra("from", "patientPrescription")
                    .putExtra("billLInk", ipdReportList.getSlipLink())
                    .putExtra("invoiceNumber", ipdReportList.getIpd())));


    }

    @Override
    public int getItemCount() {
        return reportLists.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtOpen, patientName, patientIds, txtDate;

        public MyViewHolder(View view) {
            super(view);
            txtOpen = view.findViewById(R.id.txtOpen);
            patientName = view.findViewById(R.id.txtHospitalName);
            patientIds = view.findViewById(R.id.txtDoctorName);
            txtDate = view.findViewById(R.id.txtDate);

        }
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy hh:mm aa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}

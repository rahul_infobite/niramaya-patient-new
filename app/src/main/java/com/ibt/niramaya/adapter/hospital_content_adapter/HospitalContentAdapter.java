package com.ibt.niramaya.adapter.hospital_content_adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital_about_content.Content;

import java.util.List;

public class HospitalContentAdapter extends RecyclerView.Adapter<HospitalContentAdapter.MyViewHolder> {

    private List<Content> contents;
    private Context mContext;
    private int pos;

    public HospitalContentAdapter(List<Content> contents, Context mContext, int pos) {
        this.contents = contents;
        this.mContext = mContext;
        this.pos = pos;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hospital_content, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Content content = contents.get(pos);
       /* holder.txtPos0.setText(content.getTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            holder.tvContent0.setText( Html.fromHtml(contents.get(pos).getContent(), Html.FROM_HTML_MODE_COMPACT));

        } else {
            holder.tvContent0.setText(Html.fromHtml(contents.get(pos).getContent()));
        }*/

    }

    @Override
    public int getItemCount() {
        return pos;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtPos0, tvContent0;

        public MyViewHolder(View view) {
            super(view);
            txtPos0 = view.findViewById(R.id.txtPos0);
            tvContent0 = view.findViewById(R.id.tvContent0);

        }
    }

}

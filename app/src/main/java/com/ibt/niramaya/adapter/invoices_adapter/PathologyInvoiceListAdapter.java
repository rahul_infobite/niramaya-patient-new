package com.ibt.niramaya.adapter.invoices_adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.invoice_modal.pathology_invoice_modal.BillDatum;
import com.ibt.niramaya.modal.invoice_modal.pathology_invoice_modal.HospitalBillInformation;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PathologyInvoiceListAdapter extends RecyclerView.Adapter<PathologyInvoiceListAdapter.MyViewHolder> {

    private List<BillDatum> pathologyBillTestList;
    private Context mContext;
    private View.OnClickListener onClickListener;
    private HospitalBillInformation hospitalBillInformation;

    public PathologyInvoiceListAdapter(List<BillDatum> pathologyBillTestList, Context mContext, View.OnClickListener onClickListener) {
        this.pathologyBillTestList = pathologyBillTestList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pharmacy_invoice_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint({"DefaultLocale", "SetTextI18n", "ResourceAsColor"})
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        BillDatum billDatum = pathologyBillTestList.get(position);
        hospitalBillInformation = billDatum.getHospitalBillInformation();

        holder.cardPharmacyBill.setTag(position);
        holder.cardPharmacyBill.setOnClickListener(onClickListener);

        holder.tvHospitalName.setText(hospitalBillInformation.getHospitalName());
        Glide.with(mContext).load(hospitalBillInformation.getHospialLogo()).placeholder(R.drawable.ic_profile).into(holder.imgHospital);
        holder.tvHospitalLocation.setText(hospitalBillInformation.getHospitalStreetName());

        holder.tvInvoiceNumber.setText("Invoice No. : " + billDatum.getBillInvoice());

        float disocunt = Float.parseFloat(billDatum.getBillDiscount());
        float amount = Float.parseFloat(billDatum.getBillAmount());
        holder.tvTotalAmount.setText(String.format("%.2f", amount));
        holder.tvDiscountAmount.setText(String.format("%.2f", disocunt));
        holder.tvBillGenerateDAte.setText(billDatum.getBillCreatedDate());
        String strBillType = billDatum.getBillType();
        switch (strBillType) {
            case "0":
                strBillType = "OPD";
                break;
            case "1":
                strBillType = "Pharmacy";
                break;
            case "2":
                strBillType = "Pathology";
                break;
            case "3":
                strBillType = "IPD";
                break;
            default:
                strBillType = "";
                break;
        }


        holder.tvPatientName.setText(billDatum.getPatientName());
        holder.tvPatientAge.setText(billDatum.getPatientAge());
        holder.tvPatientGender.setText(billDatum.getPatientGender());
        String strBillStatus = billDatum.getBillPaymentStatus();
        if (!strBillStatus.isEmpty()){
            if (strBillStatus.equals("0")) {
                holder.tvBillStatus.setTextColor(mContext.getResources().getColor(R.color.green_dark));
                strBillStatus = "Paid";
            } else if (strBillStatus.equals("1")) {
                holder.tvBillStatus.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
                strBillStatus = "Pay";
            } else if (strBillStatus.equals("2")) {
                holder.tvBillStatus.setTextColor(mContext.getResources().getColor(R.color.yellow_d));
                strBillStatus = "Partial Payment";
            }
        }
        holder.tvBillStatus.setText(strBillStatus);

    }

    @Override
    public int getItemCount() {
        return pathologyBillTestList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvHospitalName, tvHospitalLocation, tvPatientName, tvPatientGender, tvInvoiceNumber, tvDiscountAmount, tvPatientAge, tvBillGenerateDAte, tvBillStatus, tvTotalAmount, tvBIllGstNUmber;
        private CircleImageView imgHospital;
        private CardView cardPharmacyBill;
        private RecyclerView recyclerViewInvoice;

        public MyViewHolder(View view) {
            super(view);
            cardPharmacyBill = view.findViewById(R.id.cardPharmacyBill);
            tvHospitalLocation = view.findViewById(R.id.tvHospitalLocation);
            tvHospitalName = view.findViewById(R.id.tvHospitalName);
            tvDiscountAmount = view.findViewById(R.id.tvDiscountAmount);
            tvBillGenerateDAte = view.findViewById(R.id.tvBillGenerateDAte);
            tvPatientName = view.findViewById(R.id.tvPatientName);
            tvBIllGstNUmber = view.findViewById(R.id.tvBIllGstNUmber);
            tvTotalAmount = view.findViewById(R.id.tvTotalAmount);
            tvBillStatus = view.findViewById(R.id.tvBillStatus);
            imgHospital = view.findViewById(R.id.imgHospital);
            recyclerViewInvoice = view.findViewById(R.id.recylerMecidine);
            tvInvoiceNumber = view.findViewById(R.id.tvInvoiceNumber);
            tvPatientAge = view.findViewById(R.id.tvPatientAge);
            tvPatientGender = view.findViewById(R.id.tvPatientGender);
        }
    }

}

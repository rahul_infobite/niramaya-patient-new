package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.patient_ipd_modal.Ipd;
import com.ibt.niramaya.ui.activity.SelectedIpdReportActivity;
import com.ibt.niramaya.utils.AppPreference;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PatientIpdReportListListAdapter extends RecyclerView.Adapter<PatientIpdReportListListAdapter.MyViewHolder> {

    private List<Ipd> ipdList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public PatientIpdReportListListAdapter(List<Ipd> ipdList, Context mContext, View.OnClickListener onClickListener) {
        this.ipdList = ipdList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_patient_ipd_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Ipd ipd = ipdList.get(position);
        holder.txtIpdNumber.setText("IPD NUMBER : " + ipd.getIpd());

        if (ipd.getHospital() != null) {
            holder.tv_hospital_name.setText(ipd.getHospital().get(position).getHospitalName());
            holder.txtHospitalContact.setText(ipd.getHospital().get(position).getHospitalContact());
            holder.hospitalFirstAdd.setText(ipd.getHospital().get(position).getHospitalHouseNumber() + "," + ipd.getHospital().get(position).getHospitalStreetName());
            holder.hospitalCity.setText(ipd.getHospital().get(position).getHospitalCity());
            holder.hospitalState.setText(ipd.getHospital().get(position).getHospitalState());
            holder.hospitalCountry.setText(ipd.getHospital().get(position).getHospitalCountry());
            holder.hospitalZipCode.setText(ipd.getHospital().get(position).getHospitalZipcode());

            AppPreference.setStringPreference(mContext, Constant.HOSPITAL_ID,ipd.getHospital().get(position).getHospital_id());

            String hospitalImage = ipd.getHospital().get(position).getHospialLogo();
            if (!hospitalImage.isEmpty()) {
                Glide.with(mContext).load(hospitalImage).placeholder(R.drawable.ic_profile).into(holder.hospitalImage);
            }
        }

        holder.txtPatientName.setText(ipd.getPatientName());
        holder.txtDoctorName.setText(ipd.getDoctorName());
        holder.txtDischargeType.setText(ipd.getDischargeType());
        holder.txtIpdDate.setText(changeDateFormat(ipd.getIpdCreatedDate()));

        holder.btnDetails.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, SelectedIpdReportActivity.class);
            intent.putExtra("patientName",ipd.getPatientName());
            intent.putExtra("pId",ipd.getPatientId());
            intent.putExtra("pDob",ipd.getPatientDob());
            intent.putExtra("pContact",ipd.getPatientContact());
            intent.putExtra("pGender",ipd.getPatientGender());
            intent.putExtra("pProfile",ipd.getPatientProfile());
            intent.putExtra("ipdId",ipd.getIpdId());
            intent.putExtra("createDate",ipd.getIpdCreatedDate());
            mContext.startActivity(intent);

        });


    }

    @Override
    public int getItemCount() {
        return ipdList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtIpdNumber, txtHospitalContact, hospitalFirstAdd, hospitalCity, hospitalCountry,
                hospitalZipCode, tv_hospital_name, txtPatientName, txtDoctorName, txtDischargeType, txtIpdDate, hospitalState;
        CircleImageView hospitalImage;
        LinearLayout llIpd;
        private Button btnPrescriptionandReport,btnDetails;

        public MyViewHolder(View view) {
            super(view);
            txtIpdNumber = view.findViewById(R.id.txtIpdNumber);
            hospitalImage = view.findViewById(R.id.hospitalImage);
            txtHospitalContact = view.findViewById(R.id.txtHospitalContact);
            hospitalFirstAdd = view.findViewById(R.id.hospitalFirstAdd);
            hospitalCity = view.findViewById(R.id.hospitalCity);
            hospitalState = view.findViewById(R.id.hospitalState);
            hospitalCountry = view.findViewById(R.id.hospitalCountry);
            hospitalZipCode = view.findViewById(R.id.hospitalZipCode);
            tv_hospital_name = view.findViewById(R.id.tv_hospital_name);
            txtPatientName = view.findViewById(R.id.txtPatientName);
            txtDoctorName = view.findViewById(R.id.txtDoctorName);
            txtDischargeType = view.findViewById(R.id.txtDischargeType);
            txtIpdDate = view.findViewById(R.id.txtIpdDate);
            llIpd = view.findViewById(R.id.llIpd);
            btnPrescriptionandReport = view.findViewById(R.id.btnPrescriptionandReport);
            btnDetails = view.findViewById(R.id.btnDetails);
        }
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


}

package com.ibt.niramaya.adapter.doctor_opd;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.specialization.all.SpecialistDoctorDatum;

import java.util.ArrayList;
import java.util.List;

public class SpecialDoctorOpdListAdapter extends RecyclerView.Adapter<SpecialDoctorOpdListAdapter.MyViewHolder> {

    private List<SpecialistDoctorDatum> doctorList;
    private List<SpecialistDoctorDatum> filterList;
    private Context mContext;
    private View.OnClickListener onClickListener;
    private String spcl;

    public SpecialDoctorOpdListAdapter(List<SpecialistDoctorDatum> vendorLists, Context mContext, View.OnClickListener onClickListener, String spcl) {
        this.doctorList = vendorLists;
        this.filterList = doctorList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
        this.spcl = spcl;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_peditrician, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        SpecialistDoctorDatum doctorData = filterList.get(position);
        holder.tvDoctorName.setText(doctorData.getName());
        holder.tvDoctorSpecialization.setText(spcl);
        holder.tvDoctorRatings.setText(doctorData.getRating());
        holder.btnBookAppointment.setTag(position);
        holder.btnBookAppointment.setOnClickListener(onClickListener);

        String hospitalName = "";
        hospitalName = doctorData.getHospitalName();
        if (!doctorData.getHospitalStreetName().isEmpty()){
            hospitalName = hospitalName+", "+doctorData.getHospitalStreetName();
        }

        holder.tvDoctorAddress.setText(hospitalName);
        Glide.with(mContext)
                .load(doctorData.getProfileImage())
                .placeholder(R.drawable.ic_profile)
                .into(holder.civDoctorProfile);
    }
    @Override
    public int getItemCount() {
        return filterList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvDoctorName, tvDoctorSpecialization, tvDoctorAddress, tvDoctorRatings;
        public ImageView civDoctorProfile;
        private LinearLayout llDoctor;
        private Button btnBookAppointment;

        public MyViewHolder(View view) {
            super(view);
            llDoctor = view.findViewById(R.id.llDoctor);

            tvDoctorName = view.findViewById(R.id.tvDoctorName);
            tvDoctorSpecialization = view.findViewById(R.id.tvDoctorSpecialization);
            tvDoctorAddress = view.findViewById(R.id.tvDoctorAddress);
            tvDoctorRatings = view.findViewById(R.id.tvDoctorRatings);

            btnBookAppointment = view.findViewById(R.id.btnBookAppointment);

            civDoctorProfile = view.findViewById(R.id.civDoctorProfile);
        }
    }

    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    filterList = doctorList;
                } else {
                    List<SpecialistDoctorDatum> filteredList = new ArrayList<>();
                    for (SpecialistDoctorDatum row : doctorList) {
                        if (row.getName().toLowerCase().contains(charString.toLowerCase()) || row.getHospitalName().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }

                    }
                    filterList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = filterList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                filterList = (ArrayList<SpecialistDoctorDatum>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }


}

package com.ibt.niramaya.adapter.ambulance;


import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;


import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.ambulance.driver_detail.AmbulanceCharge;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ChargeListAdapter extends RecyclerView.Adapter<ChargeListAdapter.SingleItemRowHolder> {

    private ArrayList<AmbulanceCharge> ambulanceList;
    private Context mContext;

    private int checkCount = 0;

    public ChargeListAdapter(Context context, ArrayList<AmbulanceCharge> ambulanceList) {
        this.ambulanceList = ambulanceList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_ambulance_prize, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, final int i) {
        holder.tvServiceName.setText(ambulanceList.get(i).getChargesTitle());
        holder.tvServicePrice.setText(ambulanceList.get(i).getChargesRate());

        holder.cbSelect.setChecked(ambulanceList.get(i).isSelected());
        holder.cbSelect.setTag(ambulanceList.get(i));

        if (ambulanceList.get(i).isSelected()){
            checkCount++;
        }

        holder.cbSelect.setOnClickListener(v -> {
                CheckBox cb = (CheckBox) v;
                AmbulanceCharge event = (AmbulanceCharge) cb.getTag();
                event.setSelected(cb.isChecked());
            ambulanceList.get(i).setSelected(cb.isChecked());
                if (cb.isChecked()){
                    checkCount++;
                }else {
                    if (checkCount!=0){
                        checkCount--;
                    }
                }
        });

    }

    @Override
    public int getItemCount() {
        return (null != ambulanceList ? ambulanceList.size() : 0);
    }


    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        private TextView tvServiceName, tvServicePrice;
        private CheckBox cbSelect;

        public SingleItemRowHolder(View view) {
            super(view);

            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvServicePrice = itemView.findViewById(R.id.tvServicePrice);
            this.cbSelect = view.findViewById(R.id.cbSelect);

        }

    }

   private String validateDate(String responseDate){
       DateFormat originalFormat = new SimpleDateFormat("dd MMM yyyy", Locale.ENGLISH);
       DateFormat targetFormat = new SimpleDateFormat("d MMM", Locale.ENGLISH);
       Date date = null;
       try {
           date = originalFormat.parse(responseDate);
       } catch (ParseException e) {
           e.printStackTrace();
       }
       return targetFormat.format(date);
   }

   public ArrayList<AmbulanceCharge> selectedStudentList(){
        return ambulanceList;
   }


}
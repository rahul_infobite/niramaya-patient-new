package com.ibt.niramaya.adapter.hospital_content_adapter;

import android.content.Context;

import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital_about_content.Faq;

import java.util.List;

public class HospitalFaqAdapter extends RecyclerView.Adapter<HospitalFaqAdapter.MyViewHolder> {

    private List<Faq> faqList;
    private Context mContext;
    private View.OnClickListener onClickListener;
    boolean onclick = false;

    public HospitalFaqAdapter(List<Faq> faqList, Context mContext, View.OnClickListener onClickListener) {
        this.faqList = faqList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hospital_content, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        Faq faq = faqList.get(position);
        holder.txtPos0.setText(faq.getFaqQuestion());
        holder.tvContent0.setText(faq.getFaqAnswer());


        holder.imgViewMore.setOnClickListener(v -> {
            if (!onclick){
                onclick = true;
                holder.imgViewMore.setImageResource(R.drawable.ic_expand_less_);
                holder.llAnswer.setVisibility(View.VISIBLE);
            }else {
                onclick = false;
                holder.imgViewMore.setImageResource(R.drawable.ic_expand_more);
                holder.llAnswer.setVisibility(View.GONE);
            }
        });

    }

    @Override
    public int getItemCount() {
        return faqList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
         TextView txtPos0, tvContent0;
        private ImageView imgViewMore;
        LinearLayout llAnswer;

        public MyViewHolder(View view) {
            super(view);
            txtPos0 = view.findViewById(R.id.txtPos0);
            tvContent0 = view.findViewById(R.id.tvContent0);
            imgViewMore = view.findViewById(R.id.imgViewMore);
            llAnswer = view.findViewById(R.id.llAnswer);

        }
    }

}

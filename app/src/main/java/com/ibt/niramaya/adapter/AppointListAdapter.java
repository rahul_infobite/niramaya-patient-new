package com.ibt.niramaya.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.appoint_list_main_modal.Datum;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AppointListAdapter extends RecyclerView.Adapter<AppointListAdapter.MyViewHolder> {

    private List<Datum> apppointmentList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public AppointListAdapter(List<Datum> apppointmentList, Context mContext, View.OnClickListener onClickListener) {
        this.apppointmentList = apppointmentList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_appointment_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Datum datum = apppointmentList.get(position);

        holder.tvHospitalName.setText(datum.getHospital().getHospitalName());
        holder.appointmentDoctor.setText(datum.getDoctorName());
        holder.txtAppointmentBookDate.setText(changeDateFormat2(datum.getAppointmentBookingDate()));
        holder.appointmentDate.setText(changeDateFormat(datum.getAppointmentDate()));
        String appStatus = datum.getAppointmentStatus();
        //holder.appointmentStatus.setText(appStatus);
        holder.appointmentAmount.setText(datum.getAppointmentAmount());
        String appType = datum.getAppointmentType();
        if (appType.equals("0")) {
            appType = "Booked By Hospital";
        } else if (appType.equals("1")) {
            appType = "Booked By You";
        }
        holder.txtAppointmentType.setText(appType);
        String payStatus = datum.getAppointmentPaymentStatus();
        if (payStatus.equals("0")) {
            payStatus = "Prepaid";
        } else if (payStatus.equals("1")) {
            payStatus = "Postpaid";
        }
        holder.paymentStatus.setText(payStatus);
        holder.tvOpdTitle.setText(datum.getOpd().getOpdTitle());
        holder.tvStartTime.setText(datum.getOpd().getOpdStartTime());
        holder.tvEndTime.setText(datum.getOpd().getOpdEndTime());

        holder.tvCancel.setTag(position);
        holder.tvCancel.setOnClickListener(onClickListener);

        if (datum.getAppointmentStatus().equals("0")) {
            holder.llRunning.setVisibility(View.VISIBLE);
            holder.llComplete.setVisibility(View.GONE);
            holder.tvAppointmentStatus.setText("Pending");
        } else if (datum.getAppointmentStatus().equals("1")) {
            holder.llRunning.setVisibility(View.VISIBLE);
            holder.llComplete.setVisibility(View.GONE);
            holder.tvAppointmentStatus.setText("Approved");
        } else if (datum.getAppointmentStatus().equals("2")) {
            holder.llRunning.setVisibility(View.GONE);
            holder.llComplete.setVisibility(View.VISIBLE);
            holder.tvCompleteMessage.setText("Completed");
        } else if (datum.getAppointmentStatus().equals("3")) {
            holder.llRunning.setVisibility(View.GONE);
            holder.llComplete.setVisibility(View.VISIBLE);
            holder.tvCompleteMessage.setText("Cancel By You");
        } else if (datum.getAppointmentStatus().equals("4")) {
            holder.llRunning.setVisibility(View.GONE);
            holder.llComplete.setVisibility(View.VISIBLE);
            holder.tvCompleteMessage.setText("Cancel By Hospital");
        } else if (datum.getAppointmentStatus().equals("5")) {
            holder.llRunning.setVisibility(View.VISIBLE);
            holder.llComplete.setVisibility(View.GONE);
            holder.tvAppointmentStatus.setText("Enqueue");
        } else if (datum.getAppointmentStatus().equals("6")) {
            holder.llRunning.setVisibility(View.VISIBLE);
            holder.llComplete.setVisibility(View.GONE);
            holder.tvAppointmentStatus.setText("Postponed");
        } else if (datum.getAppointmentStatus().equals("8")) {
            holder.llRunning.setVisibility(View.VISIBLE);
            holder.llComplete.setVisibility(View.GONE);
            holder.tvAppointmentStatus.setText("Ongoing Consultation");
        }


    }

    @Override
    public int getItemCount() {
        return apppointmentList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView appointmentDoctor, txtAppointmentBookDate, appointmentDate, appointmentStatus,
                appointmentAmount, txtAppointmentType, paymentStatus, tvOpdTitle, tvStartTime, tvEndTime,
                tvAppointmentStatus, tvCancel, tvCompleteMessage, tvHospitalName;
        public ImageView rc_img;
        private CardView cardViewItem;
        private LinearLayout llRunning, llComplete;

        public MyViewHolder(View view) {
            super(view);
            appointmentDoctor = view.findViewById(R.id.appointmentDoctor);
            txtAppointmentBookDate = view.findViewById(R.id.txtAppointmentBookDate);
            appointmentDate = view.findViewById(R.id.appointmentDate);
            appointmentAmount = view.findViewById(R.id.appointmentAmount);
            txtAppointmentType = view.findViewById(R.id.txtAppointmentType);
            paymentStatus = view.findViewById(R.id.paymentStatus);
            tvOpdTitle = view.findViewById(R.id.tvOpdTitle);
            tvStartTime = view.findViewById(R.id.tvStartTime);
            tvEndTime = view.findViewById(R.id.tvEndTime);

            tvAppointmentStatus = view.findViewById(R.id.tvAppointmentStatus);
            tvCancel = view.findViewById(R.id.tvCancel);
            tvCompleteMessage = view.findViewById(R.id.tvCompleteMessage);
            tvHospitalName = view.findViewById(R.id.tvHospitalName);

            llRunning = view.findViewById(R.id.llRunning);
            llComplete = view.findViewById(R.id.llComplete);
        }
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd-MM-yyyy hh:mm aa";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public String changeDateFormat2(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd-MM-yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}

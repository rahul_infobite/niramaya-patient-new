package com.ibt.niramaya.adapter.HospitalIpdDetailAdapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pathology_detail_modal.IpdPathologyBillTest;

import java.util.List;

public class HospitalIpdPathologyMedicineListAdapter extends RecyclerView.Adapter<HospitalIpdPathologyMedicineListAdapter.MyViewHolder> {

    private List<IpdPathologyBillTest> testList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public HospitalIpdPathologyMedicineListAdapter(List<IpdPathologyBillTest> testList, Context mContext) {
        this.testList = testList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_account_pathology_test, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IpdPathologyBillTest test = testList.get(position);

        holder.tvExpireDate.setVisibility(View.GONE);

        holder.tvTestName.setText(test.getTestName());
        holder.tvTestSample.setText(test.getTestSample());
        holder.tvTestCode.setText(test.getTestCode());
        holder.tvTestCost.setText(test.getTestCost());
        holder.tvTestSchedule.setText(test.getTestSchedule());
        holder.tvTestDiscount.setText(test.getTestDiscount());
        holder.tvTestType.setText(test.getTestType());
        holder.tvPrequestics.setText(test.getTestPrerequisites());
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTestName, tvTestSample, tvTestCode, tvTestCost, tvTestSchedule, tvTestDiscount, tvTestType, tvPrequestics,tvExpireDate;

        public MyViewHolder(View view) {
            super(view);

            tvTestName = view.findViewById(R.id.tvTestName);
            tvTestSample = view.findViewById(R.id.tvTestSample);
            tvTestCode = view.findViewById(R.id.tvTestCode);
            tvTestCost = view.findViewById(R.id.tvTestCost);
            tvTestSchedule = view.findViewById(R.id.tvTestSchedule);
            tvTestDiscount = view.findViewById(R.id.tvTestDiscount);
            tvTestType = view.findViewById(R.id.tvTestType);
            tvPrequestics = view.findViewById(R.id.tvPrequestics);
            tvExpireDate = view.findViewById(R.id.tvExpireDate);

        }
    }

}

package com.ibt.niramaya.adapter;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.ibt.niramaya.ui.fragment.blood_donation.CheckAvailbilityFragment;
import com.ibt.niramaya.ui.fragment.blood_donation.DonationFragment;
import com.ibt.niramaya.ui.fragment.blood_donation.RecievedFragment;

public class BloodDonationPagerAdapter extends FragmentPagerAdapter {

    private int COUNT = 3;

    public BloodDonationPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int i) {
        Fragment fragment = null;
        switch (i){
            case 0 :
                fragment = new CheckAvailbilityFragment();
                break;
            case 1 :
                fragment = new DonationFragment();
                break;
            case 2 :
                fragment = new RecievedFragment();
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return COUNT;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        String title = "";
        switch (position){
            case 0 :
                title = "Check Availability";
                break;
            case 1 :
                title = "Donations";
                break;
            case 2 :
                title = "Received";
                break;
        }
        return title;
    }

}

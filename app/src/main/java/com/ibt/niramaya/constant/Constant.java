package com.ibt.niramaya.constant;

/**
 * Created by Dell on 12/3/2018.
 */

public class Constant {
    public static final String USER_TOKEN = "user-app-token.php";
    public static final String USER_LOGIN = "patient/user-login.php";
    public static final String OTP_VERIFICATION = "patient/user-contact-verification.php";
    public static final String CREATE_PATIENT_PROFILE = "patient/create_patient_profile.php";
    public static final String PATIENT_LIST = "patient/select-patient-list.php";
    public static final String PATIENT_PRESCRIPTION_LIST = "patient/select_patient_preception.php";
    public static final String PATIENT_PRESCRIPTION_DETAIL = "patient/select_patient_preception_details.php";
    public static final String HOSPITAL_LIST = "patient/select-hospital-list.php";
    public static final String UPDATE_PATIENNT_PROFILE = "patient/update_patient_profile.php";
    public static final String PHARMACY_INVOICE_LIST = "patient/select_pharmacy_bill.php";
    public static final String PATHOLOGY_INVOICE_LIST = "patient/select_pathology_bill.php";
    public static final String OPD_INVOICE_LIST = "patient/select_opd_bill.php";
    public static final String PATIENT_FINANCE_LIST = "patient/select-patient-finance.php";
    public static final String PATIENT_TOKEN = "patient/select_appointment_token.php";
    public static final String PATIENT_REPORT = "patient/select_patient_test_report.php";
    public static final String DOCTOR_OPD_LIST = "patient/select-hospital-doctor-opd-list.php";
    public static final String DOCTOR_OPD = "patient/select-hospital-doctor-opd.php";
    public static final String SPECIALIST_DOCTOR_OPD = "patient/select-doctor-specialization-opd-list.php";
    public static final String HOSPITAL_SPECIALIST_DOCTOR_OPD = "patient/select-hospital-doctor-specialization-opd-list.php";
    public static final String BOOK_APPOINTMENT = "patient/create-apointment.php";
    public static final String ADD_PATIENT_FINANCE_REPORT = "patient/add-patient-finance.php";
    public static final String SELECT_PATIENT_APPOINTMENT = "patient/select-appointment-at-patient.php";
    public static final String CANCEL_APPOINTMENT = "appointment-status.php";

    //Ambulance
    public static final String AMBULANCE_DETAIL_API = "select-ambulance-charges.php";
    public static final String AMBULANCE_BOOKING_API = "patient/add-ambulance_booking.php";
    public static final String AMBULANCE_ONGOING_BOOKING_API = "patient/select-ongoing-ambulance-booking.php";
    public static final String AMBULANCE_EMERGENCY_ONGOING_BOOKING_API = "patient/select-ongoing-emergency-ambulance-booking.php";
    public static final String UPDATE_AMBULANCE_BOOKING_STATUS = "patient/update-ambulance-booking-status.php";
    public static final String SELECT_ACTIVE_DRIVER_AMBULANCE = "select-ambulance-for-booking.php";
    public static final String ADD_EMERGENCY = "patient/add-emergency-ambulance_booking.php";
    public static final String EMERGENCY_ONGOING_BOOKING = "patient/select-ongoing-emergency-ambulance-booking.php";

    // hospital ipd management

    public static final String SELECT_HOSPITAL_IPD_SLIP = "select_ipd_slip.php";
    public static final String SELECT_HOSPITAL_IPD_OPERATIONS = "select_ipd_operation_list.php";
    public static final String SELECT_HOSPITAL_IPD_PHARMACY_BILL = "select_pharmacy_ipd_bill.php";
    public static final String SELECT_HOSPITAL_IPD_PATHOLOGY_BILL = "select_pathology_ipd_bill.php";
    public static final String SELECT_HOSPITAL_IPD_BED = "select_ipd_bed_list.php";
    public static final String HOSPITAL_SELECT_IPD_BILL_SERVICE = "select_ipd_bill_services.php";


    public static final String SELECT_HOSPITAL_IPD_SLIP_DETAIL = "select_ipd_slip_detials.php";
    public static final String SELECT_HOSPITAL_IPD_OPERATION_DETAIL = "select_ipd_operation_list_detials.php";
    public static final String SELECT_HOSPITAL_IPD_PATHOLOGY_DETAIL = "select_pathology_ipd_bill_detials.php";
    public static final String SELECT_HOSPITAL_IPD_PHARMACY_DETAIL = "select_pharmacy_ipd_bill_detials.php";
    public static final String SELECT_HOSPITAL_IPD_DETAIL = "select_hospital_ipd_details.php";
    public static final String SELECT_PATIENT_IPD_LIST_DETAIL = "patient/select-patient_ipd.php";

    public static final String SELECT_NURSE_IPD_REPORT = "select_ipd_nusre_reporting.php";
    public static final String SELECT_DOCTOR_PRESCRIPTION = "select_ipd_preception.php";
    public static final String SELECT_Doctor_PRESCRIPTION_DETAIL = "select_patient_ipd_preception_slip.php";


    // SELECT REFER SLIP
    public static final String SELECT_REEFER_SLIP = "select_refer_slip.php";
    public static final String SELECT_BLOOD_REQUISITION_SLIP = "select_blood_requisition.php";

    //about content api
    public static final String ABOUT_CONTENT_API = "patient/fetch_content.php";
    public static final String ABOUT_FAQ_CONTENT_API = "patient/fetch_faq.php";

    //user management api
    public static final String INVOKE_PATIENT_API = "patient/user-profile-invoke.php";
    public static final String INVOKE_PATIENT_CONTACT_VERIFICATION = "patient/patient-profile-contact-verification.php";
    public static final String SELECT_PATIENT_TEST_REPORTS = "api/select_patient_ipd_test_report.php";


    // Fragment constant
    public static final String HomeFragment = "Home_Fragment";
    public static final String PrescriptionFragment = "PrescriptionFragment";
    public static final String SignUpFragment = "SignUp_Fragment";
    public static final String Otp_Fragment = "OtpFragment";
    public static final String LoginFragment = "LoginFragment";
    public static final String Verification_Fragment = "Verification_Fragment";
    public static final String ReportsFragment = "ReportsFragment";
    public static final String InvoiceFragment = "InvoiceFragment";
    public static final String PatientFinanceDetailFragment = "PatientFinanceDetailFragment";
    public static final String BedFragment = "BedFragment";
    public static final String HistoryFragment = "HistoryFragment";
    public static final String BloodDonationFragment = "BloodDonationFragment";
    public static final String DocumentsFragment = "DocumentsFragment";
    public static final String SettingsFragment = "SettingsFragment";
    public static final String AddUserFragment = "PatientFragment";
    public static final String AmbulanceFragment = "AmbulanceFragment";

    public static final String FacilitiesFragment = "FacilitiesFragment";
    public static final String DepartmentsFragment = "DepartmentsFragment";
    public static final String DoctorFragment = "DoctorFragment";

    // Preference
    public static final String Is_Login = "Is_Login";
    public static final String USER_CONTACT = "userContact";
    public static final String USER_PROFILE_IMAGE = "userProfileImage";
    public static final String USER_ID = "userId";
    public static final String USER_NAME = "userName";

    public static final String CURRENT_PATENT_ID = "currentPatientId";
    public static final String CURRENT_PATENT_NAME = "currentPatientName";

    // public static final String PATIENT_ID = "patient_id";

    public static final String IS_PRIVACY_POLICY_ACCEPTED = "policy";

    // global topic to receive app wide push notifications
    public static final String TOPIC_GLOBAL = "global";

    // broadcast receiver intent filters
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final String PUSH_NOTIFICATION = "pushNotification";

    // id to handle the notification in the notification tray
    public static final int NOTIFICATION_ID = 100;
    public static final int NOTIFICATION_ID_BIG_IMAGE = 101;

    public static final String SHARED_PREF = "noty_firebase";

    public static final String FIREBASE_TOKEN = "firebaseToke";
    public static final String HOSPITAL_ID = "hospitalId";
    public static final String CURRENT_PATENT_NUMBER = "Patient Number";
    public static final String PATIENT_AMBULANCE_BOOKING_STATUS = "bookingStatus";
    public static final String PATIENT_NAME_AMBULANCE_BOOKED = "patientNameForAmbulanceBooked";
    public static final String PATIENT_BOOKING_LATITUDE = "booking_latitude";
    public static final String PATIENT_BOOKING_LONGITUDE = "booking_longitude";
    public static final String PATIENT_BOOKED_BY_USER_CONTACT = "bookedByUserContact";
    public static String AppointmentFragment = "AppointmentFragment";
    public static String PatientIpdListFragment = "PatientIpdListFragment";
    public static String PatientReportIpdListFragment = "PatientReportIpdListFragment";
    public static String NurseIpdReportsFragment = "NurseIpdReportsFragment";
    public static String PrescriptionReportsFragmentDoctor = "PrescriptionReportsFragmentDoctor";
    public static String AboutUsFragment = "AboutUsFragment";
    public static String IpdTestReportsFragment = "IpdTestReportsFragment";
    public static String BloodRequisitionSlipFragment = "BloodRequisitionSlipFragment";
}

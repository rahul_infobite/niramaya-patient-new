package com.ibt.niramaya.hospital_ipd_adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital.ipd_services.IpdBillService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HospitalIpdBilServiceAdapter extends RecyclerView.Adapter<HospitalIpdBilServiceAdapter.MyViewHolder> {

    private List<IpdBillService> billServiceList;
    private Context mContext;
    private View.OnClickListener onClickListener;

    public HospitalIpdBilServiceAdapter(List<IpdBillService> billServiceList, Context mContext, View.OnClickListener onClickListener) {
        this.billServiceList = billServiceList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hospital_ipd_bill_services_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IpdBillService billDatum = billServiceList.get(position);

        holder.rlRootIpdBillServices.setTag(position);
        holder.rlRootIpdBillServices.setOnClickListener(onClickListener);

        String strStatus = billDatum.getStatus();
        if (strStatus.equals("0")) {
            strStatus = "Activate";
        } else if (strStatus.equals("1")) {
            strStatus = "Deactivate";
        } else {
            strStatus = "";
        }
        holder.tvIpdPatientName.setText(billDatum.getPatientName());
        holder.tvIpdPatientAge.setText(getAge(billDatum.getIpdPatientDob()) + " Years");
        holder.tvIpdPatientGender.setText(billDatum.getPatientGender());
        holder.tvServiceTitle.setText(billDatum.getTitle());
        holder.tvUnit.setText(billDatum.getUnit());
        holder.tvStatus.setText(strStatus);
        holder.tvDiscount.setText(billDatum.getDiscount());
        holder.tvAmount.setText(billDatum.getAmount());
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public int getItemCount() {
        return billServiceList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIpdPatientName, tvIpdPatientAge, tvIpdPatientGender, tvServiceTitle, tvUnit, tvStatus, tvDiscount, tvAmount;
        public RelativeLayout rlRootIpdBillServices;

        public MyViewHolder(View view) {
            super(view);


            tvIpdPatientName = view.findViewById(R.id.tvIpdPatientName);
            tvIpdPatientAge = view.findViewById(R.id.tvIpdPatientAge);
            tvIpdPatientGender = view.findViewById(R.id.tvIpdPatientGender);
            tvServiceTitle = view.findViewById(R.id.tvServiceTitle);
            tvUnit = view.findViewById(R.id.tvUnit);
            tvStatus = view.findViewById(R.id.tvStatus);
            tvDiscount = view.findViewById(R.id.tvDiscount);
            tvAmount = view.findViewById(R.id.tvAmount);

            rlRootIpdBillServices = view.findViewById(R.id.rlRootIpdBillServices);
        }
    }

    /***********************************************
     * Start Method block for calculating age
     **************************************************/

    private String getAge(String dobString) {
        Date date = null;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return String.valueOf(0);

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age1 = (today.get(Calendar.YEAR) - dob.get(Calendar.YEAR));
        int month1 = (today.get(Calendar.MONTH) - dob.get(Calendar.MONTH));
        String age = (age1 + "." + month1);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
        }


        return age;
    }

}

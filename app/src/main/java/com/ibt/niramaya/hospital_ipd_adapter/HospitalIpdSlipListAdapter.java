package com.ibt.niramaya.hospital_ipd_adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital.ipd_slip.IpdSlip;
import com.ibt.niramaya.ui.activity.ViewInvoiceDetailActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HospitalIpdSlipListAdapter extends RecyclerView.Adapter<HospitalIpdSlipListAdapter.MyViewHolder> {

    private List<com.ibt.niramaya.modal.hospital.ipd_slip.IpdSlip> ipdList;
    private Context mContext;
    private View.OnClickListener onClickListener;
    private boolean isAcount;

    public HospitalIpdSlipListAdapter(List<com.ibt.niramaya.modal.hospital.ipd_slip.IpdSlip> ipdsList, Context mContext, View.OnClickListener onClickListener) {
        this.ipdList = ipdsList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hospital_ipd_slip_list, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        IpdSlip ipd = ipdList.get(position);

        holder.llRootIpd.setTag(position);
        holder.llRootIpd.setOnClickListener(view -> mContext.startActivity(new Intent(mContext, ViewInvoiceDetailActivity.class)
                .putExtra("billLInk", ipd.getIpdSlip())
                .putExtra("invoiceNumber", ipd.getIpd())
        ));

        holder.tvDoctorName.setText(ipd.getStaffName());
        holder.tvIpdPatientName.setText(ipd.getIpdPatientName());
        holder.tvIpdPatientAge.setText(getAge(ipd.getIpdPatientDob()) + " Years");
        //holder.tvIpdPatientContact.setText(ipd.getPatientContact());

    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public int getItemCount() {
        return ipdList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvIpdPatientName, tvIpdPatientAge, tvIpdPatientContact, tvDoctorName;
        public RelativeLayout llRootIpd;

        public MyViewHolder(View view) {
            super(view);

            tvIpdPatientName = view.findViewById(R.id.tvIpdPatientName);
            tvIpdPatientAge = view.findViewById(R.id.tvIpdPatientAge);
            tvIpdPatientContact = view.findViewById(R.id.tvIpdPatientContact);
            tvDoctorName = view.findViewById(R.id.tvDoctorName);

            llRootIpd = view.findViewById(R.id.llRootIpd);
        }
    }

    /***********************************************
     * Start Method block for calculating age
     **************************************************/

    private String getAge(String dobString) {
        Date date = null;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return String.valueOf(0);

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age1 = (today.get(Calendar.YEAR) - dob.get(Calendar.YEAR));
        int month1 = (today.get(Calendar.MONTH) - dob.get(Calendar.MONTH));
        String age = (age1 + "." + month1);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
        }


        return age;
    }

}

package com.ibt.niramaya.hospital_ipd_adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital.operation.OperationSchedule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class HospitaIpdOperationAdapter extends RecyclerView.Adapter<HospitaIpdOperationAdapter.MyViewHolder> {

    private List<OperationSchedule> ipdList;
    private Context mContext;
    private View.OnClickListener onClickListener;
    private boolean isAcount;

    public HospitaIpdOperationAdapter(List<OperationSchedule> ipdsList, Context mContext, View.OnClickListener onClickListener) {
        this.ipdList = ipdsList;
        this.mContext = mContext;
        this.onClickListener = onClickListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_hospital_ipd_operation_schedule, parent, false);
        return new MyViewHolder(itemView);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        OperationSchedule opSchedule = ipdList.get(position);

        holder.rlRootOperation.setTag(position);
        holder.rlRootOperation.setOnClickListener(onClickListener);

        holder.tvSurgeryDateTime.setText(opSchedule.getSurgeryDate() + " " + opSchedule.getSurgeryTime());
        holder.tvWardName.setText(opSchedule.getBedNumber());
        holder.tvSurgeonName.setText(opSchedule.getSurgeon());
        holder.tvDoctorAssistantName.setText(opSchedule.getAssistant());
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public int getItemCount() {
        return ipdList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvSurgeryDateTime, tvWardName, tvSurgeonName, tvDoctorAssistantName;
        public RelativeLayout rlRootOperation;

        public MyViewHolder(View view) {
            super(view);


            tvSurgeryDateTime = view.findViewById(R.id.tvSurgeryDateTime);
            tvWardName = view.findViewById(R.id.tvWardName);
            tvSurgeonName = view.findViewById(R.id.tvSurgeonName);
            tvDoctorAssistantName = view.findViewById(R.id.tvDoctorAssistantName);
            rlRootOperation = view.findViewById(R.id.rlRootOperation);

        }
    }

    /***********************************************
     * Start Method block for calculating age
     **************************************************/

    private int getAge(String dobString) {

        Date date = null;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }


        return age;
    }

}

package com.ibt.niramaya.retrofit;

import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.ambulance.book_ambulance.ActiveAmbulanceModal;
import com.ibt.niramaya.modal.ambulance.driver_detail.AmbulanceDetailModel;
import com.ibt.niramaya.modal.ambulance.ongoing.AmbulanceOngoingModel;
import com.ibt.niramaya.modal.appoint_list_main_modal.AppointmentModal;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisitonMainModal;
import com.ibt.niramaya.modal.doctor_opd_model.DoctorOpdDataModel;
import com.ibt.niramaya.modal.doctor_refer_slip_modal.DoctorReeferSlipMainModal;
import com.ibt.niramaya.modal.driver.driver_list_modal.DriverMainModal;
import com.ibt.niramaya.modal.finance.PatientFinanceListModel;
import com.ibt.niramaya.modal.home.HomeDataModel;
import com.ibt.niramaya.modal.hospital.HospitalIpdDetailMainModal;
import com.ibt.niramaya.modal.hospital.bed.HospitalBedListModel;
import com.ibt.niramaya.modal.hospital.ipd_services.SelectIpdServicesMainModal;
import com.ibt.niramaya.modal.hospital.ipd_slip.HospitalIpdModel;
import com.ibt.niramaya.modal.hospital.operation.HospitalOperationModel;
import com.ibt.niramaya.modal.hospital.pathology.HospitalPathologyBillModel;
import com.ibt.niramaya.modal.hospital.pharmacy.HospitalPharmacyBillModel;
import com.ibt.niramaya.modal.hospital_about_content.AboutContentMainModal;
import com.ibt.niramaya.modal.hospital_about_content.FaqQandAMainModal;
import com.ibt.niramaya.modal.hospital_detail.HospitalDetailModel;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_opetation_detail_modal.IpdOperationDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pathology_detail_modal.IpdPathologyDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pharmacy_detail_modal.IpdPharmacyDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.IpdSlipDetailMainModal;
import com.ibt.niramaya.modal.invoice_modal.opd_invoice_modal.OpdInvoiceMainModal;
import com.ibt.niramaya.modal.invoice_modal.pathology_invoice_modal.PahologyInvoiceListMainModal;
import com.ibt.niramaya.modal.invoice_modal.pharmacy_invoice_modal.PharmacyInvoiceMainModal;
import com.ibt.niramaya.modal.ipd_prescription.details.PatientPastPrescriptionDetailsModel;
import com.ibt.niramaya.modal.ipd_prescription.list.PatientPastPrescriptionModel;
import com.ibt.niramaya.modal.ipd_test_report.IpdReportsModal;
import com.ibt.niramaya.modal.nurse_ipd_reporting.NurseIpdReportingMainModal;
import com.ibt.niramaya.modal.otp_verifacation_modal.OtpVerificationMainModal;
import com.ibt.niramaya.modal.patient_ipd_modal.PatientIpdMainModal;
import com.ibt.niramaya.modal.patient_modal.PatientMainModal;
import com.ibt.niramaya.modal.prescription.PrescritionListModel;
import com.ibt.niramaya.modal.prescription.detail.PrescriptionDetailModel;
import com.ibt.niramaya.modal.report.PatientReportsModel;
import com.ibt.niramaya.modal.specialization.all.SpecialistDoctorModel;
import com.ibt.niramaya.modal.specialization.hospital.HospitalSpecialistDoctorModel;
import com.ibt.niramaya.modal.token.TokenModel;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetrofitApiClient {


    @FormUrlEncoded
    @POST(Constant.USER_LOGIN)
    Call<ResponseBody> usersLogin(@Field("contact") String contact);

    @FormUrlEncoded
    @POST(Constant.OTP_VERIFICATION)
    Call<OtpVerificationMainModal> fetchOtp(@Field("contact") String contact, @Field("otp_number") String otp_number);

    @Multipart
    @POST(Constant.CREATE_PATIENT_PROFILE)
    Call<ResponseBody> createPatientProfile(@Part("name") RequestBody name,
                                            @Part("bloodgroup") RequestBody bloodgroup,
                                            @Part("contact") RequestBody contact,
                                            @Part("date_of_birth") RequestBody date_of_birth,
                                            @Part("email") RequestBody email,
                                            @Part("house_number") RequestBody house_number,
                                            @Part("street_name") RequestBody street_name,
                                            @Part("city") RequestBody city,
                                            @Part("state") RequestBody state,
                                            @Part("country") RequestBody country,
                                            @Part("zipcode") RequestBody zipcode,
                                            @Part("gender") RequestBody gender,
                                            @Part("gardian_name") RequestBody gardian_name,
                                            @Part("relationship_with_gardian") RequestBody relationship_with_gardian,
                                            @Part("gardian_contact") RequestBody gardian_contact,
                                            @Part("gardian_address") RequestBody gardian_address,
                                            @Part("aadhar_number") RequestBody aadhar_number,
                                            @Part("user_id") RequestBody user_id,
                                            @Part("relationship_status") RequestBody relationship_status,
                                            @Part MultipartBody.Part profile_picture);

    @FormUrlEncoded
    @POST(Constant.PATIENT_LIST)
    Call<PatientMainModal> patientList(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("Add_Property.php")
    Call<ResponseBody> uploadPropertyDataToServer(@Field("Image1") String Image1,
                                                  @Field("Image2") String Image2,
                                                  @Field("Image3") String Image3,
                                                  @Field("Image4") String Image4,

                                                  @Field("WardName") String WardName,
                                                  @Field("ZoneName") String ZoneName,
                                                  @Field("BlockNumber") String BlockNumber,
                                                  @Field("PropertyNumber") String PropertyNumber,
                                                  @Field("PropertyName") String PropertyName,
                                                  @Field("Address") String Address,

                                                  @Field("CorporationPropertyNumber") String CorporationPropertyNumber,
                                                  @Field("PropertyUsageType") String PropertyUsageType,
                                                  @Field("PropertyType") String PropertyType,
                                                  @Field("ResidentialCount") String ResidentialCount,
                                                  @Field("CommercialCount") String CommercialCount,
                                                  @Field("Remark1") String Remark1,
                                                  @Field("Remark2") String Remark2,
                                                  @Field("DeviceId") String DeviceId,

                                                  @Field("Longitude") String Longitude,
                                                  @Field("Latitude") String Latitude,
                                                  @Field("UserId") String UserId);

    @FormUrlEncoded
    @POST(Constant.PATIENT_PRESCRIPTION_LIST)
    Call<PrescritionListModel> patientPrescriptionList(@Field("user_id") String user_id,
                                                       @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST(Constant.PATIENT_PRESCRIPTION_DETAIL)
    Call<PrescriptionDetailModel> patientPrescriptionDetail(@Field("user_id") String user_id,
                                                            @Field("patient_id") String patient_id,
                                                            @Field("opd_id") String opd_id);

    @Multipart
    @POST(Constant.UPDATE_PATIENNT_PROFILE)
    Call<ResponseBody> updatePatientProfile(@Part("name") RequestBody name,
                                            @Part("bloodgroup") RequestBody bloodgroup,
                                            @Part("contact") RequestBody contact,
                                            @Part("date_of_birth") RequestBody date_of_birth,
                                            @Part("email") RequestBody email,
                                            @Part("house_number") RequestBody house_number,
                                            @Part("street_name") RequestBody street_name,
                                            @Part("city") RequestBody city,
                                            @Part("state") RequestBody state,
                                            @Part("country") RequestBody country,
                                            @Part("zipcode") RequestBody zipcode,
                                            @Part("gender") RequestBody gender,
                                            @Part("gardian_name") RequestBody gardian_name,
                                            @Part("relationship_with_gardian") RequestBody relationship_with_gardian,
                                            @Part("gardian_contact") RequestBody gardian_contact,
                                            @Part("gardian_address") RequestBody gardian_address,
                                            @Part("aadhar_number") RequestBody aadhar_number,
                                            @Part("user_id") RequestBody user_id,
                                            @Part("relationship_status") RequestBody relationship_status,
                                            @Part("patient_id") RequestBody patient_id,
                                            @Part MultipartBody.Part profile_picture);

    @FormUrlEncoded
    @POST(Constant.HOSPITAL_LIST)
    Call<HomeDataModel> hospitalList(@Field("user_id") String user_id,
                                     @Field("patient_id") String patient_id,
                                     @Field("latitude") String latitude,
                                     @Field("longitude") String longitude,
                                     @Field("near_by") String near_by);

  /*  Call<ResponseBody> updatePatientProfie(@Field("name") String name,
                                           @Field("bloodgroup") String bloodgroup,
                                           @Field("contact") String contact,
                                           @Field("date_of_birth") String date_of_birth,
                                           @Field("email") String email,
                                           @Field("house_number") String house_number,
                                           @Field("street_name") String street_name,
                                           @Field("city") String city,
                                           @Field("state") String state,
                                           @Field("country") String country,
                                           @Field("zipcode") String zipcode,
                                           @Field("gender") String gender,
                                           @Field("gardian_name") String gardian_name,
                                           @Field("relationship_with_gardian") String relationship_with_gardian,
                                           @Field("gardian_contact") String gardian_contact,
                                           @Field("gardian_address") String gardian_address,
                                           @Field("aadhar_number") String aadhar_number,
                                           @Field("user_id") String user_id,
                                           @Field("relationship_status") String relationship_status,
                                           @Field("patient_id") String patient_id,
                                           @Field("profile_picture") String profile_picture);*/

    @FormUrlEncoded
    @POST(Constant.PHARMACY_INVOICE_LIST)
    Call<PharmacyInvoiceMainModal> pharmacyInvoiceList(@Field("patient_id") String patient_id,
                                                       @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.PATHOLOGY_INVOICE_LIST)
    Call<PahologyInvoiceListMainModal> pathologyInvoiceList(@Field("patient_id") String patient_id,
                                                            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.OPD_INVOICE_LIST)
    Call<OpdInvoiceMainModal> opdInvoiceList(@Field("patient_id") String patient_id,
                                             @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.CANCEL_APPOINTMENT)
    Call<ResponseBody> cancelAppointment(@Field("appointment_id") String appointment_id,
                                         @Field("appointment_status") String appointment_status);

    @FormUrlEncoded
    @POST(Constant.SELECT_PATIENT_APPOINTMENT)
    Call<AppointmentModal> appointmentList(@Field("patient_id") String patient_id,
                                           @Field("user_id") String user_id,
                                           @Field("appointment_call_type") String appointment_call_type);

    @FormUrlEncoded
    @POST(Constant.DOCTOR_OPD_LIST)
    Call<HospitalDetailModel> doctorOpdList(@Field("hospital_id") String hospital_id,
                                            @Field("user_id") String user_id,
                                            @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST(Constant.SPECIALIST_DOCTOR_OPD)
    Call<SpecialistDoctorModel> getSpecialistDoctor(@Field("specialization_id") String specialization_id,
                                                    @Field("user_id") String user_id,
                                                    @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST(Constant.HOSPITAL_SPECIALIST_DOCTOR_OPD)
    Call<HospitalSpecialistDoctorModel> getHospitalSpecialistDoctor(@Field("specialization_id") String specialization_id,
                                                                    @Field("hospital_id") String hospital_id,
                                                                    @Field("user_id") String user_id,
                                                                    @Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST(Constant.DOCTOR_OPD)
    Call<DoctorOpdDataModel> doctorOpd(@Field("hospital_id") String hospital_id,
                                       @Field("user_id") String user_id,
                                       @Field("patient_id") String patient_id,
                                       @Field("doctor_id") String doctor_id);

    @FormUrlEncoded
    @POST(Constant.BOOK_APPOINTMENT)
    Call<ResponseBody> bookPatientApponitment(@Field("patient_id") String patient_id,
                                              @Field("user_id") String user_id,
                                              @Field("opd_id") String opd_id,
                                              @Field("payment_status") String payment_status,
                                              @Field("type") String type,
                                              @Field("status") String status,
                                              @Field("amount") String amount,
                                              @Field("booking_date") String booking_date,
                                              @Field("referred_by") String referred_by,
                                              @Field("referred_doctor_name") String referred_doctor_name);

    @Multipart
    @POST(Constant.ADD_PATIENT_FINANCE_REPORT)
    Call<ResponseBody> addPatientFinanceReprot(@Part("patient_finance_id") RequestBody patient_finance_id,
                                               @Part("patient_id") RequestBody patient_id,
                                               @Part("title") RequestBody title,
                                               @Part("provider") RequestBody provider,
                                               @Part("policy_number") RequestBody policy_number,
                                               @Part("policy_valid_time") RequestBody policy_valid_time,
                                               @Part("status") RequestBody status,
                                               @Part MultipartBody.Part policy_document);

    @FormUrlEncoded
    @POST(Constant.PATIENT_FINANCE_LIST)
    Call<PatientFinanceListModel> patientFinanceList(@Field("patient_id") String patient_id);


    @FormUrlEncoded
    @POST(Constant.PATIENT_TOKEN)
    Call<TokenModel> patientToken(@Field("patient_id") String patient_id);

    @FormUrlEncoded
    @POST(Constant.PATIENT_REPORT)
    Call<PatientReportsModel> patientReports(@Field("patient_id") String patient_id);

    @GET("driver.json")
    Call<DriverMainModal> driverList();

    /*************************************************************
     * Ambulance
     ************************************************************/
    @FormUrlEncoded
    @POST(Constant.AMBULANCE_DETAIL_API)
    Call<AmbulanceDetailModel> ambulanceDetail(@Field("driver_id") String driver_id);

    @FormUrlEncoded
    @POST(Constant.AMBULANCE_ONGOING_BOOKING_API)
    Call<AmbulanceOngoingModel> ongoingAmbulance(@Field("patient_id") String patient_id);


    @FormUrlEncoded
    @POST(Constant.AMBULANCE_EMERGENCY_ONGOING_BOOKING_API)
    Call<AmbulanceOngoingModel> ongoingEnergencyAmbulance(@Field("user_contact") String user_contact);


    @FormUrlEncoded
    @POST(Constant.SELECT_ACTIVE_DRIVER_AMBULANCE)
    Call<ActiveAmbulanceModal> activeDriverAmbulance(@Field("hospital_id") String hospital_id, @Field("call_type") String call_type);

    @FormUrlEncoded
    @POST(Constant.UPDATE_AMBULANCE_BOOKING_STATUS)
    Call<ResponseBody> updateAmbulanceStatus(@Field("ambulance_booking_id") String ambulance_booking_id,
                                             @Field("ambulance_booking_status") String ambulance_booking_status);

    @FormUrlEncoded
    @POST(Constant.AMBULANCE_BOOKING_API)
    Call<ResponseBody> bookAmbulance(@Field("ambulance_id") String ambulance_id,
                                     @Field("hospital_id") String hospital_id,
                                     @Field("hospital_admin_id") String hospital_admin_id,
                                     @Field("driver_id") String driver_id,
                                     @Field("patient_id") String patient_id,
                                     @Field("patient_name") String patient_name,
                                     @Field("patient_age") String patient_age,
                                     @Field("patient_number") String patient_number,
                                     @Field("patient_address") String patient_address,
                                     @Field("ambulance_booking_latitude") String ambulance_booking_latitude,
                                     @Field("ambulance_booking_longitude") String ambulance_booking_longitude,
                                     @Field("ambulance_booking_end_latitude") String ambulance_booking_end_latitude,
                                     @Field("ambulance_booking_end_longitude") String ambulance_booking_end_longitude,
                                     @Field("ambulance_booking_trip_type") String ambulance_booking_trip_type);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_SLIP)
    Call<HospitalIpdModel> getHospitalIpdList(@Field("ipd_id") String ipd_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_OPERATIONS)
    Call<HospitalOperationModel> getHospitalOperationsList(@Field("ipd_id") String ipd_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_PHARMACY_BILL)
    Call<HospitalPharmacyBillModel> getHospitalPharmacyBill0List(@Field("ipd_id") String ipd_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_PATHOLOGY_BILL)
    Call<HospitalPathologyBillModel> getHospitalPathologyBill0List(@Field("ipd_id") String ipd_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_BED)
    Call<HospitalBedListModel> getHospitalIpdBedList(@Field("ipd_id") String ipd_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_SLIP_DETAIL)
    Call<IpdSlipDetailMainModal> getHospitalIpdDetail(@Field("ipd_slip_id") String ipd_slip_id);

    @FormUrlEncoded
    @POST(Constant.HOSPITAL_SELECT_IPD_BILL_SERVICE)
    Call<SelectIpdServicesMainModal> getHospitalIpBillServicesList(@Field("ipd") String ipd);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_OPERATION_DETAIL)
    Call<IpdOperationDetailMainModal> getHospitalOperationDetail(@Field("operation_schedule_id") String operation_schedule_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_PATHOLOGY_DETAIL)
    Call<IpdPathologyDetailMainModal> getHospitalPathologyDetail(@Field("ipd_pathology_bill_id") String ipd_pathology_bill_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_PHARMACY_DETAIL)
    Call<IpdPharmacyDetailMainModal> getHospitalPharmacyDetail(@Field("ipd_pharmacy_bill_id") String ipd_pharmacy_bill_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_HOSPITAL_IPD_DETAIL)
    Call<HospitalIpdDetailMainModal> getHospitalIpdDetailDetail(@Field("ipd_id") String ipd_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_PATIENT_IPD_LIST_DETAIL)
    Call<PatientIpdMainModal> ipdListData(@Field("patient_id") String patient_id,
                                          @Field("ipd_status") String ipd_status);

    @FormUrlEncoded
    @POST(Constant.SELECT_NURSE_IPD_REPORT)
    Call<NurseIpdReportingMainModal> selectNurseIpdReport(@Field("ipd_id") String ipd_id);


    @FormUrlEncoded
    @POST(Constant.SELECT_DOCTOR_PRESCRIPTION)
    Call<PatientPastPrescriptionModel> selectDoctorPrescription(@Field("ipd_id") String ipd_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_Doctor_PRESCRIPTION_DETAIL)
    Call<PatientPastPrescriptionDetailsModel> selectDoctorPrescriptionDetail(@Field("ipd_slip") String ipd_slip, @Field("hospital_id") String hospital_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_REEFER_SLIP)
    Call<DoctorReeferSlipMainModal> refferedData(@Field("reference") String reference,
                                                 @Field("reference_id") String reference_id);

    @FormUrlEncoded
    @POST(Constant.SELECT_BLOOD_REQUISITION_SLIP)
    Call<BloodRequisitonMainModal> selectBloodRequisitionData(@Field("reference") String reference,
                                                              @Field("reference_id") String reference_id);

    @FormUrlEncoded
    @POST(Constant.INVOKE_PATIENT_API)
    Call<ResponseBody> invokeNewPatient(@Field("patient_number") String patient_number,
                                        @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constant.INVOKE_PATIENT_CONTACT_VERIFICATION)
    Call<ResponseBody> invokeNewPatientContactvarification(@Field("contact") String contact,
                                                           @Field("otp_number") String otp_number,
                                                           @Field("verification_type") String verification_type,
                                                           @Field("patient_id") String patient_id,
                                                           @Field("user_id") String user_id);

    @GET(Constant.ABOUT_CONTENT_API)
    Call<AboutContentMainModal> contentData();

    @GET(Constant.ABOUT_FAQ_CONTENT_API)
    Call<FaqQandAMainModal> faqData();

    @FormUrlEncoded
    @POST(Constant.USER_TOKEN)
    Call<ResponseBody> userToken(@Field("device_id") String device_id,
                                 @Field("token") String token,
                                 @Field("user_id") String user_id,
                                 @Field("type") String type);

    @FormUrlEncoded
    @POST(Constant.SELECT_PATIENT_TEST_REPORTS)
    Call<IpdReportsModal> patientIpdTestReports(@Field("ipd_id") String ipd_id);


    @FormUrlEncoded
    @POST(Constant.ADD_EMERGENCY)
    Call<ResponseBody> addEmergencyData(@Field("patient_name") String patient_name,
                                        @Field("patient_age") String patient_age,
                                        @Field("patient_number") String patient_number,
                                        @Field("patient_address") String patient_address,
                                        @Field("ambulance_booking_latitude") String ambulance_booking_latitude,
                                        @Field("ambulance_booking_longitude") String ambulance_booking_longitude);


    @FormUrlEncoded
    @POST(Constant.EMERGENCY_ONGOING_BOOKING)
    Call<AmbulanceOngoingModel> patientEmergencyOngoingBooking(@Field("user_contact") String user_contact);
}
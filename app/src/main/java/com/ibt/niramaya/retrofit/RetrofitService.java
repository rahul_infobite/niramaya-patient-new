package com.ibt.niramaya.retrofit;


import android.app.Dialog;

import com.ibt.niramaya.BuildConfig;
import com.ibt.niramaya.modal.ambulance.book_ambulance.ActiveAmbulanceModal;
import com.ibt.niramaya.modal.ambulance.driver_detail.AmbulanceDetailModel;
import com.ibt.niramaya.modal.ambulance.ongoing.AmbulanceOngoingModel;
import com.ibt.niramaya.modal.appoint_list_main_modal.AppointmentModal;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisitonMainModal;
import com.ibt.niramaya.modal.doctor_opd_model.DoctorOpdDataModel;
import com.ibt.niramaya.modal.doctor_refer_slip_modal.DoctorReeferSlipMainModal;
import com.ibt.niramaya.modal.finance.PatientFinanceListModel;
import com.ibt.niramaya.modal.home.HomeDataModel;
import com.ibt.niramaya.modal.hospital.HospitalIpdDetailMainModal;
import com.ibt.niramaya.modal.hospital.HospitalListModel;
import com.ibt.niramaya.modal.hospital.bed.HospitalBedListModel;
import com.ibt.niramaya.modal.hospital.ipd_services.SelectIpdServicesMainModal;
import com.ibt.niramaya.modal.hospital.ipd_slip.HospitalIpdModel;
import com.ibt.niramaya.modal.hospital.operation.HospitalOperationModel;
import com.ibt.niramaya.modal.hospital.pathology.HospitalPathologyBillModel;
import com.ibt.niramaya.modal.hospital.pharmacy.HospitalPharmacyBillModel;
import com.ibt.niramaya.modal.hospital_about_content.AboutContentMainModal;
import com.ibt.niramaya.modal.hospital_about_content.FaqQandAMainModal;
import com.ibt.niramaya.modal.hospital_detail.HospitalDetailModel;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_opetation_detail_modal.IpdOperationDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pathology_detail_modal.IpdPathologyDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pharmacy_detail_modal.IpdPharmacyDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.IpdSlipDetailMainModal;
import com.ibt.niramaya.modal.invoice_modal.opd_invoice_modal.OpdInvoiceMainModal;
import com.ibt.niramaya.modal.invoice_modal.pathology_invoice_modal.PahologyInvoiceListMainModal;
import com.ibt.niramaya.modal.invoice_modal.pharmacy_invoice_modal.PharmacyInvoiceMainModal;
import com.ibt.niramaya.modal.ipd_prescription.details.PatientPastPrescriptionDetailsModel;
import com.ibt.niramaya.modal.ipd_prescription.list.PatientPastPrescriptionModel;
import com.ibt.niramaya.modal.ipd_test_report.IpdReportsModal;
import com.ibt.niramaya.modal.nurse_ipd_reporting.NurseIpdReportingMainModal;
import com.ibt.niramaya.modal.otp_verifacation_modal.OtpVerificationMainModal;
import com.ibt.niramaya.modal.patient_ipd_modal.PatientIpdMainModal;
import com.ibt.niramaya.modal.patient_modal.PatientMainModal;
import com.ibt.niramaya.modal.prescription.PrescritionListModel;
import com.ibt.niramaya.modal.prescription.detail.PrescriptionDetailModel;
import com.ibt.niramaya.modal.report.PatientReportsModel;
import com.ibt.niramaya.modal.specialization.all.SpecialistDoctorModel;
import com.ibt.niramaya.modal.specialization.hospital.HospitalSpecialistDoctorModel;
import com.ibt.niramaya.modal.token.TokenModel;
import com.ibt.niramaya.utils.AppProgressDialog;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitService {
    public static RetrofitApiClient client;
    public static String BASE_URL = "http://infobitetechnology.tech/niramaya/api/";
    public RetrofitService() {
        HttpLoggingInterceptor mHttpLoginInterceptor = new HttpLoggingInterceptor();

        mHttpLoginInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder mOkClient = new OkHttpClient.Builder().readTimeout(300,
                TimeUnit.SECONDS).writeTimeout(300, TimeUnit.SECONDS).connectTimeout(300, TimeUnit.SECONDS);

        if (BuildConfig.DEBUG) {
            mOkClient.addInterceptor(mHttpLoginInterceptor);
        }
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(mOkClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        client = retrofit.create(RetrofitApiClient.class);
    }

    public static RetrofitApiClient getRxClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build();
        return retrofit.create(RetrofitApiClient.class);
    }

    public static RetrofitApiClient getRetrofit() {
        if (client == null)
            new RetrofitService();

        return client;
    }

    public static void getServerResponse(final Dialog dialog, final Call<ResponseBody> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getOtpResponse(final Dialog dialog, final Call<OtpVerificationMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<OtpVerificationMainModal>() {
            @Override
            public void onResponse(Call<OtpVerificationMainModal> call, Response<OtpVerificationMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<OtpVerificationMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getPatientList(final Dialog dialog, final Call<PatientMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PatientMainModal>() {
            @Override
            public void onResponse(Call<PatientMainModal> call, Response<PatientMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PatientMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientPrescriptionList(final Dialog dialog, final Call<PrescritionListModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PrescritionListModel>() {
            @Override
            public void onResponse(Call<PrescritionListModel> call, Response<PrescritionListModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PrescritionListModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientPrescriptionDetail(final Dialog dialog, final Call<PrescriptionDetailModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PrescriptionDetailModel>() {
            @Override
            public void onResponse(Call<PrescriptionDetailModel> call, Response<PrescriptionDetailModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PrescriptionDetailModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getPharmacyInvoiceList(final Dialog dialog, final Call<PharmacyInvoiceMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PharmacyInvoiceMainModal>() {
            @Override
            public void onResponse(Call<PharmacyInvoiceMainModal> call, Response<PharmacyInvoiceMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PharmacyInvoiceMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getOpdInvoiceList(final Dialog dialog, final Call<OpdInvoiceMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<OpdInvoiceMainModal>() {
            @Override
            public void onResponse(Call<OpdInvoiceMainModal> call, Response<OpdInvoiceMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<OpdInvoiceMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getPathologyInvoiceList(final Dialog dialog, final Call<PahologyInvoiceListMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PahologyInvoiceListMainModal>() {
            @Override
            public void onResponse(Call<PahologyInvoiceListMainModal> call, Response<PahologyInvoiceListMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PahologyInvoiceListMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHospitalList(final Dialog dialog, final Call<HospitalListModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalListModel>() {
            @Override
            public void onResponse(Call<HospitalListModel> call, Response<HospitalListModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<HospitalListModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHomePageData(final Dialog dialog, final Call<HomeDataModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HomeDataModel>() {
            @Override
            public void onResponse(Call<HomeDataModel> call, Response<HomeDataModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<HomeDataModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void getPatientIpd(final Dialog dialog, final Call<PatientIpdMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PatientIpdMainModal>() {
            @Override
            public void onResponse(Call<PatientIpdMainModal> call, Response<PatientIpdMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PatientIpdMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getOpdDoctorList(final Dialog dialog, final Call<HospitalDetailModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalDetailModel>() {
            @Override
            public void onResponse(Call<HospitalDetailModel> call, Response<HospitalDetailModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<HospitalDetailModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientFinanceList(final Dialog dialog, final Call<PatientFinanceListModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PatientFinanceListModel>() {
            @Override
            public void onResponse(Call<PatientFinanceListModel> call, Response<PatientFinanceListModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PatientFinanceListModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void doctorDetail(final Dialog dialog, final Call<DoctorOpdDataModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<DoctorOpdDataModel>() {
            @Override
            public void onResponse(Call<DoctorOpdDataModel> call, Response<DoctorOpdDataModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<DoctorOpdDataModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getSpecializationDoctor(final Dialog dialog, final Call<SpecialistDoctorModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<SpecialistDoctorModel>() {
            @Override
            public void onResponse(Call<SpecialistDoctorModel> call, Response<SpecialistDoctorModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<SpecialistDoctorModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHospitalSpecializationDoctor(final Dialog dialog, final Call<HospitalSpecialistDoctorModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalSpecialistDoctorModel>() {
            @Override
            public void onResponse(Call<HospitalSpecialistDoctorModel> call, Response<HospitalSpecialistDoctorModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<HospitalSpecialistDoctorModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientToken(final Dialog dialog, final Call<TokenModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<TokenModel>() {
            @Override
            public void onResponse(Call<TokenModel> call, Response<TokenModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<TokenModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void ambulanceDetail(final Dialog dialog, final Call<AmbulanceDetailModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<AmbulanceDetailModel>() {
            @Override
            public void onResponse(Call<AmbulanceDetailModel> call, Response<AmbulanceDetailModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<AmbulanceDetailModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientTestReports(final Dialog dialog, final Call<PatientReportsModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PatientReportsModel>() {
            @Override
            public void onResponse(Call<PatientReportsModel> call, Response<PatientReportsModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PatientReportsModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientOnGoingambulanceBooking(final Dialog dialog, final Call<AmbulanceOngoingModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<AmbulanceOngoingModel>() {
            @Override
            public void onResponse(Call<AmbulanceOngoingModel> call, Response<AmbulanceOngoingModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<AmbulanceOngoingModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientAppointment(final Dialog dialog, final Call<AppointmentModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<AppointmentModal>() {
            @Override
            public void onResponse(Call<AppointmentModal> call, Response<AppointmentModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<AppointmentModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void getHospitalIpdListDetail(final Dialog dialog,
                                                final Call<HospitalIpdDetailMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalIpdDetailMainModal>() {
            @Override
            public void onResponse(Call<HospitalIpdDetailMainModal> call, Response<HospitalIpdDetailMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<HospitalIpdDetailMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void getHospitalIpdList(final Dialog dialog,
                                          final Call<HospitalIpdModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalIpdModel>() {
            @Override
            public void onResponse(Call<HospitalIpdModel> call, Response<HospitalIpdModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<HospitalIpdModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void getHospitalOperationsList(final Dialog dialog,
                                                 final Call<HospitalOperationModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalOperationModel>() {
            @Override
            public void onResponse(Call<HospitalOperationModel> call, Response<HospitalOperationModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<HospitalOperationModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void getHospitalPharmacyBillList(final Dialog dialog,
                                                   final Call<HospitalPharmacyBillModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalPharmacyBillModel>() {
            @Override
            public void onResponse(Call<HospitalPharmacyBillModel> call, Response<HospitalPharmacyBillModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<HospitalPharmacyBillModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void getHospitalPathologyBillList(final Dialog dialog,
                                                    final Call<HospitalPathologyBillModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalPathologyBillModel>() {
            @Override
            public void onResponse(Call<HospitalPathologyBillModel> call, Response<HospitalPathologyBillModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<HospitalPathologyBillModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void getHospitalIpdBedList(final Dialog dialog,
                                             final Call<HospitalBedListModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalBedListModel>() {
            @Override
            public void onResponse(Call<HospitalBedListModel> call, Response<HospitalBedListModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<HospitalBedListModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }


    public static void getHospitalIpdBillservicesList(final Dialog dialog, final Call<SelectIpdServicesMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<SelectIpdServicesMainModal>() {
            @Override
            public void onResponse(Call<SelectIpdServicesMainModal> call, Response<SelectIpdServicesMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<SelectIpdServicesMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHospitalIpdSlipDetailList(final Dialog dialog,
                                                    final Call<IpdSlipDetailMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IpdSlipDetailMainModal>() {
            @Override
            public void onResponse(Call<IpdSlipDetailMainModal> call, Response<IpdSlipDetailMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<IpdSlipDetailMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHospitalOperationScheduleDetailList(final Dialog dialog,
                                                              final Call<IpdOperationDetailMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IpdOperationDetailMainModal>() {
            @Override
            public void onResponse(Call<IpdOperationDetailMainModal> call, Response<IpdOperationDetailMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<IpdOperationDetailMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHospitalPharmacyDetailList(final Dialog dialog,
                                                     final Call<IpdPharmacyDetailMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IpdPharmacyDetailMainModal>() {
            @Override
            public void onResponse(Call<IpdPharmacyDetailMainModal> call, Response<IpdPharmacyDetailMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<IpdPharmacyDetailMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHospitalPathologyDetailList(final Dialog dialog,
                                                      final Call<IpdPathologyDetailMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IpdPathologyDetailMainModal>() {
            @Override
            public void onResponse(Call<IpdPathologyDetailMainModal> call, Response<IpdPathologyDetailMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<IpdPathologyDetailMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void getHospitalIpdListDetailIpd(final Dialog dialog, final Call<HospitalIpdDetailMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<HospitalIpdDetailMainModal>() {
            @Override
            public void onResponse(Call<HospitalIpdDetailMainModal> call, Response<HospitalIpdDetailMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            public void onFailure(Call<HospitalIpdDetailMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void selectNurseIpdReport(final Dialog dialog, final Call<NurseIpdReportingMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<NurseIpdReportingMainModal>() {
            @Override
            public void onResponse(Call<NurseIpdReportingMainModal> call, Response<NurseIpdReportingMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<NurseIpdReportingMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void selectDoctorPrescription(final Dialog dialog, final Call<PatientPastPrescriptionModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PatientPastPrescriptionModel>() {
            @Override
            public void onResponse(Call<PatientPastPrescriptionModel> call, Response<PatientPastPrescriptionModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PatientPastPrescriptionModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void selectDoctroPrescriptionDetail(final Dialog dialog, final Call<PatientPastPrescriptionDetailsModel> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<PatientPastPrescriptionDetailsModel>() {
            @Override
            public void onResponse(Call<PatientPastPrescriptionDetailsModel> call, Response<PatientPastPrescriptionDetailsModel> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<PatientPastPrescriptionDetailsModel> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void selectPatientReffered(final Dialog dialog, final Call<DoctorReeferSlipMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<DoctorReeferSlipMainModal>() {
            @Override
            public void onResponse(Call<DoctorReeferSlipMainModal> call, Response<DoctorReeferSlipMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }
            @Override
            public void onFailure(Call<DoctorReeferSlipMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void selectBloodRequisition(final Dialog dialog, final Call<BloodRequisitonMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<BloodRequisitonMainModal>() {
            @Override
            public void onResponse(Call<BloodRequisitonMainModal> call, Response<BloodRequisitonMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }
            @Override
            public void onFailure(Call<BloodRequisitonMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void fetchContent(final Dialog dialog, final Call<AboutContentMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<AboutContentMainModal>() {
            @Override
            public void onResponse(Call<AboutContentMainModal> call, Response<AboutContentMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }
            @Override
            public void onFailure(Call<AboutContentMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void fetchFaq(final Dialog dialog, final Call<FaqQandAMainModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<FaqQandAMainModal>() {
            @Override
            public void onResponse(Call<FaqQandAMainModal> call, Response<FaqQandAMainModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }
            @Override
            public void onFailure(Call<FaqQandAMainModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

    public static void patientIpdReportData(final Dialog dialog, final Call<IpdReportsModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<IpdReportsModal>() {
            @Override
            public void onResponse(Call<IpdReportsModal> call, Response<IpdReportsModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<IpdReportsModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }
    public static void activeAmbulanceList(final Dialog dialog, final Call<ActiveAmbulanceModal> method, final WebResponse webResponse) {
        if (dialog != null)
            AppProgressDialog.show(dialog);

        method.enqueue(new Callback<ActiveAmbulanceModal>() {
            @Override
            public void onResponse(Call<ActiveAmbulanceModal> call, Response<ActiveAmbulanceModal> response) {
                AppProgressDialog.hide(dialog);
                WebServiceResponse.handleResponse(response, webResponse);
            }

            @Override
            public void onFailure(Call<ActiveAmbulanceModal> call, Throwable throwable) {
                AppProgressDialog.hide(dialog);
                webResponse.onResponseFailed(throwable.getMessage());
            }
        });
    }

}
package com.ibt.niramaya.modal.ambulance.ongoing;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AmbulanceOngoingModel implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ambulance_booking_list")
    @Expose
    private AmbulanceBookingList ambulanceBookingList;
    public final static Parcelable.Creator<AmbulanceOngoingModel> CREATOR = new Creator<AmbulanceOngoingModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AmbulanceOngoingModel createFromParcel(Parcel in) {
            return new AmbulanceOngoingModel(in);
        }

        public AmbulanceOngoingModel[] newArray(int size) {
            return (new AmbulanceOngoingModel[size]);
        }

    }
            ;

    protected AmbulanceOngoingModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingList = ((AmbulanceBookingList) in.readValue((AmbulanceBookingList.class.getClassLoader())));
    }

    public AmbulanceOngoingModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public AmbulanceBookingList getAmbulanceBookingList() {
        return ambulanceBookingList;
    }

    public void setAmbulanceBookingList(AmbulanceBookingList ambulanceBookingList) {
        this.ambulanceBookingList = ambulanceBookingList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(ambulanceBookingList);
    }

    public int describeContents() {
        return 0;
    }

}
package com.ibt.niramaya.modal.ipd_prescription.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PreceptionSlip implements Parcelable
{

    @SerializedName("ipd_slip_id")
    @Expose
    private String ipdSlipId;
    @SerializedName("ipd_id")
    @Expose
    private String ipdId;
    @SerializedName("ipd_staff_allotment_id")
    @Expose
    private String ipdStaffAllotmentId;
    @SerializedName("ipd_cheif_complaints")
    @Expose
    private String ipdCheifComplaints;
    @SerializedName("ipd_vitals")
    @Expose
    private String ipdVitals;
    @SerializedName("ipd_bp")
    @Expose
    private String ipdBp;
    @SerializedName("ipd_heart_rate_per_min")
    @Expose
    private String ipdHeartRatePerMin;
    @SerializedName("ipd_resp_rate_min")
    @Expose
    private String ipdRespRateMin;
    @SerializedName("ipd_temp")
    @Expose
    private String ipdTemp;
    @SerializedName("ipd_pain_score")
    @Expose
    private String ipdPainScore;
    @SerializedName("ipd_treatement_given")
    @Expose
    private String ipdTreatementGiven;
    @SerializedName("ipd_treatment_advice")
    @Expose
    private String ipdTreatmentAdvice;
    @SerializedName("ipd_staff_id")
    @Expose
    private String ipdStaffId;
    @SerializedName("staff_name")
    @Expose
    private String staffName;
    @SerializedName("ipd_staff_type")
    @Expose
    private String ipdStaffType;
    @SerializedName("ipd_follow_up_advice")
    @Expose
    private String ipdFollowUpAdvice;
    @SerializedName("ipd_spo2")
    @Expose
    private String ipdSpo2;
    @SerializedName("ipd_gcs")
    @Expose
    private String ipdGcs;
    @SerializedName("ipd_status")
    @Expose
    private String ipdStatus;
    @SerializedName("ipd_admin_status")
    @Expose
    private String ipdAdminStatus;
    @SerializedName("ipd_created_date")
    @Expose
    private String ipdCreatedDate;
    @SerializedName("ipd_preception")
    @Expose
    private List<IpdPreception> ipdPreception = null;
    @SerializedName("ipd_preception_test")
    @Expose
    private List<IpdPreceptionTest> ipdPreceptionTest = null;
    public final static Creator<PreceptionSlip> CREATOR = new Creator<PreceptionSlip>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PreceptionSlip createFromParcel(Parcel in) {
            return new PreceptionSlip(in);
        }

        public PreceptionSlip[] newArray(int size) {
            return (new PreceptionSlip[size]);
        }

    }
            ;

    protected PreceptionSlip(Parcel in) {
        this.ipdSlipId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdStaffAllotmentId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdCheifComplaints = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdVitals = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdBp = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdHeartRatePerMin = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdRespRateMin = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdTemp = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPainScore = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdTreatementGiven = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdTreatmentAdvice = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdStaffId = ((String) in.readValue((String.class.getClassLoader())));
        this.staffName = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdStaffType = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdFollowUpAdvice = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdSpo2 = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdGcs = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdAdminStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipdPreception, (com.ibt.niramaya.modal.ipd_prescription.details.IpdPreception.class.getClassLoader()));
        in.readList(this.ipdPreceptionTest, (com.ibt.niramaya.modal.ipd_prescription.details.IpdPreceptionTest.class.getClassLoader()));
    }

    public PreceptionSlip() {
    }

    public String getIpdSlipId() {
        return ipdSlipId;
    }

    public void setIpdSlipId(String ipdSlipId) {
        this.ipdSlipId = ipdSlipId;
    }

    public String getIpdId() {
        return ipdId;
    }

    public void setIpdId(String ipdId) {
        this.ipdId = ipdId;
    }

    public String getIpdStaffAllotmentId() {
        return ipdStaffAllotmentId;
    }

    public void setIpdStaffAllotmentId(String ipdStaffAllotmentId) {
        this.ipdStaffAllotmentId = ipdStaffAllotmentId;
    }

    public String getIpdCheifComplaints() {
        return ipdCheifComplaints;
    }

    public void setIpdCheifComplaints(String ipdCheifComplaints) {
        this.ipdCheifComplaints = ipdCheifComplaints;
    }

    public String getIpdVitals() {
        return ipdVitals;
    }

    public void setIpdVitals(String ipdVitals) {
        this.ipdVitals = ipdVitals;
    }

    public String getIpdBp() {
        return ipdBp;
    }

    public void setIpdBp(String ipdBp) {
        this.ipdBp = ipdBp;
    }

    public String getIpdHeartRatePerMin() {
        return ipdHeartRatePerMin;
    }

    public void setIpdHeartRatePerMin(String ipdHeartRatePerMin) {
        this.ipdHeartRatePerMin = ipdHeartRatePerMin;
    }

    public String getIpdRespRateMin() {
        return ipdRespRateMin;
    }

    public void setIpdRespRateMin(String ipdRespRateMin) {
        this.ipdRespRateMin = ipdRespRateMin;
    }

    public String getIpdTemp() {
        return ipdTemp;
    }

    public void setIpdTemp(String ipdTemp) {
        this.ipdTemp = ipdTemp;
    }

    public String getIpdPainScore() {
        return ipdPainScore;
    }

    public void setIpdPainScore(String ipdPainScore) {
        this.ipdPainScore = ipdPainScore;
    }

    public String getIpdTreatementGiven() {
        return ipdTreatementGiven;
    }

    public void setIpdTreatementGiven(String ipdTreatementGiven) {
        this.ipdTreatementGiven = ipdTreatementGiven;
    }

    public String getIpdTreatmentAdvice() {
        return ipdTreatmentAdvice;
    }

    public void setIpdTreatmentAdvice(String ipdTreatmentAdvice) {
        this.ipdTreatmentAdvice = ipdTreatmentAdvice;
    }

    public String getIpdStaffId() {
        return ipdStaffId;
    }

    public void setIpdStaffId(String ipdStaffId) {
        this.ipdStaffId = ipdStaffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getIpdStaffType() {
        return ipdStaffType;
    }

    public void setIpdStaffType(String ipdStaffType) {
        this.ipdStaffType = ipdStaffType;
    }

    public String getIpdFollowUpAdvice() {
        return ipdFollowUpAdvice;
    }

    public void setIpdFollowUpAdvice(String ipdFollowUpAdvice) {
        this.ipdFollowUpAdvice = ipdFollowUpAdvice;
    }

    public String getIpdSpo2() {
        return ipdSpo2;
    }

    public void setIpdSpo2(String ipdSpo2) {
        this.ipdSpo2 = ipdSpo2;
    }

    public String getIpdGcs() {
        return ipdGcs;
    }

    public void setIpdGcs(String ipdGcs) {
        this.ipdGcs = ipdGcs;
    }

    public String getIpdStatus() {
        return ipdStatus;
    }

    public void setIpdStatus(String ipdStatus) {
        this.ipdStatus = ipdStatus;
    }

    public String getIpdAdminStatus() {
        return ipdAdminStatus;
    }

    public void setIpdAdminStatus(String ipdAdminStatus) {
        this.ipdAdminStatus = ipdAdminStatus;
    }

    public String getIpdCreatedDate() {
        return ipdCreatedDate;
    }

    public void setIpdCreatedDate(String ipdCreatedDate) {
        this.ipdCreatedDate = ipdCreatedDate;
    }

    public List<IpdPreception> getIpdPreception() {
        return ipdPreception;
    }

    public void setIpdPreception(List<IpdPreception> ipdPreception) {
        this.ipdPreception = ipdPreception;
    }

    public List<IpdPreceptionTest> getIpdPreceptionTest() {
        return ipdPreceptionTest;
    }

    public void setIpdPreceptionTest(List<IpdPreceptionTest> ipdPreceptionTest) {
        this.ipdPreceptionTest = ipdPreceptionTest;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ipdSlipId);
        dest.writeValue(ipdId);
        dest.writeValue(ipdStaffAllotmentId);
        dest.writeValue(ipdCheifComplaints);
        dest.writeValue(ipdVitals);
        dest.writeValue(ipdBp);
        dest.writeValue(ipdHeartRatePerMin);
        dest.writeValue(ipdRespRateMin);
        dest.writeValue(ipdTemp);
        dest.writeValue(ipdPainScore);
        dest.writeValue(ipdTreatementGiven);
        dest.writeValue(ipdTreatmentAdvice);
        dest.writeValue(ipdStaffId);
        dest.writeValue(staffName);
        dest.writeValue(ipdStaffType);
        dest.writeValue(ipdFollowUpAdvice);
        dest.writeValue(ipdSpo2);
        dest.writeValue(ipdGcs);
        dest.writeValue(ipdStatus);
        dest.writeValue(ipdAdminStatus);
        dest.writeValue(ipdCreatedDate);
        dest.writeList(ipdPreception);
        dest.writeList(ipdPreceptionTest);
    }

    public int describeContents() {
        return 0;
    }

}

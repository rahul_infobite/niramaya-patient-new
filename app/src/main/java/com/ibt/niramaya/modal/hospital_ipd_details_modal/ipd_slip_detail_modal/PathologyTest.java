package com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PathologyTest implements Parcelable
{

@SerializedName("ipd_pathology_test_id")
@Expose
private String ipdPathologyTestId;
@SerializedName("test")
@Expose
private Integer test;
@SerializedName("pathology_id")
@Expose
private String pathologyId;
@SerializedName("test_name")
@Expose
private String testName;
@SerializedName("ipd_pathology_test_type")
@Expose
private String ipdPathologyTestType;
@SerializedName("ipd_pathology_test_created_date")
@Expose
private String ipdPathologyTestCreatedDate;
public final static Creator<PathologyTest> CREATOR = new Creator<PathologyTest>() {


@SuppressWarnings({
"unchecked"
})
public PathologyTest createFromParcel(Parcel in) {
return new PathologyTest(in);
}

public PathologyTest[] newArray(int size) {
return (new PathologyTest[size]);
}

}
;

protected PathologyTest(Parcel in) {
this.ipdPathologyTestId = ((String) in.readValue((String.class.getClassLoader())));
this.test = ((Integer) in.readValue((Integer.class.getClassLoader())));
this.pathologyId = ((String) in.readValue((String.class.getClassLoader())));
this.testName = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPathologyTestType = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPathologyTestCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
}

public PathologyTest() {
}

public String getIpdPathologyTestId() {
return ipdPathologyTestId;
}

public void setIpdPathologyTestId(String ipdPathologyTestId) {
this.ipdPathologyTestId = ipdPathologyTestId;
}

public Integer getTest() {
return test;
}

public void setTest(Integer test) {
this.test = test;
}

public String getPathologyId() {
return pathologyId;
}

public void setPathologyId(String pathologyId) {
this.pathologyId = pathologyId;
}

public String getTestName() {
return testName;
}

public void setTestName(String testName) {
this.testName = testName;
}

public String getIpdPathologyTestType() {
return ipdPathologyTestType;
}

public void setIpdPathologyTestType(String ipdPathologyTestType) {
this.ipdPathologyTestType = ipdPathologyTestType;
}

public String getIpdPathologyTestCreatedDate() {
return ipdPathologyTestCreatedDate;
}

public void setIpdPathologyTestCreatedDate(String ipdPathologyTestCreatedDate) {
this.ipdPathologyTestCreatedDate = ipdPathologyTestCreatedDate;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(ipdPathologyTestId);
dest.writeValue(test);
dest.writeValue(pathologyId);
dest.writeValue(testName);
dest.writeValue(ipdPathologyTestType);
dest.writeValue(ipdPathologyTestCreatedDate);
}

public int describeContents() {
return 0;
}

}
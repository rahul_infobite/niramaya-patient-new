package com.ibt.niramaya.modal.doctor_refer_slip_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorReeferSlipMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("refer_slip")
    @Expose
    private ReferSlip referSlip;
    public final static Creator<DoctorReeferSlipMainModal> CREATOR = new Creator<DoctorReeferSlipMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DoctorReeferSlipMainModal createFromParcel(Parcel in) {
            return new DoctorReeferSlipMainModal(in);
        }

        public DoctorReeferSlipMainModal[] newArray(int size) {
            return (new DoctorReeferSlipMainModal[size]);
        }

    }
            ;

    protected DoctorReeferSlipMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.referSlip = ((ReferSlip) in.readValue((ReferSlip.class.getClassLoader())));
    }

    public DoctorReeferSlipMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ReferSlip getReferSlip() {
        return referSlip;
    }

    public void setReferSlip(ReferSlip referSlip) {
        this.referSlip = referSlip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(referSlip);
    }

    public int describeContents() {
        return 0;
    }

}

package com.ibt.niramaya.modal.hospital.select_ipd_services_at_patient_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdService implements Parcelable {

    @SerializedName("ipd_service_id")
    @Expose
    private String ipdServiceId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    public final static Creator<IpdService> CREATOR = new Creator<IpdService>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdService createFromParcel(Parcel in) {
            return new IpdService(in);
        }

        public IpdService[] newArray(int size) {
            return (new IpdService[size]);
        }

    };

    protected IpdService(Parcel in) {
        this.ipdServiceId = ((String) in.readValue((String.class.getClassLoader())));
        this.title = ((String) in.readValue((String.class.getClassLoader())));
        this.description = ((String) in.readValue((String.class.getClassLoader())));
        this.amount = ((String) in.readValue((String.class.getClassLoader())));
        this.discount = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IpdService() {
    }

    public String getIpdServiceId() {
        return ipdServiceId;
    }

    public void setIpdServiceId(String ipdServiceId) {
        this.ipdServiceId = ipdServiceId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ipdServiceId);
        dest.writeValue(title);
        dest.writeValue(description);
        dest.writeValue(amount);
        dest.writeValue(discount);
        dest.writeValue(createdDate);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return title;
    }
}
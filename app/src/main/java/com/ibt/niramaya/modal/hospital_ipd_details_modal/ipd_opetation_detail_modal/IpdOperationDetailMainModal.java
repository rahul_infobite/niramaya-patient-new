package com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_opetation_detail_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdOperationDetailMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("operation_schedule")
    @Expose
    private OperationSchedule operationSchedule;
    public final static Creator<IpdOperationDetailMainModal> CREATOR = new Creator<IpdOperationDetailMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdOperationDetailMainModal createFromParcel(Parcel in) {
            return new IpdOperationDetailMainModal(in);
        }

        public IpdOperationDetailMainModal[] newArray(int size) {
            return (new IpdOperationDetailMainModal[size]);
        }

    }
            ;

    protected IpdOperationDetailMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.operationSchedule = ((OperationSchedule) in.readValue((OperationSchedule.class.getClassLoader())));
    }

    public IpdOperationDetailMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public OperationSchedule getOperationSchedule() {
        return operationSchedule;
    }

    public void setOperationSchedule(OperationSchedule operationSchedule) {
        this.operationSchedule = operationSchedule;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(operationSchedule);
    }

    public int describeContents() {
        return 0;
    }

}
package com.ibt.niramaya.modal.ambulance.book_ambulance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ambulance implements Parcelable {

    @SerializedName("ambulance_id")
    @Expose
    private String ambulanceId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("facility")
    @Expose
    private String facility;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("rto_number")
    @Expose
    private String rtoNumber;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("driver")
    @Expose
    private Driver driver;
    @SerializedName("driver_status_latitude")
    @Expose
    private String driverStatusLatitude;
    @SerializedName("driver_status_logitude")
    @Expose
    private String driverStatusLogitude;
    @SerializedName("driver_status_status")
    @Expose
    private String driverStatusStatus;
    @SerializedName("last_updated_date")
    @Expose
    private String lastUpdatedDate;
    public final static Creator<Ambulance> CREATOR = new Creator<Ambulance>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Ambulance createFromParcel(Parcel in) {
            return new Ambulance(in);
        }

        public Ambulance[] newArray(int size) {
            return (new Ambulance[size]);
        }

    };

    protected Ambulance(Parcel in) {
        this.ambulanceId = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.facility = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.rtoNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.driverId = ((String) in.readValue((String.class.getClassLoader())));
        this.driver = ((Driver) in.readValue((Driver.class.getClassLoader())));
        this.driverStatusLatitude = ((String) in.readValue((String.class.getClassLoader())));
        this.driverStatusLogitude = ((String) in.readValue((String.class.getClassLoader())));
        this.driverStatusStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.lastUpdatedDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Ambulance() {
    }

    public String getAmbulanceId() {
        return ambulanceId;
    }

    public void setAmbulanceId(String ambulanceId) {
        this.ambulanceId = ambulanceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRtoNumber() {
        return rtoNumber;
    }

    public void setRtoNumber(String rtoNumber) {
        this.rtoNumber = rtoNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getDriverStatusLatitude() {
        return driverStatusLatitude;
    }

    public void setDriverStatusLatitude(String driverStatusLatitude) {
        this.driverStatusLatitude = driverStatusLatitude;
    }

    public String getDriverStatusLogitude() {
        return driverStatusLogitude;
    }

    public void setDriverStatusLogitude(String driverStatusLogitude) {
        this.driverStatusLogitude = driverStatusLogitude;
    }

    public String getDriverStatusStatus() {
        return driverStatusStatus;
    }

    public void setDriverStatusStatus(String driverStatusStatus) {
        this.driverStatusStatus = driverStatusStatus;
    }

    public String getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(String lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ambulanceId);
        dest.writeValue(name);
        dest.writeValue(type);
        dest.writeValue(facility);
        dest.writeValue(registrationNumber);
        dest.writeValue(rtoNumber);
        dest.writeValue(image);
        dest.writeValue(createdDate);
        dest.writeValue(driverId);
        dest.writeValue(driver);
        dest.writeValue(driverStatusLatitude);
        dest.writeValue(driverStatusLogitude);
        dest.writeValue(driverStatusStatus);
        dest.writeValue(lastUpdatedDate);
    }

    public int describeContents() {
        return 0;
    }

}
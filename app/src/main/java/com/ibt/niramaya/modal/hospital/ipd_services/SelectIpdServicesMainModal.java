package com.ibt.niramaya.modal.hospital.ipd_services;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SelectIpdServicesMainModal implements Parcelable
{

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd_bill_service")
    @Expose
    private List<IpdBillService> ipdBillService = new ArrayList<IpdBillService>();
    public final static Creator<SelectIpdServicesMainModal> CREATOR = new Creator<SelectIpdServicesMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SelectIpdServicesMainModal createFromParcel(Parcel in) {
            return new SelectIpdServicesMainModal(in);
        }

        public SelectIpdServicesMainModal[] newArray(int size) {
            return (new SelectIpdServicesMainModal[size]);
        }

    }
            ;

    protected SelectIpdServicesMainModal(Parcel in) {
        this.result = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipdBillService, (com.ibt.niramaya.modal.hospital.ipd_services.IpdBillService.class.getClassLoader()));
    }

    public SelectIpdServicesMainModal() {
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IpdBillService> getIpdBillService() {
        return ipdBillService;
    }

    public void setIpdBillService(List<IpdBillService> ipdBillService) {
        this.ipdBillService = ipdBillService;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(result);
        dest.writeValue(message);
        dest.writeList(ipdBillService);
    }

    public int describeContents() {
        return 0;
    }

}
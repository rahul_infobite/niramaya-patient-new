package com.ibt.niramaya.modal.ipd_prescription.list;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PatientPastPrescriptionModel implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd_preception")
    @Expose
    private List<IpdPreception> ipdPreception = new ArrayList<>();
    public final static Creator<PatientPastPrescriptionModel> CREATOR = new Creator<PatientPastPrescriptionModel>() {

        @SuppressWarnings({
                "unchecked"
        })
        public PatientPastPrescriptionModel createFromParcel(Parcel in) {
            return new PatientPastPrescriptionModel(in);
        }

        public PatientPastPrescriptionModel[] newArray(int size) {
            return (new PatientPastPrescriptionModel[size]);
        }

    };

    protected PatientPastPrescriptionModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipdPreception, (com.ibt.niramaya.modal.ipd_prescription.list.IpdPreception.class.getClassLoader()));
    }

    public PatientPastPrescriptionModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IpdPreception> getIpdPreception() {
        return ipdPreception;
    }

    public void setIpdPreception(List<IpdPreception> ipdPreception) {
        this.ipdPreception = ipdPreception;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(ipdPreception);
    }

    public int describeContents() {
        return 0;
    }

}
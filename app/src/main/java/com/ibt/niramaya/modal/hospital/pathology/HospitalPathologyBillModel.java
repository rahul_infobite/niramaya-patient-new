package com.ibt.niramaya.modal.hospital.pathology;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HospitalPathologyBillModel implements Parcelable
{

@SerializedName("error")
@Expose
private Boolean error;
@SerializedName("message")
@Expose
private String message;
@SerializedName("bill_data")
@Expose
private List<PathologyBillDatum> billData = null;
public final static Creator<HospitalPathologyBillModel> CREATOR = new Creator<HospitalPathologyBillModel>() {


@SuppressWarnings({
"unchecked"
})
public HospitalPathologyBillModel createFromParcel(Parcel in) {
return new HospitalPathologyBillModel(in);
}

public HospitalPathologyBillModel[] newArray(int size) {
return (new HospitalPathologyBillModel[size]);
}

}
;

protected HospitalPathologyBillModel(Parcel in) {
this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
this.message = ((String) in.readValue((String.class.getClassLoader())));
in.readList(this.billData, (PathologyBillDatum.class.getClassLoader()));
}

public HospitalPathologyBillModel() {
}

public Boolean getError() {
return error;
}

public void setError(Boolean error) {
this.error = error;
}

public String getMessage() {
return message;
}

public void setMessage(String message) {
this.message = message;
}

public List<PathologyBillDatum> getBillData() {
return billData;
}

public void setBillData(List<PathologyBillDatum> billData) {
this.billData = billData;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(error);
dest.writeValue(message);
dest.writeList(billData);
}

public int describeContents() {
return 0;
}

}
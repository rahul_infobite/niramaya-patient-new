package com.ibt.niramaya.modal.ipd_prescription.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdPreceptionTest implements Parcelable
{

@SerializedName("ipd_pathology_test_id")
@Expose
private String ipdPathologyTestId;
@SerializedName("preception")
@Expose
private Integer preception;
@SerializedName("pathology_id")
@Expose
private String pathologyId;
@SerializedName("test_name")
@Expose
private String testName;
@SerializedName("ipd_pathology_test_type")
@Expose
private String ipdPathologyTestType;
@SerializedName("ipd_pathology_test_created_date")
@Expose
private String ipdPathologyTestCreatedDate;
public final static Creator<IpdPreceptionTest> CREATOR = new Creator<IpdPreceptionTest>() {


@SuppressWarnings({
"unchecked"
})
public IpdPreceptionTest createFromParcel(Parcel in) {
return new IpdPreceptionTest(in);
}

public IpdPreceptionTest[] newArray(int size) {
return (new IpdPreceptionTest[size]);
}

}
;

protected IpdPreceptionTest(Parcel in) {
this.ipdPathologyTestId = ((String) in.readValue((String.class.getClassLoader())));
this.preception = ((Integer) in.readValue((Integer.class.getClassLoader())));
this.pathologyId = ((String) in.readValue((String.class.getClassLoader())));
this.testName = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPathologyTestType = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPathologyTestCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
}

public IpdPreceptionTest() {
}

public String getIpdPathologyTestId() {
return ipdPathologyTestId;
}

public void setIpdPathologyTestId(String ipdPathologyTestId) {
this.ipdPathologyTestId = ipdPathologyTestId;
}

public Integer getPreception() {
return preception;
}

public void setPreception(Integer preception) {
this.preception = preception;
}

public String getPathologyId() {
return pathologyId;
}

public void setPathologyId(String pathologyId) {
this.pathologyId = pathologyId;
}

public String getTestName() {
return testName;
}

public void setTestName(String testName) {
this.testName = testName;
}

public String getIpdPathologyTestType() {
return ipdPathologyTestType;
}

public void setIpdPathologyTestType(String ipdPathologyTestType) {
this.ipdPathologyTestType = ipdPathologyTestType;
}

public String getIpdPathologyTestCreatedDate() {
return ipdPathologyTestCreatedDate;
}

public void setIpdPathologyTestCreatedDate(String ipdPathologyTestCreatedDate) {
this.ipdPathologyTestCreatedDate = ipdPathologyTestCreatedDate;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(ipdPathologyTestId);
dest.writeValue(preception);
dest.writeValue(pathologyId);
dest.writeValue(testName);
dest.writeValue(ipdPathologyTestType);
dest.writeValue(ipdPathologyTestCreatedDate);
}

public int describeContents() {
return 0;
}

}
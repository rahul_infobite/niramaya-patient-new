package com.ibt.niramaya.modal.hospital;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ipd implements Parcelable
{

@SerializedName("ipd_id")
@Expose
private String ipdId;
@SerializedName("ipd")
@Expose
private String ipd;
@SerializedName("patient_name")
@Expose
private String patientName;
@SerializedName("patient_id")
@Expose
private String patientId;
@SerializedName("patient_gender")
@Expose
private String patientGender;
@SerializedName("patient_dob")
@Expose
private String patientDob;
@SerializedName("patient_contact")
@Expose
private String patientContact;
@SerializedName("patient_profile")
@Expose
private String patientProfile;
@SerializedName("user_id")
@Expose
private String userId;
@SerializedName("hospital_id")
@Expose
private String hospitalId;
@SerializedName("doctor_id")
@Expose
private String doctorId;
@SerializedName("doctor_name")
@Expose
private String doctorName;
@SerializedName("ipd_patient_adhar_number")
@Expose
private String ipdPatientAdharNumber;
@SerializedName("ipd_admission_type")
@Expose
private String ipdAdmissionType;
@SerializedName("ipd_admission_type_id")
@Expose
private String ipdAdmissionTypeId;
@SerializedName("ipd_referred_dr_name")
@Expose
private String ipdReferredDrName;
@SerializedName("ipd_referred_dr_id")
@Expose
private String ipdReferredDrId;
@SerializedName("ipd_payment_limit")
@Expose
private String ipdPaymentLimit;
@SerializedName("ipd_document")
@Expose
private String ipdDocument;
@SerializedName("discharge_type")
@Expose
private String dischargeType;
@SerializedName("discharge_doctor_status")
@Expose
private String dischargeDoctorStatus;
@SerializedName("discharge_hospital_status")
@Expose
private String dischargeHospitalStatus;
@SerializedName("discharge_date_time")
@Expose
private String dischargeDateTime;
@SerializedName("discharge_by_doctor_id")
@Expose
private String dischargeByDoctorId;
@SerializedName("discharge_by_doctor_name")
@Expose
private String dischargeByDoctorName;
@SerializedName("ipd_created_date")
@Expose
private String ipdCreatedDate;
public final static Creator<Ipd> CREATOR = new Creator<Ipd>() {


@SuppressWarnings({
"unchecked"
})
public Ipd createFromParcel(Parcel in) {
return new Ipd(in);
}

public Ipd[] newArray(int size) {
return (new Ipd[size]);
}

}
;

protected Ipd(Parcel in) {
this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
this.ipd = ((String) in.readValue((String.class.getClassLoader())));
this.patientName = ((String) in.readValue((String.class.getClassLoader())));
this.patientId = ((String) in.readValue((String.class.getClassLoader())));
this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
this.patientDob = ((String) in.readValue((String.class.getClassLoader())));
this.patientContact = ((String) in.readValue((String.class.getClassLoader())));
this.patientProfile = ((String) in.readValue((String.class.getClassLoader())));
this.userId = ((String) in.readValue((String.class.getClassLoader())));
this.hospitalId = ((String) in.readValue((String.class.getClassLoader())));
this.doctorId = ((String) in.readValue((String.class.getClassLoader())));
this.doctorName = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPatientAdharNumber = ((String) in.readValue((String.class.getClassLoader())));
this.ipdAdmissionType = ((String) in.readValue((String.class.getClassLoader())));
this.ipdAdmissionTypeId = ((String) in.readValue((String.class.getClassLoader())));
this.ipdReferredDrName = ((String) in.readValue((String.class.getClassLoader())));
this.ipdReferredDrId = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPaymentLimit = ((String) in.readValue((String.class.getClassLoader())));
this.ipdDocument = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeType = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeDoctorStatus = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeHospitalStatus = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeDateTime = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeByDoctorId = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeByDoctorName = ((String) in.readValue((String.class.getClassLoader())));
this.ipdCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
}

public Ipd() {
}

public String getIpdId() {
return ipdId;
}

public void setIpdId(String ipdId) {
this.ipdId = ipdId;
}

public String getIpd() {
return ipd;
}

public void setIpd(String ipd) {
this.ipd = ipd;
}

public String getPatientName() {
return patientName;
}

public void setPatientName(String patientName) {
this.patientName = patientName;
}

public String getPatientId() {
return patientId;
}

public void setPatientId(String patientId) {
this.patientId = patientId;
}

public String getPatientGender() {
return patientGender;
}

public void setPatientGender(String patientGender) {
this.patientGender = patientGender;
}

public String getPatientDob() {
return patientDob;
}

public void setPatientDob(String patientDob) {
this.patientDob = patientDob;
}

public String getPatientContact() {
return patientContact;
}

public void setPatientContact(String patientContact) {
this.patientContact = patientContact;
}

public String getPatientProfile() {
return patientProfile;
}

public void setPatientProfile(String patientProfile) {
this.patientProfile = patientProfile;
}

public String getUserId() {
return userId;
}

public void setUserId(String userId) {
this.userId = userId;
}

public String getHospitalId() {
return hospitalId;
}

public void setHospitalId(String hospitalId) {
this.hospitalId = hospitalId;
}

public String getDoctorId() {
return doctorId;
}

public void setDoctorId(String doctorId) {
this.doctorId = doctorId;
}

public String getDoctorName() {
return doctorName;
}

public void setDoctorName(String doctorName) {
this.doctorName = doctorName;
}

public String getIpdPatientAdharNumber() {
return ipdPatientAdharNumber;
}

public void setIpdPatientAdharNumber(String ipdPatientAdharNumber) {
this.ipdPatientAdharNumber = ipdPatientAdharNumber;
}

public String getIpdAdmissionType() {
return ipdAdmissionType;
}

public void setIpdAdmissionType(String ipdAdmissionType) {
this.ipdAdmissionType = ipdAdmissionType;
}

public String getIpdAdmissionTypeId() {
return ipdAdmissionTypeId;
}

public void setIpdAdmissionTypeId(String ipdAdmissionTypeId) {
this.ipdAdmissionTypeId = ipdAdmissionTypeId;
}

public String getIpdReferredDrName() {
return ipdReferredDrName;
}

public void setIpdReferredDrName(String ipdReferredDrName) {
this.ipdReferredDrName = ipdReferredDrName;
}

public String getIpdReferredDrId() {
return ipdReferredDrId;
}

public void setIpdReferredDrId(String ipdReferredDrId) {
this.ipdReferredDrId = ipdReferredDrId;
}

public String getIpdPaymentLimit() {
return ipdPaymentLimit;
}

public void setIpdPaymentLimit(String ipdPaymentLimit) {
this.ipdPaymentLimit = ipdPaymentLimit;
}

public String getIpdDocument() {
return ipdDocument;
}

public void setIpdDocument(String ipdDocument) {
this.ipdDocument = ipdDocument;
}

public String getDischargeType() {
return dischargeType;
}

public void setDischargeType(String dischargeType) {
this.dischargeType = dischargeType;
}

public String getDischargeDoctorStatus() {
return dischargeDoctorStatus;
}

public void setDischargeDoctorStatus(String dischargeDoctorStatus) {
this.dischargeDoctorStatus = dischargeDoctorStatus;
}

public String getDischargeHospitalStatus() {
return dischargeHospitalStatus;
}

public void setDischargeHospitalStatus(String dischargeHospitalStatus) {
this.dischargeHospitalStatus = dischargeHospitalStatus;
}

public String getDischargeDateTime() {
return dischargeDateTime;
}

public void setDischargeDateTime(String dischargeDateTime) {
this.dischargeDateTime = dischargeDateTime;
}

public String getDischargeByDoctorId() {
return dischargeByDoctorId;
}

public void setDischargeByDoctorId(String dischargeByDoctorId) {
this.dischargeByDoctorId = dischargeByDoctorId;
}

public String getDischargeByDoctorName() {
return dischargeByDoctorName;
}

public void setDischargeByDoctorName(String dischargeByDoctorName) {
this.dischargeByDoctorName = dischargeByDoctorName;
}

public String getIpdCreatedDate() {
return ipdCreatedDate;
}

public void setIpdCreatedDate(String ipdCreatedDate) {
this.ipdCreatedDate = ipdCreatedDate;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(ipdId);
dest.writeValue(ipd);
dest.writeValue(patientName);
dest.writeValue(patientId);
dest.writeValue(patientGender);
dest.writeValue(patientDob);
dest.writeValue(patientContact);
dest.writeValue(patientProfile);
dest.writeValue(userId);
dest.writeValue(hospitalId);
dest.writeValue(doctorId);
dest.writeValue(doctorName);
dest.writeValue(ipdPatientAdharNumber);
dest.writeValue(ipdAdmissionType);
dest.writeValue(ipdAdmissionTypeId);
dest.writeValue(ipdReferredDrName);
dest.writeValue(ipdReferredDrId);
dest.writeValue(ipdPaymentLimit);
dest.writeValue(ipdDocument);
dest.writeValue(dischargeType);
dest.writeValue(dischargeDoctorStatus);
dest.writeValue(dischargeHospitalStatus);
dest.writeValue(dischargeDateTime);
dest.writeValue(dischargeByDoctorId);
dest.writeValue(dischargeByDoctorName);
dest.writeValue(ipdCreatedDate);
}

public int describeContents() {
return 0;
}

}
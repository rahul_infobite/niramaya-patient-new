package com.ibt.niramaya.modal.appoint_list_main_modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Datum {

    @SerializedName("appointment_id")
    @Expose
    private String appointmentId;
    @SerializedName("appointment_type")
    @Expose
    private String appointmentType;
    @SerializedName("appointment_payment_status")
    @Expose
    private String appointmentPaymentStatus;
    @SerializedName("appointment_date")
    @Expose
    private String appointmentDate;
    @SerializedName("appointment_status")
    @Expose
    private String appointmentStatus;
    @SerializedName("appointment_amount")
    @Expose
    private String appointmentAmount;
    @SerializedName("appointment_booking_date")
    @Expose
    private String appointmentBookingDate;
    @SerializedName("appointment_referred_by")
    @Expose
    private String appointmentReferredBy;
    @SerializedName("appointment_referred_doctor_name")
    @Expose
    private String appointmentReferredDoctorName;
    @SerializedName("doctor_id")
    @Expose
    private String doctorId;
    @SerializedName("doctor_name")
    @Expose
    private String doctorName;
    @SerializedName("opd")
    @Expose
    private Opd opd;
    @SerializedName("hospital")
    @Expose
    private Hospital hospital;

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public String getAppointmentType() {
        return appointmentType;
    }

    public void setAppointmentType(String appointmentType) {
        this.appointmentType = appointmentType;
    }

    public String getAppointmentPaymentStatus() {
        return appointmentPaymentStatus;
    }

    public void setAppointmentPaymentStatus(String appointmentPaymentStatus) {
        this.appointmentPaymentStatus = appointmentPaymentStatus;
    }

    public String getAppointmentDate() {
        return appointmentDate;
    }

    public void setAppointmentDate(String appointmentDate) {
        this.appointmentDate = appointmentDate;
    }

    public String getAppointmentStatus() {
        return appointmentStatus;
    }

    public void setAppointmentStatus(String appointmentStatus) {
        this.appointmentStatus = appointmentStatus;
    }

    public String getAppointmentAmount() {
        return appointmentAmount;
    }

    public void setAppointmentAmount(String appointmentAmount) {
        this.appointmentAmount = appointmentAmount;
    }

    public String getAppointmentBookingDate() {
        return appointmentBookingDate;
    }

    public void setAppointmentBookingDate(String appointmentBookingDate) {
        this.appointmentBookingDate = appointmentBookingDate;
    }

    public String getAppointmentReferredBy() {
        return appointmentReferredBy;
    }

    public void setAppointmentReferredBy(String appointmentReferredBy) {
        this.appointmentReferredBy = appointmentReferredBy;
    }

    public String getAppointmentReferredDoctorName() {
        return appointmentReferredDoctorName;
    }

    public void setAppointmentReferredDoctorName(String appointmentReferredDoctorName) {
        this.appointmentReferredDoctorName = appointmentReferredDoctorName;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public Opd getOpd() {
        return opd;
    }

    public void setOpd(Opd opd) {
        this.opd = opd;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }
}
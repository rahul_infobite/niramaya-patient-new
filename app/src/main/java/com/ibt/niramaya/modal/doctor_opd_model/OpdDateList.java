package com.ibt.niramaya.modal.doctor_opd_model;

import java.util.ArrayList;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OpdDateList implements Parcelable {

    @SerializedName("opd_date")
    @Expose
    private String opdDate;
    @SerializedName("doctor_schedule")
    @Expose
    private List<DoctorSchedule> doctorSchedule = new ArrayList<>();
    public final static Parcelable.Creator<OpdDateList> CREATOR = new Creator<OpdDateList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public OpdDateList createFromParcel(Parcel in) {
            return new OpdDateList(in);
        }

        public OpdDateList[] newArray(int size) {
            return (new OpdDateList[size]);
        }

    };

    protected OpdDateList(Parcel in) {
        this.opdDate = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.doctorSchedule, (com.ibt.niramaya.modal.doctor_opd_model.DoctorSchedule.class.getClassLoader()));
    }

    public OpdDateList() {
    }

    public String getOpdDate() {
        return opdDate;
    }

    public void setOpdDate(String opdDate) {
        this.opdDate = opdDate;
    }

    public List<DoctorSchedule> getDoctorSchedule() {
        return doctorSchedule;
    }

    public void setDoctorSchedule(List<DoctorSchedule> doctorSchedule) {
        this.doctorSchedule = doctorSchedule;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(opdDate);
        dest.writeList(doctorSchedule);
    }

    public int describeContents() {
        return 0;
    }

}
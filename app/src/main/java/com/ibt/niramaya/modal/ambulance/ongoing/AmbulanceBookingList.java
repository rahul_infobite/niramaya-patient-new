package com.ibt.niramaya.modal.ambulance.ongoing;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AmbulanceBookingList implements Parcelable {

    @SerializedName("ambulance_booking_id")
    @Expose
    private String ambulanceBookingId;
    @SerializedName("ambulance_id")
    @Expose
    private String ambulanceId;
    @SerializedName("ambulance")
    @Expose
    private Ambulance ambulance;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("driver")
    @Expose
    private Driver driver;
    @SerializedName("hospital_admin_id")
    @Expose
    private String hospitalAdminId;
    @SerializedName("hospital_admin")
    @Expose
    private HospitalAdmin hospitalAdmin;
    @SerializedName("hospital_id")
    @Expose
    private String hospitalId;
    @SerializedName("hospital")
    @Expose
    private Hospital hospital;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("patient_age")
    @Expose
    private String patientAge;
    @SerializedName("patient_number")
    @Expose
    private String patientNumber;
    @SerializedName("patient_address")
    @Expose
    private String patientAddress;
    @SerializedName("ambulance_booking_by")
    @Expose
    private String ambulanceBookingBy;
    @SerializedName("ambulance_booking_type")
    @Expose
    private String ambulanceBookingType;
    @SerializedName("ambulance_booking_latitude")
    @Expose
    private String ambulanceBookingLatitude;
    @SerializedName("ambulance_booking_longitude")
    @Expose
    private String ambulanceBookingLongitude;
    @SerializedName("ambulance_booking_end_latitude")
    @Expose
    private String ambulanceBookingEndLatitude;
    @SerializedName("ambulance_booking_end_longitude")
    @Expose
    private String ambulanceBookingEndLongitude;
    @SerializedName("ambulance_booking_trip_type")
    @Expose
    private String ambulanceBookingTripType;
    @SerializedName("ambulance_booking_status")
    @Expose
    private String ambulanceBookingStatus;
    @SerializedName("ambulance_booking_km")
    @Expose
    private String ambulanceBookingKm;
    @SerializedName("ambulance_booking_payment")
    @Expose
    private String ambulanceBookingPayment;
    @SerializedName("ambulance_booking_discount")
    @Expose
    private String ambulanceBookingDiscount;
    @SerializedName("ambulance_booking_comment")
    @Expose
    private String ambulanceBookingComment;
    @SerializedName("ambulance_booking_created_date")
    @Expose
    private String ambulanceBookingCreatedDate;
    @SerializedName("ambulance_booking_drive_status")
    @Expose
    private String bookingDriveStatus;
    public final static Parcelable.Creator<AmbulanceBookingList> CREATOR = new Creator<AmbulanceBookingList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AmbulanceBookingList createFromParcel(Parcel in) {
            return new AmbulanceBookingList(in);
        }

        public AmbulanceBookingList[] newArray(int size) {
            return (new AmbulanceBookingList[size]);
        }

    };

    protected AmbulanceBookingList(Parcel in) {
        this.ambulanceBookingId = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceId = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulance = ((Ambulance) in.readValue((Ambulance.class.getClassLoader())));
        this.driverId = ((String) in.readValue((String.class.getClassLoader())));
        this.driver = ((Driver) in.readValue((Driver.class.getClassLoader())));
        this.hospitalAdminId = ((String) in.readValue((String.class.getClassLoader())));
        this.hospitalAdmin = ((HospitalAdmin) in.readValue((HospitalAdmin.class.getClassLoader())));
        this.hospitalId = ((String) in.readValue((String.class.getClassLoader())));
        this.hospital = ((Hospital) in.readValue((Hospital.class.getClassLoader())));
        this.patientId = ((String) in.readValue((String.class.getClassLoader())));
        this.patientName = ((String) in.readValue((String.class.getClassLoader())));
        this.patientAge = ((String) in.readValue((String.class.getClassLoader())));
        this.patientNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.patientAddress = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingBy = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingType = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingLatitude = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingLongitude = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingEndLatitude = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingEndLongitude = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingTripType = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingKm = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingPayment = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingDiscount = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingComment = ((String) in.readValue((String.class.getClassLoader())));
        this.ambulanceBookingCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
        this.bookingDriveStatus = ((String) in.readValue((String.class.getClassLoader())));
    }

    public AmbulanceBookingList() {
    }

    public String getAmbulanceBookingId() {
        return ambulanceBookingId;
    }

    public void setAmbulanceBookingId(String ambulanceBookingId) {
        this.ambulanceBookingId = ambulanceBookingId;
    }

    public String getAmbulanceId() {
        return ambulanceId;
    }

    public void setAmbulanceId(String ambulanceId) {
        this.ambulanceId = ambulanceId;
    }

    public Ambulance getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(Ambulance ambulance) {
        this.ambulance = ambulance;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public String getHospitalAdminId() {
        return hospitalAdminId;
    }

    public void setHospitalAdminId(String hospitalAdminId) {
        this.hospitalAdminId = hospitalAdminId;
    }

    public HospitalAdmin getHospitalAdmin() {
        return hospitalAdmin;
    }

    public void setHospitalAdmin(HospitalAdmin hospitalAdmin) {
        this.hospitalAdmin = hospitalAdmin;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientAge() {
        return patientAge;
    }

    public void setPatientAge(String patientAge) {
        this.patientAge = patientAge;
    }

    public String getPatientNumber() {
        return patientNumber;
    }

    public void setPatientNumber(String patientNumber) {
        this.patientNumber = patientNumber;
    }

    public String getPatientAddress() {
        return patientAddress;
    }

    public void setPatientAddress(String patientAddress) {
        this.patientAddress = patientAddress;
    }

    public String getAmbulanceBookingBy() {
        return ambulanceBookingBy;
    }

    public void setAmbulanceBookingBy(String ambulanceBookingBy) {
        this.ambulanceBookingBy = ambulanceBookingBy;
    }

    public String getAmbulanceBookingType() {
        return ambulanceBookingType;
    }

    public void setAmbulanceBookingType(String ambulanceBookingType) {
        this.ambulanceBookingType = ambulanceBookingType;
    }

    public String getAmbulanceBookingLatitude() {
        return ambulanceBookingLatitude;
    }

    public void setAmbulanceBookingLatitude(String ambulanceBookingLatitude) {
        this.ambulanceBookingLatitude = ambulanceBookingLatitude;
    }

    public String getAmbulanceBookingLongitude() {
        return ambulanceBookingLongitude;
    }

    public void setAmbulanceBookingLongitude(String ambulanceBookingLongitude) {
        this.ambulanceBookingLongitude = ambulanceBookingLongitude;
    }

    public String getAmbulanceBookingEndLatitude() {
        return ambulanceBookingEndLatitude;
    }

    public void setAmbulanceBookingEndLatitude(String ambulanceBookingEndLatitude) {
        this.ambulanceBookingEndLatitude = ambulanceBookingEndLatitude;
    }

    public String getAmbulanceBookingEndLongitude() {
        return ambulanceBookingEndLongitude;
    }

    public void setAmbulanceBookingEndLongitude(String ambulanceBookingEndLongitude) {
        this.ambulanceBookingEndLongitude = ambulanceBookingEndLongitude;
    }

    public String getAmbulanceBookingTripType() {
        return ambulanceBookingTripType;
    }

    public void setAmbulanceBookingTripType(String ambulanceBookingTripType) {
        this.ambulanceBookingTripType = ambulanceBookingTripType;
    }

    public String getAmbulanceBookingStatus() {
        return ambulanceBookingStatus;
    }

    public void setAmbulanceBookingStatus(String ambulanceBookingStatus) {
        this.ambulanceBookingStatus = ambulanceBookingStatus;
    }

    public String getAmbulanceBookingKm() {
        return ambulanceBookingKm;
    }

    public void setAmbulanceBookingKm(String ambulanceBookingKm) {
        this.ambulanceBookingKm = ambulanceBookingKm;
    }

    public String getAmbulanceBookingPayment() {
        return ambulanceBookingPayment;
    }

    public void setAmbulanceBookingPayment(String ambulanceBookingPayment) {
        this.ambulanceBookingPayment = ambulanceBookingPayment;
    }

    public String getAmbulanceBookingDiscount() {
        return ambulanceBookingDiscount;
    }

    public void setAmbulanceBookingDiscount(String ambulanceBookingDiscount) {
        this.ambulanceBookingDiscount = ambulanceBookingDiscount;
    }

    public String getAmbulanceBookingComment() {
        return ambulanceBookingComment;
    }

    public void setAmbulanceBookingComment(String ambulanceBookingComment) {
        this.ambulanceBookingComment = ambulanceBookingComment;
    }

    public String getAmbulanceBookingCreatedDate() {
        return ambulanceBookingCreatedDate;
    }

    public void setAmbulanceBookingCreatedDate(String ambulanceBookingCreatedDate) {
        this.ambulanceBookingCreatedDate = ambulanceBookingCreatedDate;
    }

    public String getBookingDriveStatus() {
        return bookingDriveStatus;
    }

    public void setBookingDriveStatus(String bookingDriveStatus) {
        this.bookingDriveStatus = bookingDriveStatus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ambulanceBookingId);
        dest.writeValue(ambulanceId);
        dest.writeValue(ambulance);
        dest.writeValue(driverId);
        dest.writeValue(driver);
        dest.writeValue(hospitalAdminId);
        dest.writeValue(hospitalAdmin);
        dest.writeValue(hospitalId);
        dest.writeValue(hospital);
        dest.writeValue(patientId);
        dest.writeValue(patientName);
        dest.writeValue(patientAge);
        dest.writeValue(patientNumber);
        dest.writeValue(patientAddress);
        dest.writeValue(ambulanceBookingBy);
        dest.writeValue(ambulanceBookingType);
        dest.writeValue(ambulanceBookingLatitude);
        dest.writeValue(ambulanceBookingLongitude);
        dest.writeValue(ambulanceBookingEndLatitude);
        dest.writeValue(ambulanceBookingEndLongitude);
        dest.writeValue(ambulanceBookingTripType);
        dest.writeValue(ambulanceBookingStatus);
        dest.writeValue(ambulanceBookingKm);
        dest.writeValue(ambulanceBookingPayment);
        dest.writeValue(ambulanceBookingDiscount);
        dest.writeValue(ambulanceBookingComment);
        dest.writeValue(ambulanceBookingCreatedDate);
        dest.writeValue(bookingDriveStatus);
    }

    public int describeContents() {
        return 0;
    }

}
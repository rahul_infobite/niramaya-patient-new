package com.ibt.niramaya.modal.patient_ipd_modal;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientIpdMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd")
    @Expose
    private List<Ipd> ipd = null;
    public final static Parcelable.Creator<PatientIpdMainModal> CREATOR = new Creator<PatientIpdMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PatientIpdMainModal createFromParcel(Parcel in) {
            return new PatientIpdMainModal(in);
        }

        public PatientIpdMainModal[] newArray(int size) {
            return (new PatientIpdMainModal[size]);
        }

    }
            ;

    protected PatientIpdMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipd, (com.ibt.niramaya.modal.patient_ipd_modal.Ipd.class.getClassLoader()));
    }

    public PatientIpdMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Ipd> getIpd() {
        return ipd;
    }

    public void setIpd(List<Ipd> ipd) {
        this.ipd = ipd;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(ipd);
    }

    public int describeContents() {
        return 0;
    }

}

package com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_opetation_detail_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OperationSchedule implements Parcelable
{

@SerializedName("operation_schedule_id")
@Expose
private String operationScheduleId;
@SerializedName("surgery_id")
@Expose
private String surgeryId;
@SerializedName("hospital_admin_id")
@Expose
private String hospitalAdminId;
@SerializedName("surgeon")
@Expose
private String surgeon;
@SerializedName("surgeon_id")
@Expose
private String surgeonId;
@SerializedName("assistant")
@Expose
private String assistant;
@SerializedName("ipd_id")
@Expose
private String ipdId;
@SerializedName("bed_id")
@Expose
private String bedId;
@SerializedName("bed_number")
@Expose
private String bedNumber;
@SerializedName("type")
@Expose
private String type;
@SerializedName("equipment")
@Expose
private String equipment;
@SerializedName("infection_case")
@Expose
private String infectionCase;
@SerializedName("surgery_date")
@Expose
private String surgeryDate;
@SerializedName("surgery_time")
@Expose
private String surgeryTime;
@SerializedName("turn_around_time")
@Expose
private String turnAroundTime;
@SerializedName("treatment_given")
@Expose
private String treatmentGiven;
@SerializedName("status")
@Expose
private String status;
@SerializedName("treatment_advice")
@Expose
private String treatmentAdvice;
@SerializedName("diagnosis")
@Expose
private String diagnosis;
@SerializedName("medical_procedure")
@Expose
private String medicalProcedure;
@SerializedName("revices")
@Expose
private String revices;
@SerializedName("remark")
@Expose
private String remark;
@SerializedName("discharged_type")
@Expose
private String dischargedType;
@SerializedName("hospital_admin_status")
@Expose
private String hospitalAdminStatus;
@SerializedName("surgery_status")
@Expose
private String surgeryStatus;
@SerializedName("anesthetic")
@Expose
private String anesthetic;
@SerializedName("anesthesia_type")
@Expose
private String anesthesiaType;
@SerializedName("room_name")
@Expose
private String roomName;
@SerializedName("room_number")
@Expose
private String roomNumber;
@SerializedName("bed_category_id")
@Expose
private String bedCategoryId;
@SerializedName("category_title")
@Expose
private String categoryTitle;
@SerializedName("ipd")
@Expose
private String ipd;
@SerializedName("patient_name")
@Expose
private String patientName;
@SerializedName("created_date")
@Expose
private String createdDate;
public final static Creator<OperationSchedule> CREATOR = new Creator<OperationSchedule>() {


@SuppressWarnings({
"unchecked"
})
public OperationSchedule createFromParcel(Parcel in) {
return new OperationSchedule(in);
}

public OperationSchedule[] newArray(int size) {
return (new OperationSchedule[size]);
}

}
;

protected OperationSchedule(Parcel in) {
this.operationScheduleId = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryId = ((String) in.readValue((String.class.getClassLoader())));
this.hospitalAdminId = ((String) in.readValue((String.class.getClassLoader())));
this.surgeon = ((String) in.readValue((String.class.getClassLoader())));
this.surgeonId = ((String) in.readValue((String.class.getClassLoader())));
this.assistant = ((String) in.readValue((String.class.getClassLoader())));
this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
this.bedId = ((String) in.readValue((String.class.getClassLoader())));
this.bedNumber = ((String) in.readValue((String.class.getClassLoader())));
this.type = ((String) in.readValue((String.class.getClassLoader())));
this.equipment = ((String) in.readValue((String.class.getClassLoader())));
this.infectionCase = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryDate = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryTime = ((String) in.readValue((String.class.getClassLoader())));
this.turnAroundTime = ((String) in.readValue((String.class.getClassLoader())));
this.treatmentGiven = ((String) in.readValue((String.class.getClassLoader())));
this.status = ((String) in.readValue((String.class.getClassLoader())));
this.treatmentAdvice = ((String) in.readValue((String.class.getClassLoader())));
this.diagnosis = ((String) in.readValue((String.class.getClassLoader())));
this.medicalProcedure = ((String) in.readValue((String.class.getClassLoader())));
this.revices = ((String) in.readValue((String.class.getClassLoader())));
this.remark = ((String) in.readValue((String.class.getClassLoader())));
this.dischargedType = ((String) in.readValue((String.class.getClassLoader())));
this.hospitalAdminStatus = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryStatus = ((String) in.readValue((String.class.getClassLoader())));
this.anesthetic = ((String) in.readValue((String.class.getClassLoader())));
this.anesthesiaType = ((String) in.readValue((String.class.getClassLoader())));
this.roomName = ((String) in.readValue((String.class.getClassLoader())));
this.roomNumber = ((String) in.readValue((String.class.getClassLoader())));
this.bedCategoryId = ((String) in.readValue((String.class.getClassLoader())));
this.categoryTitle = ((String) in.readValue((String.class.getClassLoader())));
this.ipd = ((String) in.readValue((String.class.getClassLoader())));
this.patientName = ((String) in.readValue((String.class.getClassLoader())));
this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
}

public OperationSchedule() {
}

public String getOperationScheduleId() {
return operationScheduleId;
}

public void setOperationScheduleId(String operationScheduleId) {
this.operationScheduleId = operationScheduleId;
}

public String getSurgeryId() {
return surgeryId;
}

public void setSurgeryId(String surgeryId) {
this.surgeryId = surgeryId;
}

public String getHospitalAdminId() {
return hospitalAdminId;
}

public void setHospitalAdminId(String hospitalAdminId) {
this.hospitalAdminId = hospitalAdminId;
}

public String getSurgeon() {
return surgeon;
}

public void setSurgeon(String surgeon) {
this.surgeon = surgeon;
}

public String getSurgeonId() {
return surgeonId;
}

public void setSurgeonId(String surgeonId) {
this.surgeonId = surgeonId;
}

public String getAssistant() {
return assistant;
}

public void setAssistant(String assistant) {
this.assistant = assistant;
}

public String getIpdId() {
return ipdId;
}

public void setIpdId(String ipdId) {
this.ipdId = ipdId;
}

public String getBedId() {
return bedId;
}

public void setBedId(String bedId) {
this.bedId = bedId;
}

public String getBedNumber() {
return bedNumber;
}

public void setBedNumber(String bedNumber) {
this.bedNumber = bedNumber;
}

public String getType() {
return type;
}

public void setType(String type) {
this.type = type;
}

public String getEquipment() {
return equipment;
}

public void setEquipment(String equipment) {
this.equipment = equipment;
}

public String getInfectionCase() {
return infectionCase;
}

public void setInfectionCase(String infectionCase) {
this.infectionCase = infectionCase;
}

public String getSurgeryDate() {
return surgeryDate;
}

public void setSurgeryDate(String surgeryDate) {
this.surgeryDate = surgeryDate;
}

public String getSurgeryTime() {
return surgeryTime;
}

public void setSurgeryTime(String surgeryTime) {
this.surgeryTime = surgeryTime;
}

public String getTurnAroundTime() {
return turnAroundTime;
}

public void setTurnAroundTime(String turnAroundTime) {
this.turnAroundTime = turnAroundTime;
}

public String getTreatmentGiven() {
return treatmentGiven;
}

public void setTreatmentGiven(String treatmentGiven) {
this.treatmentGiven = treatmentGiven;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getTreatmentAdvice() {
return treatmentAdvice;
}

public void setTreatmentAdvice(String treatmentAdvice) {
this.treatmentAdvice = treatmentAdvice;
}

public String getDiagnosis() {
return diagnosis;
}

public void setDiagnosis(String diagnosis) {
this.diagnosis = diagnosis;
}

public String getMedicalProcedure() {
return medicalProcedure;
}

public void setMedicalProcedure(String medicalProcedure) {
this.medicalProcedure = medicalProcedure;
}

public String getRevices() {
return revices;
}

public void setRevices(String revices) {
this.revices = revices;
}

public String getRemark() {
return remark;
}

public void setRemark(String remark) {
this.remark = remark;
}

public String getDischargedType() {
return dischargedType;
}

public void setDischargedType(String dischargedType) {
this.dischargedType = dischargedType;
}

public String getHospitalAdminStatus() {
return hospitalAdminStatus;
}

public void setHospitalAdminStatus(String hospitalAdminStatus) {
this.hospitalAdminStatus = hospitalAdminStatus;
}

public String getSurgeryStatus() {
return surgeryStatus;
}

public void setSurgeryStatus(String surgeryStatus) {
this.surgeryStatus = surgeryStatus;
}

public String getAnesthetic() {
return anesthetic;
}

public void setAnesthetic(String anesthetic) {
this.anesthetic = anesthetic;
}

public String getAnesthesiaType() {
return anesthesiaType;
}

public void setAnesthesiaType(String anesthesiaType) {
this.anesthesiaType = anesthesiaType;
}

public String getRoomName() {
return roomName;
}

public void setRoomName(String roomName) {
this.roomName = roomName;
}

public String getRoomNumber() {
return roomNumber;
}

public void setRoomNumber(String roomNumber) {
this.roomNumber = roomNumber;
}

public String getBedCategoryId() {
return bedCategoryId;
}

public void setBedCategoryId(String bedCategoryId) {
this.bedCategoryId = bedCategoryId;
}

public String getCategoryTitle() {
return categoryTitle;
}

public void setCategoryTitle(String categoryTitle) {
this.categoryTitle = categoryTitle;
}

public String getIpd() {
return ipd;
}

public void setIpd(String ipd) {
this.ipd = ipd;
}

public String getPatientName() {
return patientName;
}

public void setPatientName(String patientName) {
this.patientName = patientName;
}

public String getCreatedDate() {
return createdDate;
}

public void setCreatedDate(String createdDate) {
this.createdDate = createdDate;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(operationScheduleId);
dest.writeValue(surgeryId);
dest.writeValue(hospitalAdminId);
dest.writeValue(surgeon);
dest.writeValue(surgeonId);
dest.writeValue(assistant);
dest.writeValue(ipdId);
dest.writeValue(bedId);
dest.writeValue(bedNumber);
dest.writeValue(type);
dest.writeValue(equipment);
dest.writeValue(infectionCase);
dest.writeValue(surgeryDate);
dest.writeValue(surgeryTime);
dest.writeValue(turnAroundTime);
dest.writeValue(treatmentGiven);
dest.writeValue(status);
dest.writeValue(treatmentAdvice);
dest.writeValue(diagnosis);
dest.writeValue(medicalProcedure);
dest.writeValue(revices);
dest.writeValue(remark);
dest.writeValue(dischargedType);
dest.writeValue(hospitalAdminStatus);
dest.writeValue(surgeryStatus);
dest.writeValue(anesthetic);
dest.writeValue(anesthesiaType);
dest.writeValue(roomName);
dest.writeValue(roomNumber);
dest.writeValue(bedCategoryId);
dest.writeValue(categoryTitle);
dest.writeValue(ipd);
dest.writeValue(patientName);
dest.writeValue(createdDate);
}

public int describeContents() {
return 0;
}

}
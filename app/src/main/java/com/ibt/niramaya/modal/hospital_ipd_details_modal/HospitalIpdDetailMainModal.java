package com.ibt.niramaya.modal.hospital_ipd_details_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HospitalIpdDetailMainModal implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd")
    @Expose
    private Ipd ipd;
    public final static Creator<HospitalIpdDetailMainModal> CREATOR = new Creator<HospitalIpdDetailMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HospitalIpdDetailMainModal createFromParcel(Parcel in) {
            return new HospitalIpdDetailMainModal(in);
        }

        public HospitalIpdDetailMainModal[] newArray(int size) {
            return (new HospitalIpdDetailMainModal[size]);
        }

    };

    protected HospitalIpdDetailMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.ipd = ((Ipd) in.readValue((Ipd.class.getClassLoader())));
    }

    public HospitalIpdDetailMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Ipd getIpd() {
        return ipd;
    }

    public void setIpd(Ipd ipd) {
        this.ipd = ipd;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(ipd);
    }

    public int describeContents() {
        return 0;
    }

}
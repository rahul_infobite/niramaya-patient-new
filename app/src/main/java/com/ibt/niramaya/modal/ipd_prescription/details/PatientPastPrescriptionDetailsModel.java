package com.ibt.niramaya.modal.ipd_prescription.details;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientPastPrescriptionDetailsModel implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("preception_slip")
    @Expose
    private PreceptionSlip preceptionSlip;
    public final static Creator<PatientPastPrescriptionDetailsModel> CREATOR = new Creator<PatientPastPrescriptionDetailsModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PatientPastPrescriptionDetailsModel createFromParcel(Parcel in) {
            return new PatientPastPrescriptionDetailsModel(in);
        }

        public PatientPastPrescriptionDetailsModel[] newArray(int size) {
            return (new PatientPastPrescriptionDetailsModel[size]);
        }

    }
            ;

    protected PatientPastPrescriptionDetailsModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.preceptionSlip = ((PreceptionSlip) in.readValue((PreceptionSlip.class.getClassLoader())));
    }

    public PatientPastPrescriptionDetailsModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PreceptionSlip getPreceptionSlip() {
        return preceptionSlip;
    }

    public void setPreceptionSlip(PreceptionSlip preceptionSlip) {
        this.preceptionSlip = preceptionSlip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(preceptionSlip);
    }

    public int describeContents() {
        return 0;
    }

}

package com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_patient_finance_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class PatientFinanceMainModal implements Parcelable
{

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("patientfinance")
    @Expose
    private List<Patientfinance> patientfinance = new ArrayList<Patientfinance>();
    public final static Creator<PatientFinanceMainModal> CREATOR = new Creator<PatientFinanceMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PatientFinanceMainModal createFromParcel(Parcel in) {
            return new PatientFinanceMainModal(in);
        }

        public PatientFinanceMainModal[] newArray(int size) {
            return (new PatientFinanceMainModal[size]);
        }

    }
            ;

    protected PatientFinanceMainModal(Parcel in) {
        this.result = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.patientfinance, (com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_patient_finance_modal.Patientfinance.class.getClassLoader()));
    }

    public PatientFinanceMainModal() {
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Patientfinance> getPatientfinance() {
        return patientfinance;
    }

    public void setPatientfinance(List<Patientfinance> patientfinance) {
        this.patientfinance = patientfinance;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(result);
        dest.writeValue(message);
        dest.writeList(patientfinance);
    }

    public int describeContents() {
        return 0;
    }

}
package com.ibt.niramaya.modal.hospital_about_content;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AboutContentMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("content")
    @Expose
    private List<Content> content = null;
    public final static Parcelable.Creator<AboutContentMainModal> CREATOR = new Creator<AboutContentMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public AboutContentMainModal createFromParcel(Parcel in) {
            return new AboutContentMainModal(in);
        }

        public AboutContentMainModal[] newArray(int size) {
            return (new AboutContentMainModal[size]);
        }

    }
            ;

    protected AboutContentMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.content, (com.ibt.niramaya.modal.hospital_about_content.Content.class.getClassLoader()));
    }

    public AboutContentMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Content> getContent() {
        return content;
    }

    public void setContent(List<Content> content) {
        this.content = content;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(content);
    }

    public int describeContents() {
        return 0;
    }

}

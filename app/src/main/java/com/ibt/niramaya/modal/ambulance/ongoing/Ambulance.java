package com.ibt.niramaya.modal.ambulance.ongoing;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ambulance implements Parcelable {

    @SerializedName("ambulance_id")
    @Expose
    private String ambulanceId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("facility")
    @Expose
    private String facility;
    @SerializedName("registration_number")
    @Expose
    private String registrationNumber;
    @SerializedName("rto_number")
    @Expose
    private String rtoNumber;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    public final static Parcelable.Creator<Ambulance> CREATOR = new Creator<Ambulance>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Ambulance createFromParcel(Parcel in) {
            return new Ambulance(in);
        }

        public Ambulance[] newArray(int size) {
            return (new Ambulance[size]);
        }

    };

    protected Ambulance(Parcel in) {
        this.ambulanceId = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.type = ((String) in.readValue((String.class.getClassLoader())));
        this.facility = ((String) in.readValue((String.class.getClassLoader())));
        this.registrationNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.rtoNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.image = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Ambulance() {
    }

    public String getAmbulanceId() {
        return ambulanceId;
    }

    public void setAmbulanceId(String ambulanceId) {
        this.ambulanceId = ambulanceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }

    public String getRtoNumber() {
        return rtoNumber;
    }

    public void setRtoNumber(String rtoNumber) {
        this.rtoNumber = rtoNumber;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ambulanceId);
        dest.writeValue(name);
        dest.writeValue(type);
        dest.writeValue(facility);
        dest.writeValue(registrationNumber);
        dest.writeValue(rtoNumber);
        dest.writeValue(image);
        dest.writeValue(createdDate);
    }

    public int describeContents() {
        return 0;
    }

}
package com.ibt.niramaya.modal.doctor_refer_slip_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ReferSlip implements Parcelable {

    @SerializedName("refer_slip_id")
    @Expose
    private String referSlipId;
    @SerializedName("hospital_id")
    @Expose
    private String hospitalId;
    @SerializedName("reference")
    @Expose
    private String reference;
    @SerializedName("reference_id")
    @Expose
    private String referenceId;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("patient_name")
    @Expose
    private String patientName;
    @SerializedName("patient_contact")
    @Expose
    private String patientContact;
    @SerializedName("patient_dob")
    @Expose
    private String patientDob;
    @SerializedName("problem_statement")
    @Expose
    private String problemStatement;
    @SerializedName("reason_of_referral")
    @Expose
    private String reasonOfReferral;
    @SerializedName("refered_to_name")
    @Expose
    private String referedToName;
    @SerializedName("refered_to_contact")
    @Expose
    private String referedToContact;
    @SerializedName("refered_to_address")
    @Expose
    private String referedToAddress;
    @SerializedName("refered_to_advisor_name")
    @Expose
    private String referedToAdvisorName;
    @SerializedName("refered_to_email")
    @Expose
    private String referedToEmail;
    @SerializedName("refered_by")
    @Expose
    private String referedBy;
    @SerializedName("referd_by_doctor_name")
    @Expose
    private String referdByDoctorName;
    @SerializedName("refer_created_date")
    @Expose
    private String referCreatedDate;
    @SerializedName("hospital")
    @Expose
    private Hospital hospital;

    @SerializedName("slip_view")
    @Expose
    private String referSlip;
    public final static Creator<ReferSlip> CREATOR = new Creator<ReferSlip>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ReferSlip createFromParcel(Parcel in) {
            return new ReferSlip(in);
        }

        public ReferSlip[] newArray(int size) {
            return (new ReferSlip[size]);
        }

    };

    protected ReferSlip(Parcel in) {
        this.referSlipId = ((String) in.readValue((String.class.getClassLoader())));
        this.hospitalId = ((String) in.readValue((String.class.getClassLoader())));
        this.reference = ((String) in.readValue((String.class.getClassLoader())));
        this.referenceId = ((String) in.readValue((String.class.getClassLoader())));
        this.patientId = ((String) in.readValue((String.class.getClassLoader())));
        this.patientName = ((String) in.readValue((String.class.getClassLoader())));
        this.patientContact = ((String) in.readValue((String.class.getClassLoader())));
        this.patientDob = ((String) in.readValue((String.class.getClassLoader())));
        this.problemStatement = ((String) in.readValue((String.class.getClassLoader())));
        this.reasonOfReferral = ((String) in.readValue((String.class.getClassLoader())));
        this.referedToName = ((String) in.readValue((String.class.getClassLoader())));
        this.referedToContact = ((String) in.readValue((String.class.getClassLoader())));
        this.referedToAddress = ((String) in.readValue((String.class.getClassLoader())));
        this.referedToAdvisorName = ((String) in.readValue((String.class.getClassLoader())));
        this.referedToEmail = ((String) in.readValue((String.class.getClassLoader())));
        this.referedBy = ((String) in.readValue((String.class.getClassLoader())));
        this.referdByDoctorName = ((String) in.readValue((String.class.getClassLoader())));
        this.referCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
        this.referSlip = ((String) in.readValue((String.class.getClassLoader())));
        this.hospital = ((Hospital) in.readValue((Hospital.class.getClassLoader())));
    }

    public ReferSlip() {
    }

    public String getReferSlipId() {
        return referSlipId;
    }

    public void setReferSlipId(String referSlipId) {
        this.referSlipId = referSlipId;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getReferenceId() {
        return referenceId;
    }

    public void setReferenceId(String referenceId) {
        this.referenceId = referenceId;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getPatientContact() {
        return patientContact;
    }

    public void setPatientContact(String patientContact) {
        this.patientContact = patientContact;
    }

    public String getPatientDob() {
        return patientDob;
    }

    public void setPatientDob(String patientDob) {
        this.patientDob = patientDob;
    }

    public String getProblemStatement() {
        return problemStatement;
    }

    public void setProblemStatement(String problemStatement) {
        this.problemStatement = problemStatement;
    }

    public String getReasonOfReferral() {
        return reasonOfReferral;
    }

    public void setReasonOfReferral(String reasonOfReferral) {
        this.reasonOfReferral = reasonOfReferral;
    }

    public String getReferedToName() {
        return referedToName;
    }

    public void setReferedToName(String referedToName) {
        this.referedToName = referedToName;
    }

    public String getReferedToContact() {
        return referedToContact;
    }

    public void setReferedToContact(String referedToContact) {
        this.referedToContact = referedToContact;
    }

    public String getReferedToAddress() {
        return referedToAddress;
    }

    public void setReferedToAddress(String referedToAddress) {
        this.referedToAddress = referedToAddress;
    }

    public String getReferedToAdvisorName() {
        return referedToAdvisorName;
    }

    public void setReferedToAdvisorName(String referedToAdvisorName) {
        this.referedToAdvisorName = referedToAdvisorName;
    }

    public String getReferedToEmail() {
        return referedToEmail;
    }

    public void setReferedToEmail(String referedToEmail) {
        this.referedToEmail = referedToEmail;
    }

    public String getReferedBy() {
        return referedBy;
    }

    public void setReferedBy(String referedBy) {
        this.referedBy = referedBy;
    }

    public String getReferdByDoctorName() {
        return referdByDoctorName;
    }

    public void setReferdByDoctorName(String referdByDoctorName) {
        this.referdByDoctorName = referdByDoctorName;
    }

    public String getReferCreatedDate() {
        return referCreatedDate;
    }

    public void setReferCreatedDate(String referCreatedDate) {
        this.referCreatedDate = referCreatedDate;
    }

    public Hospital getHospital() {
        return hospital;
    }

    public void setHospital(Hospital hospital) {
        this.hospital = hospital;
    }

    public String getReferSlip() {
        return referSlip;
    }

    public void setReferSlip(String referSlip) {
        this.referSlip = referSlip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(referSlipId);
        dest.writeValue(hospitalId);
        dest.writeValue(reference);
        dest.writeValue(referenceId);
        dest.writeValue(patientId);
        dest.writeValue(patientName);
        dest.writeValue(patientContact);
        dest.writeValue(patientDob);
        dest.writeValue(problemStatement);
        dest.writeValue(reasonOfReferral);
        dest.writeValue(referedToName);
        dest.writeValue(referedToContact);
        dest.writeValue(referedToAddress);
        dest.writeValue(referedToAdvisorName);
        dest.writeValue(referedToEmail);
        dest.writeValue(referedBy);
        dest.writeValue(referdByDoctorName);
        dest.writeValue(referCreatedDate);
        dest.writeValue(hospital);
        dest.writeValue(referSlip);
    }

    public int describeContents() {
        return 0;
    }

}

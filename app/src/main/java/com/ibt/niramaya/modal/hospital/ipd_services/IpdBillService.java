package com.ibt.niramaya.modal.hospital.ipd_services;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdBillService implements Parcelable
{

@SerializedName("ipd_bill_service_id")
@Expose
private String ipdBillServiceId;
@SerializedName("ipd_service_id")
@Expose
private String ipdServiceId;
@SerializedName("title")
@Expose
private String title;
@SerializedName("unit")
@Expose
private String unit;
@SerializedName("amount")
@Expose
private String amount;
@SerializedName("status")
@Expose
private String status;
@SerializedName("discount")
@Expose
private String discount;
@SerializedName("created_date")
@Expose
private String createdDate;
@SerializedName("ipd_id")
@Expose
private String ipdId;
@SerializedName("ipd")
@Expose
private String ipd;
@SerializedName("patient_name")
@Expose
private String patientName;
@SerializedName("patient_gender")
@Expose
private String patientGender;
@SerializedName("ipd_patient_dob")
@Expose
private String ipdPatientDob;
@SerializedName("ipd_patient_adhar_number")
@Expose
private String ipdPatientAdharNumber;
public final static Creator<IpdBillService> CREATOR = new Creator<IpdBillService>() {


@SuppressWarnings({
"unchecked"
})
public IpdBillService createFromParcel(Parcel in) {
return new IpdBillService(in);
}

public IpdBillService[] newArray(int size) {
return (new IpdBillService[size]);
}

}
;

protected IpdBillService(Parcel in) {
this.ipdBillServiceId = ((String) in.readValue((String.class.getClassLoader())));
this.ipdServiceId = ((String) in.readValue((String.class.getClassLoader())));
this.title = ((String) in.readValue((String.class.getClassLoader())));
this.unit = ((String) in.readValue((String.class.getClassLoader())));
this.amount = ((String) in.readValue((String.class.getClassLoader())));
this.status = ((String) in.readValue((String.class.getClassLoader())));
this.discount = ((String) in.readValue((String.class.getClassLoader())));
this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
this.ipd = ((String) in.readValue((String.class.getClassLoader())));
this.patientName = ((String) in.readValue((String.class.getClassLoader())));
this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPatientDob = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPatientAdharNumber = ((String) in.readValue((String.class.getClassLoader())));
}

public IpdBillService() {
}

public String getIpdBillServiceId() {
return ipdBillServiceId;
}

public void setIpdBillServiceId(String ipdBillServiceId) {
this.ipdBillServiceId = ipdBillServiceId;
}

public String getIpdServiceId() {
return ipdServiceId;
}

public void setIpdServiceId(String ipdServiceId) {
this.ipdServiceId = ipdServiceId;
}

public String getTitle() {
return title;
}

public void setTitle(String title) {
this.title = title;
}

public String getUnit() {
return unit;
}

public void setUnit(String unit) {
this.unit = unit;
}

public String getAmount() {
return amount;
}

public void setAmount(String amount) {
this.amount = amount;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getDiscount() {
return discount;
}

public void setDiscount(String discount) {
this.discount = discount;
}

public String getCreatedDate() {
return createdDate;
}

public void setCreatedDate(String createdDate) {
this.createdDate = createdDate;
}

public String getIpdId() {
return ipdId;
}

public void setIpdId(String ipdId) {
this.ipdId = ipdId;
}

public String getIpd() {
return ipd;
}

public void setIpd(String ipd) {
this.ipd = ipd;
}

public String getPatientName() {
return patientName;
}

public void setPatientName(String patientName) {
this.patientName = patientName;
}

public String getPatientGender() {
return patientGender;
}

public void setPatientGender(String patientGender) {
this.patientGender = patientGender;
}

public String getIpdPatientDob() {
return ipdPatientDob;
}

public void setIpdPatientDob(String ipdPatientDob) {
this.ipdPatientDob = ipdPatientDob;
}

public String getIpdPatientAdharNumber() {
return ipdPatientAdharNumber;
}

public void setIpdPatientAdharNumber(String ipdPatientAdharNumber) {
this.ipdPatientAdharNumber = ipdPatientAdharNumber;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(ipdBillServiceId);
dest.writeValue(ipdServiceId);
dest.writeValue(title);
dest.writeValue(unit);
dest.writeValue(amount);
dest.writeValue(status);
dest.writeValue(discount);
dest.writeValue(createdDate);
dest.writeValue(ipdId);
dest.writeValue(ipd);
dest.writeValue(patientName);
dest.writeValue(patientGender);
dest.writeValue(ipdPatientDob);
dest.writeValue(ipdPatientAdharNumber);
}

public int describeContents() {
return 0;
}

}
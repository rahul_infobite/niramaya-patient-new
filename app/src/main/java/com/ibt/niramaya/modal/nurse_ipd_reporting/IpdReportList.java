package com.ibt.niramaya.modal.nurse_ipd_reporting;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdReportList implements Parcelable {

    @SerializedName("ipd_nurse_reporting_slip_id")
    @Expose
    private String ipdNurseReportingSlipId;
    @SerializedName("ipd_id")
    @Expose
    private String ipdId;
    @SerializedName("staff_allotment_id")
    @Expose
    private String staffAllotmentId;
    @SerializedName("cheif_complaints")
    @Expose
    private String cheifComplaints;
    @SerializedName("history")
    @Expose
    private String history;
    @SerializedName("last_treatment")
    @Expose
    private String lastTreatment;
    @SerializedName("releavent_drug")
    @Expose
    private String releaventDrug;
    @SerializedName("allergy")
    @Expose
    private String allergy;
    @SerializedName("vitals")
    @Expose
    private String vitals;
    @SerializedName("bp")
    @Expose
    private String bp;
    @SerializedName("heart_rate_per_min")
    @Expose
    private String heartRatePerMin;
    @SerializedName("resp_rate_min")
    @Expose
    private String respRateMin;
    @SerializedName("temp")
    @Expose
    private String temp;
    @SerializedName("pain_score")
    @Expose
    private String painScore;
    @SerializedName("spo2")
    @Expose
    private String spo2;
    @SerializedName("gcs")
    @Expose
    private String gcs;
    @SerializedName("staff_id")
    @Expose
    private String staffId;
    @SerializedName("staff_name")
    @Expose
    private String staffName;
    @SerializedName("staff_type")
    @Expose
    private String staffType;
    @SerializedName("follow_up_advice")
    @Expose
    private String followUpAdvice;
    @SerializedName("nausea")
    @Expose
    private String nausea;
    @SerializedName("fall_risk")
    @Expose
    private String fallRisk;
    @SerializedName("is_oxygen")
    @Expose
    private String isOxygen;
    @SerializedName("note")
    @Expose
    private String note;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("red_flag")
    @Expose
    private String redFlag;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    @SerializedName("slip_link")
    @Expose
    private String ipdSlip;
    public final static Creator<IpdReportList> CREATOR = new Creator<IpdReportList>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdReportList createFromParcel(Parcel in) {
            return new IpdReportList(in);
        }

        public IpdReportList[] newArray(int size) {
            return (new IpdReportList[size]);
        }

    };

    protected IpdReportList(Parcel in) {
        this.ipdNurseReportingSlipId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
        this.staffAllotmentId = ((String) in.readValue((String.class.getClassLoader())));
        this.cheifComplaints = ((String) in.readValue((String.class.getClassLoader())));
        this.history = ((String) in.readValue((String.class.getClassLoader())));
        this.lastTreatment = ((String) in.readValue((String.class.getClassLoader())));
        this.releaventDrug = ((String) in.readValue((String.class.getClassLoader())));
        this.allergy = ((String) in.readValue((String.class.getClassLoader())));
        this.vitals = ((String) in.readValue((String.class.getClassLoader())));
        this.bp = ((String) in.readValue((String.class.getClassLoader())));
        this.heartRatePerMin = ((String) in.readValue((String.class.getClassLoader())));
        this.respRateMin = ((String) in.readValue((String.class.getClassLoader())));
        this.temp = ((String) in.readValue((String.class.getClassLoader())));
        this.painScore = ((String) in.readValue((String.class.getClassLoader())));
        this.spo2 = ((String) in.readValue((String.class.getClassLoader())));
        this.gcs = ((String) in.readValue((String.class.getClassLoader())));
        this.staffId = ((String) in.readValue((String.class.getClassLoader())));
        this.staffName = ((String) in.readValue((String.class.getClassLoader())));
        this.staffType = ((String) in.readValue((String.class.getClassLoader())));
        this.followUpAdvice = ((String) in.readValue((String.class.getClassLoader())));
        this.nausea = ((String) in.readValue((String.class.getClassLoader())));
        this.fallRisk = ((String) in.readValue((String.class.getClassLoader())));
        this.isOxygen = ((String) in.readValue((String.class.getClassLoader())));
        this.note = ((String) in.readValue((String.class.getClassLoader())));
        this.comment = ((String) in.readValue((String.class.getClassLoader())));
        this.redFlag = ((String) in.readValue((String.class.getClassLoader())));
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdSlip = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IpdReportList() {
    }

    public String getIpdNurseReportingSlipId() {
        return ipdNurseReportingSlipId;
    }

    public void setIpdNurseReportingSlipId(String ipdNurseReportingSlipId) {
        this.ipdNurseReportingSlipId = ipdNurseReportingSlipId;
    }

    public String getIpdId() {
        return ipdId;
    }

    public void setIpdId(String ipdId) {
        this.ipdId = ipdId;
    }

    public String getStaffAllotmentId() {
        return staffAllotmentId;
    }

    public void setStaffAllotmentId(String staffAllotmentId) {
        this.staffAllotmentId = staffAllotmentId;
    }

    public String getCheifComplaints() {
        return cheifComplaints;
    }

    public void setCheifComplaints(String cheifComplaints) {
        this.cheifComplaints = cheifComplaints;
    }

    public String getHistory() {
        return history;
    }

    public void setHistory(String history) {
        this.history = history;
    }

    public String getLastTreatment() {
        return lastTreatment;
    }

    public void setLastTreatment(String lastTreatment) {
        this.lastTreatment = lastTreatment;
    }

    public String getReleaventDrug() {
        return releaventDrug;
    }

    public void setReleaventDrug(String releaventDrug) {
        this.releaventDrug = releaventDrug;
    }

    public String getAllergy() {
        return allergy;
    }

    public void setAllergy(String allergy) {
        this.allergy = allergy;
    }

    public String getVitals() {
        return vitals;
    }

    public void setVitals(String vitals) {
        this.vitals = vitals;
    }

    public String getBp() {
        return bp;
    }

    public void setBp(String bp) {
        this.bp = bp;
    }

    public String getHeartRatePerMin() {
        return heartRatePerMin;
    }

    public void setHeartRatePerMin(String heartRatePerMin) {
        this.heartRatePerMin = heartRatePerMin;
    }

    public String getRespRateMin() {
        return respRateMin;
    }

    public void setRespRateMin(String respRateMin) {
        this.respRateMin = respRateMin;
    }

    public String getTemp() {
        return temp;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getPainScore() {
        return painScore;
    }

    public void setPainScore(String painScore) {
        this.painScore = painScore;
    }

    public String getSpo2() {
        return spo2;
    }

    public void setSpo2(String spo2) {
        this.spo2 = spo2;
    }

    public String getGcs() {
        return gcs;
    }

    public void setGcs(String gcs) {
        this.gcs = gcs;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffType() {
        return staffType;
    }

    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }

    public String getFollowUpAdvice() {
        return followUpAdvice;
    }

    public void setFollowUpAdvice(String followUpAdvice) {
        this.followUpAdvice = followUpAdvice;
    }

    public String getNausea() {
        return nausea;
    }

    public void setNausea(String nausea) {
        this.nausea = nausea;
    }

    public String getFallRisk() {
        return fallRisk;
    }

    public void setFallRisk(String fallRisk) {
        this.fallRisk = fallRisk;
    }

    public String getIsOxygen() {
        return isOxygen;
    }

    public void setIsOxygen(String isOxygen) {
        this.isOxygen = isOxygen;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getRedFlag() {
        return redFlag;
    }

    public void setRedFlag(String redFlag) {
        this.redFlag = redFlag;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }
    public String getIpdSlip() {
        return ipdSlip;
    }

    public void setIpdSlip(String ipdSlip) {
        this.ipdSlip = ipdSlip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ipdNurseReportingSlipId);
        dest.writeValue(ipdId);
        dest.writeValue(staffAllotmentId);
        dest.writeValue(cheifComplaints);
        dest.writeValue(history);
        dest.writeValue(lastTreatment);
        dest.writeValue(releaventDrug);
        dest.writeValue(allergy);
        dest.writeValue(vitals);
        dest.writeValue(bp);
        dest.writeValue(heartRatePerMin);
        dest.writeValue(respRateMin);
        dest.writeValue(temp);
        dest.writeValue(painScore);
        dest.writeValue(spo2);
        dest.writeValue(gcs);
        dest.writeValue(staffId);
        dest.writeValue(staffName);
        dest.writeValue(staffType);
        dest.writeValue(followUpAdvice);
        dest.writeValue(nausea);
        dest.writeValue(fallRisk);
        dest.writeValue(isOxygen);
        dest.writeValue(note);
        dest.writeValue(comment);
        dest.writeValue(redFlag);
        dest.writeValue(status);
        dest.writeValue(createdDate);
        dest.writeValue(ipdSlip);
    }


    public int describeContents() {
        return 0;
    }

}
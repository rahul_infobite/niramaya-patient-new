package com.ibt.niramaya.modal.patient_ipd_modal;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Ipd implements Parcelable
{

@SerializedName("ipd_id")
@Expose
private String ipdId;
@SerializedName("ipd")
@Expose
private String ipd;
@SerializedName("patient_name")
@Expose
private String patientName;
@SerializedName("patient_id")
@Expose
private String patientId;
@SerializedName("patient_gender")
@Expose
private String patientGender;
@SerializedName("patient_dob")
@Expose
private String patientDob;
@SerializedName("patient_contact")
@Expose
private String patientContact;
@SerializedName("patient_profile")
@Expose
private String patientProfile;
@SerializedName("doctor_id")
@Expose
private String doctorId;
@SerializedName("doctor_name")
@Expose
private String doctorName;
@SerializedName("discharge_type")
@Expose
private String dischargeType;
@SerializedName("discharge_doctor_status")
@Expose
private String dischargeDoctorStatus;
@SerializedName("discharge_hospital_status")
@Expose
private String dischargeHospitalStatus;
@SerializedName("ipd_created_date")
@Expose
private String ipdCreatedDate;
@SerializedName("hospital")
@Expose
private List<Hospital> hospital = null;
public final static Parcelable.Creator<Ipd> CREATOR = new Creator<Ipd>() {


@SuppressWarnings({
"unchecked"
})
public Ipd createFromParcel(Parcel in) {
return new Ipd(in);
}

public Ipd[] newArray(int size) {
return (new Ipd[size]);
}

}
;

protected Ipd(Parcel in) {
this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
this.ipd = ((String) in.readValue((String.class.getClassLoader())));
this.patientName = ((String) in.readValue((String.class.getClassLoader())));
this.patientId = ((String) in.readValue((String.class.getClassLoader())));
this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
this.patientDob = ((String) in.readValue((String.class.getClassLoader())));
this.patientContact = ((String) in.readValue((String.class.getClassLoader())));
this.patientProfile = ((String) in.readValue((String.class.getClassLoader())));
this.doctorId = ((String) in.readValue((String.class.getClassLoader())));
this.doctorName = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeType = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeDoctorStatus = ((String) in.readValue((String.class.getClassLoader())));
this.dischargeHospitalStatus = ((String) in.readValue((String.class.getClassLoader())));
this.ipdCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
in.readList(this.hospital, (com.ibt.niramaya.modal.patient_ipd_modal.Hospital.class.getClassLoader()));
}

public Ipd() {
}

public String getIpdId() {
return ipdId;
}

public void setIpdId(String ipdId) {
this.ipdId = ipdId;
}

public String getIpd() {
return ipd;
}

public void setIpd(String ipd) {
this.ipd = ipd;
}

public String getPatientName() {
return patientName;
}

public void setPatientName(String patientName) {
this.patientName = patientName;
}

public String getPatientId() {
return patientId;
}

public void setPatientId(String patientId) {
this.patientId = patientId;
}

public String getPatientGender() {
return patientGender;
}

public void setPatientGender(String patientGender) {
this.patientGender = patientGender;
}

public String getPatientDob() {
return patientDob;
}

public void setPatientDob(String patientDob) {
this.patientDob = patientDob;
}

public String getPatientContact() {
return patientContact;
}

public void setPatientContact(String patientContact) {
this.patientContact = patientContact;
}

public String getPatientProfile() {
return patientProfile;
}

public void setPatientProfile(String patientProfile) {
this.patientProfile = patientProfile;
}

public String getDoctorId() {
return doctorId;
}

public void setDoctorId(String doctorId) {
this.doctorId = doctorId;
}

public String getDoctorName() {
return doctorName;
}

public void setDoctorName(String doctorName) {
this.doctorName = doctorName;
}

public String getDischargeType() {
return dischargeType;
}

public void setDischargeType(String dischargeType) {
this.dischargeType = dischargeType;
}

public String getDischargeDoctorStatus() {
return dischargeDoctorStatus;
}

public void setDischargeDoctorStatus(String dischargeDoctorStatus) {
this.dischargeDoctorStatus = dischargeDoctorStatus;
}

public String getDischargeHospitalStatus() {
return dischargeHospitalStatus;
}

public void setDischargeHospitalStatus(String dischargeHospitalStatus) {
this.dischargeHospitalStatus = dischargeHospitalStatus;
}

public String getIpdCreatedDate() {
return ipdCreatedDate;
}

public void setIpdCreatedDate(String ipdCreatedDate) {
this.ipdCreatedDate = ipdCreatedDate;
}

public List<Hospital> getHospital() {
return hospital;
}

public void setHospital(List<Hospital> hospital) {
this.hospital = hospital;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(ipdId);
dest.writeValue(ipd);
dest.writeValue(patientName);
dest.writeValue(patientId);
dest.writeValue(patientGender);
dest.writeValue(patientDob);
dest.writeValue(patientContact);
dest.writeValue(patientProfile);
dest.writeValue(doctorId);
dest.writeValue(doctorName);
dest.writeValue(dischargeType);
dest.writeValue(dischargeDoctorStatus);
dest.writeValue(dischargeHospitalStatus);
dest.writeValue(ipdCreatedDate);
dest.writeList(hospital);
}

public int describeContents() {
return 0;
}

}


package com.ibt.niramaya.modal.hospital_about_content;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Faq implements Parcelable
{

@SerializedName("faq_id")
@Expose
private String faqId;
@SerializedName("faq_question")
@Expose
private String faqQuestion;
@SerializedName("faq_answer")
@Expose
private String faqAnswer;
public final static Parcelable.Creator<Faq> CREATOR = new Creator<Faq>() {


@SuppressWarnings({
"unchecked"
})
public Faq createFromParcel(Parcel in) {
return new Faq(in);
}

public Faq[] newArray(int size) {
return (new Faq[size]);
}

}
;

protected Faq(Parcel in) {
this.faqId = ((String) in.readValue((String.class.getClassLoader())));
this.faqQuestion = ((String) in.readValue((String.class.getClassLoader())));
this.faqAnswer = ((String) in.readValue((String.class.getClassLoader())));
}

public Faq() {
}

public String getFaqId() {
return faqId;
}

public void setFaqId(String faqId) {
this.faqId = faqId;
}

public String getFaqQuestion() {
return faqQuestion;
}

public void setFaqQuestion(String faqQuestion) {
this.faqQuestion = faqQuestion;
}

public String getFaqAnswer() {
return faqAnswer;
}

public void setFaqAnswer(String faqAnswer) {
this.faqAnswer = faqAnswer;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(faqId);
dest.writeValue(faqQuestion);
dest.writeValue(faqAnswer);
}

public int describeContents() {
return 0;
}

}

package com.ibt.niramaya.modal.appoint_list_main_modal;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Opd {

@SerializedName("opd_schedule_id")
@Expose
private String opdScheduleId;
@SerializedName("opd_title")
@Expose
private String opdTitle;
@SerializedName("opd_start_time")
@Expose
private String opdStartTime;
@SerializedName("opd_end_time")
@Expose
private String opdEndTime;

public String getOpdScheduleId() {
return opdScheduleId;
}

public void setOpdScheduleId(String opdScheduleId) {
this.opdScheduleId = opdScheduleId;
}

public String getOpdTitle() {
return opdTitle;
}

public void setOpdTitle(String opdTitle) {
this.opdTitle = opdTitle;
}

public String getOpdStartTime() {
return opdStartTime;
}

public void setOpdStartTime(String opdStartTime) {
this.opdStartTime = opdStartTime;
}

public String getOpdEndTime() {
return opdEndTime;
}

public void setOpdEndTime(String opdEndTime) {
this.opdEndTime = opdEndTime;
}

}
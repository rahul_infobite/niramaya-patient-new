package com.ibt.niramaya.modal.ipd_test_report;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class IpdReportsModal implements Serializable, Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("test_list")
    @Expose
    private List<TestList> testList = null;
    public final static Creator<IpdReportsModal> CREATOR = new Creator<IpdReportsModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdReportsModal createFromParcel(Parcel in) {
            return new IpdReportsModal(in);
        }

        public IpdReportsModal[] newArray(int size) {
            return (new IpdReportsModal[size]);
        }

    }
            ;
    private final static long serialVersionUID = 1544517652468531940L;

    protected IpdReportsModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.testList, (com.ibt.niramaya.modal.ipd_test_report.TestList.class.getClassLoader()));
    }

    public IpdReportsModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TestList> getTestList() {
        return testList;
    }

    public void setTestList(List<TestList> testList) {
        this.testList = testList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(testList);
    }

    public int describeContents() {
        return 0;
    }

}

package com.ibt.niramaya.modal.hospital.select_ipd_services_at_patient_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SelectIpdServiceAtPatientModal implements Parcelable
{

    @SerializedName("result")
    @Expose
    private Boolean result;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd_service")
    @Expose
    private List<IpdService> ipdService = new ArrayList<IpdService>();
    public final static Creator<SelectIpdServiceAtPatientModal> CREATOR = new Creator<SelectIpdServiceAtPatientModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SelectIpdServiceAtPatientModal createFromParcel(Parcel in) {
            return new SelectIpdServiceAtPatientModal(in);
        }

        public SelectIpdServiceAtPatientModal[] newArray(int size) {
            return (new SelectIpdServiceAtPatientModal[size]);
        }

    }
            ;

    protected SelectIpdServiceAtPatientModal(Parcel in) {
        this.result = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipdService, (com.ibt.niramaya.modal.hospital.select_ipd_services_at_patient_modal.IpdService.class.getClassLoader()));
    }

    public SelectIpdServiceAtPatientModal() {
    }

    public Boolean getResult() {
        return result;
    }

    public void setResult(Boolean result) {
        this.result = result;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IpdService> getIpdService() {
        return ipdService;
    }

    public void setIpdService(List<IpdService> ipdService) {
        this.ipdService = ipdService;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(result);
        dest.writeValue(message);
        dest.writeList(ipdService);
    }

    public int describeContents() {
        return 0;
    }

}
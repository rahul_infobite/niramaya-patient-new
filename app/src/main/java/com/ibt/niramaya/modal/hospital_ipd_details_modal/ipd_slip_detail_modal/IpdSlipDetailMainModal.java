package com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdSlipDetailMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd_Slip")
    @Expose
    private IpdSlip ipdSlip;
    public final static Creator<IpdSlipDetailMainModal> CREATOR = new Creator<IpdSlipDetailMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdSlipDetailMainModal createFromParcel(Parcel in) {
            return new IpdSlipDetailMainModal(in);
        }

        public IpdSlipDetailMainModal[] newArray(int size) {
            return (new IpdSlipDetailMainModal[size]);
        }

    }
            ;

    protected IpdSlipDetailMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdSlip = ((IpdSlip) in.readValue((IpdSlip.class.getClassLoader())));
    }

    public IpdSlipDetailMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public IpdSlip getIpdSlip() {
        return ipdSlip;
    }

    public void setIpdSlip(IpdSlip ipdSlip) {
        this.ipdSlip = ipdSlip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(ipdSlip);
    }

    public int describeContents() {
        return 0;
    }

}
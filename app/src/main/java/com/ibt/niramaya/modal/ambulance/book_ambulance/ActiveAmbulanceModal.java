package com.ibt.niramaya.modal.ambulance.book_ambulance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ActiveAmbulanceModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ambulance")
    @Expose
    private List<Ambulance> ambulance = null;
    public final static Creator<ActiveAmbulanceModal> CREATOR = new Creator<ActiveAmbulanceModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public ActiveAmbulanceModal createFromParcel(Parcel in) {
            return new ActiveAmbulanceModal(in);
        }

        public ActiveAmbulanceModal[] newArray(int size) {
            return (new ActiveAmbulanceModal[size]);
        }

    }
            ;

    protected ActiveAmbulanceModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ambulance, (com.ibt.niramaya.modal.ambulance.book_ambulance.Ambulance.class.getClassLoader()));
    }

    public ActiveAmbulanceModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Ambulance> getAmbulance() {
        return ambulance;
    }

    public void setAmbulance(List<Ambulance> ambulance) {
        this.ambulance = ambulance;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(ambulance);
    }

    public int describeContents() {
        return 0;
    }

}
package com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pharmacy_detail_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdPharmacyDetailMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("bill_data")
    @Expose
    private BillData billData;
    public final static Creator<IpdPharmacyDetailMainModal> CREATOR = new Creator<IpdPharmacyDetailMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdPharmacyDetailMainModal createFromParcel(Parcel in) {
            return new IpdPharmacyDetailMainModal(in);
        }

        public IpdPharmacyDetailMainModal[] newArray(int size) {
            return (new IpdPharmacyDetailMainModal[size]);
        }

    }
            ;

    protected IpdPharmacyDetailMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.billData = ((BillData) in.readValue((BillData.class.getClassLoader())));
    }

    public IpdPharmacyDetailMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BillData getBillData() {
        return billData;
    }

    public void setBillData(BillData billData) {
        this.billData = billData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(billData);
    }

    public int describeContents() {
        return 0;
    }

}
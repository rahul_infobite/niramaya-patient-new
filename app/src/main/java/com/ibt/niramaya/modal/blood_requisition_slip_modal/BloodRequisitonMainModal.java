package com.ibt.niramaya.modal.blood_requisition_slip_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class BloodRequisitonMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("blood_requisition")
    @Expose
    private List<BloodRequisition> bloodRequisition = new ArrayList<>();
    public final static Creator<BloodRequisitonMainModal> CREATOR = new Creator<BloodRequisitonMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public BloodRequisitonMainModal createFromParcel(Parcel in) {
            return new BloodRequisitonMainModal(in);
        }

        public BloodRequisitonMainModal[] newArray(int size) {
            return (new BloodRequisitonMainModal[size]);
        }

    }
            ;

    protected BloodRequisitonMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.bloodRequisition, (com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition.class.getClassLoader()));
    }

    public BloodRequisitonMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<BloodRequisition> getBloodRequisition() {
        return bloodRequisition;
    }

    public void setBloodRequisition(List<BloodRequisition> bloodRequisition) {
        this.bloodRequisition = bloodRequisition;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(bloodRequisition);
    }

    public int describeContents() {
        return 0;
    }

}

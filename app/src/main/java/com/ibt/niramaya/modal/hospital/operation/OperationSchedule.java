package com.ibt.niramaya.modal.hospital.operation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OperationSchedule implements Parcelable
{

@SerializedName("operation_schedule_id")
@Expose
private String operationScheduleId;
@SerializedName("surgery_id")
@Expose
private String surgeryId;
@SerializedName("surgeon")
@Expose
private String surgeon;
@SerializedName("surgeon_id")
@Expose
private String surgeonId;
@SerializedName("assistant")
@Expose
private String assistant;
@SerializedName("bed_id")
@Expose
private String bedId;
@SerializedName("bed_number")
@Expose
private String bedNumber;
@SerializedName("type")
@Expose
private String type;
@SerializedName("surgery_date")
@Expose
private String surgeryDate;
@SerializedName("surgery_time")
@Expose
private String surgeryTime;
@SerializedName("turn_around_time")
@Expose
private String turnAroundTime;
@SerializedName("status")
@Expose
private String status;
@SerializedName("discharged_type")
@Expose
private String dischargedType;
@SerializedName("surgery_status")
@Expose
private String surgeryStatus;
@SerializedName("room_name")
@Expose
private String roomName;
@SerializedName("room_number")
@Expose
private String roomNumber;
@SerializedName("bed_category_id")
@Expose
private String bedCategoryId;
@SerializedName("category_title")
@Expose
private String categoryTitle;
@SerializedName("ipd_id")
@Expose
private String ipdId;
@SerializedName("ipd")
@Expose
private String ipd;
@SerializedName("patient_name")
@Expose
private String patientName;
@SerializedName("created_date")
@Expose
private String createdDate;
public final static Creator<OperationSchedule> CREATOR = new Creator<OperationSchedule>() {


@SuppressWarnings({
"unchecked"
})
public OperationSchedule createFromParcel(Parcel in) {
return new OperationSchedule(in);
}

public OperationSchedule[] newArray(int size) {
return (new OperationSchedule[size]);
}

}
;

protected OperationSchedule(Parcel in) {
this.operationScheduleId = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryId = ((String) in.readValue((String.class.getClassLoader())));
this.surgeon = ((String) in.readValue((String.class.getClassLoader())));
this.surgeonId = ((String) in.readValue((String.class.getClassLoader())));
this.assistant = ((String) in.readValue((String.class.getClassLoader())));
this.bedId = ((String) in.readValue((String.class.getClassLoader())));
this.bedNumber = ((String) in.readValue((String.class.getClassLoader())));
this.type = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryDate = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryTime = ((String) in.readValue((String.class.getClassLoader())));
this.turnAroundTime = ((String) in.readValue((String.class.getClassLoader())));
this.status = ((String) in.readValue((String.class.getClassLoader())));
this.dischargedType = ((String) in.readValue((String.class.getClassLoader())));
this.surgeryStatus = ((String) in.readValue((String.class.getClassLoader())));
this.roomName = ((String) in.readValue((String.class.getClassLoader())));
this.roomNumber = ((String) in.readValue((String.class.getClassLoader())));
this.bedCategoryId = ((String) in.readValue((String.class.getClassLoader())));
this.categoryTitle = ((String) in.readValue((String.class.getClassLoader())));
this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
this.ipd = ((String) in.readValue((String.class.getClassLoader())));
this.patientName = ((String) in.readValue((String.class.getClassLoader())));
this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
}

public OperationSchedule() {
}

public String getOperationScheduleId() {
return operationScheduleId;
}

public void setOperationScheduleId(String operationScheduleId) {
this.operationScheduleId = operationScheduleId;
}

public String getSurgeryId() {
return surgeryId;
}

public void setSurgeryId(String surgeryId) {
this.surgeryId = surgeryId;
}

public String getSurgeon() {
return surgeon;
}

public void setSurgeon(String surgeon) {
this.surgeon = surgeon;
}

public String getSurgeonId() {
return surgeonId;
}

public void setSurgeonId(String surgeonId) {
this.surgeonId = surgeonId;
}

public String getAssistant() {
return assistant;
}

public void setAssistant(String assistant) {
this.assistant = assistant;
}

public String getBedId() {
return bedId;
}

public void setBedId(String bedId) {
this.bedId = bedId;
}

public String getBedNumber() {
return bedNumber;
}

public void setBedNumber(String bedNumber) {
this.bedNumber = bedNumber;
}

public String getType() {
return type;
}

public void setType(String type) {
this.type = type;
}

public String getSurgeryDate() {
return surgeryDate;
}

public void setSurgeryDate(String surgeryDate) {
this.surgeryDate = surgeryDate;
}

public String getSurgeryTime() {
return surgeryTime;
}

public void setSurgeryTime(String surgeryTime) {
this.surgeryTime = surgeryTime;
}

public String getTurnAroundTime() {
return turnAroundTime;
}

public void setTurnAroundTime(String turnAroundTime) {
this.turnAroundTime = turnAroundTime;
}

public String getStatus() {
return status;
}

public void setStatus(String status) {
this.status = status;
}

public String getDischargedType() {
return dischargedType;
}

public void setDischargedType(String dischargedType) {
this.dischargedType = dischargedType;
}

public String getSurgeryStatus() {
return surgeryStatus;
}

public void setSurgeryStatus(String surgeryStatus) {
this.surgeryStatus = surgeryStatus;
}

public String getRoomName() {
return roomName;
}

public void setRoomName(String roomName) {
this.roomName = roomName;
}

public String getRoomNumber() {
return roomNumber;
}

public void setRoomNumber(String roomNumber) {
this.roomNumber = roomNumber;
}

public String getBedCategoryId() {
return bedCategoryId;
}

public void setBedCategoryId(String bedCategoryId) {
this.bedCategoryId = bedCategoryId;
}

public String getCategoryTitle() {
return categoryTitle;
}

public void setCategoryTitle(String categoryTitle) {
this.categoryTitle = categoryTitle;
}

public String getIpdId() {
return ipdId;
}

public void setIpdId(String ipdId) {
this.ipdId = ipdId;
}

public String getIpd() {
return ipd;
}

public void setIpd(String ipd) {
this.ipd = ipd;
}

public String getPatientName() {
return patientName;
}

public void setPatientName(String patientName) {
this.patientName = patientName;
}

public String getCreatedDate() {
return createdDate;
}

public void setCreatedDate(String createdDate) {
this.createdDate = createdDate;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(operationScheduleId);
dest.writeValue(surgeryId);
dest.writeValue(surgeon);
dest.writeValue(surgeonId);
dest.writeValue(assistant);
dest.writeValue(bedId);
dest.writeValue(bedNumber);
dest.writeValue(type);
dest.writeValue(surgeryDate);
dest.writeValue(surgeryTime);
dest.writeValue(turnAroundTime);
dest.writeValue(status);
dest.writeValue(dischargedType);
dest.writeValue(surgeryStatus);
dest.writeValue(roomName);
dest.writeValue(roomNumber);
dest.writeValue(bedCategoryId);
dest.writeValue(categoryTitle);
dest.writeValue(ipdId);
dest.writeValue(ipd);
dest.writeValue(patientName);
dest.writeValue(createdDate);
}

public int describeContents() {
return 0;
}

}
package com.ibt.niramaya.modal.report;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PatientReportsModel implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("test_list")
    @Expose
    private List<TestList> testList = null;
    public final static Parcelable.Creator<PatientReportsModel> CREATOR = new Creator<PatientReportsModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PatientReportsModel createFromParcel(Parcel in) {
            return new PatientReportsModel(in);
        }

        public PatientReportsModel[] newArray(int size) {
            return (new PatientReportsModel[size]);
        }

    }
            ;

    protected PatientReportsModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.testList, (com.ibt.niramaya.modal.report.TestList.class.getClassLoader()));
    }

    public PatientReportsModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<TestList> getTestList() {
        return testList;
    }

    public void setTestList(List<TestList> testList) {
        this.testList = testList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(testList);
    }

    public int describeContents() {
        return 0;
    }

}
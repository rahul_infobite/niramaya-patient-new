package com.ibt.niramaya.modal.hospital.pharmacy;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HospitalPharmacyBillModel implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("bill_data")
    @Expose
    private List<PharmacyBillDatum> billData = null;
    public final static Creator<HospitalPharmacyBillModel> CREATOR = new Creator<HospitalPharmacyBillModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HospitalPharmacyBillModel createFromParcel(Parcel in) {
            return new HospitalPharmacyBillModel(in);
        }

        public HospitalPharmacyBillModel[] newArray(int size) {
            return (new HospitalPharmacyBillModel[size]);
        }

    };

    protected HospitalPharmacyBillModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.billData, (PharmacyBillDatum.class.getClassLoader()));
    }

    public HospitalPharmacyBillModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<PharmacyBillDatum> getBillData() {
        return billData;
    }

    public void setBillData(List<PharmacyBillDatum> billData) {
        this.billData = billData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(billData);
    }

    public int describeContents() {
        return 0;
    }

}
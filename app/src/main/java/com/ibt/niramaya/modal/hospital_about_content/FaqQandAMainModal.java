package com.ibt.niramaya.modal.hospital_about_content;

import java.util.List;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FaqQandAMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("faq")
    @Expose
    private List<Faq> faq = null;
    public final static Parcelable.Creator<FaqQandAMainModal> CREATOR = new Creator<FaqQandAMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public FaqQandAMainModal createFromParcel(Parcel in) {
            return new FaqQandAMainModal(in);
        }

        public FaqQandAMainModal[] newArray(int size) {
            return (new FaqQandAMainModal[size]);
        }

    }
            ;

    protected FaqQandAMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.faq, (com.ibt.niramaya.modal.hospital_about_content.Faq.class.getClassLoader()));
    }

    public FaqQandAMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Faq> getFaq() {
        return faq;
    }

    public void setFaq(List<Faq> faq) {
        this.faq = faq;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(faq);
    }

    public int describeContents() {
        return 0;
    }

}

package com.ibt.niramaya.modal.ipd_prescription.list;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdPreception implements Parcelable {

    @SerializedName("ipd_slip_id")
    @Expose
    private String ipdSlipId;
    @SerializedName("ipd_id")
    @Expose
    private String ipdId;
    @SerializedName("ipd")
    @Expose
    private String ipd;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("ipd_patient_name")
    @Expose
    private String ipdPatientName;
    @SerializedName("patient_gender")
    @Expose
    private String patientGender;
    @SerializedName("ipd_patient_dob")
    @Expose
    private String ipdPatientDob;
    @SerializedName("ipd_patient_adhar_number")
    @Expose
    private String ipdPatientAdharNumber;
    @SerializedName("bed_numer")
    @Expose
    private String bedNumer;
    @SerializedName("bed_id")
    @Expose
    private String bedId;
    @SerializedName("room_name")
    @Expose
    private String roomName;
    @SerializedName("room_number")
    @Expose
    private String roomNumber;
    @SerializedName("bed_category_id")
    @Expose
    private String bedCategoryId;
    @SerializedName("category_title")
    @Expose
    private String categoryTitle;
    @SerializedName("staff_id")
    @Expose
    private String staffId;
    @SerializedName("staff_name")
    @Expose
    private String staffName;
    @SerializedName("staff_type")
    @Expose
    private String staffType;
    @SerializedName("ipd_created_date")
    @Expose
    private String ipdCreatedDate;
    @SerializedName("slip_link")
    @Expose
    private String slipLink;
    public final static Creator<IpdPreception> CREATOR = new Creator<IpdPreception>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdPreception createFromParcel(Parcel in) {
            return new IpdPreception(in);
        }

        public IpdPreception[] newArray(int size) {
            return (new IpdPreception[size]);
        }

    };

    protected IpdPreception(Parcel in) {
        this.ipdSlipId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipd = ((String) in.readValue((String.class.getClassLoader())));
        this.patientId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientName = ((String) in.readValue((String.class.getClassLoader())));
        this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientDob = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientAdharNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.bedNumer = ((String) in.readValue((String.class.getClassLoader())));
        this.bedId = ((String) in.readValue((String.class.getClassLoader())));
        this.roomName = ((String) in.readValue((String.class.getClassLoader())));
        this.roomNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.bedCategoryId = ((String) in.readValue((String.class.getClassLoader())));
        this.categoryTitle = ((String) in.readValue((String.class.getClassLoader())));
        this.staffId = ((String) in.readValue((String.class.getClassLoader())));
        this.staffName = ((String) in.readValue((String.class.getClassLoader())));
        this.staffType = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
        this.slipLink = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IpdPreception() {
    }

    public String getIpdSlipId() {
        return ipdSlipId;
    }

    public void setIpdSlipId(String ipdSlipId) {
        this.ipdSlipId = ipdSlipId;
    }

    public String getIpdId() {
        return ipdId;
    }

    public void setIpdId(String ipdId) {
        this.ipdId = ipdId;
    }

    public String getIpd() {
        return ipd;
    }

    public void setIpd(String ipd) {
        this.ipd = ipd;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getIpdPatientName() {
        return ipdPatientName;
    }

    public void setIpdPatientName(String ipdPatientName) {
        this.ipdPatientName = ipdPatientName;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getIpdPatientDob() {
        return ipdPatientDob;
    }

    public void setIpdPatientDob(String ipdPatientDob) {
        this.ipdPatientDob = ipdPatientDob;
    }

    public String getIpdPatientAdharNumber() {
        return ipdPatientAdharNumber;
    }

    public void setIpdPatientAdharNumber(String ipdPatientAdharNumber) {
        this.ipdPatientAdharNumber = ipdPatientAdharNumber;
    }

    public String getBedNumer() {
        return bedNumer;
    }

    public void setBedNumer(String bedNumer) {
        this.bedNumer = bedNumer;
    }

    public String getBedId() {
        return bedId;
    }

    public void setBedId(String bedId) {
        this.bedId = bedId;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBedCategoryId() {
        return bedCategoryId;
    }

    public void setBedCategoryId(String bedCategoryId) {
        this.bedCategoryId = bedCategoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffType() {
        return staffType;
    }

    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }

    public String getIpdCreatedDate() {
        return ipdCreatedDate;
    }

    public void setIpdCreatedDate(String ipdCreatedDate) {
        this.ipdCreatedDate = ipdCreatedDate;
    }

    public String getSlipLink() {
        return slipLink;
    }

    public void setSlipLink(String slipLink) {
        this.slipLink = slipLink;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ipdSlipId);
        dest.writeValue(ipdId);
        dest.writeValue(ipd);
        dest.writeValue(patientId);
        dest.writeValue(ipdPatientName);
        dest.writeValue(patientGender);
        dest.writeValue(ipdPatientDob);
        dest.writeValue(ipdPatientAdharNumber);
        dest.writeValue(bedNumer);
        dest.writeValue(bedId);
        dest.writeValue(roomName);
        dest.writeValue(roomNumber);
        dest.writeValue(bedCategoryId);
        dest.writeValue(categoryTitle);
        dest.writeValue(staffId);
        dest.writeValue(staffName);
        dest.writeValue(staffType);
        dest.writeValue(ipdCreatedDate);
        dest.writeValue(slipLink);
    }

    public int describeContents() {
        return 0;
    }

}
package com.ibt.niramaya.modal.hospital_ipd_details_modal.search_ipd_patient;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SearchIpdPatientMainModal implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd")
    @Expose
    private List<Ipd> ipd = new ArrayList<>();
    public final static Creator<SearchIpdPatientMainModal> CREATOR = new Creator<SearchIpdPatientMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SearchIpdPatientMainModal createFromParcel(Parcel in) {
            return new SearchIpdPatientMainModal(in);
        }

        public SearchIpdPatientMainModal[] newArray(int size) {
            return (new SearchIpdPatientMainModal[size]);
        }

    }
            ;

    protected SearchIpdPatientMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipd, (com.ibt.niramaya.modal.hospital_ipd_details_modal.search_ipd_patient.Ipd.class.getClassLoader()));
    }

    public SearchIpdPatientMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Ipd> getIpd() {
        return ipd;
    }

    public void setIpd(List<Ipd> ipd) {
        this.ipd = ipd;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(ipd);
    }

    public int describeContents() {
        return 0;
    }

}
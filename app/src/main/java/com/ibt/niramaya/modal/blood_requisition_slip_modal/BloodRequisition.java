package com.ibt.niramaya.modal.blood_requisition_slip_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BloodRequisition implements Parcelable
{

@SerializedName("blood_requisition_id")
@Expose
private String bloodRequisitionId;
@SerializedName("hospital_id")
@Expose
private String hospitalId;
@SerializedName("reference")
@Expose
private String reference;
@SerializedName("reference_id")
@Expose
private String referenceId;
@SerializedName("patient_id")
@Expose
private String patientId;
@SerializedName("patient_name")
@Expose
private String patientName;
@SerializedName("patient_contact")
@Expose
private String patientContact;
@SerializedName("patient_gender")
@Expose
private String patientGender;
@SerializedName("patient_dob")
@Expose
private String patientDob;
@SerializedName("requisition_dob")
@Expose
private String requisitionDob;
@SerializedName("problem_statement")
@Expose
private String problemStatement;
@SerializedName("patient_weight")
@Expose
private String patientWeight;
@SerializedName("whole_blood")
@Expose
private String wholeBlood;
@SerializedName("rcc")
@Expose
private String rcc;
@SerializedName("ffp")
@Expose
private String ffp;
@SerializedName("cryoprecipitate")
@Expose
private String cryoprecipitate;
@SerializedName("platelets_concentrate")
@Expose
private String plateletsConcentrate;
@SerializedName("requisition_unit")
@Expose
private String requisitionUnit;
@SerializedName("requisition_type")
@Expose
private String requisitionType;
@SerializedName("requisition_history")
@Expose
private String requisitionHistory;
@SerializedName("requisition_by")
@Expose
private String requisitionBy;
@SerializedName("requisition_by_doctor_name")
@Expose
private String requisitionByDoctorName;
@SerializedName("hospital")
@Expose
private Hospital hospital;
    @SerializedName("slip_view")
    @Expose
    private String slipLink;
public final static Creator<BloodRequisition> CREATOR = new Creator<BloodRequisition>() {


@SuppressWarnings({
"unchecked"
})
public BloodRequisition createFromParcel(Parcel in) {
return new BloodRequisition(in);
}

public BloodRequisition[] newArray(int size) {
return (new BloodRequisition[size]);
}

}
;

protected BloodRequisition(Parcel in) {
this.bloodRequisitionId = ((String) in.readValue((String.class.getClassLoader())));
this.hospitalId = ((String) in.readValue((String.class.getClassLoader())));
this.reference = ((String) in.readValue((String.class.getClassLoader())));
this.referenceId = ((String) in.readValue((String.class.getClassLoader())));
this.patientId = ((String) in.readValue((String.class.getClassLoader())));
this.patientName = ((String) in.readValue((String.class.getClassLoader())));
this.patientContact = ((String) in.readValue((String.class.getClassLoader())));
this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
this.patientDob = ((String) in.readValue((String.class.getClassLoader())));
this.requisitionDob = ((String) in.readValue((String.class.getClassLoader())));
this.problemStatement = ((String) in.readValue((String.class.getClassLoader())));
this.patientWeight = ((String) in.readValue((String.class.getClassLoader())));
this.wholeBlood = ((String) in.readValue((String.class.getClassLoader())));
this.rcc = ((String) in.readValue((String.class.getClassLoader())));
this.ffp = ((String) in.readValue((String.class.getClassLoader())));
this.cryoprecipitate = ((String) in.readValue((String.class.getClassLoader())));
this.plateletsConcentrate = ((String) in.readValue((String.class.getClassLoader())));
this.requisitionUnit = ((String) in.readValue((String.class.getClassLoader())));
this.requisitionType = ((String) in.readValue((String.class.getClassLoader())));
this.requisitionHistory = ((String) in.readValue((String.class.getClassLoader())));
this.requisitionBy = ((String) in.readValue((String.class.getClassLoader())));
this.requisitionByDoctorName = ((String) in.readValue((String.class.getClassLoader())));
this.slipLink = ((String) in.readValue((String.class.getClassLoader())));
this.hospital = ((Hospital) in.readValue((Hospital.class.getClassLoader())));
}

public BloodRequisition() {
}

public String getBloodRequisitionId() {
return bloodRequisitionId;
}

public void setBloodRequisitionId(String bloodRequisitionId) {
this.bloodRequisitionId = bloodRequisitionId;
}

public String getHospitalId() {
return hospitalId;
}

public void setHospitalId(String hospitalId) {
this.hospitalId = hospitalId;
}

public String getReference() {
return reference;
}

public void setReference(String reference) {
this.reference = reference;
}

public String getReferenceId() {
return referenceId;
}

public void setReferenceId(String referenceId) {
this.referenceId = referenceId;
}

public String getPatientId() {
return patientId;
}

public void setPatientId(String patientId) {
this.patientId = patientId;
}

public String getPatientName() {
return patientName;
}

public void setPatientName(String patientName) {
this.patientName = patientName;
}

public String getPatientContact() {
return patientContact;
}

public void setPatientContact(String patientContact) {
this.patientContact = patientContact;
}

public String getPatientGender() {
return patientGender;
}

public void setPatientGender(String patientGender) {
this.patientGender = patientGender;
}

public String getPatientDob() {
return patientDob;
}

public void setPatientDob(String patientDob) {
this.patientDob = patientDob;
}

public String getRequisitionDob() {
return requisitionDob;
}

public void setRequisitionDob(String requisitionDob) {
this.requisitionDob = requisitionDob;
}

public String getProblemStatement() {
return problemStatement;
}

public void setProblemStatement(String problemStatement) {
this.problemStatement = problemStatement;
}

public String getPatientWeight() {
return patientWeight;
}

public void setPatientWeight(String patientWeight) {
this.patientWeight = patientWeight;
}

public String getWholeBlood() {
return wholeBlood;
}

public void setWholeBlood(String wholeBlood) {
this.wholeBlood = wholeBlood;
}

public String getRcc() {
return rcc;
}

public void setRcc(String rcc) {
this.rcc = rcc;
}

public String getFfp() {
return ffp;
}

public void setFfp(String ffp) {
this.ffp = ffp;
}

public String getCryoprecipitate() {
return cryoprecipitate;
}

public void setCryoprecipitate(String cryoprecipitate) {
this.cryoprecipitate = cryoprecipitate;
}

public String getPlateletsConcentrate() {
return plateletsConcentrate;
}

public void setPlateletsConcentrate(String plateletsConcentrate) {
this.plateletsConcentrate = plateletsConcentrate;
}

public String getRequisitionUnit() {
return requisitionUnit;
}

public void setRequisitionUnit(String requisitionUnit) {
this.requisitionUnit = requisitionUnit;
}

public String getRequisitionType() {
return requisitionType;
}

public void setRequisitionType(String requisitionType) {
this.requisitionType = requisitionType;
}

public String getRequisitionHistory() {
return requisitionHistory;
}

public void setRequisitionHistory(String requisitionHistory) {
this.requisitionHistory = requisitionHistory;
}

public String getRequisitionBy() {
return requisitionBy;
}

public void setRequisitionBy(String requisitionBy) {
this.requisitionBy = requisitionBy;
}

public String getRequisitionByDoctorName() {
return requisitionByDoctorName;
}

public void setRequisitionByDoctorName(String requisitionByDoctorName) {
this.requisitionByDoctorName = requisitionByDoctorName;
}

public Hospital getHospital() {
return hospital;
}

public void setHospital(Hospital hospital) {
this.hospital = hospital;
}

    public String getSlipLink() {
        return slipLink;
    }

    public void setSlipLink(String slipLink) {
        this.slipLink = slipLink;
    }

    public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(bloodRequisitionId);
dest.writeValue(hospitalId);
dest.writeValue(reference);
dest.writeValue(referenceId);
dest.writeValue(patientId);
dest.writeValue(patientName);
dest.writeValue(patientContact);
dest.writeValue(patientGender);
dest.writeValue(patientDob);
dest.writeValue(requisitionDob);
dest.writeValue(problemStatement);
dest.writeValue(patientWeight);
dest.writeValue(wholeBlood);
dest.writeValue(rcc);
dest.writeValue(ffp);
dest.writeValue(cryoprecipitate);
dest.writeValue(plateletsConcentrate);
dest.writeValue(requisitionUnit);
dest.writeValue(requisitionType);
dest.writeValue(requisitionHistory);
dest.writeValue(requisitionBy);
dest.writeValue(requisitionByDoctorName);
dest.writeValue(hospital);
dest.writeValue(slipLink);
}

public int describeContents() {
return 0;
}

}
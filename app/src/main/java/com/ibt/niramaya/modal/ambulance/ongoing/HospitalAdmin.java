package com.ibt.niramaya.modal.ambulance.ongoing;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HospitalAdmin implements Parcelable {

    @SerializedName("hospital_admin_id")
    @Expose
    private String hospitalAdminId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("date_of_birth")
    @Expose
    private String dateOfBirth;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("created_date")
    @Expose
    private String createdDate;
    public final static Parcelable.Creator<HospitalAdmin> CREATOR = new Creator<HospitalAdmin>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HospitalAdmin createFromParcel(Parcel in) {
            return new HospitalAdmin(in);
        }

        public HospitalAdmin[] newArray(int size) {
            return (new HospitalAdmin[size]);
        }

    };

    protected HospitalAdmin(Parcel in) {
        this.hospitalAdminId = ((String) in.readValue((String.class.getClassLoader())));
        this.name = ((String) in.readValue((String.class.getClassLoader())));
        this.contact = ((String) in.readValue((String.class.getClassLoader())));
        this.gender = ((String) in.readValue((String.class.getClassLoader())));
        this.dateOfBirth = ((String) in.readValue((String.class.getClassLoader())));
        this.profileImage = ((String) in.readValue((String.class.getClassLoader())));
        this.createdDate = ((String) in.readValue((String.class.getClassLoader())));
    }

    public HospitalAdmin() {
    }

    public String getHospitalAdminId() {
        return hospitalAdminId;
    }

    public void setHospitalAdminId(String hospitalAdminId) {
        this.hospitalAdminId = hospitalAdminId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getProfileImage() {
        return profileImage;
    }

    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(hospitalAdminId);
        dest.writeValue(name);
        dest.writeValue(contact);
        dest.writeValue(gender);
        dest.writeValue(dateOfBirth);
        dest.writeValue(profileImage);
        dest.writeValue(createdDate);
    }

    public int describeContents() {
        return 0;
    }

}
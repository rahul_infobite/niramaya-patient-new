package com.ibt.niramaya.modal.hospital.bed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HospitalBedListModel implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd_bed")
    @Expose
    private List<IpdBed> ipdBed = null;
    public final static Creator<HospitalBedListModel> CREATOR = new Creator<HospitalBedListModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HospitalBedListModel createFromParcel(Parcel in) {
            return new HospitalBedListModel(in);
        }

        public HospitalBedListModel[] newArray(int size) {
            return (new HospitalBedListModel[size]);
        }

    };

    protected HospitalBedListModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipdBed, (com.ibt.niramaya.modal.hospital.bed.IpdBed.class.getClassLoader()));
    }

    public HospitalBedListModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IpdBed> getIpdBed() {
        return ipdBed;
    }

    public void setIpdBed(List<IpdBed> ipdBed) {
        this.ipdBed = ipdBed;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(ipdBed);
    }

    public int describeContents() {
        return 0;
    }

}
package com.ibt.niramaya.modal.hospital.ipd_slip;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdSlip implements Parcelable {

    @SerializedName("ipd_slip_id")
    @Expose
    private String ipdSlipId;
    @SerializedName("ipd_id")
    @Expose
    private String ipdId;
    @SerializedName("ipd")
    @Expose
    private String ipd;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("ipd_patient_name")
    @Expose
    private String ipdPatientName;
    @SerializedName("patient_gender")
    @Expose
    private String patientGender;
    @SerializedName("ipd_patient_dob")
    @Expose
    private String ipdPatientDob;
    @SerializedName("ipd_patient_adhar_number")
    @Expose
    private String ipdPatientAdharNumber;
    @SerializedName("staff_id")
    @Expose
    private String staffId;
    @SerializedName("staff_name")
    @Expose
    private String staffName;
    @SerializedName("staff_type")
    @Expose
    private String staffType;
    @SerializedName("ipd_created_date")
    @Expose
    private String ipdCreatedDate;
    @SerializedName("slip_link")
    @Expose
    private String ipdSlip;
    public final static Creator<IpdSlip> CREATOR = new Creator<IpdSlip>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdSlip createFromParcel(Parcel in) {
            return new IpdSlip(in);
        }

        public IpdSlip[] newArray(int size) {
            return (new IpdSlip[size]);
        }

    };

    protected IpdSlip(Parcel in) {
        this.ipdSlipId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipd = ((String) in.readValue((String.class.getClassLoader())));
        this.patientId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientName = ((String) in.readValue((String.class.getClassLoader())));
        this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientDob = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientAdharNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.staffId = ((String) in.readValue((String.class.getClassLoader())));
        this.staffName = ((String) in.readValue((String.class.getClassLoader())));
        this.staffType = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdSlip = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IpdSlip() {
    }

    public String getIpdSlipId() {
        return ipdSlipId;
    }

    public void setIpdSlipId(String ipdSlipId) {
        this.ipdSlipId = ipdSlipId;
    }

    public String getIpdId() {
        return ipdId;
    }

    public void setIpdId(String ipdId) {
        this.ipdId = ipdId;
    }

    public String getIpd() {
        return ipd;
    }

    public void setIpd(String ipd) {
        this.ipd = ipd;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getIpdPatientName() {
        return ipdPatientName;
    }

    public void setIpdPatientName(String ipdPatientName) {
        this.ipdPatientName = ipdPatientName;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getIpdPatientDob() {
        return ipdPatientDob;
    }

    public void setIpdPatientDob(String ipdPatientDob) {
        this.ipdPatientDob = ipdPatientDob;
    }

    public String getIpdPatientAdharNumber() {
        return ipdPatientAdharNumber;
    }

    public void setIpdPatientAdharNumber(String ipdPatientAdharNumber) {
        this.ipdPatientAdharNumber = ipdPatientAdharNumber;
    }

    public String getStaffId() {
        return staffId;
    }

    public void setStaffId(String staffId) {
        this.staffId = staffId;
    }

    public String getStaffName() {
        return staffName;
    }

    public void setStaffName(String staffName) {
        this.staffName = staffName;
    }

    public String getStaffType() {
        return staffType;
    }

    public void setStaffType(String staffType) {
        this.staffType = staffType;
    }

    public String getIpdCreatedDate() {
        return ipdCreatedDate;
    }

    public void setIpdCreatedDate(String ipdCreatedDate) {
        this.ipdCreatedDate = ipdCreatedDate;
    }


    public String getIpdSlip() {
        return ipdSlip;
    }

    public void setIpdSlip(String ipdSlip) {
        this.ipdSlip = ipdSlip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ipdSlipId);
        dest.writeValue(ipdId);
        dest.writeValue(ipd);
        dest.writeValue(patientId);
        dest.writeValue(ipdPatientName);
        dest.writeValue(patientGender);
        dest.writeValue(ipdPatientDob);
        dest.writeValue(ipdPatientAdharNumber);
        dest.writeValue(staffId);
        dest.writeValue(staffName);
        dest.writeValue(staffType);
        dest.writeValue(ipdCreatedDate);
        dest.writeValue(ipdSlip);
    }

    public int describeContents() {
        return 0;
    }

}
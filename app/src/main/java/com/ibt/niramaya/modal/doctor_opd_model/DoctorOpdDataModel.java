package com.ibt.niramaya.modal.doctor_opd_model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DoctorOpdDataModel implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("doctor_data")
    @Expose
    private DoctorOpdData doctorData;
    public final static Parcelable.Creator<DoctorOpdDataModel> CREATOR = new Creator<DoctorOpdDataModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public DoctorOpdDataModel createFromParcel(Parcel in) {
            return new DoctorOpdDataModel(in);
        }

        public DoctorOpdDataModel[] newArray(int size) {
            return (new DoctorOpdDataModel[size]);
        }

    }
            ;

    protected DoctorOpdDataModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        this.doctorData = ((DoctorOpdData) in.readValue((DoctorOpdData.class.getClassLoader())));
    }

    public DoctorOpdDataModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DoctorOpdData getDoctorData() {
        return doctorData;
    }

    public void setDoctorData(DoctorOpdData doctorData) {
        this.doctorData = doctorData;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeValue(doctorData);
    }

    public int describeContents() {
        return 0;
    }

}
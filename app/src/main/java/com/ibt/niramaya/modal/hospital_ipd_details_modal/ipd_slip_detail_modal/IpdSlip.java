package com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class IpdSlip implements Parcelable
{

@SerializedName("ipd_slip_id")
@Expose
private String ipdSlipId;
@SerializedName("ipd_id")
@Expose
private String ipdId;
@SerializedName("ipd")
@Expose
private String ipd;
@SerializedName("patient_id")
@Expose
private String patientId;
@SerializedName("ipd_patient_name")
@Expose
private String ipdPatientName;
@SerializedName("patient_gender")
@Expose
private String patientGender;
@SerializedName("ipd_patient_dob")
@Expose
private String ipdPatientDob;
@SerializedName("ipd_patient_adhar_number")
@Expose
private String ipdPatientAdharNumber;
@SerializedName("cheif_complaints")
@Expose
private String cheifComplaints;
@SerializedName("vitals")
@Expose
private String vitals;
@SerializedName("bp")
@Expose
private String bp;
@SerializedName("heart_rate_per_min")
@Expose
private String heartRatePerMin;
@SerializedName("resp_rate_min")
@Expose
private String respRateMin;
@SerializedName("temp")
@Expose
private String temp;
@SerializedName("pain_score")
@Expose
private String painScore;
@SerializedName("treatement_given")
@Expose
private String treatementGiven;
@SerializedName("treatment_advice")
@Expose
private String treatmentAdvice;
@SerializedName("follow_up_advice")
@Expose
private String followUpAdvice;
@SerializedName("staff_id")
@Expose
private String staffId;
@SerializedName("staff_name")
@Expose
private String staffName;
@SerializedName("staff_type")
@Expose
private String staffType;
@SerializedName("preception")
@Expose
private List<Preception> preception = new ArrayList<Preception>();
@SerializedName("pathology_test")
@Expose
private List<PathologyTest> pathologyTest = new ArrayList<PathologyTest>();
@SerializedName("ipd_created_date")
@Expose
private String ipdCreatedDate;
public final static Creator<IpdSlip> CREATOR = new Creator<IpdSlip>() {


@SuppressWarnings({
"unchecked"
})
public IpdSlip createFromParcel(Parcel in) {
return new IpdSlip(in);
}

public IpdSlip[] newArray(int size) {
return (new IpdSlip[size]);
}

}
;

protected IpdSlip(Parcel in) {
this.ipdSlipId = ((String) in.readValue((String.class.getClassLoader())));
this.ipdId = ((String) in.readValue((String.class.getClassLoader())));
this.ipd = ((String) in.readValue((String.class.getClassLoader())));
this.patientId = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPatientName = ((String) in.readValue((String.class.getClassLoader())));
this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPatientDob = ((String) in.readValue((String.class.getClassLoader())));
this.ipdPatientAdharNumber = ((String) in.readValue((String.class.getClassLoader())));
this.cheifComplaints = ((String) in.readValue((String.class.getClassLoader())));
this.vitals = ((String) in.readValue((String.class.getClassLoader())));
this.bp = ((String) in.readValue((String.class.getClassLoader())));
this.heartRatePerMin = ((String) in.readValue((String.class.getClassLoader())));
this.respRateMin = ((String) in.readValue((String.class.getClassLoader())));
this.temp = ((String) in.readValue((String.class.getClassLoader())));
this.painScore = ((String) in.readValue((String.class.getClassLoader())));
this.treatementGiven = ((String) in.readValue((String.class.getClassLoader())));
this.treatmentAdvice = ((String) in.readValue((String.class.getClassLoader())));
this.followUpAdvice = ((String) in.readValue((String.class.getClassLoader())));
this.staffId = ((String) in.readValue((String.class.getClassLoader())));
this.staffName = ((String) in.readValue((String.class.getClassLoader())));
this.staffType = ((String) in.readValue((String.class.getClassLoader())));
in.readList(this.preception, (com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.Preception.class.getClassLoader()));
in.readList(this.pathologyTest, (com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.PathologyTest.class.getClassLoader()));
this.ipdCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
}

public IpdSlip() {
}

public String getIpdSlipId() {
return ipdSlipId;
}

public void setIpdSlipId(String ipdSlipId) {
this.ipdSlipId = ipdSlipId;
}

public String getIpdId() {
return ipdId;
}

public void setIpdId(String ipdId) {
this.ipdId = ipdId;
}

public String getIpd() {
return ipd;
}

public void setIpd(String ipd) {
this.ipd = ipd;
}

public String getPatientId() {
return patientId;
}

public void setPatientId(String patientId) {
this.patientId = patientId;
}

public String getIpdPatientName() {
return ipdPatientName;
}

public void setIpdPatientName(String ipdPatientName) {
this.ipdPatientName = ipdPatientName;
}

public String getPatientGender() {
return patientGender;
}

public void setPatientGender(String patientGender) {
this.patientGender = patientGender;
}

public String getIpdPatientDob() {
return ipdPatientDob;
}

public void setIpdPatientDob(String ipdPatientDob) {
this.ipdPatientDob = ipdPatientDob;
}

public String getIpdPatientAdharNumber() {
return ipdPatientAdharNumber;
}

public void setIpdPatientAdharNumber(String ipdPatientAdharNumber) {
this.ipdPatientAdharNumber = ipdPatientAdharNumber;
}

public String getCheifComplaints() {
return cheifComplaints;
}

public void setCheifComplaints(String cheifComplaints) {
this.cheifComplaints = cheifComplaints;
}

public String getVitals() {
return vitals;
}

public void setVitals(String vitals) {
this.vitals = vitals;
}

public String getBp() {
return bp;
}

public void setBp(String bp) {
this.bp = bp;
}

public String getHeartRatePerMin() {
return heartRatePerMin;
}

public void setHeartRatePerMin(String heartRatePerMin) {
this.heartRatePerMin = heartRatePerMin;
}

public String getRespRateMin() {
return respRateMin;
}

public void setRespRateMin(String respRateMin) {
this.respRateMin = respRateMin;
}

public String getTemp() {
return temp;
}

public void setTemp(String temp) {
this.temp = temp;
}

public String getPainScore() {
return painScore;
}

public void setPainScore(String painScore) {
this.painScore = painScore;
}

public String getTreatementGiven() {
return treatementGiven;
}

public void setTreatementGiven(String treatementGiven) {
this.treatementGiven = treatementGiven;
}

public String getTreatmentAdvice() {
return treatmentAdvice;
}

public void setTreatmentAdvice(String treatmentAdvice) {
this.treatmentAdvice = treatmentAdvice;
}

public String getFollowUpAdvice() {
return followUpAdvice;
}

public void setFollowUpAdvice(String followUpAdvice) {
this.followUpAdvice = followUpAdvice;
}

public String getStaffId() {
return staffId;
}

public void setStaffId(String staffId) {
this.staffId = staffId;
}

public String getStaffName() {
return staffName;
}

public void setStaffName(String staffName) {
this.staffName = staffName;
}

public String getStaffType() {
return staffType;
}

public void setStaffType(String staffType) {
this.staffType = staffType;
}

public List<Preception> getPreception() {
return preception;
}

public void setPreception(List<Preception> preception) {
this.preception = preception;
}

public List<PathologyTest> getPathologyTest() {
return pathologyTest;
}

public void setPathologyTest(List<PathologyTest> pathologyTest) {
this.pathologyTest = pathologyTest;
}

public String getIpdCreatedDate() {
return ipdCreatedDate;
}

public void setIpdCreatedDate(String ipdCreatedDate) {
this.ipdCreatedDate = ipdCreatedDate;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(ipdSlipId);
dest.writeValue(ipdId);
dest.writeValue(ipd);
dest.writeValue(patientId);
dest.writeValue(ipdPatientName);
dest.writeValue(patientGender);
dest.writeValue(ipdPatientDob);
dest.writeValue(ipdPatientAdharNumber);
dest.writeValue(cheifComplaints);
dest.writeValue(vitals);
dest.writeValue(bp);
dest.writeValue(heartRatePerMin);
dest.writeValue(respRateMin);
dest.writeValue(temp);
dest.writeValue(painScore);
dest.writeValue(treatementGiven);
dest.writeValue(treatmentAdvice);
dest.writeValue(followUpAdvice);
dest.writeValue(staffId);
dest.writeValue(staffName);
dest.writeValue(staffType);
dest.writeList(preception);
dest.writeList(pathologyTest);
dest.writeValue(ipdCreatedDate);
}

public int describeContents() {
return 0;
}

}
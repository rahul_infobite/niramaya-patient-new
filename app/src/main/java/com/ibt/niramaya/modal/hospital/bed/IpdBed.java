package com.ibt.niramaya.modal.hospital.bed;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class IpdBed implements Parcelable {

    @SerializedName("ipd")
    @Expose
    private String ipd;
    @SerializedName("patient_id")
    @Expose
    private String patientId;
    @SerializedName("ipd_patient_name")
    @Expose
    private String ipdPatientName;
    @SerializedName("patient_gender")
    @Expose
    private String patientGender;
    @SerializedName("ipd_patient_dob")
    @Expose
    private String ipdPatientDob;
    @SerializedName("ipd_patient_adhar_number")
    @Expose
    private String ipdPatientAdharNumber;
    @SerializedName("bed_numer")
    @Expose
    private String bedNumer;
    @SerializedName("bed_id")
    @Expose
    private String bedId;
    @SerializedName("ipd_bed_start_date_time")
    @Expose
    private String ipdBedStartDateTime;
    @SerializedName("ipd_bed_end_date_time")
    @Expose
    private String ipdBedEndDateTime;
    @SerializedName("ipd_bed_charges")
    @Expose
    private String ipdBedCharges;
    @SerializedName("ipd_bed_status")
    @Expose
    private String ipdBedStatus;
    @SerializedName("ipd_bed_created_date")
    @Expose
    private String ipdBedCreatedDate;
    @SerializedName("room_name")
    @Expose
    private String roomName;
    @SerializedName("room_number")
    @Expose
    private String roomNumber;
    @SerializedName("bed_category_id")
    @Expose
    private String bedCategoryId;
    @SerializedName("category_title")
    @Expose
    private String categoryTitle;
    public final static Creator<IpdBed> CREATOR = new Creator<IpdBed>() {


        @SuppressWarnings({
                "unchecked"
        })
        public IpdBed createFromParcel(Parcel in) {
            return new IpdBed(in);
        }

        public IpdBed[] newArray(int size) {
            return (new IpdBed[size]);
        }

    };

    protected IpdBed(Parcel in) {
        this.ipd = ((String) in.readValue((String.class.getClassLoader())));
        this.patientId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientName = ((String) in.readValue((String.class.getClassLoader())));
        this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientDob = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdPatientAdharNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.bedNumer = ((String) in.readValue((String.class.getClassLoader())));
        this.bedId = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdBedStartDateTime = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdBedEndDateTime = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdBedCharges = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdBedStatus = ((String) in.readValue((String.class.getClassLoader())));
        this.ipdBedCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
        this.roomName = ((String) in.readValue((String.class.getClassLoader())));
        this.roomNumber = ((String) in.readValue((String.class.getClassLoader())));
        this.bedCategoryId = ((String) in.readValue((String.class.getClassLoader())));
        this.categoryTitle = ((String) in.readValue((String.class.getClassLoader())));
    }

    public IpdBed() {
    }

    public String getIpd() {
        return ipd;
    }

    public void setIpd(String ipd) {
        this.ipd = ipd;
    }

    public String getPatientId() {
        return patientId;
    }

    public void setPatientId(String patientId) {
        this.patientId = patientId;
    }

    public String getIpdPatientName() {
        return ipdPatientName;
    }

    public void setIpdPatientName(String ipdPatientName) {
        this.ipdPatientName = ipdPatientName;
    }

    public String getPatientGender() {
        return patientGender;
    }

    public void setPatientGender(String patientGender) {
        this.patientGender = patientGender;
    }

    public String getIpdPatientDob() {
        return ipdPatientDob;
    }

    public void setIpdPatientDob(String ipdPatientDob) {
        this.ipdPatientDob = ipdPatientDob;
    }

    public String getIpdPatientAdharNumber() {
        return ipdPatientAdharNumber;
    }

    public void setIpdPatientAdharNumber(String ipdPatientAdharNumber) {
        this.ipdPatientAdharNumber = ipdPatientAdharNumber;
    }

    public String getBedNumer() {
        return bedNumer;
    }

    public void setBedNumer(String bedNumer) {
        this.bedNumer = bedNumer;
    }

    public String getBedId() {
        return bedId;
    }

    public void setBedId(String bedId) {
        this.bedId = bedId;
    }

    public String getIpdBedStartDateTime() {
        return ipdBedStartDateTime;
    }

    public void setIpdBedStartDateTime(String ipdBedStartDateTime) {
        this.ipdBedStartDateTime = ipdBedStartDateTime;
    }

    public String getIpdBedEndDateTime() {
        return ipdBedEndDateTime;
    }

    public void setIpdBedEndDateTime(String ipdBedEndDateTime) {
        this.ipdBedEndDateTime = ipdBedEndDateTime;
    }

    public String getIpdBedCharges() {
        return ipdBedCharges;
    }

    public void setIpdBedCharges(String ipdBedCharges) {
        this.ipdBedCharges = ipdBedCharges;
    }

    public String getIpdBedStatus() {
        return ipdBedStatus;
    }

    public void setIpdBedStatus(String ipdBedStatus) {
        this.ipdBedStatus = ipdBedStatus;
    }

    public String getIpdBedCreatedDate() {
        return ipdBedCreatedDate;
    }

    public void setIpdBedCreatedDate(String ipdBedCreatedDate) {
        this.ipdBedCreatedDate = ipdBedCreatedDate;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getBedCategoryId() {
        return bedCategoryId;
    }

    public void setBedCategoryId(String bedCategoryId) {
        this.bedCategoryId = bedCategoryId;
    }

    public String getCategoryTitle() {
        return categoryTitle;
    }

    public void setCategoryTitle(String categoryTitle) {
        this.categoryTitle = categoryTitle;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(ipd);
        dest.writeValue(patientId);
        dest.writeValue(ipdPatientName);
        dest.writeValue(patientGender);
        dest.writeValue(ipdPatientDob);
        dest.writeValue(ipdPatientAdharNumber);
        dest.writeValue(bedNumer);
        dest.writeValue(bedId);
        dest.writeValue(ipdBedStartDateTime);
        dest.writeValue(ipdBedEndDateTime);
        dest.writeValue(ipdBedCharges);
        dest.writeValue(ipdBedStatus);
        dest.writeValue(ipdBedCreatedDate);
        dest.writeValue(roomName);
        dest.writeValue(roomNumber);
        dest.writeValue(bedCategoryId);
        dest.writeValue(categoryTitle);
    }

    public int describeContents() {
        return 0;
    }

}
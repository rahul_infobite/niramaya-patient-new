package com.ibt.niramaya.modal.ipd_test_report;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class TestList implements Serializable, Parcelable
{

@SerializedName("test_sample_status")
@Expose
private String testSampleStatus;
@SerializedName("test_sample_list")
@Expose
private String testSampleList;
@SerializedName("test_sample_date")
@Expose
private String testSampleDate;
@SerializedName("test_id")
@Expose
private String testId;
@SerializedName("report_id")
@Expose
private String reportId;
@SerializedName("test_status_report_file")
@Expose
private String testStatusReportFile;
@SerializedName("ipd_test_report_file_type")
@Expose
private String ipdTestReportFileType;
@SerializedName("ipd_test_report_description")
@Expose
private String ipdTestReportDescription;
@SerializedName("ipd_test_report_suggestion")
@Expose
private String ipdTestReportSuggestion;
@SerializedName("test_refer_doctor")
@Expose
private String testReferDoctor;
@SerializedName("test_refer_doctor_id")
@Expose
private String testReferDoctorId;
@SerializedName("pathology_test_name")
@Expose
private String pathologyTestName;
@SerializedName("pathology_test_id")
@Expose
private String pathologyTestId;
@SerializedName("test_code")
@Expose
private String testCode;
@SerializedName("test_sample")
@Expose
private String testSample;
@SerializedName("test_schedule")
@Expose
private String testSchedule;
@SerializedName("test_prerequisites")
@Expose
private String testPrerequisites;
@SerializedName("test_created_date")
@Expose
private String testCreatedDate;
@SerializedName("patient_name")
@Expose
private String patientName;
@SerializedName("patient_id")
@Expose
private String patientId;
@SerializedName("patient_gender")
@Expose
private String patientGender;
@SerializedName("patient_date_of_birth")
@Expose
private String patientDateOfBirth;
@SerializedName("hospital_id")
@Expose
private String hospitalId;
@SerializedName("hospital_name")
@Expose
private String hospitalName;
@SerializedName("hospital_image")
@Expose
private String hospitalImage;
public final static Creator<TestList> CREATOR = new Creator<TestList>() {


@SuppressWarnings({
"unchecked"
})
public TestList createFromParcel(Parcel in) {
return new TestList(in);
}

public TestList[] newArray(int size) {
return (new TestList[size]);
}

}
;
private final static long serialVersionUID = -4897352899806172458L;

protected TestList(Parcel in) {
this.testSampleStatus = ((String) in.readValue((String.class.getClassLoader())));
this.testSampleList = ((String) in.readValue((String.class.getClassLoader())));
this.testSampleDate = ((String) in.readValue((String.class.getClassLoader())));
this.testId = ((String) in.readValue((String.class.getClassLoader())));
this.reportId = ((String) in.readValue((String.class.getClassLoader())));
this.testStatusReportFile = ((String) in.readValue((String.class.getClassLoader())));
this.ipdTestReportFileType = ((String) in.readValue((String.class.getClassLoader())));
this.ipdTestReportDescription = ((String) in.readValue((String.class.getClassLoader())));
this.ipdTestReportSuggestion = ((String) in.readValue((String.class.getClassLoader())));
this.testReferDoctor = ((String) in.readValue((String.class.getClassLoader())));
this.testReferDoctorId = ((String) in.readValue((String.class.getClassLoader())));
this.pathologyTestName = ((String) in.readValue((String.class.getClassLoader())));
this.pathologyTestId = ((String) in.readValue((String.class.getClassLoader())));
this.testCode = ((String) in.readValue((String.class.getClassLoader())));
this.testSample = ((String) in.readValue((String.class.getClassLoader())));
this.testSchedule = ((String) in.readValue((String.class.getClassLoader())));
this.testPrerequisites = ((String) in.readValue((String.class.getClassLoader())));
this.testCreatedDate = ((String) in.readValue((String.class.getClassLoader())));
this.patientName = ((String) in.readValue((String.class.getClassLoader())));
this.patientId = ((String) in.readValue((String.class.getClassLoader())));
this.patientGender = ((String) in.readValue((String.class.getClassLoader())));
this.patientDateOfBirth = ((String) in.readValue((String.class.getClassLoader())));
this.hospitalId = ((String) in.readValue((String.class.getClassLoader())));
this.hospitalName = ((String) in.readValue((String.class.getClassLoader())));
this.hospitalImage = ((String) in.readValue((String.class.getClassLoader())));
}

public TestList() {
}

public String getTestSampleStatus() {
return testSampleStatus;
}

public void setTestSampleStatus(String testSampleStatus) {
this.testSampleStatus = testSampleStatus;
}

public String getTestSampleList() {
return testSampleList;
}

public void setTestSampleList(String testSampleList) {
this.testSampleList = testSampleList;
}

public String getTestSampleDate() {
return testSampleDate;
}

public void setTestSampleDate(String testSampleDate) {
this.testSampleDate = testSampleDate;
}

public String getTestId() {
return testId;
}

public void setTestId(String testId) {
this.testId = testId;
}

public String getReportId() {
return reportId;
}

public void setReportId(String reportId) {
this.reportId = reportId;
}

public String getTestStatusReportFile() {
return testStatusReportFile;
}

public void setTestStatusReportFile(String testStatusReportFile) {
this.testStatusReportFile = testStatusReportFile;
}

public String getIpdTestReportFileType() {
return ipdTestReportFileType;
}

public void setIpdTestReportFileType(String ipdTestReportFileType) {
this.ipdTestReportFileType = ipdTestReportFileType;
}

public String getIpdTestReportDescription() {
return ipdTestReportDescription;
}

public void setIpdTestReportDescription(String ipdTestReportDescription) {
this.ipdTestReportDescription = ipdTestReportDescription;
}

public String getIpdTestReportSuggestion() {
return ipdTestReportSuggestion;
}

public void setIpdTestReportSuggestion(String ipdTestReportSuggestion) {
this.ipdTestReportSuggestion = ipdTestReportSuggestion;
}

public String getTestReferDoctor() {
return testReferDoctor;
}

public void setTestReferDoctor(String testReferDoctor) {
this.testReferDoctor = testReferDoctor;
}

public String getTestReferDoctorId() {
return testReferDoctorId;
}

public void setTestReferDoctorId(String testReferDoctorId) {
this.testReferDoctorId = testReferDoctorId;
}

public String getPathologyTestName() {
return pathologyTestName;
}

public void setPathologyTestName(String pathologyTestName) {
this.pathologyTestName = pathologyTestName;
}

public String getPathologyTestId() {
return pathologyTestId;
}

public void setPathologyTestId(String pathologyTestId) {
this.pathologyTestId = pathologyTestId;
}

public String getTestCode() {
return testCode;
}

public void setTestCode(String testCode) {
this.testCode = testCode;
}

public String getTestSample() {
return testSample;
}

public void setTestSample(String testSample) {
this.testSample = testSample;
}

public String getTestSchedule() {
return testSchedule;
}

public void setTestSchedule(String testSchedule) {
this.testSchedule = testSchedule;
}

public String getTestPrerequisites() {
return testPrerequisites;
}

public void setTestPrerequisites(String testPrerequisites) {
this.testPrerequisites = testPrerequisites;
}

public String getTestCreatedDate() {
return testCreatedDate;
}

public void setTestCreatedDate(String testCreatedDate) {
this.testCreatedDate = testCreatedDate;
}

public String getPatientName() {
return patientName;
}

public void setPatientName(String patientName) {
this.patientName = patientName;
}

public String getPatientId() {
return patientId;
}

public void setPatientId(String patientId) {
this.patientId = patientId;
}

public String getPatientGender() {
return patientGender;
}

public void setPatientGender(String patientGender) {
this.patientGender = patientGender;
}

public String getPatientDateOfBirth() {
return patientDateOfBirth;
}

public void setPatientDateOfBirth(String patientDateOfBirth) {
this.patientDateOfBirth = patientDateOfBirth;
}

public String getHospitalId() {
return hospitalId;
}

public void setHospitalId(String hospitalId) {
this.hospitalId = hospitalId;
}

public String getHospitalName() {
return hospitalName;
}

public void setHospitalName(String hospitalName) {
this.hospitalName = hospitalName;
}

public String getHospitalImage() {
return hospitalImage;
}

public void setHospitalImage(String hospitalImage) {
this.hospitalImage = hospitalImage;
}

public void writeToParcel(Parcel dest, int flags) {
dest.writeValue(testSampleStatus);
dest.writeValue(testSampleList);
dest.writeValue(testSampleDate);
dest.writeValue(testId);
dest.writeValue(reportId);
dest.writeValue(testStatusReportFile);
dest.writeValue(ipdTestReportFileType);
dest.writeValue(ipdTestReportDescription);
dest.writeValue(ipdTestReportSuggestion);
dest.writeValue(testReferDoctor);
dest.writeValue(testReferDoctorId);
dest.writeValue(pathologyTestName);
dest.writeValue(pathologyTestId);
dest.writeValue(testCode);
dest.writeValue(testSample);
dest.writeValue(testSchedule);
dest.writeValue(testPrerequisites);
dest.writeValue(testCreatedDate);
dest.writeValue(patientName);
dest.writeValue(patientId);
dest.writeValue(patientGender);
dest.writeValue(patientDateOfBirth);
dest.writeValue(hospitalId);
dest.writeValue(hospitalName);
dest.writeValue(hospitalImage);
}

public int describeContents() {
return 0;
}

}

package com.ibt.niramaya.modal.hospital.operation;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HospitalOperationModel implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("operation_schedule")
    @Expose
    private List<OperationSchedule> operationSchedule = new ArrayList<OperationSchedule>();
    public final static Creator<HospitalOperationModel> CREATOR = new Creator<HospitalOperationModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HospitalOperationModel createFromParcel(Parcel in) {
            return new HospitalOperationModel(in);
        }

        public HospitalOperationModel[] newArray(int size) {
            return (new HospitalOperationModel[size]);
        }

    }
            ;

    protected HospitalOperationModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.operationSchedule, (com.ibt.niramaya.modal.hospital.operation.OperationSchedule.class.getClassLoader()));
    }

    public HospitalOperationModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<OperationSchedule> getOperationSchedule() {
        return operationSchedule;
    }

    public void setOperationSchedule(List<OperationSchedule> operationSchedule) {
        this.operationSchedule = operationSchedule;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(operationSchedule);
    }

    public int describeContents() {
        return 0;
    }

}
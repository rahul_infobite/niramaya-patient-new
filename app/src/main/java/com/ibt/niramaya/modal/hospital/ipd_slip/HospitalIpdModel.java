package com.ibt.niramaya.modal.hospital.ipd_slip;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class HospitalIpdModel implements Parcelable
{

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd_Slip")
    @Expose
    private List<IpdSlip> ipdSlip = null;
    public final static Creator<HospitalIpdModel> CREATOR = new Creator<HospitalIpdModel>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HospitalIpdModel createFromParcel(Parcel in) {
            return new HospitalIpdModel(in);
        }

        public HospitalIpdModel[] newArray(int size) {
            return (new HospitalIpdModel[size]);
        }

    }
            ;

    protected HospitalIpdModel(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipdSlip, (com.ibt.niramaya.modal.hospital.ipd_slip.IpdSlip.class.getClassLoader()));
    }

    public HospitalIpdModel() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IpdSlip> getIpdSlip() {
        return ipdSlip;
    }

    public void setIpdSlip(List<IpdSlip> ipdSlip) {
        this.ipdSlip = ipdSlip;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(ipdSlip);
    }

    public int describeContents() {
        return 0;
    }

}
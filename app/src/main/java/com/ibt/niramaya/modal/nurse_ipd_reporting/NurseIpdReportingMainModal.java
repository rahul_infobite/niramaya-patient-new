package com.ibt.niramaya.modal.nurse_ipd_reporting;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NurseIpdReportingMainModal implements Parcelable {

    @SerializedName("error")
    @Expose
    private Boolean error;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ipd_report_list")
    @Expose
    private List<IpdReportList> ipdReportList = null;
    public final static Creator<NurseIpdReportingMainModal> CREATOR = new Creator<NurseIpdReportingMainModal>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NurseIpdReportingMainModal createFromParcel(Parcel in) {
            return new NurseIpdReportingMainModal(in);
        }

        public NurseIpdReportingMainModal[] newArray(int size) {
            return (new NurseIpdReportingMainModal[size]);
        }

    };

    protected NurseIpdReportingMainModal(Parcel in) {
        this.error = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.ipdReportList, (com.ibt.niramaya.modal.nurse_ipd_reporting.IpdReportList.class.getClassLoader()));
    }

    public NurseIpdReportingMainModal() {
    }

    public Boolean getError() {
        return error;
    }

    public void setError(Boolean error) {
        this.error = error;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<IpdReportList> getIpdReportList() {
        return ipdReportList;
    }

    public void setIpdReportList(List<IpdReportList> ipdReportList) {
        this.ipdReportList = ipdReportList;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(error);
        dest.writeValue(message);
        dest.writeList(ipdReportList);
    }

    public int describeContents() {
        return 0;
    }

}
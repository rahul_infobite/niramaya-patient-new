package com.ibt.niramaya.ui.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.os.Bundle
import android.view.View
import com.ibt.niramaya.R
import com.ibt.niramaya.constant.Constant
import com.ibt.niramaya.retrofit.RetrofitService
import com.ibt.niramaya.retrofit.WebResponse
import com.ibt.niramaya.utils.Alerts
import com.ibt.niramaya.utils.AppPreference
import com.ibt.niramaya.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_user_management.*
import okhttp3.ResponseBody
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Response
import java.io.IOException

class UserManagementActivity : BaseActivity(), View.OnClickListener {
    private var invokeContact = ""
    private var invokePatientId = ""
    private var invokePatientName = ""
    private var invokeVerificationType = ""
    private var PatientNumber = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_user_management)

        btnSubmitUserId.setOnClickListener(this)
        btnSubmitOtp.setOnClickListener(this)
    }

    override fun onClick(view: View?) {
        when (view!!.id) {
            R.id.btnSubmitUserId -> {
                PatientNumber =  etPatientId.text.toString()
                if (PatientNumber.isNotEmpty()){
                    invokeNewPatient()
                }else{
                    Alerts.show(mContext,"Enter Patient Number!!!")
                }
            }
            R.id.btnSubmitOtp -> {
                val otpNumber = etOtpNumber.text.toString()
                if (otpNumber.isNotBlank()){
                    invokeNewPatientContactVerification(otpNumber)
                }else{
                    Alerts.show(mContext,"Enter Otp!!!")
                }

            }
        }
    }

    private fun invokeNewPatient() {
        if (cd.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext, Constant.USER_ID)
            RetrofitService.getServerResponse(Dialog(mContext), retrofitApiClient.invokeNewPatient(PatientNumber,userId), object : WebResponse {
                @SuppressLint("SetTextI18n")
                override fun onResponseSuccess(result: Response<*>) {
                    val body = result.body() as ResponseBody?
                    try {
                        assert(body != null)
                        val jsonObject = JSONObject(body!!.string())
                        if (!jsonObject.getBoolean("error")) {
                            Alerts.show(mContext, jsonObject.getString("message"))
                            invokeContact = jsonObject.getString("contact")
                            invokeVerificationType = jsonObject.getString("verification_type")
                            invokePatientName = jsonObject.getString("patient_name")
                            invokePatientId = jsonObject.getString("patient_id")
                            txtPatientName.text = invokePatientName
                            txtMobileNumber.text = "OTP sent on :$invokeContact"
                            llPatientVerification.visibility= View.VISIBLE
                            llPatientDetail.visibility= View.GONE

                        } else {
                            Alerts.show(mContext, jsonObject.getString("message"))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                override fun onResponseFailed(error: String) {
                    Alerts.show(mContext,"Server Error!!!")

                }
            })
        }
    }

    private fun invokeNewPatientContactVerification(otpNumber: String) {
        if (cd.isNetworkAvailable) {
            val userId = AppPreference.getStringPreference(mContext, Constant.USER_ID)
            RetrofitService.getServerResponse(Dialog(mContext), retrofitApiClient.invokeNewPatientContactvarification(invokeContact,
                    otpNumber, invokeVerificationType, invokePatientId,userId ), object : WebResponse {
                override fun onResponseSuccess(result: Response<*>) {
                    val body = result.body() as ResponseBody?
                    try {
                        assert(body != null)
                        val jsonObject = JSONObject(body!!.string())
                        if (!jsonObject.getBoolean("error")) {
                            Alerts.show(mContext, jsonObject.getString("message"))
                            finish()
                        } else {
                            Alerts.show(mContext, jsonObject.getString("message"))
                        }
                    } catch (e: JSONException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }

                override fun onResponseFailed(error: String) {
                    Alerts.show(mContext,"Server Error!!!")
                }            })
        }
    }


}

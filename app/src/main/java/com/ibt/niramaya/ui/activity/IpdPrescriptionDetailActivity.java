package com.ibt.niramaya.ui.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.BloodRequisitionAdapter;
import com.ibt.niramaya.adapter.ipd_prescription_adapter.IpdPathologyTestDetailAdapter;
import com.ibt.niramaya.adapter.ipd_prescription_adapter.IpdPrescriptionMedicineAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisitonMainModal;
import com.ibt.niramaya.modal.ipd_prescription.details.IpdPreception;
import com.ibt.niramaya.modal.ipd_prescription.details.IpdPreceptionTest;
import com.ibt.niramaya.modal.ipd_prescription.details.PatientPastPrescriptionDetailsModel;
import com.ibt.niramaya.modal.ipd_prescription.details.PreceptionSlip;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.fragment.BloodRecuisitionBottomDialogFragment;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;

import java.util.ArrayList;

import retrofit2.Response;

public class IpdPrescriptionDetailActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvDischargeType;
    private String ipdSlipId, patientName, patientId,ipdId;
    private PreceptionSlip preceptionSlip;
    private ArrayList<IpdPreception> ipdPreceptionMedicine = new ArrayList<>();
    private ArrayList<IpdPreceptionTest> ipdPreceptionTest = new ArrayList<>();
    RecyclerView rvMedicine, rvPathologyTest;

    // blood requisition slip
    private MenuItem menuBlood,menuView;
    private RelativeLayout rlAddBloodRecuisition;
    private BloodRequisitionAdapter bloodRequisitionAdapter;
    private ArrayList<BloodRequisition> bloodRequisitions = new ArrayList<>();
    private TextView txtMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ipd_prescription_detail);

        Toolbar toolbar = findViewById(R.id.toolbar);
        showHomeBackOnToolbar(toolbar);
        this.setTitle("");

        ipdSlipId = getIntent().getExtras().getString("IPD_SLIP_ID");
        ipdId = getIntent().getExtras().getString("IPD_ID");
        patientName = getIntent().getExtras().getString("IPD_PATIENT_NAME");
        patientId = getIntent().getExtras().getString("IPD_PATIENT_ID");

        fetchPrescription();

    }

    @SuppressLint("SetTextI18n")
    private void initViews(PatientPastPrescriptionDetailsModel prescriptionModel) {
        TextView tvPatientName = findViewById(R.id.tvPatientName);
        TextView tvPatientId = findViewById(R.id.tvPatientId);
        TextView tvDoctorName = findViewById(R.id.tvDoctorName);
        TextView tvHospitalName = findViewById(R.id.tvHospitalName);
        TextView tvOpdCreatedDate = findViewById(R.id.tvOpdCreatedDate);
        TextView tvAge = findViewById(R.id.tvAge);
        TextView tvContact = findViewById(R.id.tvContact);
        ImageView ivHospitalLogo = findViewById(R.id.ivHospitalLogo);

        TextView tvComplaint = findViewById(R.id.tvComplaint);
        TextView tvBpCount = findViewById(R.id.tvBpCount);
        TextView tvHrCount = findViewById(R.id.tvHrCount);
        TextView tvRespCount = findViewById(R.id.tvRespCount);
        TextView tvTempCount = findViewById(R.id.tvTempCount);
        TextView tvSPO2Count = findViewById(R.id.tvSPO2Count);
        TextView tvPainScoreCount = findViewById(R.id.tvPainScoreCount);
        TextView tvGcsCount = findViewById(R.id.tvGcsCount);
        tvDischargeType = findViewById(R.id.tvDischargeType);

        tvPatientName.setText("Patient Name : "+patientName);
        tvPatientId.setText("Patient ID : "+patientId);
        tvDoctorName.setText("Doctor Name : "+preceptionSlip.getStaffName());
        tvOpdCreatedDate.setText(preceptionSlip.getIpdCreatedDate());

        if (preceptionSlip.getIpdBp().isEmpty()){
            tvBpCount.setText("-");
        }else {
            tvBpCount.setText(preceptionSlip.getIpdBp());
        }
        if (preceptionSlip.getIpdHeartRatePerMin().isEmpty()){
            tvHrCount.setText("-");
        }else {
            tvHrCount.setText(preceptionSlip.getIpdHeartRatePerMin());
        }
        if (preceptionSlip.getIpdRespRateMin().isEmpty()){
            tvRespCount.setText("-");
        }else {
            tvRespCount.setText(preceptionSlip.getIpdRespRateMin());
        }
        if (preceptionSlip.getIpdTemp().isEmpty()){
            tvTempCount.setText("-");
        }else {
            tvTempCount.setText(preceptionSlip.getIpdTemp());
        }
        if (preceptionSlip.getIpdPainScore().isEmpty()){
            tvPainScoreCount.setText("-");
        }else {
            tvPainScoreCount.setText(preceptionSlip.getIpdPainScore());
        }
        if (preceptionSlip.getIpdSpo2().isEmpty()){
            tvSPO2Count.setText("-");
        }else {
            tvSPO2Count.setText(preceptionSlip.getIpdSpo2());
        } if (preceptionSlip.getIpdGcs().isEmpty()){
            tvGcsCount.setText("-");
        }else {
            tvGcsCount.setText(preceptionSlip.getIpdGcs());
        }

        tvComplaint.setText(preceptionSlip.getIpdCheifComplaints());


        if (prescriptionModel.getPreceptionSlip().getIpdPreception().size() > 0) {
            ipdPreceptionMedicine.addAll(prescriptionModel.getPreceptionSlip().getIpdPreception());

            rvMedicine = findViewById(R.id.rvMedicine);

            rvMedicine.setLayoutManager(new LinearLayoutManager(mContext));
            IpdPrescriptionMedicineAdapter givenAdapter = new IpdPrescriptionMedicineAdapter(ipdPreceptionMedicine, mContext);
            rvMedicine.setAdapter(givenAdapter);
            givenAdapter.notifyDataSetChanged();


        }
        if (prescriptionModel.getPreceptionSlip().getIpdPreceptionTest().size() > 0) {
            ipdPreceptionTest.addAll(prescriptionModel.getPreceptionSlip().getIpdPreceptionTest());
            rvPathologyTest = findViewById(R.id.rvPathologyTest);
            rvPathologyTest.setLayoutManager(new LinearLayoutManager(mContext));
            IpdPathologyTestDetailAdapter givenAdapter = new IpdPathologyTestDetailAdapter(ipdPreceptionTest, mContext);
            rvPathologyTest.setAdapter(givenAdapter);
            givenAdapter.notifyDataSetChanged();
        }


        rlAddBloodRecuisition = findViewById(R.id.rlAddBloodRecuisition);
        rlAddBloodRecuisition.setVisibility(View.VISIBLE);
        rlAddBloodRecuisition.setOnClickListener(this);
        txtMessage = findViewById(R.id.txtMessage);
        txtMessage.setVisibility(View.GONE);

        RecyclerView rvBloodRequisition = findViewById(R.id.rvBloodRequisition);
        rvBloodRequisition.setHasFixedSize(true);
        rvBloodRequisition.setLayoutManager(new LinearLayoutManager(mContext));
        bloodRequisitionAdapter = new BloodRequisitionAdapter(bloodRequisitions, mContext, this);
        rvBloodRequisition.setAdapter(bloodRequisitionAdapter);

        bloodRecuisitionApi();

    }

    private void fetchPrescription(){
        if (cd.isNetworkAvailable()){
            String hId = AppPreference.getStringPreference(mContext,Constant.HOSPITAL_ID);
            RetrofitService.selectDoctroPrescriptionDetail(new Dialog(mContext),
                    retrofitApiClient.selectDoctorPrescriptionDetail(ipdSlipId,hId),
                    new WebResponse() {
                        @Override
                        public void onResponseSuccess(Response<?> result) {
                            PatientPastPrescriptionDetailsModel prescriptionModel = (PatientPastPrescriptionDetailsModel) result.body();
                            assert prescriptionModel != null;
                            if (!prescriptionModel.getError()){
                                Alerts.show(mContext, prescriptionModel.getMessage());
                                preceptionSlip = prescriptionModel.getPreceptionSlip();
                                initViews(prescriptionModel);
                            }else{
                                Alerts.show(mContext, prescriptionModel.getMessage());
                            }
                        }

                        @Override
                        public void onResponseFailed(String error) {
                            Alerts.show(mContext,"Server Error!!!");
                        }
                    });
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bill_detail, menu);
        menuView  = menu.findItem(R.id.action_view);
        menuView.setVisible(false);

        menuBlood = menu.findItem(R.id.view_blood_requisition);
        if (bloodRequisitions.size() ==0){
            menuBlood.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_view) {

        } else if (item.getItemId() == R.id.view_blood_requisition) {
            // drawer.openDrawer(Gravity.START);
            Bundle bundle = new Bundle();
            BloodRecuisitionBottomDialogFragment bottomDialogFragment = new BloodRecuisitionBottomDialogFragment();
            bottomDialogFragment.show(getSupportFragmentManager(),
                    "bottom_dialog_fragment");
            bundle.putInt("opdId", Integer.parseInt(ipdId));
            bottomDialogFragment.setArguments(bundle);
        }
        return super.onOptionsItemSelected(item);
    }
    private void bloodRecuisitionApi() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.selectBloodRequisition(new Dialog(mContext), retrofitApiClient.selectBloodRequisitionData("1", ipdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    bloodRequisitions.clear();
                    BloodRequisitonMainModal mainModal = (BloodRequisitonMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getBloodRequisition().size() > 0) {
                            bloodRequisitions.addAll(mainModal.getBloodRequisition());
                            bloodRequisitionAdapter.notifyDataSetChanged();
                            menuBlood.setVisible(true);
                        } else {
                            menuBlood.setVisible(false);
                            rlAddBloodRecuisition.setVisibility(View.VISIBLE);
                            txtMessage.setVisibility(View.VISIBLE);
                            txtMessage.setText(mainModal.getMessage());
                        }
                    } else {
                        menuBlood.setVisible(false);
                        rlAddBloodRecuisition.setVisibility(View.VISIBLE);
                        txtMessage.setVisibility(View.VISIBLE);
                        txtMessage.setText(mainModal.getMessage());
                    }
                    bloodRequisitionAdapter.notifyDataSetChanged();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext,"Server Error!!!");
                }
            });
        }
    }


    @Override
    public void onClick(View v) {


    }
}

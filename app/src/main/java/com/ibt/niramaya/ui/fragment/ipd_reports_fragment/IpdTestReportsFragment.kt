package com.ibt.niramaya.ui.fragment.ipd_reports_fragment

import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.ibt.niramaya.R
import com.ibt.niramaya.adapter.PatientIpdTestListAdapter
import com.ibt.niramaya.modal.ipd_test_report.IpdReportsModal
import com.ibt.niramaya.modal.ipd_test_report.TestList
import com.ibt.niramaya.retrofit.RetrofitApiClient
import com.ibt.niramaya.retrofit.RetrofitService
import com.ibt.niramaya.retrofit.WebResponse
import com.ibt.niramaya.ui.activity.ViewImageAndFilesActivity
import com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity.PatientIpdReportDetailActivity
import com.ibt.niramaya.utils.Alerts
import com.ibt.niramaya.utils.ConnectionDetector
import kotlinx.android.synthetic.main.fragment_report.*
import retrofit2.Response
import java.util.*

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class IpdTestReportsFragment : Fragment(), View.OnClickListener {
    var rootView: View? = null
    var cd: ConnectionDetector? = null
    var mContext: Context? = null
    var retrofitApiClient: RetrofitApiClient? = null
    private var ipdReportLists: ArrayList<TestList> = ArrayList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        rootView = inflater.inflate(R.layout.fragment_report, container, false)
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mContext = activity
        cd = ConnectionDetector(mContext)
        retrofitApiClient = RetrofitService.getRetrofit()

        val bundle = arguments
        val ipdId = bundle!!.getString("ipdId")
        selectIpdTestReport(ipdId)
    }

    private fun selectIpdTestReport(ipdId: String) {
        if (cd!!.isNetworkAvailable) {
            RetrofitService.patientIpdReportData(Dialog(mContext!!), retrofitApiClient!!.patientIpdTestReports(ipdId), object : WebResponse {
                override fun onResponseSuccess(result: Response<*>) {
                    ipdReportLists.clear()
                    val mainModal: IpdReportsModal = (result.body() as IpdReportsModal?)!!
                    if (!mainModal.error) {
                        if (mainModal.testList.size > 0) {
                            ipdReportLists = mainModal.testList as ArrayList<TestList>
                            recyclerViewReports.setHasFixedSize(true)
                            recyclerViewReports.layoutManager = GridLayoutManager(mContext, 2)
                            val patientTestListAdapter = PatientIpdTestListAdapter(ipdReportLists, mContext, this@IpdTestReportsFragment)
                            recyclerViewReports.adapter = patientTestListAdapter
                            patientTestListAdapter.notifyDataSetChanged()
                        } else {
                            Alerts.show(mContext, mainModal.message)
                        }
                    }
                }

                override fun onResponseFailed(error: String?) {
                    Alerts.show(mContext, "Server Error!!!")
                }
            })
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.txtView -> {
                val tag1 = v.tag as Int
                val fileUrl: String = ipdReportLists[tag1].testStatusReportFile
                openFile(fileUrl, ipdReportLists[tag1].ipdTestReportFileType)
            }
            R.id.cardViewItem -> {
                val tag = v.tag as Int
                val intent = Intent(mContext, PatientIpdReportDetailActivity::class.java)
                intent.putExtra("patientReportData", ipdReportLists as Parcelable?)
                mContext!!.startActivity(intent)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun openFile(url: String, testReportFileType: String) {
        try { //  Uri uri = Uri.fromFile(url);
            val intent = Intent(Intent.ACTION_VIEW)
            if (testReportFileType.contains("doc") || testReportFileType.contains(".docx")) {
                activityIntent("file", url)
            } else if (testReportFileType.contains("pdf")) {
                activityIntent("file", url)
            } else if (testReportFileType.contains("ppt") || testReportFileType.contains(".pptx")) { // Powerpoint file
                activityIntent("file", url)
            } else if (testReportFileType.contains("xls") || testReportFileType.contains("xlsx")) { // Excel file
                activityIntent("file", url)
            } else if (testReportFileType.contains("jpg") || testReportFileType.contains("jpeg") || testReportFileType.contains("png")) {
                activityIntent("image", url)
            } else if (testReportFileType.contains("txt")) { // Text file
                activityIntent("file", url)
            }
        } catch (e: ActivityNotFoundException) {
            Toast.makeText(mContext, "No application found which can open the file", Toast.LENGTH_SHORT).show()
        }
    }

    private fun activityIntent(type: String?, url: String?) {
        startActivity(Intent(mContext, ViewImageAndFilesActivity::class.java)
                .putExtra("type", type)
                .putExtra("url", url)
        )


    }

}
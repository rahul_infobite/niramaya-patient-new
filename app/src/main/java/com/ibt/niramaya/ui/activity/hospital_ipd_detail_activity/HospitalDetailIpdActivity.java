package com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital.Ipd;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.ConnectionDetector;

import de.hdodenhof.circleimageview.CircleImageView;

public class HospitalDetailIpdActivity extends BaseActivity implements View.OnClickListener {
    private Ipd ipd;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_idp_full_detail);
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        init();
    }

    private void init() {
        Intent intent = getIntent();
        if (getIntent() != null) {
            ipd = intent.getParcelableExtra("hospitalIpd");
        }

        Toolbar toolBarHospitalIpdDetail = findViewById(R.id.toolBarHospitalIpdDetail);
        toolBarHospitalIpdDetail.setTitle("Ipd Detail");
        setSupportActionBar(toolBarHospitalIpdDetail);
        toolBarHospitalIpdDetail.setNavigationIcon(R.drawable.ic_back);
        toolBarHospitalIpdDetail.setNavigationOnClickListener(v -> onBackPressed());
        getMainOperationData();
    }

    private void getMainOperationData() {

        if (ipd != null) {
            if (ipd.getPatientProfile() != null) {
                findViewById(R.id.imgProfile).setVisibility(View.VISIBLE);
                Glide.with(mContext).load(ipd.getPatientProfile()).placeholder(R.drawable.ic_profile).into(((CircleImageView) findViewById(R.id.imgProfile)));
            } else {
                findViewById(R.id.imgProfile).setVisibility(View.GONE);
            }
            String strType = ipd.getIpdAdmissionType();
            if (strType.equals("0")) {
                strType = "OPD";
            } else if (strType.equals("1")) {
                strType = "Pharmacy";
            } else if (strType.equals("2")) {
                strType = "Pathology";
            } else if (strType.equals("3")) {
                strType = "IPD";
            } else {
                strType = "";
            }

            ((TextView) findViewById(R.id.tvPtName)).setText(ipd.getPatientName());
            ((TextView) findViewById(R.id.tvDrName)).setText(ipd.getDoctorName());
            ((TextView) findViewById(R.id.tvRefferdDrNameName)).setText(ipd.getIpdReferredDrName());
            ((TextView) findViewById(R.id.tvpatienaadhar)).setText(ipd.getIpdPatientAdharNumber());
            ((TextView) findViewById(R.id.tvPatientGender)).setText(ipd.getPatientGender());
            ((TextView) findViewById(R.id.tvContact)).setText(ipd.getPatientContact());
            ((TextView) findViewById(R.id.tvAddmitionType)).setText(strType);
            ((TextView) findViewById(R.id.tvdischargeType)).setText(ipd.getDischargeType());
            ((TextView) findViewById(R.id.tvDischarByDr)).setText(ipd.getDischargeByDoctorName());
            ((TextView) findViewById(R.id.tvIpdDAteTiem)).setText(ipd.getDischargeDateTime());
            ((TextView) findViewById(R.id.tvPaymentLimit)).setText(ipd.getIpdPaymentLimit());
            if (!ipd.getIpdDocument().isEmpty()){
                Glide.with(mContext).load(ipd.getIpdDocument()).into((ImageView) findViewById(R.id.imgDocument));
            }
        }

    }

    @Override
    public void onClick(View v) {
    }


}

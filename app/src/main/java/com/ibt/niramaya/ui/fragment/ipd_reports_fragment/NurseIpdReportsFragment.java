package com.ibt.niramaya.ui.fragment.ipd_reports_fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.NurseIpdReportListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.nurse_ipd_reporting.IpdReportList;
import com.ibt.niramaya.modal.nurse_ipd_reporting.NurseIpdReportingMainModal;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.NurseIpdReportDetailActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class NurseIpdReportsFragment extends BaseFragment implements View.OnClickListener {
    private View rootView;
    private NurseIpdReportListAdapter nurseIpdReportListAdapter;
    private List<IpdReportList> ipdReportLists = new ArrayList<>();
    private String patientId = "", patientName = "", ipdId = "", staffAllotmentId = "", ipdDate = "", dob = "";
    private String PatientGender = "", PatientContact = "";
    private TextView txtMessage;
    private String patientNumber;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_nurse_report_ipd, container, false);
        mContext = getActivity();
        mContext = getContext();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        Bundle bundle = getArguments();
        if (bundle != null) {
            patientId = bundle.getString("PatientId");
            patientName = bundle.getString("PatientName");
            ipdId = bundle.getString("ipdId");
            dob = bundle.getString("PatientDOB");
            PatientGender = bundle.getString("PatientGender");
            PatientContact = bundle.getString("PatientContact");
            ipdDate = bundle.getString("ipdDate");
        }

        txtMessage = rootView.findViewById(R.id.txtMessage);
        txtMessage.setVisibility(View.GONE);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        txtMessage.setVisibility(View.GONE);
        selectNurseReportingApi();
    }

    private void selectNurseReportingApi() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.selectNurseIpdReport(new Dialog(mContext), retrofitApiClient.selectNurseIpdReport(ipdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ipdReportLists.clear();
                    NurseIpdReportingMainModal mainModal = (NurseIpdReportingMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getIpdReportList() != null) {
                            RecyclerView rvViewReports = rootView.findViewById(R.id.rvPastHistory);

                            rvViewReports.setHasFixedSize(true);

                            rvViewReports.setLayoutManager(new GridLayoutManager(mContext, 2));
                            nurseIpdReportListAdapter = new NurseIpdReportListAdapter(ipdReportLists, mContext, patientId, patientName, ipdDate, NurseIpdReportsFragment.this);
                            rvViewReports.setAdapter(nurseIpdReportListAdapter);
                            ipdReportLists.addAll(mainModal.getIpdReportList());
                            nurseIpdReportListAdapter.notifyDataSetChanged();
                        } else {
                            txtMessage.setVisibility(View.VISIBLE);
                            txtMessage.setText(mainModal.getMessage());
                        }
                    } else {
                        txtMessage.setVisibility(View.VISIBLE);
                        txtMessage.setText(mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");

                    /*txtMessage.setVisibility(View.VISIBLE);
                    txtMessage.setText(error);   */
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtOpen:
                int tag = (int) v.getTag();
                Intent intent1 = new Intent(mContext, NurseIpdReportDetailActivity.class);
                intent1.putExtra("ipdReportList", ipdReportLists.get(tag));
                intent1.putExtra("dob", dob);
                intent1.putExtra("pname", patientName);
                intent1.putExtra("pid", patientId);
                intent1.putExtra("ipdDate", ipdDate);
                startActivity(intent1);
                break;
        }

    }
}

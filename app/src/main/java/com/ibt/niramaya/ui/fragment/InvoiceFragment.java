package com.ibt.niramaya.ui.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.tabs.TabLayout;
import androidx.viewpager.widget.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.InvoicePagerAdapter;
import com.ibt.niramaya.utils.BaseFragment;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;

public class InvoiceFragment extends BaseFragment implements View.OnClickListener {
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_invoice, container, false);
        init();
        return rootView;
    }

    private void init() {
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.VISIBLE);

        ImageView ivFilter = getActivity().findViewById(R.id.ivFilter);
        ivFilter.setVisibility(View.GONE);

        setViewPager();
    }

    private void setViewPager() {
        ViewPager viewPager = rootView.findViewById(R.id.viewPager);
        TabLayout tab = rootView.findViewById(R.id.tabs);

        if (viewPager != null) {
            InvoicePagerAdapter invoicePagerAdapter = new InvoicePagerAdapter(getChildFragmentManager());
            viewPager.setAdapter(invoicePagerAdapter);
            tab.setupWithViewPager(viewPager);
            tab.setTabGravity(TabLayout.GRAVITY_FILL);
        }
    }


    @Override
    public void onClick(View v) {

    }
}

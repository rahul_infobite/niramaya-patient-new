package com.ibt.niramaya.ui.fragment.ipd_reports_fragment;

import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.DoctorIpdPrescriptionListAdapter;
import com.ibt.niramaya.modal.ipd_prescription.list.IpdPreception;
import com.ibt.niramaya.modal.ipd_prescription.list.PatientPastPrescriptionModel;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;
import java.util.ArrayList;

import retrofit2.Response;

public class PrescriptionReportsFragmentDoctor extends BaseFragment implements View.OnClickListener {
    private View rootView;
    private DoctorIpdPrescriptionListAdapter doctorIpdPrescriptionListAdapter;
    private ArrayList<IpdPreception> ipdReportLists = new ArrayList<>();
    private String patientId = "", patientName = "", ipdId = "", staffAllotmentId = "", ipdDate = "", dob;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ipd_past_prescription, container, false);
        mContext = getContext();
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        Bundle bundle = getArguments();
        if (bundle != null) {
            patientId = bundle.getString("PatientId");
            patientName = bundle.getString("PatientName");
            ipdId = bundle.getString("ipdId");
            dob = bundle.getString("PatientDOB");
            staffAllotmentId = bundle.getString("staffAllotmentId");
            ipdDate = bundle.getString("ipdDate");
        }
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        selectDoctroPrescriptionApi();
    }

    private void selectDoctroPrescriptionApi() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.selectDoctorPrescription(new Dialog(mContext), retrofitApiClient.selectDoctorPrescription(ipdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ipdReportLists.clear();
                    PatientPastPrescriptionModel mainModal = (PatientPastPrescriptionModel) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getIpdPreception().size() > 0) {
                           ipdReportLists = (ArrayList<IpdPreception>) mainModal.getIpdPreception();
                            RecyclerView rvViewReports = rootView.findViewById(R.id.rvIpdList);
                            rvViewReports.setHasFixedSize(true);
                            rvViewReports.setLayoutManager(new GridLayoutManager(mContext, 2));
                            doctorIpdPrescriptionListAdapter = new DoctorIpdPrescriptionListAdapter(ipdReportLists, mContext, patientId, patientName, ipdDate, PrescriptionReportsFragmentDoctor.this);
                            rvViewReports.setAdapter(doctorIpdPrescriptionListAdapter);
                            doctorIpdPrescriptionListAdapter.notifyDataSetChanged();
                        } else {
                            Alerts.show(mContext, mainModal.getMessage());
                        }
                    }else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");                }
            });
        }
    }

    @Override
    public void onClick(View v) {

    }
}

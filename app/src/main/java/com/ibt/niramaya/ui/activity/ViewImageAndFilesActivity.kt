package com.ibt.niramaya.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.ibt.niramaya.R
import kotlinx.android.synthetic.main.activity_view_image_and_files.*

class ViewImageAndFilesActivity : AppCompatActivity() {
    private var myWebView: WebView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_image_and_files)

        toolbarReports.setNavigationOnClickListener { finish() }
        val intent: Intent = intent
        if (intent != null) {
            val type: String = intent.getStringExtra("type")
            val url: String = intent.getStringExtra("url")
            if (type == "image") {
                imageViewReports.visibility = View.VISIBLE
                Glide.with(this).load(url).placeholder(R.drawable.ic_profile).into(imageViewReports)
            } else {
                webViewFiles.visibility = View.VISIBLE
                openDefferentFileFormates(url)
            }
        }
    }

    override fun onBackPressed() {
        finish()
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun openDefferentFileFormates(url1: String) {
        webViewFiles.settings.javaScriptEnabled = true
        webViewFiles.settings.loadWithOverviewMode = true;
        webViewFiles.settings.useWideViewPort = true;
        val url = "http://docs.google.com/gview?embedded=true&url=$url1"
        webViewFiles.loadUrl(url)
        webViewFiles.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return false
            }

            override fun onPageFinished(view: WebView, url: String) {
                myWebView = null
            }
        }

      /*  webViewFiles.settings.loadWithOverviewMode = true
        webViewFiles.settings.useWideViewPort = true
        val newUA =
                "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0"
        webViewFiles.settings.userAgentString = newUA
        webViewFiles.loadUrl(url1)

        myWebView = webViewFiles
*/
    }
}

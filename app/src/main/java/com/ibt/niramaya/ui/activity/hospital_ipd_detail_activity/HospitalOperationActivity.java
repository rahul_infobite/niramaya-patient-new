package com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_opetation_detail_modal.IpdOperationDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_opetation_detail_modal.OperationSchedule;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.ConnectionDetector;

import retrofit2.Response;

public class HospitalOperationActivity extends BaseActivity implements View.OnClickListener {
    private IpdOperationDetailMainModal mainModal;
    private String strId = "";
    private TextView tvOd, tvPd;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_operation_detail);
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        init();
    }

    private void init() {

        Intent intent = getIntent();
        if (getIntent() != null) {
            strId = intent.getStringExtra("id");
        }

        Toolbar toolBarHospitalIpdDetail = findViewById(R.id.toolBarHospitalIpdDetail);
        toolBarHospitalIpdDetail.setTitle("Ipd Operation Detail");
        setSupportActionBar(toolBarHospitalIpdDetail);
        toolBarHospitalIpdDetail.setNavigationIcon(R.drawable.ic_back);
        toolBarHospitalIpdDetail.setNavigationOnClickListener(v -> onBackPressed());

        findViewById(R.id.llPatientDetails).setOnClickListener(this);
        findViewById(R.id.llOperationDetail).setOnClickListener(this);

        tvOd = findViewById(R.id.tvOD);
        tvPd = findViewById(R.id.tvPD);

        fatchOpetationDetail();
    }

    @SuppressLint("SetTextI18n")
    private void getMainOperationData() {
        if (!mainModal.getError()) {
            OperationSchedule operationSchedule = mainModal.getOperationSchedule();
            if (operationSchedule != null) {

                String strType = operationSchedule.getType();
                switch (strType) {
                    case "0":
                        strType = "Pharmacy";
                        break;
                    case "1":
                        strType = "Pathology";
                        break;
                    case "2":
                        strType = "IPD";
                        break;
                    case "3":
                        strType = "IPD slip bill";
                        break;
                    case "4":
                        strType = "Bed Bill";
                        break;
                    case "5":
                        strType = "Operation Bill";
                        break;
                    default:
                        strType = "";
                        break;
                }


                ((TextView) findViewById(R.id.tvPtName)).setText(operationSchedule.getPatientName());
                ((TextView) findViewById(R.id.tvSurgeonName)).setText(operationSchedule.getSurgeon());
                ((TextView) findViewById(R.id.bedNumber)).setText(operationSchedule.getBedNumber());
                ((TextView) findViewById(R.id.tvAssistantName)).setText(operationSchedule.getAssistant());
                ((TextView) findViewById(R.id.tvSurgeryDateTime)).setText(operationSchedule.getSurgeryDate()
                        + " " + operationSchedule.getSurgeryTime());
                ((TextView) findViewById(R.id.roomNumber)).setText(operationSchedule.getRoomName());
                ((TextView) findViewById(R.id.tvBedCategory)).setText(operationSchedule.getCategoryTitle());
                ((TextView) findViewById(R.id.equipment)).setText(operationSchedule.getEquipment());
                ((TextView) findViewById(R.id.operationType)).setText(strType);
                ((TextView) findViewById(R.id.tvAnesthetic)).setText(operationSchedule.getAnesthetic());
                ((TextView) findViewById(R.id.tvTreatmentGiven)).setText(operationSchedule.getTreatmentGiven());
                ((TextView) findViewById(R.id.tvTreatmentAdvice)).setText(operationSchedule.getTreatmentAdvice());
                ((TextView) findViewById(R.id.tvDiagnosis)).setText(operationSchedule.getDiagnosis());
                ((TextView) findViewById(R.id.txtMedicalProcedure)).setText(operationSchedule.getMedicalProcedure());
                ((TextView) findViewById(R.id.txtRevices)).setText(operationSchedule.getRevices());
                ((TextView) findViewById(R.id.txtRemarks)).setText(operationSchedule.getRevices());
                String dischargeType = operationSchedule.getDischargedType();
                if (dischargeType.equals("0")) {
                    dischargeType = "Normal";
                } else {
                    dischargeType = "Emergency";
                }
                ((TextView) findViewById(R.id.dischargeType)).setText(dischargeType);
            }
        }

    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.llPatientDetails:
                tvOd.setTextColor(getResources().getColor(R.color.gray_f));
                tvPd.setTextColor(getResources().getColor(R.color.white));
                tvOd.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tvPd.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                findViewById(R.id.rlOD).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                findViewById(R.id.rlPD).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                findViewById(R.id.viewPatientDetail).setVisibility(View.VISIBLE);
                findViewById(R.id.viewOprDetail).setVisibility(View.GONE);
                findViewById(R.id.llTvPatientDetail).setVisibility(View.VISIBLE);
                findViewById(R.id.llTvOperationDetail).setVisibility(View.GONE);
                break;
            case R.id.llOperationDetail:
                tvOd.setTextColor(getResources().getColor(R.color.white));
                tvPd.setTextColor(getResources().getColor(R.color.gray_f));
                tvOd.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                tvPd.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                findViewById(R.id.rlOD).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                findViewById(R.id.rlPD).setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                findViewById(R.id.viewPatientDetail).setVisibility(View.GONE);
                findViewById(R.id.viewOprDetail).setVisibility(View.VISIBLE);
                findViewById(R.id.llTvOperationDetail).setVisibility(View.VISIBLE);
                findViewById(R.id.llTvPatientDetail).setVisibility(View.GONE);
                break;
        }
    }

    private void fatchOpetationDetail() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalOperationScheduleDetailList(new Dialog(mContext), retrofitApiClient.getHospitalOperationDetail(strId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    mainModal = (IpdOperationDetailMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getOperationSchedule() != null) {
                            getMainOperationData();
                        }
                        Alerts.show(mContext, mainModal.getMessage());
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }

}

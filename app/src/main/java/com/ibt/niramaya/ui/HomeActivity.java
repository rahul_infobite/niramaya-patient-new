package com.ibt.niramaya.ui;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.viewpager.widget.ViewPager;

import com.bumptech.glide.Glide;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.InstallStateUpdatedListener;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.InstallStatus;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.home.TokenPageSliderAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.patient_modal.PaitentProfile;
import com.ibt.niramaya.modal.patient_modal.PatientMainModal;
import com.ibt.niramaya.modal.token.TokenDatum;
import com.ibt.niramaya.modal.token.TokenModel;
import com.ibt.niramaya.retrofit.RetrofitApiClient;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.LoginActivity;
import com.ibt.niramaya.ui.activity.ambulance.AmbulanceDetailActivity;
import com.ibt.niramaya.ui.fragment.AboutUsFragment;
import com.ibt.niramaya.ui.fragment.AmbulanceFragment;
import com.ibt.niramaya.ui.fragment.AppointmentFragment;
import com.ibt.niramaya.ui.fragment.BedFragment;
import com.ibt.niramaya.ui.fragment.HomeFragment;
import com.ibt.niramaya.ui.fragment.InvoiceFragment;
import com.ibt.niramaya.ui.fragment.PatientFinanceDetailFragment;
import com.ibt.niramaya.ui.fragment.PatientFragment;
import com.ibt.niramaya.ui.fragment.PatientIpdListFragment;
import com.ibt.niramaya.ui.fragment.PatientReportIpdListFragment;
import com.ibt.niramaya.ui.fragment.PrescriptionsFragment;
import com.ibt.niramaya.ui.fragment.ReportFragment;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.ConnectionDetector;
import com.ibt.niramaya.utils.FixedSpeedScroller;
import com.ibt.niramaya.utils.FragmentUtils;
import com.yarolegovich.slidingrootnav.SlidingRootNav;
import com.yarolegovich.slidingrootnav.SlidingRootNavBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    private AppUpdateManager mAppUpdateManager;
    private InstallStateUpdatedListener installStateUpdatedListener;
    private int RC_APP_UPDATE = 123;
    public RetrofitApiClient retrofitApiClient;
    public ConnectionDetector cd;
    public Context mContext;
    @SuppressLint("StaticFieldLeak")
    public static TextView txtTitle;
    @SuppressLint("StaticFieldLeak")
    public static ImageView imgSearch, imgSort;
    private SlidingRootNav slidingRootNav;
    private Spinner spnPatient;
    private CardView cvPatient;
    private TextView txtName, txtPhone;
    private CircleImageView imgProfile;
    private FragmentUtils fragmentUtils;
    private FragmentManager fragmentManager;
    private List<PaitentProfile> patientList = new ArrayList<>();
    private BroadcastReceiver mRegistrationBroadcastReceiver;
    private ArrayList<TokenDatum> tokenList = new ArrayList<>();
    private ViewPager tokenPager;
    private RelativeLayout rlBottom;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        mContext = this;
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        /*******************************************************
         * _____________ play store in app update ________________________*/


        playStoreUpdateChekcing();
        callingInstallUpdateListner();

        /**************** close ______________________________________*/
        init(savedInstanceState);
    }

    private void initTokenSlider() {


        rlBottom.setVisibility(View.VISIBLE);

        tokenPager = findViewById(R.id.tokenPager);

        TokenPageSliderAdapter tokenSliderAdapter = new TokenPageSliderAdapter(
                mContext, tokenList, tokenListener);
        tokenPager.setAdapter(tokenSliderAdapter);
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new SliderTimer(), 4000, 6000);

        try {
            Field mScroller;
            mScroller = ViewPager.class.getDeclaredField("mScroller");
            mScroller.setAccessible(true);
            FixedSpeedScroller scroller = new FixedSpeedScroller(tokenPager.getContext());
            //FixedSpeedScroller scroller = new FixedSpeedScroller(tokenPager.getContext(), new AccelerateInterpolator());
            // scroller.setFixedDuration(5000);
            mScroller.set(tokenPager, scroller);
        } catch (NoSuchFieldException ignored) {
        } catch (IllegalArgumentException ignored) {
        } catch (IllegalAccessException ignored) {
        }

    }

    private class SliderTimer extends TimerTask {
        @Override
        public void run() {
            HomeActivity.this.runOnUiThread(() -> {
                if (tokenPager.getCurrentItem() < tokenList.size() - 1) {
                    tokenPager.setCurrentItem(tokenPager.getCurrentItem() + 1);
                } else {
                    tokenPager.setCurrentItem(0);
                }
            });
        }

    }

    private void init(Bundle savedInstanceState) {
        imgSearch = findViewById(R.id.imgSearch);
        imgSort = findViewById(R.id.imgSort);
        txtTitle = findViewById(R.id.txtTitle);
        rlBottom = findViewById(R.id.rlBottom);

        fragmentManager = getSupportFragmentManager();
        fragmentUtils = new FragmentUtils(fragmentManager);
        Intent intent = getIntent();
        if (intent.getStringExtra("from") != null) {
            String fromOpen = intent.getStringExtra("from");
            if (fromOpen.equals("otp")) {
                fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            }
        } else if ((getIntent().getExtras()).getString("data") != null) {
            String value = getIntent().getExtras().getString("data");
            try {
                JSONObject valueObject = new JSONObject(value);
                String bookingType = valueObject.getString("booking_type");
                fragmentUtils.replaceFragment(new AppointmentFragment(), Constant.AppointmentFragment, R.id.home_frame);

            } catch (JSONException e) {
                e.printStackTrace();
            }
            //Alerts.show(mContext, value);
        }


        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        slidingRootNav = new SlidingRootNavBuilder(this)
                .withToolbarMenuToggle(toolbar)
                .withMenuOpened(false)
                .withContentClickableWhenMenuOpened(true)
                .withSavedState(savedInstanceState)
                .withMenuLayout(R.layout.menu_left_drawer)
                .inject();

        spnPatient = findViewById(R.id.spnPatient);
        cvPatient = findViewById(R.id.cvPatient);

        txtPhone = findViewById(R.id.txtPhone);
        txtName = findViewById(R.id.txtName);
        imgProfile = findViewById(R.id.imgProfile);

        txtName.setText(AppPreference.getStringPreference(mContext, Constant.USER_NAME));
        txtPhone.setText(AppPreference.getStringPreference(mContext, Constant.USER_CONTACT));
        String userProfileImage = AppPreference.getStringPreference(mContext, Constant.USER_PROFILE_IMAGE);
        Glide.with(mContext)
                .load(userProfileImage)
                .placeholder(R.drawable.ic_profile)
                .into(imgProfile);

        String firebaseToken = AppPreference.getStringPreference(mContext, Constant.FIREBASE_TOKEN);
        Log.e("myToken", firebaseToken);
        // Alerts.show(mContext, firebaseToken);

        //  initPatientSpinner();

        clickListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        initPatientSpinner();
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver, new IntentFilter("appointment"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mMessageReceiver);
    }

    private void initPatientSpinner() {
        if (cd.isNetworkAvailable()) {
            String strUserId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
            RetrofitService.getPatientList(new Dialog(mContext), retrofitApiClient.patientList(strUserId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    PatientMainModal mainModal = (PatientMainModal) result.body();
                    if (mainModal != null) {
                        patientList = mainModal.getUser().getPaitentProfile();

                        for (PaitentProfile pt : patientList) {
                            if (pt.getPatientId().equals(strUserId)) {
                                AppPreference.setStringPreference(mContext, Constant.USER_PROFILE_IMAGE, pt.getPatientProfilePicture());
                                Glide.with(mContext)
                                        .load(pt.getPatientProfilePicture())
                                        .placeholder(R.drawable.ic_profile)
                                        .into(imgProfile);
                            }
                        }

                        PaitentProfile paitentProfile1 = new PaitentProfile();
                        paitentProfile1.setPatientId("0");
                        paitentProfile1.setPatientName("Select Patient");
                        patientList.add(0, paitentProfile1);

                        ArrayAdapter aa = new ArrayAdapter<>(mContext, R.layout.row_spinner_item, patientList);
                        //aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        spnPatient.setAdapter(aa);
                        if (patientList.size() > 0) {
                            for (int i = 0; i < patientList.size(); i++) {
                                String cPatientId = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);
                                if (patientList.get(i).getPatientId().equals(cPatientId)) {
                                    spnPatient.setSelection(i);
                                    txtName.setText(patientList.get(i).getPatientName());
                                    txtPhone.setText(patientList.get(i).getPatientNumber());
                                    AppPreference.setStringPreference(mContext, Constant.CURRENT_PATENT_ID, patientList.get(i).getPatientId());
                                    AppPreference.setStringPreference(mContext, Constant.CURRENT_PATENT_NUMBER, patientList.get(i).getPatientNumber());
                                    AppPreference.setStringPreference(mContext, Constant.CURRENT_PATENT_NAME, patientList.get(i).getPatientName());
                                }
                            }
                            spnPatient.setVisibility(View.VISIBLE);
                            cvPatient.setVisibility(View.VISIBLE);
                        } else {
                            spnPatient.setVisibility(View.GONE);
                            cvPatient.setVisibility(View.GONE);
                        }
                        spnPatient.setOnItemSelectedListener(spinnerListener);
                    } else {
                        Alerts.show(mContext, Objects.requireNonNull(mainModal).getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }

    private void clickListener() {
        findViewById(R.id.txtHome).setOnClickListener(this);
        findViewById(R.id.txtPrescription).setOnClickListener(this);
        findViewById(R.id.txtReports).setOnClickListener(this);
        findViewById(R.id.txtInvoice).setOnClickListener(this);
        findViewById(R.id.txtFinance).setOnClickListener(this);
        findViewById(R.id.txtBed).setOnClickListener(this);
        findViewById(R.id.txtIpdDetail).setOnClickListener(this);
        findViewById(R.id.txtIpdReports).setOnClickListener(this);
        findViewById(R.id.txtDocuments).setOnClickListener(this);
        findViewById(R.id.txtSettings).setOnClickListener(this);
        findViewById(R.id.txtAddUser).setOnClickListener(this);
        findViewById(R.id.llLogout).setOnClickListener(this);
        findViewById(R.id.txtAmbulance).setOnClickListener(this);
        findViewById(R.id.txtAppointment).setOnClickListener(this);
        findViewById(R.id.txtAboutUs).setOnClickListener(this);
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        Fragment AddUserFragment = fragmentManager.findFragmentByTag(Constant.AddUserFragment);
        Fragment AboutUsFragment = fragmentManager.findFragmentByTag(Constant.AboutUsFragment);
        Fragment BedFragment = fragmentManager.findFragmentByTag(Constant.BedFragment);
        Fragment PatientIpdListFragment = fragmentManager.findFragmentByTag(Constant.PatientIpdListFragment);
        Fragment HomeFragment = fragmentManager.findFragmentByTag(Constant.HomeFragment);
        Fragment InvoiceFragment = fragmentManager.findFragmentByTag(Constant.InvoiceFragment);
        Fragment PatientFinanceDetailFragment = fragmentManager.findFragmentByTag(Constant.PatientFinanceDetailFragment);
        Fragment DocumentsFragment = fragmentManager.findFragmentByTag(Constant.DocumentsFragment);
        Fragment PrescriptionFragment = fragmentManager.findFragmentByTag(Constant.PrescriptionFragment);
        Fragment ReportFragment = fragmentManager.findFragmentByTag(Constant.ReportsFragment);
        Fragment AmbulanceFragment = fragmentManager.findFragmentByTag(Constant.AmbulanceFragment);
        Fragment AppointmentFragment = fragmentManager.findFragmentByTag(Constant.AppointmentFragment);
        Fragment PatientReportIpdListFragment = fragmentManager.findFragmentByTag(Constant.PatientReportIpdListFragment);

        String patientId = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);

        switch (v.getId()) {
            case R.id.txtHome:
                txtTitle.setText("Home");
                if (HomeFragment == null) {
                    fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;
            case R.id.txtPrescription:
                txtTitle.setText("Prescription");
                if (PrescriptionFragment == null) {
                    if (!patientId.isEmpty() && !patientId.equals("0")) {
                        fragmentUtils.replaceFragment(new PrescriptionsFragment(), Constant.PrescriptionFragment, R.id.home_frame);
                        slidingRootNav.closeMenu();
                    } else {
                        Alerts.show(mContext, "No Patient Selected!");
                    }
                }
                break;
            case R.id.txtReports:
                txtTitle.setText("Report");
                if (ReportFragment == null) {
                    if (!patientId.isEmpty() && !patientId.equals("0")) {
                        fragmentUtils.replaceFragment(new ReportFragment(), Constant.ReportsFragment, R.id.home_frame);
                        slidingRootNav.closeMenu();
                    } else {
                        Alerts.show(mContext, "No Patient Selected!");
                    }
                }
                break;
            case R.id.txtInvoice:
                txtTitle.setText("Invoice");
                if (InvoiceFragment == null) {
                    if (!patientId.isEmpty() && !patientId.equals("0")) {
                        fragmentUtils.replaceFragment(new InvoiceFragment(), Constant.InvoiceFragment, R.id.home_frame);
                        slidingRootNav.closeMenu();
                    } else {
                        Alerts.show(mContext, "No Patient Selected!");
                    }
                }
                break;
            case R.id.txtFinance:
                txtTitle.setText("Finance");
                if (PatientFinanceDetailFragment == null) {
                    if (!patientId.isEmpty() && !patientId.equals("0")) {
                        fragmentUtils.replaceFragment(new PatientFinanceDetailFragment(), Constant.PatientFinanceDetailFragment, R.id.home_frame);
                        slidingRootNav.closeMenu();
                    } else {
                        Alerts.show(mContext, "No Patient Selected!");
                    }
                }
                break;
            case R.id.txtBed:
                txtTitle.setText("Bed History");
                if (BedFragment == null) {
                    fragmentUtils.replaceFragment(new BedFragment(), Constant.BedFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;
            case R.id.txtIpdDetail:
                txtTitle.setText("IPD Data");
                if (PatientIpdListFragment == null) {
                    if (!patientId.isEmpty() && !patientId.equals("0")) {
                        fragmentUtils.replaceFragment(new PatientIpdListFragment(), Constant.PatientIpdListFragment, R.id.home_frame);
                        slidingRootNav.closeMenu();
                    } else {
                        Alerts.show(mContext, "No Patient Selected!");
                    }
                }
                break;
            case R.id.txtIpdReports:
                txtTitle.setText("IPD Data");
                if (PatientReportIpdListFragment == null) {
                    if (!patientId.isEmpty() && !patientId.equals("0")) {
                        fragmentUtils.replaceFragment(new PatientReportIpdListFragment(), Constant.PatientReportIpdListFragment, R.id.home_frame);
                        slidingRootNav.closeMenu();
                    } else {
                        Alerts.show(mContext, "No Patient Selected!");
                    }
                }
                break;
          /*  case R.id.txtBloodDonation:
                txtTitle.setText("Blood Donation");
                if (BloodDonationFragment == null) {
                    fragmentUtils.replaceFragment(new BloodDonationFragment(), Constant.BloodDonationFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;*/
      /*      case R.id.txtDocuments:
                txtTitle.setText("Documents");
                if (DocumentsFragment == null) {
                    fragmentUtils.replaceFragment(new DocumentsFragment(), Constant.DocumentsFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;*/
            case R.id.txtSettings:
                break;
            case R.id.txtAddUser:
                txtTitle.setText("Add Patient");
                if (AddUserFragment == null) {
                    fragmentUtils.replaceFragment(new PatientFragment(), Constant.AddUserFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;
            case R.id.txtAmbulance:
                txtTitle.setText("Ambulance");
                if (AmbulanceFragment == null) {
                    fragmentUtils.replaceFragment(new AmbulanceFragment(), Constant.AmbulanceFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;
            case R.id.txtAppointment:
                txtTitle.setText("Appointment");
                if (AppointmentFragment == null) {
                    fragmentUtils.replaceFragment(new AppointmentFragment(), Constant.AppointmentFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;
            case R.id.txtAboutUs:
                txtTitle.setText("About Us");
                if (AboutUsFragment == null) {
                    fragmentUtils.replaceFragment(new AboutUsFragment(), Constant.AboutUsFragment, R.id.home_frame);
                    slidingRootNav.closeMenu();
                }
                break;
            case R.id.llLogout:
                doLogout();
                slidingRootNav.closeMenu();
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBackPressed() {
        Fragment AddUserFragment = fragmentManager.findFragmentByTag(Constant.AddUserFragment);
        Fragment BedFragment = fragmentManager.findFragmentByTag(Constant.BedFragment);
        Fragment HomeFragment = fragmentManager.findFragmentByTag(Constant.HomeFragment);
        Fragment InvoiceFragment = fragmentManager.findFragmentByTag(Constant.InvoiceFragment);
        Fragment PatientFinanceDetailFragment = fragmentManager.findFragmentByTag(Constant.PatientFinanceDetailFragment);
        Fragment PrescriptionFragment = fragmentManager.findFragmentByTag(Constant.PrescriptionFragment);
        Fragment ReportFragment = fragmentManager.findFragmentByTag(Constant.ReportsFragment);
        Fragment AmbulanceFragment = fragmentManager.findFragmentByTag(Constant.AmbulanceFragment);
        Fragment AppointmentFragment = fragmentManager.findFragmentByTag(Constant.AppointmentFragment);
        Fragment AboutUsFragment = fragmentManager.findFragmentByTag(Constant.AboutUsFragment);
        Fragment PatientIpdListFragment = fragmentManager.findFragmentByTag(Constant.PatientIpdListFragment);
        Fragment PatientReportIpdListFragment = fragmentManager.findFragmentByTag(Constant.PatientReportIpdListFragment);

        if (HomeFragment != null) {
            finish();
        } else if (PrescriptionFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (PatientFinanceDetailFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (ReportFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (PatientIpdListFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (PatientReportIpdListFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (InvoiceFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (BedFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (AmbulanceFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } /*else if (BloodDonationFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        */ else if (AppointmentFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (AboutUsFragment != null) {
            txtTitle.setText("Home");
            fragmentUtils.replaceFragment(new HomeFragment(), Constant.HomeFragment, R.id.home_frame);
            slidingRootNav.closeMenu();
        } else if (slidingRootNav.isMenuOpened()) {
            slidingRootNav.closeMenu();
        } else {
            finish();
        }
    }

    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @SuppressLint("SetTextI18n")
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position > 0) {
                txtName.setText(patientList.get(position).getPatientName());
                txtPhone.setText(patientList.get(position).getPatientNumber());
            } else {
                txtName.setText(AppPreference.getStringPreference(mContext, Constant.USER_NAME));
                txtPhone.setText(AppPreference.getStringPreference(mContext, Constant.USER_CONTACT));
            }
            AppPreference.setStringPreference(mContext, Constant.CURRENT_PATENT_ID,
                    patientList.get(position).getPatientId());
            AppPreference.setStringPreference(mContext, Constant.CURRENT_PATENT_NAME,
                    patientList.get(position).getPatientName());

            //  Fragment HomeFragment = fragmentManager.findFragmentByTag(Constant.HomeFragment);

            initTokenView(patientList.get(position).getPatientId());

            /*if (HomeFragment == null) {
                onBackPressed();
            }*/

            /*if (slidingRootNav.isMenuOpened()) {
                slidingRootNav.closeMenu();
            }*/

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private void initTokenView(String patientId) {
        if (cd.isNetworkAvailable()) {
            RetrofitService.patientToken(new Dialog(mContext), retrofitApiClient.patientToken(patientId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    TokenModel tokenModel = (TokenModel) result.body();
                    assert tokenModel != null;
                    if (!tokenModel.getError()) {
                        if (tokenModel.getData().size() > 0) {
                            tokenList = (ArrayList<TokenDatum>) tokenModel.getData();
                            initTokenSlider();
                        } else {
                            rlBottom.setVisibility(View.GONE);
                        }
                    } else {
                        Alerts.show(mContext, tokenModel.getMessage());
                        rlBottom.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    rlBottom.setVisibility(View.GONE);
                }
            });
        }
    }

    private View.OnClickListener tokenListener = v -> {
        switch (v.getId()) {
            case R.id.ivRefresh:
                initTokenView(AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID));
                break;
            case R.id.rlRoot:
                int tag = (int) v.getTag();
                TokenDatum tokenData = tokenList.get(tag);
                break;
        }
    };

    private void doLogout() {
        new AlertDialog.Builder(mContext)
                .setTitle("Logout")
                .setMessage("Are you sure want to doLogout ?")
                .setPositiveButton("YES", (dialog, which) -> {
                    AppPreference.clearAllPreferences(mContext);
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();
                })
                .setNegativeButton("NO", null)
                .create()
                .show();
    }


    private void playStoreUpdateChekcing() {
        mAppUpdateManager = AppUpdateManagerFactory.create(mContext);

        mAppUpdateManager.registerListener(installStateUpdatedListener);

        mAppUpdateManager.getAppUpdateInfo().addOnSuccessListener(appUpdateInfo -> {

            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE
                    && appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)) {
                try {
                    mAppUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo, AppUpdateType.FLEXIBLE, (Activity) mContext, RC_APP_UPDATE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackbarForCompleteUpdate();
            } else {
                Log.e("UpdateCheck", "checkForAppUpdateAvailability: something else");
            }
        });
    }

    private void callingInstallUpdateListner() {
        installStateUpdatedListener = state -> {
            if (state.installStatus() == InstallStatus.DOWNLOADED) {
                popupSnackbarForCompleteUpdate();
            } else if (state.installStatus() == InstallStatus.INSTALLED) {
                if (mAppUpdateManager != null) {
                    mAppUpdateManager.unregisterListener(installStateUpdatedListener);
                }

            } else {
                Log.i("UpdateState", "InstallStateUpdatedListener: state: " + state.installStatus());
            }
        };
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_APP_UPDATE) {
            if (resultCode != RESULT_OK) {
                Toast.makeText(this,
                        "App Update failed, please try again on the next app launch.",
                        Toast.LENGTH_SHORT)
                        .show();
            } else {
                Log.e("updateResults", "onActivityResult: app download failed");
            }
        } else {
        }
    }

    private void popupSnackbarForCompleteUpdate() {
        Snackbar snackbar =
                Snackbar.make(findViewById(R.id.drawer_layout),
                        "New app is ready!",
                        Snackbar.LENGTH_INDEFINITE);
        Toast.makeText(mContext, "New app is ready!", Toast.LENGTH_LONG).show();
        snackbar.setAction("Install", view -> {
            if (mAppUpdateManager != null) {
                mAppUpdateManager.completeUpdate();
            }
        });
        snackbar.setActionTextColor(getResources().getColor(R.color.black));
        snackbar.show();
    }

/*
    private void imediateAppUpdate() {
        mAppUpdateManager
                .getAppUpdateInfo()
                .addOnSuccessListener(new OnSuccessListener<AppUpdateInfo>() {
                    @Override
                    public void onSuccess(AppUpdateInfo result) {
                        if (getApplicationInfo().ge.installStatus() == InstallStatus.DOWNLOADED) {
                            popupSnackbarForCompleteUpdate()
                        }
                    }
                }) {
            appUpdateInfo ->

            // If the update is downloaded but not installed,
            // notify the user to complete the update.


            //Check if Immediate update is required
            try {
                if (appUpdateInfo.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                    // If an in-app update is already running, resume the update.
                    appUpdateManager.startUpdateFlowForResult(
                            appUpdateInfo,
                            AppUpdateType.IMMEDIATE,
                            this,
                            APP_UPDATE_REQUEST_CODE)
                }
            } catch (e:IntentSender.SendIntentException){
                e.printStackTrace()
            }
        }
    }
*/
    /*private void registerBrodcast() {
        mRegistrationBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {


                if (intent.getAction().equals(Constant.PUSH_NOTIFICATION)) {
                    // new push notification is received

                    String message = intent.getStringExtra("message");

                    //Toast.makeText(getApplicationContext(), "Push notification: " + message, Toast.LENGTH_LONG).show();



                }
            }
        };
        displayFirebaseRegId();

    }

    private void displayFirebaseRegId() {
        *//*String regId = AppPreference.getStringPreference(mContext, Constant.FIREBASE_TOKEN);

        Alerts.show(mContext,regId);*//*

    }*/

    /******************************************************************
     * Block for performing action when specific notification received
     *******************************************************************/

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String payloadStatus = intent.getExtras().getString("PayloadStatus");
            fragmentUtils.replaceFragment(new AppointmentFragment(), Constant.AppointmentFragment, R.id.home_frame);
        }
        /*override fun onReceive(p0: Context?, p1: Intent?) {
            val payloadData = p1!!.extras!!.getString("PayloadData")
            //Toast.makeText(mContext, "Notification Received : $PayloadData", Toast.LENGTH_SHORT).show()
            replaceFragment(HomeFragment(), Constant.HomeFragment)
            //Alerts.showAlertMessage(mContext, "Notification Received", payloadData)

        }*/
    };


}

package com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.HospitalIpdDetailAdapter.HospitalIpdMedicineDetailAdapter;
import com.ibt.niramaya.adapter.HospitalIpdDetailAdapter.HospitalPathologyIpdDetailAdapter;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.IpdSlip;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.IpdSlipDetailMainModal;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.PathologyTest;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_slip_detail_modal.Preception;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Response;

public class HospitalIpdActivity extends BaseActivity implements View.OnClickListener {
    private IpdSlipDetailMainModal mainModal;
    private HospitalIpdMedicineDetailAdapter ipdMedicineDetailAdapter;
    private HospitalPathologyIpdDetailAdapter pathologyIpdDetailAdapter;
    private List<Preception> medicineList = new ArrayList<>();
    private List<PathologyTest> pathologyTests = new ArrayList<>();
    private String strId = "";

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_ipd_detail2);
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        init();
    }

    private void init() {

        Intent intent = getIntent();
        if (getIntent() != null) {
            strId = intent.getStringExtra("id");
        }

        Toolbar toolBarHospitalIpdDetail = findViewById(R.id.toolBarHospitalIpdDetail);
        toolBarHospitalIpdDetail.setTitle("Ipd Detail");
        setSupportActionBar(toolBarHospitalIpdDetail);
        toolBarHospitalIpdDetail.setNavigationIcon(R.drawable.ic_back);
        toolBarHospitalIpdDetail.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        RecyclerView rvMedicine = findViewById(R.id.rvMedicine);
        rvMedicine.setHasFixedSize(true);
        rvMedicine.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        ipdMedicineDetailAdapter = new HospitalIpdMedicineDetailAdapter(medicineList, mContext, this);
        rvMedicine.setAdapter(ipdMedicineDetailAdapter);
        ipdMedicineDetailAdapter.notifyDataSetChanged();

        RecyclerView rvTest = findViewById(R.id.rvTest);
        rvTest.setHasFixedSize(true);
        rvTest.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        pathologyIpdDetailAdapter = new HospitalPathologyIpdDetailAdapter(pathologyTests, mContext, this);
        rvTest.setAdapter(pathologyIpdDetailAdapter);
        pathologyIpdDetailAdapter.notifyDataSetChanged();

        fatchIpdDetail();
    }

    @SuppressLint("SetTextI18n")
    private void getMainIpdData() {
        if (!mainModal.getError()) {
            IpdSlip ipdSlip = mainModal.getIpdSlip();
            if (ipdSlip != null) {
                ((TextView) findViewById(R.id.tvPtName)).setText(ipdSlip.getIpdPatientName());
                ((TextView) findViewById(R.id.tvAadhar)).setText(ipdSlip.getIpdPatientAdharNumber());
                ((TextView) findViewById(R.id.tvPtAge)).setText(getAge(ipdSlip.getIpdPatientDob()) + " Year");
                ((TextView) findViewById(R.id.tvPtGender)).setText(ipdSlip.getPatientGender());
                ((TextView) findViewById(R.id.tvDoctorName)).setText(ipdSlip.getStaffName());
                ((TextView) findViewById(R.id.tvBp)).setText(ipdSlip.getBp());
                ((TextView) findViewById(R.id.tvVitals)).setText(ipdSlip.getVitals());
                ((TextView) findViewById(R.id.tvHeartRate)).setText(ipdSlip.getHeartRatePerMin());
                ((TextView) findViewById(R.id.tvResp)).setText(ipdSlip.getRespRateMin());
                ((TextView) findViewById(R.id.tvTemp)).setText(ipdSlip.getTemp());
                ((TextView) findViewById(R.id.tvPainScore)).setText(ipdSlip.getPainScore());
            }
        }

    }

    /***********************************************
     * Start Method block for calculating age
     **************************************************/

    private String getAge(String dobString) {
        Date date = null;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return String.valueOf(0);

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age1 = (today.get(Calendar.YEAR) - dob.get(Calendar.YEAR));
        int month1 = (today.get(Calendar.MONTH) - dob.get(Calendar.MONTH));
        String age = (age1 + "." + month1);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
        }


        return age;
    }


    @Override
    public void onClick(View v) {
    }

    private void fatchIpdDetail() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalIpdSlipDetailList(new Dialog(mContext), retrofitApiClient.getHospitalIpdDetail(strId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    mainModal = (IpdSlipDetailMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getIpdSlip() != null) {
                            getMainIpdData();
                            if (mainModal.getIpdSlip().getPreception() != null) {
                                medicineList.addAll(mainModal.getIpdSlip().getPreception());
                                ipdMedicineDetailAdapter.notifyDataSetChanged();
                            } else {
                                Alerts.show(mContext, mainModal.getMessage());
                            }
                            if (mainModal.getIpdSlip().getPathologyTest() != null) {
                                pathologyTests.addAll(mainModal.getIpdSlip().getPathologyTest());
                                pathologyIpdDetailAdapter.notifyDataSetChanged();
                            } else {
                                Alerts.show(mContext, mainModal.getMessage());
                            }
                        }
                        Alerts.show(mContext, mainModal.getMessage());
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }

}

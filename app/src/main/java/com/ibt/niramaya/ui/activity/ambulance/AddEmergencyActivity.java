package com.ibt.niramaya.ui.activity.ambulance;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.GpsTracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class AddEmergencyActivity extends BaseActivity implements View.OnClickListener {
    private double sLatitude = 0.0;
    private double sLongitude = 0.0;
    private LatLng latLng;
    private EditText etEmergencyAddress;
    private String address;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_emergency);

        onclickIds();
        getLatLong();

    }

    private void onclickIds() {
        findViewById(R.id.backEmergency).setOnClickListener(this);
        findViewById(R.id.icEmergencyBook).setOnClickListener(this);
        findViewById(R.id.btnAddEmergency).setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.backEmergency:
                finish();
                break;
            case R.id.btnAddEmergency:
                addEmergencyApi();
                break;
           /* case R.id.icEmergencyBook:
                openEmergencyDialog();
                break;*/
        }
    }

    private void getLatLong() {
        GpsTracker gpsTracker = new GpsTracker(this);
        sLatitude = gpsTracker.getLatitude();
        sLongitude = gpsTracker.getLongitude();
        latLng = new LatLng(sLatitude, sLongitude);
        getAddressList(sLatitude, sLongitude);
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void openEmergencyDialog() {
        AlertDialog.Builder dialogBox = new AlertDialog.Builder(mContext);
        dialogBox.setCancelable(true);

        LayoutInflater li = LayoutInflater.from(mContext);
        @SuppressLint("InflateParams") final View dialogView = li.inflate(R.layout.dialog_add_emergency, null);
        dialogBox.setView(dialogView);
        final AlertDialog alertDialog = dialogBox.create();
        alertDialog.show();
        etEmergencyAddress = dialogView.findViewById(R.id.etPatientAddress);
        //   dialogEmergencyData();

        Button btnBook = dialogView.findViewById(R.id.btnAddEmergency);

        //   distance = calculateDistance(sLatitude, sLongitude, dLatitude, dLongitude);

        btnBook.setOnClickListener(v -> {
            String patientAge = ((EditText) dialogView.findViewById(R.id.etPatientAge)).getText().toString();
            //   addEmergencyApi(alertDialog, patientAge);
        });
    }

    private void dialogEmergencyData() {
        Places.initialize(mContext, getResources().getString(R.string.google_key));

        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        assert autocompleteFragment != null;
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                LatLng latLng = place.getLatLng();
                assert latLng != null;
                double lat = latLng.latitude;
                double longi = latLng.longitude;
                Log.e("lat : ", String.valueOf(lat));
                Log.e("longi : ", String.valueOf(longi));
                sLatitude = latLng.latitude;
                sLongitude = latLng.longitude;
                getAddressList(sLatitude, sLongitude);
            }

            @Override
            public void onError(Status status) {
                Alerts.show(mContext, status.getStatusMessage());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    private void getAddressList(final Double latitude, final Double longitude) {
        //AppProgressDialog.show(dialog);
        Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                // AppProgressDialog.hide(dialog);

                String area = addresses.get(0).getAddressLine(0);
                String strCity = "" + addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String zipcode = addresses.get(0).getPostalCode();
                address = area + "" + strCity + "" + state + "" + country + "" + zipcode;

             /*   etEmergencyAddress.setVisibility(View.VISIBLE);
                etEmergencyAddress.setText(area + "," + strCity + "," + state + "," + country + "," + zipcode);*/
            } else {
                // AppProgressDialog.show(dialog);
                new Handler().postDelayed(() -> getAddressList(latitude, longitude), 3000);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void addEmergencyApi() {
        if (cd.isNetworkAvailable()) {
            String userId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
            String userContact = AppPreference.getStringPreference(mContext, Constant.USER_CONTACT);
            String userName = AppPreference.getStringPreference(mContext, Constant.USER_NAME);
            String latitute = String.valueOf(sLatitude);
            String longitute = String.valueOf(sLongitude);
            String patientAge = ((EditText) findViewById(R.id.etPatientAge)).getText().toString();
            RetrofitService.getServerResponse(new Dialog(mContext), retrofitApiClient.addEmergencyData(userName, patientAge, userContact, address, latitute, longitute), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ResponseBody responseData = (ResponseBody) result.body();
                    try {
                        assert responseData != null;
                        JSONObject jsonObject = new JSONObject(responseData.string());
                        Alerts.show(mContext, jsonObject.getString("message"));
                        if (!jsonObject.getBoolean("error")) {
                            AppPreference.setBooleanPreference(mContext, Constant.PATIENT_AMBULANCE_BOOKING_STATUS, true);
                            AppPreference.setStringPreference(mContext, Constant.PATIENT_BOOKED_BY_USER_CONTACT, userContact);
                            AppPreference.setStringPreference(mContext, Constant.PATIENT_BOOKING_LATITUDE, String.valueOf(sLatitude));
                            AppPreference.setStringPreference(mContext, Constant.PATIENT_BOOKING_LONGITUDE, String.valueOf(sLongitude));
                            startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                                    .putExtra("From", "emergency")
                                    .putExtra("userContact", userContact)
                                    .putExtra("Latitude", sLatitude)
                                    .putExtra("Longitude", sLongitude));
                            finish();

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailed(String error) {

                }
            });
        }
    }

    @Override
    public void onBackPressed() {
        finish();
    }
}

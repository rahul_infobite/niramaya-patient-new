package com.ibt.niramaya.ui.activity;

import android.app.Dialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.doctor_opd.SpecialDoctorOpdListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.home.SystemSpecialization;
import com.ibt.niramaya.modal.specialization.all.SpecialistDoctorDatum;
import com.ibt.niramaya.modal.specialization.all.SpecialistDoctorModel;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.invoice_data.BookAppointmentActivityKt;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;

import java.util.ArrayList;
import java.util.Objects;

import retrofit2.Response;

public class SpecialistDoctorActivity extends BaseActivity implements View.OnClickListener {

    private SystemSpecialization specialization;
    private RecyclerView rvSpecialization;
    private ArrayList<SpecialistDoctorDatum> doctorList;
    private SpecialDoctorOpdListAdapter specialAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_peditrician);
        specialization = Objects.requireNonNull(getIntent().getExtras()).getParcelable("SPECIALIZATION");

        getAllSpecialDoctor();

        init();
    }

    private void getAllSpecialDoctor() {
        String userId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
        if (cd.isNetworkAvailable()){
            RetrofitService.getSpecializationDoctor(new Dialog(mContext), retrofitApiClient.getSpecialistDoctor(
                    specialization.getSpecializationId(), userId, ""), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    SpecialistDoctorModel specialModel = (SpecialistDoctorModel) result.body();
                    assert specialModel != null;
                    if (!specialModel.getError()) {
                        if (specialModel.getDoctorData().size()>0) {
                            doctorList = (ArrayList<SpecialistDoctorDatum>) specialModel.getDoctorData();
                            rvSpecialization = findViewById(R.id.rvSpecialization);
                            rvSpecialization.setLayoutManager(new LinearLayoutManager(mContext));
                             specialAdapter = new SpecialDoctorOpdListAdapter(
                                   doctorList, mContext, SpecialistDoctorActivity.this, specialization.getSpecializationTitle()
                            );
                            rvSpecialization.setAdapter(specialAdapter);
                            specialAdapter.notifyDataSetChanged();
                        }
                    }
                }

                @Override
                public void onResponseFailed(String error) {

                }
            });
        }
    }

    private void init() {
        findViewById(R.id.imgSearch).setVisibility(View.GONE);
        findViewById(R.id.imgFilter).setVisibility(View.GONE);

        TextView txtTitle = findViewById(R.id.txtTitle);
        ImageView imgBack = findViewById(R.id.imgBack);

        Toolbar toolbar = findViewById(R.id.toolbar1);
        toolbar.setTitle(specialization.getSpecializationTitle());
        toolbar.setNavigationIcon(R.drawable.ic_back_b);
        toolbar.setNavigationOnClickListener(v -> finish());
        setSupportActionBar(toolbar);


        txtTitle.setText(specialization.getSpecializationTitle());
        imgBack.setOnClickListener((v -> onBackPressed()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBookAppointment:
                int tag = (int) v.getTag();
                startActivity(new Intent(mContext, BookAppointmentActivityKt.class)
                        .putExtra("DoctorId", doctorList.get(tag).getDoctorId())
                        .putExtra("HospitalId", doctorList.get(tag).getHospitalId()));
                break;
        }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.search:
          /*      etSearch.setVisibility(View.VISIBLE);
                findViewById(R.id.tvTitle).setVisibility(View.GONE);*/
                break;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_navigation_search, menu);
        MenuItem searchItem = menu.findItem(R.id.search);

        SearchManager searchManager = (SearchManager) SpecialistDoctorActivity.this.getSystemService(Context.SEARCH_SERVICE);

        SearchView searchView = null;

        searchView = (SearchView) menu.findItem(R.id.search).getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                specialAdapter.getFilter().filter(query);
                specialAdapter.notifyDataSetChanged();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                specialAdapter.getFilter().filter(newText);
                specialAdapter.notifyDataSetChanged();
                return false;
            }
        });


        return true;
    }
}

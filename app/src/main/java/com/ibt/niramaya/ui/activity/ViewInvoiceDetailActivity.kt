package com.ibt.niramaya.ui.activity

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.pdf.PdfDocument
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.print.PrintAttributes
import android.print.PrintDocumentAdapter
import android.print.PrintManager
import android.view.View
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import com.bumptech.glide.Glide
import com.ibt.niramaya.R
import com.ibt.niramaya.utils.Alerts
import com.ibt.niramaya.utils.BaseActivity
import kotlinx.android.synthetic.main.activity_view_invoice_detail.*
import java.io.File
import java.io.FileOutputStream

class ViewInvoiceDetailActivity : BaseActivity() {
    private var myWebView: WebView? = null
    private var invoiceLink = ""
    private var invoiceNumber = ""

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_view_invoice_detail)

        toolbarInvoiceDetail.setNavigationOnClickListener { finish() }

        val intent = intent
        if (intent != null) {
            invoiceLink = intent.getStringExtra("billLInk")!!
            invoiceNumber = intent.getStringExtra("invoiceNumber")!!
            tvTitle.text = invoiceNumber
        }
        init()
    }
    override fun onBackPressed() {
        finish()
    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun init() {
        webView.webViewClient = object : WebViewClient() {

            override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                return false
            }

            override fun onPageFinished(view: WebView, url: String) {
                myWebView = null
            }
        }

        webView.settings.loadWithOverviewMode = true
        webView.settings.useWideViewPort = true
        val newUA =
                "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0"
        webView.settings.userAgentString = newUA
        webView.loadUrl(invoiceLink)

        myWebView = webView



        imgShare.setOnClickListener {
            webviewtoPdf()
        }

        btnPrintInvoice.setOnClickListener {
            try {
                runOnUiThread {
                    createWebPrintJob(webView!!)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }

    }

    @RequiresApi(Build.VERSION_CODES.KITKAT)
    private fun webviewtoPdf() {
        //Create PDF document
        val doc = PdfDocument();

        //Create A4 sized PDF page
        val pageInfo = PdfDocument.PageInfo.Builder(595, 842, 1).create();

        val page: PdfDocument.Page = doc.startPage(pageInfo);


        page.getCanvas().setDensity(200);

        //Draw the webview to the canvas
        webView.draw(page.getCanvas());

        doc.finishPage(page);

        val root: File = Environment.getExternalStorageDirectory();
        val file = File(root, "$invoiceNumber.pdf");
        val out: FileOutputStream = FileOutputStream(file)
        doc.writeTo(out);
        shareFile(file)
        out.close()
        doc.close()

    }

    private fun shareFile(file: File) {
        val uri = FileProvider.getUriForFile(this, applicationContext.packageName + ".provider", file)

        val shareIntent = Intent(Intent.ACTION_SEND)
        shareIntent.type = "application/octet-stream"

        shareIntent.putExtra(Intent.EXTRA_STREAM, uri)
        startActivityForResult(Intent.createChooser(shareIntent, "Backup"), 222)

    }


    @SuppressLint("NewApi")
    private fun createWebPrintJob(webView: WebView) {
        val jobName = "NHS-$invoiceNumber"

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            /*Context c = webview.getContext();*/
            var printAdapter: PrintDocumentAdapter
            val printManager = getSystemService(Context.PRINT_SERVICE) as PrintManager
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                printAdapter = webView.createPrintDocumentAdapter(jobName);
            } else {
                printAdapter = webView.createPrintDocumentAdapter();
            }
            if (printManager != null) {
                printManager.print(jobName, printAdapter, PrintAttributes.Builder().build())
            }
        } else {
            Alerts.show(mContext, "ERROR: Method called on too low Android API version");
        }


    }
}

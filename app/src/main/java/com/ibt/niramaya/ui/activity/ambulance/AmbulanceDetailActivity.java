package com.ibt.niramaya.ui.activity.ambulance;

import android.Manifest;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.bumptech.glide.Glide;
import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.ambulance.DriverLocation;
import com.ibt.niramaya.modal.ambulance.ongoing.AmbulanceBookingList;
import com.ibt.niramaya.modal.ambulance.ongoing.AmbulanceOngoingModel;
import com.ibt.niramaya.modal.driver.driver_list_modal.DriverList;
import com.ibt.niramaya.modal.driver.driver_list_modal.DriverMainModal;
import com.ibt.niramaya.retrofit.RetrofitApiClient;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.GpsTracker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class AmbulanceDetailActivity extends BaseActivity implements LocationListener,
        OnMapReadyCallback, RoutingListener {

    private RetrofitApiClient client;
    private DriverMainModal mainModal;
    private List<DriverList> driverListsUpdate = new ArrayList<>();
    private GoogleMap mMap;
    private static final String TAG = "TAG";
    private boolean isUp = false;

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private String from = "";
    private double userLatitude = 0;
    private double userLongitude = 0;
    private String patientID = "";


    private double latitude = 0.0;
    private double longitude = 0.0;
    private double driverOldLatitude = 0.0;
    private double driverOldLongitude = 0.0;
    private long UPDATE_INTERVAL = 2000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private LocationRequest mLocationRequest;
    private static final int DEFAULT_ZOOM_LEVEL = 14;
    private LatLng latLng;
    private LatLng uLatLng;
    private Marker mMarker;
    private ArrayList<DriverLocation> driverList;
    private ArrayList<DriverLocation> activeDriverList;
    private List<Polyline> polylines = new ArrayList<>();
    private static final int[] COLORS = new int[]{R.color.colorAccent};
    private Marker driverMarker;
    private boolean isMarkerRotating = false;
    private RelativeLayout
            cvDriverDetail;
    private String driverId;
    private DriverLocation driverLocation;

    private MarkerOptions dOptions;
    private Marker dMarker;
    private TextView txtTitle;
    private ImageView imgBack;
    private float start_rotation = 0;
    private float rotationBearing = 0;
    private LatLng endPosition;
    private Timer onGoingTimer;
    private int count = 0;


    private LatLng oldPos;
    private float v;
    private int mTimerCount = 0;
    private ArrayList<LatLng> coordinates = new ArrayList<>();
    ArrayList<LatLng> routeLL = new ArrayList<>();
    private TextView tvBookingStatus;
    // emergency

    private String userContact;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance_detail);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);


        if ((getIntent().getExtras())!=null) {
            if ((getIntent().getExtras()).getString("From") != null) {
                from = getIntent().getExtras().getString("From");
                if (from.equals("listing")) {
                    patientID = getIntent().getExtras().getString("PatientId");
                    fetchUserOnGoing();
                } else {
                    userContact = getIntent().getExtras().getString("userContact");
                    fetchUserEmergencyOnGoing();
                }
            } else if (getIntent().getExtras().getString("data") != null) {
                /*for (String key : getIntent().getExtras().keySet()) {
                    String value = getIntent().getExtras().getString(key);
                    Log.d(TAG, "Key: " + key + " Value: " + value);
                }*/
                String value = getIntent().getExtras().getString("data");
                try {
                    JSONObject valueObject = new JSONObject(value);
                    String bookingType = valueObject.getString("booking_type");
                    if (bookingType.equals("0")) {
                        from = "listing";
                    } else {
                        from = "emergency";
                    }

                    if (from.equals("listing")) {
                        patientID = valueObject.getString("booking_type_id");
                        fetchUserOnGoing();
                    } else {
                        userContact = valueObject.getString("booking_type_id");
                        fetchUserEmergencyOnGoing();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Alerts.show(mContext, value);
            }
        }

        /*String nTitle = getIntent().getExtras().getString("data");
        Alerts.show(mContext, nTitle);*/

        /*userLatitude = getIntent().getExtras().getDouble("Latitude");
        userLongitude = getIntent().getExtras().getDouble("Longitude");
        uLatLng = new LatLng(userLatitude, userLongitude);*/

        Toolbar toolbar = findViewById(R.id.toolbar);
        showHomeBackOnToolbar(toolbar);

        cvDriverDetail = findViewById(R.id.cvDriverDetail);
        tvBookingStatus = findViewById(R.id.tvBookingStatus);
        txtTitle = findViewById(R.id.txtTitle);

        /*imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(v -> onBackPressed());*/

        (findViewById(R.id.btnClose)).setOnClickListener(v -> {
            cvDriverDetail.setVisibility(View.GONE);
        });

        /***********************************************************
         *   ongoing job api blog
         * ********************************************************/

        (findViewById(R.id.imgRefresh)).setOnClickListener(v -> {

            if (from.equals("listing")) {
                fetchUserOnGoing();
            } else {
                fetchUserEmergencyOnGoing();
            }
        });

        /*******************************************************
         *  end
         * **********************************************/

        //driverId = getIntent().getExtras().getString("DRIVER_ID");

        onGoingTimer = new Timer();
        /*onGoingBookingTimer();*/

    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(mContext).unregisterReceiver(mMessageReceiver);
        /*onGoingTimer.cancel();
        onGoingTimer.purge();
        Alerts.show(mContext, "Timer Stopped");
        finish();
        System.exit(0);*/
    }

    private void onGoingBookingTimer() {
        onGoingTimer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                //Alerts.show(mContext, "30 Seconds");
                runOnUiThread(() -> {
                    fetchUserOnGoing();
                    /*count++;
                    Alerts.show(mContext, "5 Seconds");
                    if (count==5){
                        onGoingTimer.cancel();
                        onGoingTimer.purge();
                        Alerts.show(mContext, "Timer Stopped");
                    }*/
                });
            }
        }, 0, 5000);
    }

    private void initFirebaseDatabase(String dId) {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("driver");

        mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                driverList = new ArrayList<>();
                activeDriverList = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    DriverLocation dl = postSnapshot.getValue(DriverLocation.class);
                    //driverList.add(dl);
                    assert dl != null;
                    if (dl.getDriverId().equals(dId)) {
                        driverLocation = dl;
                        txtTitle.setText(dl.getDriverName());
                        if (driverOldLatitude > 0 && driverOldLatitude > 0 && dMarker != null) {
                            LatLng startPosition = new LatLng(driverOldLatitude, driverOldLongitude);
                            endPosition = new LatLng(driverLocation.getDriverLat(), driverLocation.getDriverLong());

                            /*if (mTimerCount==0){
                                coordinates.add(endPosition);
                            }else{
                                coordinates.add(0, oldPos);
                                coordinates.add(1, coordinates.get(mTimerCount+1));
                            }*/
/*
<<<<<<< HEAD
                            if (mTimerCount==0){
                                routeLL.add(endPosition);
                                if (routeLL.size()>1){
=======*/
                            if (mTimerCount == 0) {
                                routeLL.add(latLng);
                                if (routeLL.size() > 1) {
                                    animateCarOnMap(routeLL);
                                }
                            } else {
                                routeLL.add(0, oldPos);
                                routeLL.add(1, endPosition);
                                animateCarOnMap(routeLL);
                            }

                            //animateMarkerToGB(dMarker, endPosition);
                            Location tempLocation = new Location(LocationManager.GPS_PROVIDER);
                            tempLocation.setLatitude(endPosition.latitude);
                            tempLocation.setLongitude(endPosition.longitude);


                            String msg = "Lat 1 : " + driverOldLatitude + "\nLong 1 : " + driverOldLongitude +
                                    "\nLat 2 : " + endPosition.latitude + "\nLong 2 : " + endPosition.longitude;

                        }
                        createRoute();
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


    }

    private void getLatLong() {
        GpsTracker gpsTracker = new GpsTracker(this);
        latitude = gpsTracker.getLatitude();
        longitude = gpsTracker.getLongitude();
        latLng = new LatLng(latitude, longitude);
        getAddressList();
    }

    private void getAddressList() {
        // AppProgressDialog.show(dialog);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses.size() > 0) {
                //  AppProgressDialog.hide(dialog);
            } else {
                // AppProgressDialog.show(dialog);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        getLatLong();
                    }
                }, 3000);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        //latLng = new LatLng(location.getLatitude(), location.getLongitude());

        // getDriverListDataApi();

        /*if (latitude > 0) {
            addMarker(latLng);
        }*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Alerts.show(this, "Please enable location permission...!!!");
            return;
        }

        getLatLong();
        if (latLng != null) {
            latitude = latLng.latitude;
            longitude = latLng.longitude;
        }
        //if (latitude > 0) {
        if (uLatLng!=null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(uLatLng, 15);
            mMap.animateCamera(cameraUpdate);
            addMarker(uLatLng);
        }else{
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.animateCamera(cameraUpdate);
            addMarker(latLng);
        }
        //}


    }

    private void addMarker(LatLng location) {
        if (mMap != null) {
            MarkerOptions mMarkerOption = new MarkerOptions();
            mMarkerOption.position(location);
            //mMarkerOption.title("My location");
            mMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_placeholder));
            removeMarker();
            mMarker = mMap.addMarker(mMarkerOption);

            /*driverList.add(new DriverLocation("1", "Driver 1", 22.72591965, 75.88166714));
            driverList.add(new DriverLocation("1", "Driver 2", 22.72370295, 75.88887691));
            driverList.add(new DriverLocation("1", "Driver 3", 22.7228321, 75.87831974));
            driverList.add(new DriverLocation("1", "Driver 4", 22.7301946, 75.88527203));
            driverList.add(new DriverLocation("2", "Driver 5", 22.71507333, 75.88278294));*/

            /*for(int i = 0 ; i < driverList.size() ; i++) {
                createMarker(driverList.get(i).getDriverLat(), driverList.get(i).getDriverLong(),
                        driverList.get(i).getDriverName(), driverList.get(i).getDriverName());
            }

            createRoute();*/

        }
    }

    private void removeMarker() {
        if (mMap != null && mMarker != null) {
            mMarker.remove();
        }
    }

    /*************************************************************
     *
     ************************************************************/
    protected Marker createMarker(double latitude, double longitude, String title, String snippet) {

        MarkerOptions dMarkerOption = new MarkerOptions();
        dMarkerOption.position(new LatLng(latitude, longitude));
        dMarkerOption.anchor(0.5f, 0.5f);
        dMarkerOption.title(title);
        dMarkerOption.snippet(snippet);
        dMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ambulance));
        //dMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ambulance_marker_pin));

        return mMap.addMarker(dMarkerOption);
    }


    @SuppressLint("SetTextI18n")
    private void createRoute() {
        if (driverLocation != null) {
            double distance = calculateDistance(userLatitude, userLongitude, driverLocation.getDriverLat(), driverLocation.getDriverLong());
            @SuppressLint("DefaultLocale") String strDistance = String.format("%.2f", distance);
            ((TextView) (findViewById(R.id.tvPatientDistance))).setText("Distance : " + strDistance + "km");
        }
        Routing routing = new Routing.Builder()
                .key("AIzaSyBvaYGedz5oMgLpYMF42wtJE8VIT28juM8")//NonRestrictedKey
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(false)
                .waypoints(new LatLng(driverLocation.getDriverLat(), driverLocation.getDriverLong()),
                        new LatLng(userLatitude, userLongitude))
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int p1) {

        if (mMap != null) {
            mMap.clear();
        }
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(latitude, longitude));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        /*bound camera between these lat lng*/
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(new LatLng(userLatitude, userLongitude));
        builder.include(new LatLng(driverLocation.getDriverLat(), driverLocation.getDriverLong()));
        LatLngBounds bounds = builder.build();
        int padding = 100; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.animateCamera(cu);
        //mMap.moveCamera(cu);


        driverOldLatitude = driverLocation.getDriverLat();
        driverOldLongitude = driverLocation.getDriverLong();

        /*CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(driverLocation.getDriverLat(), driverLocation.getDriverLong()), 15);
        //mMap.animateCamera(cameraUpdate);
        mMap.moveCamera(cameraUpdate);*/


        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);

            //Toast.makeText(getApplicationContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }

        // Start marker
        MarkerOptions options = new MarkerOptions();
        options.position(new LatLng(userLatitude, userLongitude));
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_placeholder));
        mMap.addMarker(options);

        // End marker
        if (dMarker != null) {
            dMarker.remove();
        }
        dOptions = new MarkerOptions();
        dOptions.position(new LatLng(driverLocation.getDriverLat(), driverLocation.getDriverLong()));
        dOptions.anchor(0.5f, 0.5f);
        dOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.ambulance));
        dMarker = mMap.addMarker(dOptions);
        /*if (endPosition!=null && rotationBearing>0) {
            rotateMarker(dMarker, rotationBearing, start_rotation);
        }*/

        if (coordinates.size() > 1) {
            animateCarOnMap(coordinates);
        }


    }

    @Override
    public void onRoutingCancelled() {

    }

    /***********************************************************
     * Start block fir Moving Ambulance Smoothly
     ************************************************************/

    private void animateCarOnMap(final ArrayList<LatLng> latLngs) {

        mTimerCount++;
        LatLngBounds.Builder builder = new LatLngBounds.Builder();

        /*java.lang.IllegalArgumentException: latlng cannot be null - a position is required.
        at com.google.android.gms.maps.model.MarkerOptions.position(Unknown Source:90)
        at com.ibt.niramaya.ui.activity.ambulance.AmbulanceDetailActivity.animateCarOnMap(AmbulanceDetailActivity.java:505)*/

        if (mTimerCount == 1 && latLngs != null && latLngs.size() > 0) {
            if (latLngs.get(0) != null) {
                dMarker = mMap.addMarker(new MarkerOptions().position(latLngs.get(0))
                        .flat(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ambulance)));
            }
        }
        if (latLngs != null) {
            dMarker.setPosition(latLngs.get(0));
            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(2000);
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(valueAnimator1 -> {
                v = valueAnimator1.getAnimatedFraction();
                double lng = v * latLngs.get(1).longitude + (1 - v)
                        * latLngs.get(0).longitude;
                double lat = v * latLngs.get(1).latitude + (1 - v)
                        * latLngs.get(0).latitude;
                LatLng newPos = new LatLng(lat, lng);
                oldPos = newPos;
                dMarker.setPosition(newPos);
                dMarker.setAnchor(0.5f, 0.5f);
                dMarker.setRotation(getBearing(latLngs.get(0), newPos));
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition
                        (new CameraPosition.Builder().target(newPos)
                                .zoom(19f).build()));
            });
            valueAnimator.start();
        }
    }


    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }

    /*************************************************************
     * User Ongoing ride   0=> Pending, 1=> Approved, 2=> Ongoing,
     * 3=> Completed, 4=> Cancel by driver, 5=> Cancel by patient,
     * 6=> Reject
     *************************************************************/
    private void fetchUserOnGoing() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.patientOnGoingambulanceBooking(new Dialog(mContext), retrofitApiClient.ongoingAmbulance(patientID), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    AmbulanceOngoingModel ongoingModel = (AmbulanceOngoingModel) result.body();
                    assert ongoingModel != null;
                    if (!ongoingModel.getError()) {
                        String dId = ongoingModel.getAmbulanceBookingList().getDriver().getDriverId();
                        initFirebaseDatabase(dId);

                        String from = "";
                        String aStatus = ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus();
                        /*onGoingTimer.cancel();
                        onGoingTimer.purge();*/
                        // If booking Approved

                        double sLat = Double.parseDouble(ongoingModel.getAmbulanceBookingList().getAmbulanceBookingLatitude());
                        double sLong = Double.parseDouble(ongoingModel.getAmbulanceBookingList().getAmbulanceBookingLongitude());

                        userLatitude = sLat;
                        userLongitude = sLong;

                        double eLat = Double.parseDouble(ongoingModel.getAmbulanceBookingList().getAmbulanceBookingEndLatitude());
                        double eLong = Double.parseDouble(ongoingModel.getAmbulanceBookingList().getAmbulanceBookingEndLongitude());

                        uLatLng = new LatLng(sLat, sLong);
                        if (ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("0")) {
                            from = "pending";
                            initFirebaseDatabase(dId);
                        }
                        if (aStatus.equals("1")) {
                            from = "approved";
                            initFirebaseDatabase(dId);
                        } else if (aStatus.equals("2")) {
                            if (ongoingModel.getAmbulanceBookingList().getBookingDriveStatus().equals("1")) {
                                userLatitude = eLat;
                                userLongitude = eLong;
                                uLatLng = new LatLng(eLat, eLong);
                                initFirebaseDatabase(dId);
                                from = "pickUp";
                            } else {
                                from = "ongoing";
                            }
                        } else if (ongoingModel.getAmbulanceBookingList().getBookingDriveStatus().equals("2")) {
                            AppPreference.setBooleanPreference(mContext, Constant.PATIENT_AMBULANCE_BOOKING_STATUS, false);
                            finish();
                        }
                        initDriverDetail(ongoingModel.getAmbulanceBookingList(), from);
                    } else {
                        AppPreference.setBooleanPreference(mContext, Constant.PATIENT_AMBULANCE_BOOKING_STATUS, false);
                        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                        builder.setTitle("Alert");
                        builder.setMessage("There is no ambulance booking...");
                        // add a button
                        builder.setPositiveButton("OK", (dialog, which) -> {
                            finish();
                        });
                        // create and show the alert dialog
                        AlertDialog dialog = builder.create();
                        dialog.show();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Log.e("NIRAMAYA_PATIENT", "Server Error!!!");
                }
            });
        }
    }

    private void fetchUserEmergencyOnGoing() {
        if (cd.isNetworkAvailable()) {
            String userContact = AppPreference.getStringPreference(mContext, Constant.USER_CONTACT);
            RetrofitService.patientOnGoingambulanceBooking(new Dialog(mContext), retrofitApiClient.ongoingEnergencyAmbulance(userContact), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    AmbulanceOngoingModel ongoingModel = (AmbulanceOngoingModel) result.body();
                    assert ongoingModel != null;
                    if (!ongoingModel.getError()) {
                        String dId = ongoingModel.getAmbulanceBookingList().getDriver().getDriverId();
                        initFirebaseDatabase(dId);

                        String from = "";
                        /*onGoingTimer.cancel();
                        onGoingTimer.purge();*/
                        // If booking Approved
                        if (ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("0")) {
                            from = "pending";
                        }
                        if (ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("1")) {
                            from = "approved";
                        } else if (ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("2")) {
                            if (ongoingModel.getAmbulanceBookingList().getBookingDriveStatus().equals("1")) {
                              /*  userLatitude = Double.parseDouble(ongoingModel.getAmbulanceBookingList().getAmbulanceBookingEndLatitude());
                                userLongitude = Double.parseDouble(ongoingModel.getAmbulanceBookingList().getAmbulanceBookingEndLongitude());
                                initFirebaseDatabase(dId);*/
                                from = "pickUp";
                            } else {
                                from = "ongoing";
                            }
                        } else if (ongoingModel.getAmbulanceBookingList().getBookingDriveStatus().equals("2")) {
                            finish();
                        }
                        initDriverDetail(ongoingModel.getAmbulanceBookingList(), from);
                    } else {
                        finish();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Log.e("NIRAMAYA_PATIENT", "Server Error!!!");
                }
            });
        }
    }

    private void updateAmbulanceStatus(String ambulanceBookingId, String status) {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getServerResponse(new Dialog(mContext), retrofitApiClient.updateAmbulanceStatus(ambulanceBookingId, status), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ResponseBody responseBody = (ResponseBody) result.body();
                    try {
                        assert responseBody != null;
                        JSONObject jsonObject = new JSONObject(responseBody.string());
                        if (!jsonObject.getBoolean("error")) {
                            AppPreference.setBooleanPreference(mContext, Constant.PATIENT_AMBULANCE_BOOKING_STATUS, false);
                            Alerts.show(mContext, jsonObject.getString("message"));
                            finish();
                        } else {
                            Alerts.show(mContext, jsonObject.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }

    @SuppressLint("SetTextI18n")
    private void initDriverDetail(AmbulanceBookingList ambulance, String from) {
        // cvBottomAction, rlConfirm, rlName, imgCall, tvPatientDistance, btnCancel, tvDriverName
        ((TextView) (findViewById(R.id.tvDriverName))).setText("Driver Name : " + ambulance.getDriver().getName());
        ((TextView) (findViewById(R.id.tvRefferencePerson))).setText("Hospital Name : " + ambulance.getHospital().getHospitalName());
        if (ambulance.getHospitalAdmin().getName().isEmpty()) {
            (findViewById(R.id.tvHospitalAdmin)).setVisibility(View.GONE);
        }

        ((TextView) (findViewById(R.id.tvHospitalAdmin))).setText("Reference Person " + ambulance.getHospitalAdmin().getName());
        ((TextView) (findViewById(R.id.tvHospitalAddress))).setText(ambulance.getHospital().getHospitalHouseNumber() + "," +
                ambulance.getHospital().getHospitalStreetName() + "," + ambulance.getHospital().getHospitalCity() + "," + ambulance.getHospital().getHospitalState() + "," +
                ambulance.getHospital().getHospitalCountry() + "," + ambulance.getHospital().getHospitalZipcode());
        Glide.with(mContext).load(ambulance.getHospital().getHospialLogo()).placeholder(R.drawable.ic_profile).into((CircleImageView) (findViewById(R.id.ivHospitalImage)));

        if (from.equals("ongoing")) {
            findViewById(R.id.btnCancel).setVisibility(View.GONE);
            tvBookingStatus.setText("Ambulance Status : On The Way");
            findViewById(R.id.imgCall).setVisibility(View.VISIBLE);
            tvBookingStatus.setTextColor(getResources().getColor(R.color.green_dark));
        } else if (from.equals("pending")) {
            tvBookingStatus.setText("Ambulance Status : Pending");
            tvBookingStatus.setTextColor(getResources().getColor(R.color.yellow_d));
            findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);
        } else if (from.equals("approved")) {
            findViewById(R.id.imgCall).setVisibility(View.GONE);
            findViewById(R.id.btnCancel).setVisibility(View.VISIBLE);
            tvBookingStatus.setText("Ambulance Status : Approved");
            tvBookingStatus.setTextColor(getResources().getColor(R.color.green_dark));
        } else if (from.equals("pickUp")) {
            findViewById(R.id.btnCancel).setVisibility(View.GONE);
            findViewById(R.id.imgCall).setVisibility(View.GONE);
            tvBookingStatus.setText("Ambulance Status : Pick Up");
            tvBookingStatus.setTextColor(getResources().getColor(R.color.green_dark));
        }

        if (driverLocation != null) {
            double distance = calculateDistance(userLatitude, userLongitude, driverLocation.getDriverLat(), driverLocation.getDriverLong());
            @SuppressLint("DefaultLocale") String strDistance = String.format("%.2f", distance);
            ((TextView) (findViewById(R.id.tvPatientDistance))).setText("Distance : " + strDistance + "km");
        }
        findViewById(R.id.imgCall).setOnClickListener(v -> {
            if (ambulance.getDriver() != null) {
                String driverContact = ambulance.getDriver().getContact();
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + driverContact));
                startActivity(callIntent);
            }
        });
        findViewById(R.id.imgCallReffPerson).setOnClickListener(v -> {
            if (ambulance.getDriver() != null) {
                String hospitalContact = ambulance.getHospital().getHospitalContact();
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + hospitalContact));
                startActivity(callIntent);
            }
        });

        if (ambulance.getHospitalAdmin().getContact().isEmpty()) {
            findViewById(R.id.imgCallHospitalAdmin).setVisibility(View.GONE);
        }
        findViewById(R.id.imgCallHospitalAdmin).setOnClickListener(v -> {
            if (ambulance.getDriver() != null) {
                String hAdminContact = ambulance.getHospitalAdmin().getContact();
                Intent callIntent = new Intent(Intent.ACTION_VIEW);
                callIntent.setData(Uri.parse("tel:" + hAdminContact));
                startActivity(callIntent);
            }
        });
        findViewById(R.id.btnCancel).setOnClickListener(v -> {
            updateAmbulanceStatus(ambulance.getAmbulanceBookingId(), "5");
        });
    }

    /********************************************************************
     *
     **********************************************************************/
    private double calculateDistance(double uLat, double uLong, double dLat, double dLong) {
        Location locationA = new Location("point A");
        locationA.setLatitude(uLat);
        locationA.setLongitude(uLong);
        Location locationB = new Location("point B");
        locationB.setLatitude(dLat);
        locationB.setLongitude(dLong);

        double distance = (locationA.distanceTo(locationB)) / 1000;
        return distance;
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(mContext).registerReceiver(mMessageReceiver, new IntentFilter("ambulanceMap"));
    }

    /******************************************************************
     * Block for performing action when specific notification received
     *******************************************************************/

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String payloadStatus = intent.getExtras().getString("PayloadStatus");
            if (from.equals("listing")) {
                fetchUserOnGoing();
            } else {
                fetchUserEmergencyOnGoing();
            }
        }
        /*override fun onReceive(p0: Context?, p1: Intent?) {
            val payloadData = p1!!.extras!!.getString("PayloadData")
            //Toast.makeText(mContext, "Notification Received : $PayloadData", Toast.LENGTH_SHORT).show()
            replaceFragment(HomeFragment(), Constant.HomeFragment)
            //Alerts.showAlertMessage(mContext, "Notification Received", payloadData)

        }*/
    };

}

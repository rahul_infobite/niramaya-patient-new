package com.ibt.niramaya.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.AppointListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.appoint_list_main_modal.AppointmentModal;
import com.ibt.niramaya.modal.appoint_list_main_modal.Datum;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import okhttp3.ResponseBody;
import retrofit2.Response;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;

public class AppointmentFragment extends BaseFragment implements View.OnClickListener {

    private List<Datum> appointmentDatum = new ArrayList<>();
    private View rootView;
    private AppointListAdapter appointListAdapter;
    private TextView txtMessage;
    private String selectionType = "0";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_invoices_list, container, false);
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        init();
        return rootView;
    }

    private void init() {
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);

        ImageView ivFilter = Objects.requireNonNull(getActivity()).findViewById(R.id.ivFilter);
        ivFilter.setVisibility(View.VISIBLE);
        ivFilter.setOnClickListener(AppointmentFragment.this);
        txtMessage = rootView.findViewById(R.id.txtMessage);
        appointmentApi();


    }


    private boolean _hasLoadedOnce = false; // your boolean field

    @Override
    public void setUserVisibleHint(boolean isFragmentVisible_) {
        super.setUserVisibleHint(true);


        if (this.isVisible()) {
            // we check that the fragment is becoming visible
            if (isFragmentVisible_ && !_hasLoadedOnce) {
                appointmentApi();
                _hasLoadedOnce = true;
            }
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivFilter:
                openFiletrDialog();
                break;
            case R.id.tvCancel:
                int tag = (int) v.getTag();
                Datum appointmentData = appointmentDatum.get(tag);
                Toast.makeText(mContext, appointmentData.getOpd().getOpdTitle(), Toast.LENGTH_SHORT).show();
                String hName = appointmentData.getHospital().getHospitalName();
                String drName = appointmentData.getDoctorName();
                new AlertDialog.Builder(mContext)
                        .setTitle("Cancel Appointment")
                        .setMessage("Are you sure want to cancel your appointment at " + hName + " with " + drName + ".")
                        .setPositiveButton("YES", (dialog, which) -> {
                            cancelAppointment(appointmentData.getAppointmentId(), tag);
                        })
                        .setNegativeButton("NO", null)
                        .create()
                        .show();
                break;
        }
    }

    private void cancelAppointment(String appointmentId, int tag) {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getServerResponse(new Dialog(mContext), retrofitApiClient.cancelAppointment(
                    appointmentId, "3"), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ResponseBody response = (ResponseBody) result.body();
                    try {
                        JSONObject jsonObject = new JSONObject(response.string());
                        Toast.makeText(mContext, jsonObject.getString("message"), Toast.LENGTH_SHORT).show();
                        if (!jsonObject.getBoolean("error")) {
                            appointmentDatum.remove(tag);
                            appointListAdapter.notifyDataSetChanged();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");

                }
            });
        }
    }

    private void openFiletrDialog() {
        AlertDialog.Builder dialogBox = new AlertDialog.Builder(mContext);
        dialogBox.setCancelable(true);

        LayoutInflater li = LayoutInflater.from(mContext);
        @SuppressLint("InflateParams") View view = li.inflate(R.layout.dialog_set_appointment_status, null);
        dialogBox.setView(view);

        final AlertDialog alertDialogBox = dialogBox.create();
        Objects.requireNonNull(alertDialogBox.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);
        alertDialogBox.show();

        TextView tvPending = alertDialogBox.findViewById(R.id.tvPending);
        TextView tvApproved = alertDialogBox.findViewById(R.id.tvApproved);

        if (selectionType.equals("0")) {
            tvPending.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_red_corner_p11));
            tvPending.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            tvApproved.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_white_p8));
            tvApproved.setTextColor(getActivity().getResources().getColor(R.color.black));
        } else {
            tvPending.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_white_p8));
            tvPending.setTextColor(getActivity().getResources().getColor(R.color.black));
            tvApproved.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_red_corner_p11));
            tvApproved.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
        }

        tvPending.setOnClickListener(v -> {
            tvPending.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_red_corner_p11));
            tvPending.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            tvApproved.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_white_p8));
            tvApproved.setTextColor(getActivity().getResources().getColor(R.color.black));
            selectionType = "0";
            appointmentApi();
            alertDialogBox.dismiss();
        });
        tvApproved.setOnClickListener(v -> {
            tvPending.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_white_p8));
            tvPending.setTextColor(getActivity().getResources().getColor(R.color.black));
            tvApproved.setBackground(getActivity().getResources().getDrawable(R.drawable.layout_bg_red_corner_p11));
            tvApproved.setTextColor(getActivity().getResources().getColor(R.color.colorPrimary));
            selectionType = "1";
            appointmentApi();
            alertDialogBox.dismiss();
        });
    }

    private void appointmentApi() {
        if (cd.isNetworkAvailable()) {
            String strUserId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
            String patientId = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);
            RetrofitService.patientAppointment(new Dialog(mContext), retrofitApiClient.appointmentList(patientId, strUserId, selectionType), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    appointmentDatum.clear();
                    AppointmentModal appointmentModal = (AppointmentModal) result.body();
                    if (appointmentModal != null && !appointmentModal.getError()) {
                        appointmentDatum = appointmentModal.getData();
                    } else {
                        assert appointmentModal != null;
                        txtMessage.setText(appointmentModal.getMessage());
                    }
                    appointmentData();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }
    private void appointmentData() {
        RecyclerView recyclerViewInvoice = rootView.findViewById(R.id.recyclerViewInvoice);
        recyclerViewInvoice.setHasFixedSize(true);
        recyclerViewInvoice.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        appointListAdapter = new AppointListAdapter(appointmentDatum, mContext, AppointmentFragment.this);
        recyclerViewInvoice.setAdapter(appointListAdapter);
        appointListAdapter.notifyDataSetChanged();

    }

}

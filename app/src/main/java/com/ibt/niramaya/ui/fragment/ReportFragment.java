package com.ibt.niramaya.ui.fragment;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.ReportListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.report.PatientReportsModel;
import com.ibt.niramaya.modal.report.TestList;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.PatientReportActivity;
import com.ibt.niramaya.ui.activity.ViewImageAndFilesActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;

public class ReportFragment extends BaseFragment implements View.OnClickListener {

    private List<TestList> prescriptionList = new ArrayList<>();
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_report, container, false);
        mContext = getActivity();
        retrofitApiClient = RetrofitService.getRetrofit();
        init();
        return rootView;
    }

    private void init() {
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);

        ImageView ivFilter = getActivity().findViewById(R.id.ivFilter);
        ivFilter.setVisibility(View.GONE);

        fetchTestReports();
        //prescriptionListApi();
    }

    private void prescriptionListApi() {

        RecyclerView recyclerViewReports = rootView.findViewById(R.id.recyclerViewReports);
        recyclerViewReports.setHasFixedSize(true);
        recyclerViewReports.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        ReportListAdapter prescriptionAdapter = new ReportListAdapter(prescriptionList, mContext, this);
        recyclerViewReports.setAdapter(prescriptionAdapter);
        prescriptionAdapter.notifyDataSetChanged();
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txtView:
                int tag1 = (int) v.getTag();
                String fileUrl = prescriptionList.get(tag1).getTestStatusReportFile();
                openFile(fileUrl, prescriptionList.get(tag1).getTestReportFileType());
                break;
            case R.id.cardViewItem:
                int tag = (int) v.getTag();
                Intent intent = new Intent(mContext, PatientReportActivity.class);
                intent.putExtra("patientReportData", prescriptionList.get(tag));
                startActivity(intent);
                break;
        }

    }

    private void fetchTestReports() {
        String patientId = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);
        RetrofitService.patientTestReports(new Dialog(mContext), retrofitApiClient.patientReports(patientId), new WebResponse() {
            @Override
            public void onResponseSuccess(Response<?> result) {
                PatientReportsModel reportsModel = (PatientReportsModel) result.body();
                assert reportsModel != null;
                if (!reportsModel.getError()) {
                    if (reportsModel.getTestList().size() > 0) {
                        prescriptionList = reportsModel.getTestList();
                        Alerts.show(mContext, reportsModel.getMessage());
                        prescriptionListApi();

                    }
                } else {
                    Alerts.show(mContext, reportsModel.getMessage());
                }
            }

            @Override
            public void onResponseFailed(String error) {
                Alerts.show(mContext, "Server Error!!!");

            }
        });
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void openFile(String url, String testReportFileType) {
        try {
            //  Uri uri = Uri.fromFile(url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (testReportFileType.contains("doc") || testReportFileType.contains(".docx")) {
                activityIntent("file", url);
            } else if (testReportFileType.contains("pdf")) {
                activityIntent("file", url);
            } else if (testReportFileType.contains("ppt") || testReportFileType.contains(".pptx")) {
                // Powerpoint file
                activityIntent("file", url);
            } else if (testReportFileType.contains("xls") || testReportFileType.contains("xlsx")) {
                // Excel file
                activityIntent("file", url);
            } else if (testReportFileType.contains("jpg") || testReportFileType.contains("jpeg") || testReportFileType.contains("png")) {
                activityIntent("image", url);
            } else if (testReportFileType.contains("txt")) {
                // Text file
                activityIntent("file", url);
            }/* else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            }else if (url.toString().contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.toString().contains(".rar")) {
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                activityIntent("file", uri);
            }
*/
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, "No application found which can open the file", Toast.LENGTH_SHORT).show();
        }
    }

    private void activityIntent(String type, String url) {
        startActivity(new Intent(mContext, ViewImageAndFilesActivity.class)
                .putExtra("type", type)
                .putExtra("url", url)
        );
    }
}

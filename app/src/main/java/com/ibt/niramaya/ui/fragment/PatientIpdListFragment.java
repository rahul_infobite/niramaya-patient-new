package com.ibt.niramaya.ui.fragment;

import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.PatientIpdListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.patient_ipd_modal.Ipd;
import com.ibt.niramaya.modal.patient_ipd_modal.PatientIpdMainModal;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;

public class PatientIpdListFragment extends BaseFragment implements View.OnClickListener {

    private List<Ipd> ipdList = new ArrayList<>();
    PatientIpdListAdapter ipdListAdapter;
    private View rootView;
    private LinearLayout llOngoingIpd, llCompletedIpd;
    private String ipdStatus = "2";
    private TextView txtMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_patient_ipd, container, false);
        mContext = getActivity();
        mContext = getContext();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        init();
        return rootView;
    }

    private void init() {
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);

        ImageView ivFilter = Objects.requireNonNull(getActivity()).findViewById(R.id.ivFilter);
        ivFilter.setVisibility(View.GONE);

        RecyclerView recyclerViewBed = rootView.findViewById(R.id.rvIpdList);
        recyclerViewBed.setHasFixedSize(true);
        recyclerViewBed.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        ipdListAdapter = new PatientIpdListAdapter(ipdList, mContext, this);
        recyclerViewBed.setAdapter(ipdListAdapter);
        ipdListAdapter.notifyDataSetChanged();


        llOngoingIpd = rootView.findViewById(R.id.llOngoingIpd);
        txtMessage = rootView.findViewById(R.id.txtMessage);
        txtMessage.setVisibility(View.GONE);
        llOngoingIpd.setOnClickListener(this);
        llCompletedIpd = rootView.findViewById(R.id.llCompletedIpd);
        llCompletedIpd.setOnClickListener(this);
        patientIpdListApi();
    }


    private void patientIpdListApi() {
        if (cd.isNetworkAvailable()) {
            String patientid = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);
            ipdList.clear();
            RetrofitService.getPatientIpd(new Dialog(mContext), retrofitApiClient.ipdListData(patientid, "2"),
                    new WebResponse() {
                        @Override
                        public void onResponseSuccess(Response<?> result) {
                            PatientIpdMainModal mainModal = (PatientIpdMainModal) result.body();
                            assert mainModal != null;
                            if (!mainModal.getError()) {
                                if (mainModal.getIpd().size() > 0) {
                                    ipdList.addAll(mainModal.getIpd());
                                    ipdListAdapter.notifyDataSetChanged();
                                }
                            }
                        }

                        @Override
                        public void onResponseFailed(String error) {
                            Alerts.show(mContext, "Server Error!!!");
//    Alerts.show(mContext,error);
                        }
                    });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llCompletedIpd:
                ipdStatus = "1";
                txtMessage.setVisibility(View.GONE);
                ((TextView) rootView.findViewById(R.id.tvCompletedIpd)).setTextColor(getResources().getColor(R.color.white));
                ((TextView) rootView.findViewById(R.id.tvOngoingIpd)).setTextColor(getResources().getColor(R.color.gray_f));
                rootView.findViewById(R.id.viewOngoingIpd).setVisibility(View.GONE);
                rootView.findViewById(R.id.viewCompletedIpd).setVisibility(View.VISIBLE);
                patientIpdListApi();
                break;
            case R.id.llOngoingIpd:
                ipdStatus = "2";
                txtMessage.setVisibility(View.GONE);
                ((TextView) rootView.findViewById(R.id.tvCompletedIpd)).setTextColor(getResources().getColor(R.color.gray_f));
                ((TextView) rootView.findViewById(R.id.tvOngoingIpd)).setTextColor(getResources().getColor(R.color.white));
                rootView.findViewById(R.id.viewOngoingIpd).setVisibility(View.VISIBLE);
                rootView.findViewById(R.id.viewCompletedIpd).setVisibility(View.GONE);
                patientIpdListApi();
                break;
        }
    }
}

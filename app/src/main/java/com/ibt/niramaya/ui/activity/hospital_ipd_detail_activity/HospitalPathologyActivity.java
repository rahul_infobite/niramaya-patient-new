package com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.HospitalIpdDetailAdapter.HospitalIpdPathologyMedicineListAdapter;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pathology_detail_modal.BillData;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pathology_detail_modal.IpdPathologyBillTest;
import com.ibt.niramaya.modal.hospital_ipd_details_modal.ipd_pathology_detail_modal.IpdPathologyDetailMainModal;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Response;

public class HospitalPathologyActivity extends BaseActivity implements View.OnClickListener {
    private IpdPathologyDetailMainModal mainModal;
    private HospitalIpdPathologyMedicineListAdapter pathologytestListAdapter;
    private List<IpdPathologyBillTest> pathologyBillTests = new ArrayList<>();
    private String strId = "";
    private String discountPrice = "";


    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_patholog_test_bill);
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        init();
    }

    private void init() {

        Intent intent = getIntent();
        if (getIntent() != null) {
            strId = intent.getStringExtra("id");
        }

        Toolbar toolBarHospitalIpdDetail = findViewById(R.id.toolbarPathTest);
        toolBarHospitalIpdDetail.setTitle("Ipd Test Detail");
        setSupportActionBar(toolBarHospitalIpdDetail);
        toolBarHospitalIpdDetail.setNavigationIcon(R.drawable.ic_back);
        toolBarHospitalIpdDetail.setNavigationOnClickListener(v -> onBackPressed());

        RecyclerView rvMedicine = findViewById(R.id.recylerTest);
        rvMedicine.setHasFixedSize(true);
        rvMedicine.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        pathologytestListAdapter = new HospitalIpdPathologyMedicineListAdapter(pathologyBillTests, mContext);
        rvMedicine.setAdapter(pathologytestListAdapter);
        pathologytestListAdapter.notifyDataSetChanged();


        fatchIpdDetail();
    }

    @SuppressLint("SetTextI18n")
    private void getMainIpdData() {
        if (!mainModal.getError()) {
            BillData billData = mainModal.getBillData();
            if (billData != null) {
                String strType = billData.getBillType();
                if (strType.equals("0")) {
                    strType = "Pharmacy";
                } else if (strType.equals("1")) {
                    strType = "Pathology";
                } else if (strType.equals("2")) {
                    strType = "IPD";
                } else if (strType.equals("3")) {
                    strType = "IPD slip bill";
                } else if (strType.equals("4")) {
                    strType = "Bed Bill";
                } else if (strType.equals("5")) {
                    strType = "Operation Bill";
                } else {
                    strType = "";
                }

                String strAmount = billData.getBillAmount();
                String strDiscount = billData.getBillDiscount();

                Float strBillDiscount = Float.valueOf(strDiscount);
                Float totalAmount = Float.valueOf(strAmount);
                billDiscountAmountGet(strBillDiscount,totalAmount);

                ((TextView) findViewById(R.id.tvPatientName)).setText(billData.getPatientName());
                ((TextView) findViewById(R.id.tvBIllCreateDate)).setText(changeDateFormat(billData.getBillCreatedDate()));
                ((TextView) findViewById(R.id.tvPatientGender)).setText(billData.getPatientGender());
                ((TextView) findViewById(R.id.tvBillType)).setText(strType);
                ((TextView) findViewById(R.id.tvBIllInvoiceNUmber)).setText(billData.getBillInvoice());
                ((TextView) findViewById(R.id.tvBIllGstNUmber)).setText(billData.getBillGstNumber());
                ((TextView) findViewById(R.id.tvDiscountAmount)).setText("Discount : " +discountPrice);
                ((TextView) findViewById(R.id.tvTotalAmount)).setText("Total Amount : " + billData.getBillAmount());
                ((TextView) findViewById(R.id.tvPaidAmount)).setText("Total Paid : " + billData.getAmountPaid());
            }
        }
    }
    @SuppressLint("DefaultLocale")
    private void billDiscountAmountGet(Float strBilDiscount, Float totalAmount) {
        int finalAmount = (int) ((totalAmount * 100) / (100 - strBilDiscount));

        float discount = finalAmount - totalAmount;
        discountPrice = String.format("%.2f",discount);
    }

    @Override
    public void onClick(View v) {
    }

    private void fatchIpdDetail() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalPathologyDetailList(new Dialog(mContext), retrofitApiClient.getHospitalPathologyDetail(strId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    mainModal = (IpdPathologyDetailMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getBillData() != null) {
                            getMainIpdData();
                        } else Alerts.show(mContext, mainModal.getMessage());
                        if (mainModal.getBillData().getIpdPathologyBillTest() != null) {
                            pathologyBillTests.addAll(mainModal.getBillData().getIpdPathologyBillTest());
                            pathologytestListAdapter.notifyDataSetChanged();
                        } else {
                            Alerts.show(mContext, mainModal.getMessage());
                        }
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }
    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yyyy";
        @SuppressLint("SimpleDateFormat") SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        @SuppressLint("SimpleDateFormat") SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}

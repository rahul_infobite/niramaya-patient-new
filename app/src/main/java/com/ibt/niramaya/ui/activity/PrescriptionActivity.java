package com.ibt.niramaya.ui.activity;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.BloodRequisitionAdapter;
import com.ibt.niramaya.adapter.MedicineBillListAdapter;
import com.ibt.niramaya.adapter.PathologyBillListAdapter;
import com.ibt.niramaya.adapter.PrescriptionAdvisedListAdapter;
import com.ibt.niramaya.adapter.PrescriptionGivenListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisitonMainModal;
import com.ibt.niramaya.modal.doctor_refer_slip_modal.DoctorReeferSlipMainModal;
import com.ibt.niramaya.modal.prescription.OpdList;
import com.ibt.niramaya.modal.prescription.PTAdvisedModel;
import com.ibt.niramaya.modal.prescription.PTGivenModel;
import com.ibt.niramaya.modal.prescription.detail.BillMedicine;
import com.ibt.niramaya.modal.prescription.detail.BillTest;
import com.ibt.niramaya.modal.prescription.detail.Medicine;
import com.ibt.niramaya.modal.prescription.detail.Preception;
import com.ibt.niramaya.modal.prescription.detail.PrescriptionDetailModel;
import com.ibt.niramaya.modal.prescription.detail.Test;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.fragment.BloodRecuisitionBottomDialogFragment;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

public class PrescriptionActivity extends BaseActivity implements View.OnClickListener {

    private DrawerLayout drawer;
    private Toolbar toolbar;
    private RelativeLayout leftDrawer;
    private RelativeLayout rightDrawer;
    private RecyclerView rvMedicineBill, rvPathologyBill;

    private OpdList opdData;
    private TextView tvPatientName, tvPatientId, tvDoctorName, tvHospitalName, tvOpdCreatedDate, tvAge, tvContact,
            tvComplaint, tvBpCount, tvHrCount, tvRespCount, tvTempCount, tvPainScoreCount, tvDischargeType;
    private ImageView ivHospitalLogo;
    private Preception perceptionData;
    private ArrayList<Medicine> medicineList;
    private ArrayList<Test> testList;
    private ArrayList<PTAdvisedModel> treatmentAdvisedList = new ArrayList<>();
    private ArrayList<PTGivenModel> treatmentGivenList = new ArrayList<>();
    private ArrayList<BillMedicine> medicineBillList = new ArrayList<>();
    private ArrayList<BillTest> pathologyBillList = new ArrayList<>();
    private String dischaType = "";

    //Blood requisition slip

    private RelativeLayout rlAddBloodRecuisition;
    private BloodRequisitionAdapter bloodRequisitionAdapter;
    private ArrayList<BloodRequisition> bloodRequisitions = new ArrayList<>();
    private TextView txtMessage;
    private MenuItem menuBlood;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prescription_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        showHomeBackOnToolbar(toolbar);

        initViews();
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {
        opdData = getIntent().getParcelableExtra("OPD_DATA");

        drawer = findViewById(R.id.drawer_layout);
        rightDrawer = findViewById(R.id.nav_right);
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        tvPatientName = findViewById(R.id.tvPatientName);
        tvPatientId = findViewById(R.id.tvPatientId);
        tvDoctorName = findViewById(R.id.tvDoctorName);
        tvHospitalName = findViewById(R.id.tvHospitalName);
        tvOpdCreatedDate = findViewById(R.id.tvOpdCreatedDate);
        tvAge = findViewById(R.id.tvAge);
        tvContact = findViewById(R.id.tvContact);
        ivHospitalLogo = findViewById(R.id.ivHospitalLogo);

        rvMedicineBill = findViewById(R.id.rvMedicineBill);
        rvPathologyBill = findViewById(R.id.rvPathologyBill);

        tvComplaint = findViewById(R.id.tvComplaint);
        tvBpCount = findViewById(R.id.tvBpCount);
        tvHrCount = findViewById(R.id.tvHrCount);
        tvRespCount = findViewById(R.id.tvRespCount);
        tvTempCount = findViewById(R.id.tvTempCount);
        tvPainScoreCount = findViewById(R.id.tvPainScoreCount);
        tvDischargeType = findViewById(R.id.tvDischargeType);
        tvDischargeType.setOnClickListener(this);

        tvPatientName.setText(AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_NAME));
        tvPatientId.setText("Patient Id : " + AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID));
        tvDoctorName.setText(opdData.getDoctorName());
        tvHospitalName.setText(opdData.getHospitalName());
        tvOpdCreatedDate.setText(changeDateFormat(opdData.getOpdCreatedDate()));


        String imgData = opdData.getHospitalImage();

        Glide.with(mContext)
                .load(imgData).placeholder(R.drawable.ic_profile).into(ivHospitalLogo);


        rlAddBloodRecuisition = findViewById(R.id.rlAddBloodRecuisition);
        rlAddBloodRecuisition.setVisibility(View.VISIBLE);
        rlAddBloodRecuisition.setOnClickListener(this);
        txtMessage = findViewById(R.id.txtMessage);
        txtMessage.setVisibility(View.GONE);

        RecyclerView rvBloodRequisition = findViewById(R.id.rvBloodRequisition);
        rvBloodRequisition.setHasFixedSize(true);
        rvBloodRequisition.setLayoutManager(new LinearLayoutManager(mContext));
        bloodRequisitionAdapter = new BloodRequisitionAdapter(bloodRequisitions, mContext, this);
        rvBloodRequisition.setAdapter(bloodRequisitionAdapter);

        fetchPrescriptionDetail();

    }

    @Override
    protected void onResume() {
        super.onResume();
        rlAddBloodRecuisition.setVisibility(View.VISIBLE);
        txtMessage.setVisibility(View.GONE);
        bloodRecuisitionApi();
    }

    private void fetchPrescriptionDetail() {
        String uId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
        String pId = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);
        String opdId = opdData.getOpdId();

        if (cd.isNetworkAvailable()) {
            RetrofitService.patientPrescriptionDetail(new Dialog(mContext), retrofitApiClient.patientPrescriptionDetail(
                    uId, pId, opdId), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    PrescriptionDetailModel detailModel = (PrescriptionDetailModel) result.body();
                    if (!detailModel.getError()) {
                        perceptionData = detailModel.getPreception();
                        medicineList = (ArrayList<Medicine>) perceptionData.getMedicine();
                        testList = (ArrayList<Test>) perceptionData.getTest();
                        //tvComplaint, tvBpCount, tvHrCount, tvRespCount, tvTempCount, tvPainScoreCount
                        tvComplaint.setText(perceptionData.getOpdCheifComplaints());
                        if (perceptionData.getOpdBp().isEmpty()) {
                            tvBpCount.setText("-");
                        } else {
                            tvBpCount.setText(perceptionData.getOpdBp());
                        }
                        if (perceptionData.getOpdHeartRatePerMin().isEmpty()) {
                            tvHrCount.setText("-");
                        } else {
                            tvHrCount.setText(perceptionData.getOpdHeartRatePerMin());
                        }
                        if (perceptionData.getOpdRespRateMin().isEmpty()) {
                            tvRespCount.setText("-");
                        } else {
                            tvRespCount.setText(perceptionData.getOpdRespRateMin());
                        }
                        if (perceptionData.getOpdTemp().isEmpty()) {
                            tvTempCount.setText("-");
                        } else {
                            tvTempCount.setText(perceptionData.getOpdTemp());
                        }
                        if (perceptionData.getOpdPainScore().isEmpty()) {
                            tvPainScoreCount.setText("-");
                        } else {
                            tvPainScoreCount.setText(perceptionData.getOpdPainScore());
                        }


                        if (perceptionData.getOpdTypeOfDischarge().equals("Referred")) {
                            dischaType = perceptionData.getOpdTypeOfDischarge();
                            tvDischargeType.setText(perceptionData.getOpdTypeOfDischarge() + "View Refer Slip");

                        } else {
                            tvDischargeType.setText(perceptionData.getOpdTypeOfDischarge());
                        }

                        treatmentAdvisedList.clear();
                        treatmentGivenList.clear();

                        if (detailModel.getPreception().getBillMedicine().size() > 0) {
                            medicineBillList = (ArrayList<BillMedicine>) detailModel.getPreception().getBillMedicine();
                        }
                        if (detailModel.getPreception().getBillTest().size() > 0) {
                            pathologyBillList = (ArrayList<BillTest>) detailModel.getPreception().getBillTest();
                        }

                        for (int i = 0; i < medicineList.size(); i++) {
                            PTGivenModel givenModel = new PTGivenModel();
                            PTAdvisedModel advisedModel = new PTAdvisedModel();
                            Medicine medicine = medicineList.get(i);
                            if (medicine.getPreceptionType().equals("0")) {
                                givenModel.setPrescriptionTreatmentType("Medicine");
                                if (medicine.getPreception().equals(0)) {
                                    givenModel.setPrescriptionContentType("Image");
                                } else {
                                    givenModel.setPrescriptionContentType("Text");
                                }
                                givenModel.setOpdPrescriptionId(medicine.getOpdPreceptionId());
                                givenModel.setMedicineId(medicine.getMedicineId());
                                givenModel.setMedicineName(medicine.getMedicineName());
                                givenModel.setMedicineDoes(medicine.getMedicineDose());
                                givenModel.setOpdPrescriptionCreatedDate(medicine.getOpdPreceptionCreatedDate());

                                treatmentGivenList.add(givenModel);
                            } else {
                                advisedModel.setPrescriptionTreatmentType("Medicine");
                                if (medicine.getPreception().equals(0)) {
                                    advisedModel.setPrescriptionContentType("Image");
                                } else {
                                    advisedModel.setPrescriptionContentType("Text");
                                }
                                advisedModel.setOpdPrescriptionId(medicine.getOpdPreceptionId());
                                advisedModel.setMedicineId(medicine.getMedicineId());
                                advisedModel.setMedicineName(medicine.getMedicineName());
                                advisedModel.setMedicineDoes(medicine.getMedicineDose());
                                advisedModel.setOpdPrescriptionCreatedDate(medicine.getOpdPreceptionCreatedDate());

                                treatmentAdvisedList.add(advisedModel);
                            }
                        }

                        for (int i = 0; i < testList.size(); i++) {
                            PTGivenModel givenModel = new PTGivenModel();
                            PTAdvisedModel advisedModel = new PTAdvisedModel();
                            Test test = testList.get(i);
                            if (test.getOpdPathologyTestType().equals("0")) {
                                givenModel.setPrescriptionTreatmentType("Test");
                                if (test.getPreception().equals(0)) {
                                    givenModel.setPrescriptionContentType("Image");
                                } else {
                                    givenModel.setPrescriptionContentType("Text");
                                }
                                givenModel.setOpdPathologyTestId(test.getOpdPathologyTestId());
                                givenModel.setPathologyId(test.getPathologyId());
                                givenModel.setTestName(test.getTestName());
                                givenModel.setOpdPathologyCreatedDate(test.getOpdPathologyTestCreatedDate());

                                treatmentGivenList.add(givenModel);

                            } else {
                                advisedModel.setPrescriptionTreatmentType("Test");
                                if (test.getPreception().equals(0)) {
                                    advisedModel.setPrescriptionContentType("Image");
                                } else {
                                    advisedModel.setPrescriptionContentType("Text");
                                }
                                advisedModel.setOpdPathologyTestId(test.getOpdPathologyTestId());
                                advisedModel.setPathologyId(test.getPathologyId());
                                advisedModel.setTestName(test.getTestName());
                                advisedModel.setOpdPathologyCreatedDate(test.getOpdPathologyTestCreatedDate());

                                treatmentAdvisedList.add(advisedModel);
                            }
                        }

                        initPrescriptionList();

                    }
                }

                @Override
                public void onResponseFailed(String error) {

                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bill_detail, menu);
        menuBlood = menu.findItem(R.id.view_blood_requisition);
        if (bloodRequisitions.size() == 0) {
            menuBlood.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_view) {
            drawer.openDrawer(GravityCompat.END);
        } else if (item.getItemId() == R.id.view_blood_requisition) {
            // drawer.openDrawer(Gravity.START);
            Bundle bundle = new Bundle();
            BloodRecuisitionBottomDialogFragment bottomDialogFragment = new BloodRecuisitionBottomDialogFragment();
            bottomDialogFragment.show(getSupportFragmentManager(),
                    "bottom_dialog_fragment");
            bundle.putInt("opdId", Integer.parseInt(opdData.getOpdId()));
            bottomDialogFragment.setArguments(bundle);
        }
        return super.onOptionsItemSelected(item);
    }

    private void initPrescriptionList() {
        RecyclerView rvTrtGiven = findViewById(R.id.rvTrtGiven);
        RecyclerView rvTrtAdvised = findViewById(R.id.rvTrtAdvised);

        rvTrtGiven.setLayoutManager(new LinearLayoutManager(mContext));
        PrescriptionGivenListAdapter givenAdapter = new PrescriptionGivenListAdapter(treatmentGivenList, mContext);
        rvTrtGiven.setAdapter(givenAdapter);
        givenAdapter.notifyDataSetChanged();

        rvTrtAdvised.setLayoutManager(new LinearLayoutManager(mContext));
        PrescriptionAdvisedListAdapter advisedAdapter = new PrescriptionAdvisedListAdapter(treatmentAdvisedList, mContext);
        rvTrtAdvised.setAdapter(advisedAdapter);
        advisedAdapter.notifyDataSetChanged();

        rvMedicineBill.setLayoutManager(new LinearLayoutManager(mContext));
        MedicineBillListAdapter medicineAdapter = new MedicineBillListAdapter(medicineBillList, mContext, PrescriptionActivity.this);
        rvMedicineBill.setAdapter(medicineAdapter);
        medicineAdapter.notifyDataSetChanged();

        rvPathologyBill.setLayoutManager(new LinearLayoutManager(mContext));
        PathologyBillListAdapter pathologyAdapter = new PathologyBillListAdapter(pathologyBillList, mContext, PrescriptionActivity.this);
        rvPathologyBill.setAdapter(pathologyAdapter);
        pathologyAdapter.notifyDataSetChanged();
    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "dd/MM/yy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @SuppressLint("SetTextI18n")
    private void selectReferDialog(DoctorReeferSlipMainModal referSlipData) {
        android.app.AlertDialog.Builder dialogBox = new android.app.AlertDialog.Builder(mContext);
        dialogBox.setCancelable(false);

        LayoutInflater li = LayoutInflater.from(PrescriptionActivity.this);
        final View dialogBoxView = li.inflate(R.layout.dialog_fill_reffered_slip_detail, null);
        dialogBox.setView(dialogBoxView);

        final android.app.AlertDialog addReferSlipDailog = dialogBox.create();
        addReferSlipDailog.show();

        if (referSlipData.getReferSlip() != null) {
            ((TextView) dialogBoxView.findViewById(R.id.txtProblemStatement)).setText("Problem Statement : " + referSlipData.getReferSlip().getProblemStatement());
            ((TextView) dialogBoxView.findViewById(R.id.txtReasonofReffer)).setText("Reason Of Refer : " + referSlipData.getReferSlip().getReasonOfReferral());
            ((TextView) dialogBoxView.findViewById(R.id.txtHospitalName)).setText("Referred To Name : " + referSlipData.getReferSlip().getReferedToName());
            ((TextView) dialogBoxView.findViewById(R.id.txtReferHospitalContact)).setText("Referred To Contact : " + referSlipData.getReferSlip().getReferedToContact());
            ((TextView) dialogBoxView.findViewById(R.id.txtHospitalAddress)).setText("Referred To Address : " + referSlipData.getReferSlip().getReferedToAddress());
            ((TextView) dialogBoxView.findViewById(R.id.txtReferAdvisorName)).setText("Adviser Name : " + referSlipData.getReferSlip().getReferedToAdvisorName());
            ((TextView) dialogBoxView.findViewById(R.id.txtReferAdviserEmail)).setText("Referred Email : " + referSlipData.getReferSlip().getReferedToEmail());
            ((TextView) dialogBoxView.findViewById(R.id.txtReferBy)).setText("Referred By : " + referSlipData.getReferSlip().getReferdByDoctorName());
        }

        assert referSlipData.getReferSlip() != null;
        if (referSlipData.getReferSlip().getHospital() != null) {
            ((TextView) dialogBoxView.findViewById(R.id.tv_hospital_name)).setText(referSlipData.getReferSlip().getHospital().getHospitalName());
            ((TextView) dialogBoxView.findViewById(R.id.txtHospitalContact)).setText(referSlipData.getReferSlip().getHospital().getHospitalContact());
            ((TextView) dialogBoxView.findViewById(R.id.hospitalFirstAdd)).setText(referSlipData.getReferSlip().getHospital().getHospitalHouseNumber() + "," + referSlipData.getReferSlip().getHospital().getHospitalStreetName());
            ((TextView) dialogBoxView.findViewById(R.id.hospitalCity)).setText(referSlipData.getReferSlip().getHospital().getHospitalCity());
            ((TextView) dialogBoxView.findViewById(R.id.hospitalState)).setText(referSlipData.getReferSlip().getHospital().getHospitalState());
            ((TextView) dialogBoxView.findViewById(R.id.hospitalCountry)).setText(referSlipData.getReferSlip().getHospital().getHospitalCountry());
            ((TextView) dialogBoxView.findViewById(R.id.hospitalZipCode)).setText(referSlipData.getReferSlip().getHospital().getHospitalZipcode());
            Glide.with(mContext).load(referSlipData.getReferSlip().getHospital().getHospialLogo()).placeholder(R.drawable.ic_profile).into(((CircleImageView) findViewById(R.id.hospitalImage)));
        }

        String referSlipId = referSlipData.getReferSlip().getReferSlipId();
        dialogBoxView.findViewById(R.id.btn_Cancel).setOnClickListener(v -> addReferSlipDailog.dismiss());
    }

    private void selectReferSleep() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.selectPatientReffered(new Dialog(mContext), retrofitApiClient.refferedData("0", opdData.getOpdId()), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    DoctorReeferSlipMainModal mainModal = (DoctorReeferSlipMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getReferSlip() != null) {
                            selectReferDialog(mainModal);
                        } else {
                            Alerts.show(mContext, mainModal.getMessage());
                        }
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, error);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llPathologyRoot:
                int pTag = (int) v.getTag();
                break;
            case R.id.tvDischargeType:
                if (dischaType.equals("Referred")) {
                    selectReferSleep();
                }
                break;
        }
    }

    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.END)) {
            drawer.closeDrawer(GravityCompat.END);
        } else {
            super.onBackPressed();
        }
    }

    private void viewDataDialog() {
        AlertDialog.Builder dialogBox = new AlertDialog.Builder(mContext);
        dialogBox.setCancelable(true);

        LayoutInflater li = LayoutInflater.from(mContext);
        final View dialogBoxView = li.inflate(R.layout.dialog_view_blood_medicine_test, null);
        dialogBox.setView(dialogBoxView);

        final AlertDialog viewDialog = dialogBox.create();
        viewDialog.show();

        dialogBoxView.findViewById(R.id.txtBloodRecusitionSlip).setOnClickListener(v -> {
            drawer.openDrawer(GravityCompat.START);
            viewDialog.dismiss();

        });
        dialogBoxView.findViewById(R.id.tvMedicineTestIntake).setOnClickListener(v -> {
            drawer.openDrawer(GravityCompat.END);
            viewDialog.dismiss();
        });


    }

    private void bloodRecuisitionApi() {
        if (cd.isNetworkAvailable()) {
            String opdId = opdData.getOpdId();
            RetrofitService.selectBloodRequisition(new Dialog(mContext), retrofitApiClient.selectBloodRequisitionData("0", opdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    bloodRequisitions.clear();
                    BloodRequisitonMainModal mainModal = (BloodRequisitonMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getBloodRequisition().size() > 0) {
                            bloodRequisitions.addAll(mainModal.getBloodRequisition());
                            bloodRequisitionAdapter.notifyDataSetChanged();
                            menuBlood.setVisible(true);
                        } else {
                            menuBlood.setVisible(false);
                            rlAddBloodRecuisition.setVisibility(View.VISIBLE);
                            txtMessage.setVisibility(View.VISIBLE);
                            txtMessage.setText(mainModal.getMessage());
                        }
                    } else {
                        menuBlood.setVisible(false);
                        rlAddBloodRecuisition.setVisibility(View.VISIBLE);
                        txtMessage.setVisibility(View.VISIBLE);
                        txtMessage.setText(mainModal.getMessage());
                    }
                    bloodRequisitionAdapter.notifyDataSetChanged();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext,"Server Error!!!");
                }
            });
        }
    }


}

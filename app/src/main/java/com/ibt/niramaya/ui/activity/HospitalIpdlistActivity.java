package com.ibt.niramaya.ui.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.ibt.niramaya.R;
import com.ibt.niramaya.hospital_ipd_adapter.HospitaIpdOperationAdapter;
import com.ibt.niramaya.hospital_ipd_adapter.HospitalIpdBilServiceAdapter;
import com.ibt.niramaya.hospital_ipd_adapter.HospitalIpdLBedAdapter;
import com.ibt.niramaya.hospital_ipd_adapter.HospitalIpdPathologyAdapter;
import com.ibt.niramaya.hospital_ipd_adapter.HospitalIpdPharmacyAdapter;
import com.ibt.niramaya.hospital_ipd_adapter.HospitalIpdSlipListAdapter;
import com.ibt.niramaya.modal.hospital.HospitalIpdDetailMainModal;
import com.ibt.niramaya.modal.hospital.bed.HospitalBedListModel;
import com.ibt.niramaya.modal.hospital.bed.IpdBed;
import com.ibt.niramaya.modal.hospital.ipd_services.IpdBillService;
import com.ibt.niramaya.modal.hospital.ipd_services.SelectIpdServicesMainModal;
import com.ibt.niramaya.modal.hospital.ipd_slip.HospitalIpdModel;
import com.ibt.niramaya.modal.hospital.ipd_slip.IpdSlip;
import com.ibt.niramaya.modal.hospital.operation.HospitalOperationModel;
import com.ibt.niramaya.modal.hospital.operation.OperationSchedule;
import com.ibt.niramaya.modal.hospital.pathology.HospitalPathologyBillModel;
import com.ibt.niramaya.modal.hospital.pathology.PathologyBillDatum;
import com.ibt.niramaya.modal.hospital.pharmacy.HospitalPharmacyBillModel;
import com.ibt.niramaya.modal.hospital.pharmacy.PharmacyBillDatum;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity.HospitalDetailIpdActivity;
import com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity.HospitalIpdActivity;
import com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity.HospitalOperationActivity;
import com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity.HospitalPathologyActivity;
import com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity.HospitalPharmacyActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

public class HospitalIpdlistActivity extends BaseActivity implements View.OnClickListener {
    private Button btnIpdSlips, btnOperations, btnPharmacy, btnPathology, btnBed, btnServices;
    private String ipdId;
    private RecyclerView rvHospitalIpd;
    private HospitalIpdSlipListAdapter slipAdapter;
    private HospitalIpdPharmacyAdapter pharmacyAdapter;
    private HospitalIpdPathologyAdapter pathologyAdapter;
    private HospitaIpdOperationAdapter operationAdapter;
    private HospitalIpdBilServiceAdapter hospitalIpdBilServiceAdapter;
    private HospitalIpdLBedAdapter bedAdapter;
    private TextView tMessage;
    private List<IpdSlip> hospitalIpdSlip = new ArrayList<>();
    private List<OperationSchedule> operationSchedule = new ArrayList<>();
    private ArrayList<PharmacyBillDatum> pharmacyBillData = new ArrayList<>();
    private ArrayList<PathologyBillDatum> pathologyBillList = new ArrayList<>();
    private ArrayList<IpdBed> ipdBedList = new ArrayList<>();
    private ArrayList<IpdBillService> ipdBillServiceList = new ArrayList<>();
    private FloatingActionButton fabCreateIpdService;
    private String strBedStatus = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_ipd_list);

        Intent intent = getIntent();
        if (getIntent() != null) {
            ipdId = intent.getStringExtra("ipdId");
        }

        btnIpdSlips = findViewById(R.id.btnIpdSlips);
        btnOperations = findViewById(R.id.btnOperations);
        btnPharmacy = findViewById(R.id.btnPharmacy);
        btnPathology = findViewById(R.id.btnPathology);
        btnBed = findViewById(R.id.btnBed);
        btnServices = findViewById(R.id.btnServices);
        tMessage = findViewById(R.id.tvMessage);
        rvHospitalIpd = findViewById(R.id.rvHospitalIpd);
        rvHospitalIpd.setLayoutManager(new LinearLayoutManager(mContext));
        btnIpdSlips.setOnClickListener(this);
        btnOperations.setOnClickListener(this);
        btnPharmacy.setOnClickListener(this);
        btnPathology.setOnClickListener(this);
        btnBed.setOnClickListener(this);
        btnServices.setOnClickListener(this);

        (findViewById(R.id.ivBackAccount)).setOnClickListener(this);
        (findViewById(R.id.btnHospitaIpd)).setOnClickListener(this);
        (findViewById(R.id.btnHospitaIpd)).setVisibility(View.VISIBLE);
        (findViewById(R.id.tvDischarge)).setVisibility(View.GONE);
        (findViewById(R.id.btnPayment)).setVisibility(View.GONE);
        fetchSlips();

    }

    @SuppressLint("RestrictedApi")
    @Override
    protected void onResume() {
        super.onResume();
        clearList();
        btnIpdSlips.setBackground(getResources().getDrawable(R.drawable.layout_bg_green_p4));
        btnOperations.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
        btnPharmacy.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
        btnPathology.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
        btnBed.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
        btnServices.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
        fetchSlips();
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnIpdSlips:
                clearList();
                btnIpdSlips.setBackground(getResources().getDrawable(R.drawable.layout_bg_green_p4));
                btnOperations.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPharmacy.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPathology.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnBed.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnServices.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                fetchSlips();
                break;
            case R.id.btnOperations:
                String strButtonClick = "operation";
                clearList();
                btnIpdSlips.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnOperations.setBackground(getResources().getDrawable(R.drawable.layout_bg_green_p4));
                btnPharmacy.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPathology.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnBed.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnServices.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                fetchOperations();
                break;
            case R.id.btnPharmacy:
                clearList();
                btnIpdSlips.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnOperations.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPharmacy.setBackground(getResources().getDrawable(R.drawable.layout_bg_green_p4));
                btnPathology.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnBed.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnServices.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                fetchPharmacy();
                break;
            case R.id.btnPathology:
                clearList();
                btnIpdSlips.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnOperations.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPharmacy.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPathology.setBackground(getResources().getDrawable(R.drawable.layout_bg_green_p4));
                btnBed.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnServices.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                fetchPathology();
                break;
            case R.id.btnBed:
                strButtonClick = "bed";
                clearList();
                btnIpdSlips.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnOperations.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPharmacy.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPathology.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnBed.setBackground(getResources().getDrawable(R.drawable.layout_bg_green_p4));
                btnServices.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                fetchBed();
                break;
            case R.id.btnServices:
                strButtonClick = "service";
                clearList();
                btnIpdSlips.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnOperations.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPharmacy.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnPathology.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnBed.setBackground(getResources().getDrawable(R.drawable.layout_bg_red_p4));
                btnServices.setBackground(getResources().getDrawable(R.drawable.layout_bg_green_p4));
                fetchServices();
                break;
            case R.id.llRootIpd:
                int ipdTag = (int) v.getTag();
                Intent intent = new Intent(mContext, HospitalIpdActivity.class);
                intent.putExtra("id", hospitalIpdSlip.get(ipdTag).getIpdSlipId());
                startActivity(intent);
                break;
            case R.id.rlRootPharmacy:
                int ipdTag1 = (int) v.getTag();
                Intent intent1 = new Intent(mContext, HospitalPharmacyActivity.class);
                intent1.putExtra("id", pharmacyBillData.get(ipdTag1).getBillId());
                startActivity(intent1);
                break;
            case R.id.rlRootPathology:
                int ipdTag2 = (int) v.getTag();
                Intent intent2 = new Intent(mContext, HospitalPathologyActivity.class);
                intent2.putExtra("id", pathologyBillList.get(ipdTag2).getBillId());
                startActivity(intent2);
                break;
            case R.id.rlRootOperation:
                int ipdTag3 = (int) v.getTag();
                Intent intent3 = new Intent(mContext, HospitalOperationActivity.class);
                intent3.putExtra("id", operationSchedule.get(ipdTag3).getOperationScheduleId());
                startActivity(intent3);
                break;
            case R.id.ivBackAccount:
                onBackPressed();
                break;
            case R.id.btnHospitaIpd:
                hospitalIpdDetail();
                break;

        }
    }

    private void fetchOperations() {
        rvHospitalIpd.setVisibility(View.GONE);
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalOperationsList(new Dialog(mContext), retrofitApiClient.getHospitalOperationsList(ipdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    rvHospitalIpd.setVisibility(View.VISIBLE);
                    operationAdapter = new HospitaIpdOperationAdapter(operationSchedule, mContext, HospitalIpdlistActivity.this);
                    rvHospitalIpd.setAdapter(operationAdapter);
                    HospitalOperationModel hospitalOperationModel = (HospitalOperationModel) result.body();
                    if (!hospitalOperationModel.getError() && hospitalOperationModel.getOperationSchedule().size() > 0) {
                        tMessage.setVisibility(View.GONE);
                        operationSchedule.addAll(hospitalOperationModel.getOperationSchedule());
                        operationAdapter.notifyDataSetChanged();
                    } else {
                        tMessage.setVisibility(View.VISIBLE);
                        tMessage.setText("No operation bill created");
                    }
                    operationAdapter.notifyDataSetChanged();


                }

                @Override
                public void onResponseFailed(String error) {
                }
            });
        }
    }

    private void hospitalIpdDetail() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalIpdListDetail(new Dialog(mContext), retrofitApiClient.getHospitalIpdDetailDetail(ipdId), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    HospitalIpdDetailMainModal hospitalIpdModel = (HospitalIpdDetailMainModal) result.body();

                    if (!hospitalIpdModel.getError() && hospitalIpdModel.getIpd() != null) {

                        tMessage.setVisibility(View.GONE);
                        Intent intent0 = new Intent(mContext, HospitalDetailIpdActivity.class);
                        intent0.putExtra("hospitalIpd", hospitalIpdModel.getIpd());
                        startActivity(intent0);
                        slipAdapter.notifyDataSetChanged();
                    } else {
                        tMessage.setVisibility(View.VISIBLE);
                        tMessage.setText("No slip created");
                    }
                    hospitalIpdServicesData();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, error);

                }
            });
        }
    }

    private void fetchSlips() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalIpdList(new Dialog(mContext), retrofitApiClient.getHospitalIpdList(ipdId), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    HospitalIpdModel hospitalIpdModel = (HospitalIpdModel) result.body();
                    assert hospitalIpdModel != null;
                    if (!hospitalIpdModel.getError() && hospitalIpdModel.getIpdSlip().size() > 0) {
                        tMessage.setVisibility(View.GONE);
                        hospitalIpdSlip = (ArrayList<IpdSlip>) hospitalIpdModel.getIpdSlip();
                    } else {

                        tMessage.setVisibility(View.VISIBLE);
                        tMessage.setText("No slip created");
                    }
                    hospitalIpdSlipData();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, error);

                }
            });
        }
    }

    private void hospitalIpdSlipData() {
        rvHospitalIpd.setHasFixedSize(true);
        rvHospitalIpd.setLayoutManager(new LinearLayoutManager(mContext));
        slipAdapter = new HospitalIpdSlipListAdapter(hospitalIpdSlip, mContext, HospitalIpdlistActivity.this);
        rvHospitalIpd.setAdapter(slipAdapter);
    }


    private void fetchPharmacy() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalPharmacyBillList(new Dialog(mContext), retrofitApiClient.getHospitalPharmacyBill0List(ipdId), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    rvHospitalIpd.setVisibility(View.VISIBLE);
                    HospitalPharmacyBillModel hospitalPharmacyBillModel = (HospitalPharmacyBillModel) result.body();
                    assert hospitalPharmacyBillModel != null;
                    if (!hospitalPharmacyBillModel.getError() && hospitalPharmacyBillModel.getBillData().size() > 0) {
                        tMessage.setVisibility(View.GONE);
                        pharmacyBillData = (ArrayList<PharmacyBillDatum>) hospitalPharmacyBillModel.getBillData();
                    } else {
                        tMessage.setVisibility(View.VISIBLE);
                        tMessage.setText("No pharmacy bill created");
                    }
                    hospitalIpdPharmacyData();
                }

                @Override
                public void onResponseFailed(String error) {

                }
            });
        }
    }

    private void hospitalIpdPharmacyData() {
        rvHospitalIpd.setHasFixedSize(true);
        rvHospitalIpd.setLayoutManager(new LinearLayoutManager(mContext));
        pharmacyAdapter = new HospitalIpdPharmacyAdapter(pharmacyBillData, mContext, HospitalIpdlistActivity.this);
        rvHospitalIpd.setAdapter(pharmacyAdapter);
    }


    private void fetchPathology() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalPathologyBillList(new Dialog(mContext), retrofitApiClient.getHospitalPathologyBill0List(ipdId), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    rvHospitalIpd.setVisibility(View.VISIBLE);
                    HospitalPathologyBillModel hospitalPathologyBillModel = (HospitalPathologyBillModel) result.body();
                    assert hospitalPathologyBillModel != null;
                    if (!hospitalPathologyBillModel.getError() && hospitalPathologyBillModel.getBillData().size() > 0) {
                        tMessage.setVisibility(View.GONE);
                        pathologyBillList = (ArrayList<PathologyBillDatum>) hospitalPathologyBillModel.getBillData();
                    } else {
                        tMessage.setVisibility(View.VISIBLE);
                        tMessage.setText("No pathology bill created");
                    }
                    hospitalIpdPathologyData();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, error);

                }
            });
        }
    }


    private void hospitalIpdPathologyData() {
        rvHospitalIpd.setHasFixedSize(true);
        rvHospitalIpd.setLayoutManager(new LinearLayoutManager(mContext));
        pathologyAdapter = new HospitalIpdPathologyAdapter(pathologyBillList, mContext, HospitalIpdlistActivity.this);
        rvHospitalIpd.setAdapter(pathologyAdapter);

    }

    private void fetchBed() {
        //getHospitalIpdBedList
        if (cd.isNetworkAvailable()) {
            RetrofitService.getHospitalIpdBedList(new Dialog(mContext), retrofitApiClient.getHospitalIpdBedList(ipdId), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    rvHospitalIpd.setVisibility(View.VISIBLE);
                    HospitalBedListModel hospitalBedListModel = (HospitalBedListModel) result.body();

                    assert hospitalBedListModel != null;
                    if (!hospitalBedListModel.getError() && hospitalBedListModel.getIpdBed().size() > 0) {
                        tMessage.setVisibility(View.GONE);
                        ipdBedList = (ArrayList<IpdBed>) hospitalBedListModel.getIpdBed();

                    } else {

                        tMessage.setVisibility(View.VISIBLE);
                        tMessage.setText("No bed created");
                    }
                    hospitalIpdBedData();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, error);
                }
            });
        }
    }


    private void hospitalIpdBedData() {
        rvHospitalIpd.setHasFixedSize(true);
        rvHospitalIpd.setLayoutManager(new LinearLayoutManager(mContext));
        bedAdapter = new HospitalIpdLBedAdapter(ipdBedList, mContext, HospitalIpdlistActivity.this);
        rvHospitalIpd.setAdapter(bedAdapter);
    }

    private void fetchServices() {
        if (cd.isNetworkAvailable()) {
            clearList();
            RetrofitService.getHospitalIpdBillservicesList(new Dialog(mContext), retrofitApiClient.getHospitalIpBillServicesList(ipdId), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    rvHospitalIpd.setVisibility(View.VISIBLE);
                    SelectIpdServicesMainModal ipdServicesMainModal = (SelectIpdServicesMainModal) result.body();

                    if (!ipdServicesMainModal.getResult() && ipdServicesMainModal.getIpdBillService().size() > 0) {
                        tMessage.setVisibility(View.GONE);
                        ipdBillServiceList = (ArrayList<IpdBillService>) ipdServicesMainModal.getIpdBillService();

                    } else {
                        tMessage.setVisibility(View.VISIBLE);
                        tMessage.setText("No Service created");
                    }
                    hospitalIpdServicesData();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, error);
                }
            });
        }
    }

    private void hospitalIpdServicesData() {
        rvHospitalIpd.setHasFixedSize(true);
        rvHospitalIpd.setLayoutManager(new LinearLayoutManager(mContext));
        hospitalIpdBilServiceAdapter = new HospitalIpdBilServiceAdapter(ipdBillServiceList, mContext, HospitalIpdlistActivity.this);
        rvHospitalIpd.setAdapter(hospitalIpdBilServiceAdapter);
    }


    private void clearList() {
        pharmacyBillData.clear();
        pathologyBillList.clear();
        ipdBedList.clear();
        hospitalIpdSlip.clear();
        operationSchedule.clear();

    }


}

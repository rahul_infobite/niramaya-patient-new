package com.ibt.niramaya.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.nurse_ipd_reporting.IpdReportList;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.io.ByteArrayOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NurseIpdReportDetailActivity extends BaseActivity implements View.OnClickListener {
    private String strAllergy = "";
    private TextView etResult;
    private IpdReportList ipdReportList;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nurse_report_detail);
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        Intent intent = getIntent();
        if (getIntent() != null) {
            ipdReportList = intent.getParcelableExtra("ipdReportList");
            String dobirth = intent.getStringExtra("dob");
            String pid = intent.getStringExtra("pid");
            String pname = intent.getStringExtra("pname");

            String dob = String.valueOf(getAge(dobirth));
            ((TextView) findViewById(R.id.tvPatientId)).setText(pid);
            ((TextView) findViewById(R.id.tvPatientName)).setText(pname);
            ((TextView) findViewById(R.id.tvYear)).setText(dob);
            init();
        }
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbarInvoice);
        showHomeBackOnToolbar(toolbar);
        this.setTitle("Nurse Report");

        etResult = findViewById(R.id.etResult);


        ImageView llSituationCanvas = findViewById(R.id.llSituationCanvas);
        ImageView llBackgroundCanvas = findViewById(R.id.llBackgroundCanvas);
        ImageView llRecommendationCanvas = findViewById(R.id.llRecommendationCanvas);

        Glide.with(mContext).load(ipdReportList.getCheifComplaints()).into(llSituationCanvas);
        Glide.with(mContext).load(ipdReportList.getHistory()).into(llBackgroundCanvas);
        Glide.with(mContext).load(ipdReportList.getNote()).into(llRecommendationCanvas);

        ((TextView) findViewById(R.id.etBP)).setText(ipdReportList.getBp());
        ((TextView) findViewById(R.id.etHeartRate)).setText(ipdReportList.getHeartRatePerMin());
        ((TextView) findViewById(R.id.etRespRate)).setText(ipdReportList.getRespRateMin());
        ((TextView) findViewById(R.id.etTemp)).setText(ipdReportList.getTemp());
        ((TextView) findViewById(R.id.etSpO2)).setText(ipdReportList.getSpo2());
        ((TextView) findViewById(R.id.etPainScore)).setText(ipdReportList.getPainScore());
        ((TextView) findViewById(R.id.etGCS)).setText(ipdReportList.getGcs());
        ((TextView) findViewById(R.id.etVitals)).setText(ipdReportList.getVitals());
        ((TextView) findViewById(R.id.etAllergies)).setText(ipdReportList.getAllergy());
        ((TextView) findViewById(R.id.etLastTreatment)).setText("Last Treatment : " + ipdReportList.getLastTreatment());
        ((TextView) findViewById(R.id.etReleavent)).setText("Releavent Drug : " + ipdReportList.getReleaventDrug());
        ((TextView) findViewById(R.id.etRemark)).setText("Follow up advice : " + ipdReportList.getFollowUpAdvice());
        ((TextView) findViewById(R.id.etComment)).setText("Comment : " + ipdReportList.getComment());

        //   ((TextView) findViewById(R.id.tvNausea)).setText("Yes");

        String strAllgery = ipdReportList.getAllergy();
        if (strAllgery.equals("1")) {
            ((TextView) findViewById(R.id.tvAllergy)).setText("Yes");
        } else {
            ((TextView) findViewById(R.id.tvAllergy)).setText("No");
        }
        String fallRisks = ipdReportList.getFallRisk();
        if (fallRisks.equals("1")) {
            ((TextView) findViewById(R.id.tvFallRisks)).setText("Yes");
        } else {
            ((TextView) findViewById(R.id.tvFallRisks)).setText("No");
        }
        String operationStatus = ipdReportList.getIsOxygen();
        if (operationStatus.equals("1")) {
            ((TextView) findViewById(R.id.tvIsOxygen)).setText("The Patient is on oxygen");
        } else {
            ((TextView) findViewById(R.id.tvIsOxygen)).setText("Not On oxygen");
        }
        String redFlag = ipdReportList.getRedFlag();
        if (redFlag.equals("1")) {
            ((TextView) findViewById(R.id.tvRedFlag)).setText("Yes");
        } else {
            ((TextView) findViewById(R.id.tvRedFlag)).setText("Yes");
        }

//
        //  radioGroupData();

        // setViewPager();
    }


    private String convertToBase64(String path) {

        Bitmap bm = BitmapFactory.decodeFile(path);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bm.compress(Bitmap.CompressFormat.PNG, 100, baos); //bm is the bitmap object
        byte[] b = baos.toByteArray();

        String encodedImage = Base64.encodeToString(b, Base64.DEFAULT);

        return encodedImage;
    }

    /* @Override
     public boolean onCreateOptionsMenu(Menu menu) {
         getMenuInflater().inflate(R.menu.menu_submit_button_nurse_report, menu);
         //   menuItem = menu.findItem(R.id.btnSubmit);
         return true;
     }

     @Override
     public boolean onOptionsItemSelected(MenuItem item) {
         switch (item.getItemId()) {
             case R.id.btnSubmit:
                 break;
             default:
                 return super.onOptionsItemSelected(item);
         }
         return true;
     }
 */
    @SuppressLint("ResourceAsColor")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        }

    }

    private String getAge(String dobString) {
        Date date = null;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return String.valueOf(0);

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age1 = (today.get(Calendar.YEAR) - dob.get(Calendar.YEAR));
        int month1 = (today.get(Calendar.MONTH) - dob.get(Calendar.MONTH));
        String age = (age1 + "." + month1);
        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
        }


        return age;
    }


}

package com.ibt.niramaya.ui.fragment;

import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.hospital_content_adapter.HospitalContentAdapter;
import com.ibt.niramaya.adapter.hospital_content_adapter.HospitalFaqAdapter;
import com.ibt.niramaya.modal.hospital_about_content.AboutContentMainModal;
import com.ibt.niramaya.modal.hospital_about_content.Content;
import com.ibt.niramaya.modal.hospital_about_content.Faq;
import com.ibt.niramaya.modal.hospital_about_content.FaqQandAMainModal;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationItem;
import com.luseen.luseenbottomnavigation.BottomNavigation.BottomNavigationView;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Response;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;
import static com.ibt.niramaya.ui.HomeActivity.txtTitle;

public class AboutUsFragment extends BaseFragment implements View.OnClickListener {

    private List<Content> contents = new ArrayList<>();
    private List<Faq> faqs = new ArrayList<>();

    private View rootView;
    private HospitalContentAdapter contentAdapter;
    private HospitalFaqAdapter faqAdapter;
    private RecyclerView rvFaq;
    private View viewFaq, viewPrivacy, viewTerms, viewAbout;
    private TextView tvContent;
    private BottomNavigationView bottomNavigationView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_about_us, container, false);
        mContext = getActivity();
        mContext = getContext();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);
        init();
        return rootView;
    }

    private void init() {
        //Recyler faq list

        rvFaq = rootView.findViewById(R.id.rvFaq);
        rvFaq.setHasFixedSize(true);
        rvFaq.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        faqAdapter = new HospitalFaqAdapter(faqs, mContext, this);
        rvFaq.setAdapter(faqAdapter);
        faqAdapter.notifyDataSetChanged();

        rootView.findViewById(R.id.tvAbout).setOnClickListener(this);
        rootView.findViewById(R.id.tvTermsandCondition).setOnClickListener(this);
        rootView.findViewById(R.id.tvPrivacyPolicy).setOnClickListener(this);
        rootView.findViewById(R.id.tvFaq).setOnClickListener(this);

        viewAbout = rootView.findViewById(R.id.viewAbout);
        viewTerms = rootView.findViewById(R.id.viewTermsandCon);
        viewPrivacy = rootView.findViewById(R.id.viewPrivacyPolicy);
        viewFaq = rootView.findViewById(R.id.viewFaq);
        tvContent = rootView.findViewById(R.id.tvContent1);
        rvFaq.setVisibility(View.GONE);


        bottomNavigationView = rootView.findViewById(R.id.bottomNavigation);

        BottomNavigationItem about = new BottomNavigationItem
                ("About", ContextCompat.getColor(mContext, R.color.colorPrimary), R.drawable.ic_about_icon);
        BottomNavigationItem tandC = new BottomNavigationItem
                (getResources().getString(R.string.tandc), ContextCompat.getColor(mContext, R.color.colorPrimaryDark), R.drawable.ic_term);
        BottomNavigationItem privacyPolicy = new BottomNavigationItem
                ("Privacy Policy", ContextCompat.getColor(mContext, R.color.red_j), R.drawable.ic_lock);
        BottomNavigationItem faq = new BottomNavigationItem
                ("FAQ", ContextCompat.getColor(mContext, R.color.red_i), R.drawable.ic_answer);

        bottomNavigationView.addTab(about);
        bottomNavigationView.addTab(tandC);
        bottomNavigationView.addTab(privacyPolicy);
        bottomNavigationView.addTab(faq);

        bottomNavigationView.setOnBottomNavigationItemClickListener(index -> {
            switch (index) {
                case 0:
                    rvFaq.setVisibility(View.GONE);
                    tvContent.setVisibility(View.VISIBLE);
                    viewAbout.setVisibility(View.VISIBLE);
                    viewTerms.setVisibility(View.GONE);
                    viewPrivacy.setVisibility(View.GONE);
                    viewFaq.setVisibility(View.GONE);
                    setvalues(contents.get(0));
                    break;
                case 1:
                    tvContent.setVisibility(View.VISIBLE);
                    rvFaq.setVisibility(View.GONE);
                    viewAbout.setVisibility(View.GONE);
                    viewTerms.setVisibility(View.VISIBLE);
                    viewPrivacy.setVisibility(View.GONE);
                    viewFaq.setVisibility(View.GONE);
                    setvalues(contents.get(1));
                    break;
                case 2:
                    tvContent.setVisibility(View.VISIBLE);
                    rvFaq.setVisibility(View.GONE);
                    viewAbout.setVisibility(View.GONE);
                    viewTerms.setVisibility(View.GONE);
                    viewPrivacy.setVisibility(View.VISIBLE);
                    viewFaq.setVisibility(View.GONE);
                    setvalues(contents.get(2));
                    break;
                case 3:
                    rvFaq.setVisibility(View.VISIBLE);
                    viewAbout.setVisibility(View.GONE);
                    viewTerms.setVisibility(View.GONE);
                    viewPrivacy.setVisibility(View.GONE);
                    viewFaq.setVisibility(View.VISIBLE);
                    txtTitle.setText("FAQ");
                    tvContent.setVisibility(View.GONE);
                    faqApi();
                    break;
            }
        });

        contentApi();
        // faqApi();
    }

    private void contentApi() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.fetchContent(new Dialog(mContext), retrofitApiClient.contentData(),
                    new WebResponse() {
                        @Override
                        public void onResponseSuccess(Response<?> result) {
                            contents.clear();
                            AboutContentMainModal mainModal = (AboutContentMainModal) result.body();
                            assert mainModal != null;
                            if (!mainModal.getError()) {
                                if (mainModal.getContent().size() > 0) {
                                    // Recyler content list

                             /*       RecyclerView rvContentList = rootView.findViewById(R.id.rvContent);
                                    rvContentList.setHasFixedSize(true);
                                    rvContentList.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
                                    contentAdapter = new HospitalContentAdapter(contents, mContext, i);
                                    rvContentList.setAdapter(contentAdapter);*/
                                    contents.addAll(mainModal.getContent());
                                    setvalues(contents.get(0));
                                    //  contentAdapter.notifyDataSetChanged();
                                    // Alerts.show(mContext, mainModal.getMessage());
                                }
                            } else {
                                Alerts.show(mContext, mainModal.getMessage());
                            }
                        }

                        @Override
                        public void onResponseFailed(String error) {
                            Alerts.show(mContext, "Server Error!!!");
//  Alerts.show(mContext, error);
                        }
                    });
        }
    }

    private void faqApi() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.fetchFaq(new Dialog(mContext), retrofitApiClient.faqData(),
                    new WebResponse() {
                        @Override
                        public void onResponseSuccess(Response<?> result) {
                            faqs.clear();
                            FaqQandAMainModal mainModal = (FaqQandAMainModal) result.body();
                            assert mainModal != null;
                            if (!mainModal.getError()) {
                                if (mainModal.getFaq().size() > 0) {
                                    rvFaq.setVisibility(View.VISIBLE);
                                    faqs.addAll(mainModal.getFaq());
                                    faqAdapter.notifyDataSetChanged();
                                    // Alerts.show(mContext, mainModal.getMessage());
                                }
                            } else {
                                Alerts.show(mContext, mainModal.getMessage());
                            }
                        }

                        @Override
                        public void onResponseFailed(String error) {
                            Alerts.show(mContext, error);
                        }
                    });
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvAbout:
                rvFaq.setVisibility(View.GONE);
                tvContent.setVisibility(View.VISIBLE);
                viewAbout.setVisibility(View.VISIBLE);
                viewTerms.setVisibility(View.GONE);
                viewPrivacy.setVisibility(View.GONE);
                viewFaq.setVisibility(View.GONE);
                setvalues(contents.get(0));
                break;
            case R.id.tvTermsandCondition:
                tvContent.setVisibility(View.VISIBLE);
                rvFaq.setVisibility(View.GONE);
                viewAbout.setVisibility(View.GONE);
                viewTerms.setVisibility(View.VISIBLE);
                viewPrivacy.setVisibility(View.GONE);
                viewFaq.setVisibility(View.GONE);
                setvalues(contents.get(1));
                break;
            case R.id.tvPrivacyPolicy:
                tvContent.setVisibility(View.VISIBLE);
                rvFaq.setVisibility(View.GONE);
                viewAbout.setVisibility(View.GONE);
                viewTerms.setVisibility(View.GONE);
                viewPrivacy.setVisibility(View.VISIBLE);
                viewFaq.setVisibility(View.GONE);
                setvalues(contents.get(2));
                break;
            case R.id.tvFaq:
                rvFaq.setVisibility(View.VISIBLE);
                viewAbout.setVisibility(View.GONE);
                viewTerms.setVisibility(View.GONE);
                viewPrivacy.setVisibility(View.GONE);
                viewFaq.setVisibility(View.VISIBLE);
                txtTitle.setText("FAQ");
                rootView.findViewById(R.id.tvContent1).setVisibility(View.GONE);
                faqApi();
                break;
        }
    }

    private void setvalues(Content content) {
        txtTitle.setText(content.getTitle());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            ((TextView) rootView.findViewById(R.id.tvContent1)).setText(Html.fromHtml(content.getContent(), Html.FROM_HTML_MODE_COMPACT));

        } else {
            ((TextView) rootView.findViewById(R.id.tvContent1)).setText(Html.fromHtml(content.getContent()));
        }

    }
}

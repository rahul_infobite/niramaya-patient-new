package com.ibt.niramaya.ui.activity.ambulance;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;

import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.ibt.niramaya.R;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseActivity;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;

public class SearchLocationActivity extends BaseActivity {

    private double latitude = 0.0;
    private double longitude = 0.0;
    private String addressName = "";
    private String from = "";
    private final String PICKUP = "pickUp";
    private final String DROP = "drop";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_location);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        showHomeBackOnToolbar(toolbar);

        from = getIntent().getExtras().getString("FROM");

        initLocationSearch();
        (findViewById(R.id.btnDone)).setOnClickListener(v -> returnData());
    }

    private void initLocationSearch() {
        Places.initialize(getApplicationContext(), getResources().getString(R.string.google_key));
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        assert autocompleteFragment != null;
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(@NotNull Place place) {
                LatLng latLng = place.getLatLng();
                latitude = latLng.latitude;
                longitude = latLng.longitude;
                addressName = place.getName();
                Alerts.show(mContext, place.getName());
                Log.e("lat : ", String.valueOf(latitude));
                Log.e("longi : ", String.valueOf(longitude));
            }

            @Override
            public void onError(Status status) {
                Alerts.show(mContext, status.getStatusMessage());
            }
        });
    }

    private void returnData() {
        Intent intent = new Intent();
        intent.putExtra("LATITUDE", latitude);
        intent.putExtra("LONGITUDE", longitude);
        intent.putExtra("ADDRESS_NAME", addressName);
        if (from.equals(PICKUP)) {
            setResult(117, intent);
        } else if (from.equals(DROP)) {
            setResult(119, intent);
        }
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent returnIntent = new Intent();
        setResult(Activity.RESULT_CANCELED, returnIntent);
        finish();
    }
}

package com.ibt.niramaya.ui.fragment;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.patient_adapter.PatientListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.ambulance.ongoing.AmbulanceOngoingModel;
import com.ibt.niramaya.modal.patient_modal.PaitentProfile;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.ambulance.AddEmergencyActivity;
import com.ibt.niramaya.ui.activity.ambulance.AmbulanceActivity;
import com.ibt.niramaya.ui.activity.ambulance.AmbulanceDetailActivity;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;

public class AmbulanceFragment extends BaseFragment implements View.OnClickListener {
    private View rootView;
    private ConnectionDetector cd;
    private PatientListAdapter patientListAdapter;
    private List<PaitentProfile> patientList = new ArrayList<>();
    private RecyclerView rvPatientList;
    private LinearLayout llAddPatient;
    private int position;
    private String ongoingCall = "emergency";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_ambulance, container, false);
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    private void init() {
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);

        ImageView ivFilter = Objects.requireNonNull(getActivity()).findViewById(R.id.ivFilter);
        ivFilter.setVisibility(View.GONE);

        rvPatientList = rootView.findViewById(R.id.rvPatientList);
        (rootView.findViewById(R.id.cvNormal)).setOnClickListener(this);
        (rootView.findViewById(R.id.cvEmergency)).setOnClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        String userMobile = AppPreference.getStringPreference(mContext, Constant.USER_CONTACT);
        fetchUserEmergencyOnGoing(userMobile);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cvNormal:
                normalCall();
                break;
            case R.id.cvEmergency:
                emergencyCall();
                break;
        }
    }

    private void normalCall() {
        if (AppPreference.getBooleanPreference(mContext, Constant.PATIENT_AMBULANCE_BOOKING_STATUS)) {
            String patientId = AppPreference.getStringPreference(mContext, Constant.PATIENT_NAME_AMBULANCE_BOOKED);
            String latitude = AppPreference.getStringPreference(mContext, Constant.PATIENT_BOOKING_LATITUDE);
            String longitude = AppPreference.getStringPreference(mContext, Constant.PATIENT_BOOKING_LONGITUDE);
            Double sLatitude = Double.valueOf(latitude);
            Double sLongitude = Double.valueOf(longitude);
            startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                    .putExtra("From", "listing")
                    .putExtra("PatientId", patientId)
                    .putExtra("Latitude", sLatitude)
                    .putExtra("Longitude", sLongitude));
        } else {
            startActivity(new Intent(mContext, AmbulanceActivity.class));
        }
    }

    private void emergencyCall() {
        startActivity(new Intent(mContext, AddEmergencyActivity.class));
    /*    if (AppPreference.getBooleanPreference(mContext, Constant.PATIENT_AMBULANCE_BOOKING_STATUS)) {
            String userContact = AppPreference.getStringPreference(mContext, Constant.PATIENT_BOOKED_BY_USER_CONTACT);
            String latitude = AppPreference.getStringPreference(mContext, Constant.PATIENT_BOOKING_LATITUDE);
            String longitude = AppPreference.getStringPreference(mContext, Constant.PATIENT_BOOKING_LONGITUDE);
            Double sLatitude = Double.valueOf(latitude);
            Double sLongitude = Double.valueOf(longitude);
            startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                    .putExtra("From", "emergency")
                    .putExtra("userContact", userContact)
                    .putExtra("Latitude", sLatitude)
                    .putExtra("Longitude", sLongitude));
        } else {
            startActivity(new Intent(mContext, AddEmergencyActivity.class));
        }
    */
    }


    private void fetchUserEmergencyOnGoing(String callBy) {
        if (cd.isNetworkAvailable()) {
            RetrofitService.patientOnGoingambulanceBooking(new Dialog(mContext), retrofitApiClient.patientEmergencyOngoingBooking(callBy), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    AmbulanceOngoingModel ongoingModel = (AmbulanceOngoingModel) result.body();
                    assert ongoingModel != null;
                    if (!ongoingModel.getError()) {
                        if (ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("0")
                                || ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("1")
                                || ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("2")) {
                            startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                                    .putExtra("From", "emergency")
                                    .putExtra("userContact", callBy)
                                    .putExtra("Latitude", ongoingModel.getAmbulanceBookingList().getAmbulanceBookingLatitude())
                                    .putExtra("Longitude", ongoingModel.getAmbulanceBookingList().getAmbulanceBookingLongitude()));

                        }
                    } else {
                        String patientId = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);
                        fetchUserOnGoing(patientId);
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Log.e("NIRAMAYA_PATIENT", "Server Error!!!");
                }
            });
        }
    }

    private void fetchUserOnGoing(String callBy) {
        if (cd.isNetworkAvailable()) {
            RetrofitService.patientOnGoingambulanceBooking(new Dialog(mContext), retrofitApiClient.ongoingAmbulance(callBy), new WebResponse() {
                @SuppressLint("SetTextI18n")
                @Override
                public void onResponseSuccess(Response<?> result) {
                    AmbulanceOngoingModel ongoingModel = (AmbulanceOngoingModel) result.body();
                    assert ongoingModel != null;
                    if (!ongoingModel.getError()) {
                        if (ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("0")
                                || ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("1")
                                || ongoingModel.getAmbulanceBookingList().getAmbulanceBookingStatus().equals("2")) {
                            startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                                    .putExtra("From", "listing")
                                    .putExtra("PatientId", callBy)
                                    .putExtra("Latitude", ongoingModel.getAmbulanceBookingList().getAmbulanceBookingLatitude())
                                    .putExtra("Longitude", ongoingModel.getAmbulanceBookingList().getAmbulanceBookingLongitude()));

                        }
                    } else {
                        init();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Log.e("NIRAMAYA_PATIENT", "Server Error!!!");
                }
            });
        }
    }


}

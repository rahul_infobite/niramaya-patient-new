package com.ibt.niramaya.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.ui.fragment.ipd_reports_fragment.BloodRequisitionSlipFragment;
import com.ibt.niramaya.ui.fragment.ipd_reports_fragment.IpdTestReportsFragment;
import com.ibt.niramaya.ui.fragment.ipd_reports_fragment.NurseIpdReportsFragment;
import com.ibt.niramaya.ui.fragment.ipd_reports_fragment.PrescriptionReportsFragmentDoctor;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class SelectedIpdReportActivity extends BaseActivity implements View.OnClickListener {
    public static FragmentManager fragmentManager;
    private String ipdId;
    private String patientName;
    private String patientDob;
    private String patientGender;
    private String patientId;
    private String patientContact;
    private String patientProfile = "";
    private String ipdDate = "";
    Toolbar toolbar;
    private TextView tvTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selected_ipd_reports);

        toolbar = findViewById(R.id.cToolbar);
        showHomeBackOnToolbar(toolbar);
        initViews();
    }

    @SuppressLint("SetTextI18n")
    private void initViews() {
        Intent intent = getIntent();
        if (getIntent() != null) {
            patientName = intent.getStringExtra("patientName");
            patientId = intent.getStringExtra("pId");
            patientDob = intent.getStringExtra("pDob");
            patientContact = intent.getStringExtra("pContact");
            patientGender = intent.getStringExtra("pGender");
            patientProfile = intent.getStringExtra("pProfile");
            ipdId = intent.getStringExtra("ipdId");
            ipdDate = intent.getStringExtra("createDate");
            if (!patientProfile.isEmpty()) {
                Glide.with(mContext).load(patientProfile).placeholder(R.drawable.ic_profile).into((CircleImageView) findViewById(R.id.ivProfileImage));
            }
        }

        TextView tvPatientId = findViewById(R.id.tvPatientId);
        TextView tvPatientName = findViewById(R.id.tvPatientName);
        String patientNumber = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_NUMBER);


        tvPatientName.setText(patientName);
        tvPatientId.setText("Patient Number : " + patientNumber);
        findViewById(R.id.tvVieweReports).setOnClickListener(this);
        findViewById(R.id.tvIpdPrescription).setOnClickListener(this);
        findViewById(R.id.tvBloodRequisitionSlip).setOnClickListener(this);
        findViewById(R.id.tvIpdTestReport).setOnClickListener(this);
        tvTitle = findViewById(R.id.tvTitle);
        tvTitle.setText("Nurse Reports");

        findViewById(R.id.llProfileSection).setVisibility(View.VISIBLE);
        findViewById(R.id.selected_patient_frame).setVisibility(View.VISIBLE);
        findViewById(R.id.llSection).setVisibility(View.VISIBLE);


        fragmentManager = getSupportFragmentManager();
        replaceFragment(new NurseIpdReportsFragment(), Constant.NurseIpdReportsFragment);

    }

    private void replaceFragment(Fragment fragment, String tag) {
        Bundle bundle = new Bundle();
        bundle.putString("PatientName", patientName);
        bundle.putString("PatientDOB", patientDob);
        bundle.putString("PatientGender", patientGender);
        bundle.putString("PatientId", patientId);
        bundle.putString("PatientContact", patientContact);
        bundle.putString("PatienProfile", patientProfile);
        bundle.putString("ipdDate", ipdDate);
        bundle.putString("ipdId", ipdId);
        fragment.setArguments(bundle);
        fragmentManager
                .beginTransaction()
                .replace(R.id.selected_patient_frame, fragment,
                        tag).commit();

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvVieweReports:
                tvTitle.setText("Nurse Reports");
                replaceFragment(new NurseIpdReportsFragment(), Constant.NurseIpdReportsFragment);
                findViewById(R.id.tvVieweReports).setBackground(getResources().getDrawable(R.drawable.layout_bg_gray_p6));
                findViewById(R.id.tvIpdPrescription).setBackgroundColor(getResources().getColor(R.color.gray_i));
                findViewById(R.id.tvBloodRequisitionSlip).setBackgroundColor(getResources().getColor(R.color.gray_i));
                findViewById(R.id.tvIpdTestReport).setBackgroundColor(getResources().getColor(R.color.gray_i));
                break;
            case R.id.tvIpdPrescription:
                tvTitle.setText("Doctor Prescription");
                replaceFragment(new PrescriptionReportsFragmentDoctor(), Constant.PrescriptionReportsFragmentDoctor);
                findViewById(R.id.tvIpdPrescription).setBackground(getResources().getDrawable(R.drawable.layout_bg_gray_p6));
                findViewById(R.id.tvVieweReports).setBackgroundColor(getResources().getColor(R.color.gray_i));
                findViewById(R.id.tvBloodRequisitionSlip).setBackgroundColor(getResources().getColor(R.color.gray_i));
                findViewById(R.id.tvIpdTestReport).setBackgroundColor(getResources().getColor(R.color.gray_i));
                break;
            case R.id.tvBloodRequisitionSlip:
                tvTitle.setText("Blood Requisition");
                replaceFragment(new BloodRequisitionSlipFragment(), Constant.BloodRequisitionSlipFragment);
                findViewById(R.id.tvBloodRequisitionSlip).setBackground(getResources().getDrawable(R.drawable.layout_bg_gray_p6));
                findViewById(R.id.tvIpdTestReport).setBackground(getResources().getDrawable(R.color.gray_i));
                findViewById(R.id.tvVieweReports).setBackgroundColor(getResources().getColor(R.color.gray_i));
                break;
            case R.id.tvIpdTestReport:
                tvTitle.setText("Test Reports");
                replaceFragment(new IpdTestReportsFragment(), Constant.IpdTestReportsFragment);
                findViewById(R.id.tvIpdTestReport).setBackground(getResources().getDrawable(R.drawable.layout_bg_gray_p6));
                findViewById(R.id.tvBloodRequisitionSlip).setBackground(getResources().getDrawable(R.color.gray_i));
                findViewById(R.id.tvIpdTestReport).setBackground(getResources().getDrawable(R.color.gray_i));
                findViewById(R.id.tvVieweReports).setBackgroundColor(getResources().getColor(R.color.gray_i));
                break;

        }
    }
}

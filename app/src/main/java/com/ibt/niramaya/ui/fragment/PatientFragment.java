package com.ibt.niramaya.ui.fragment;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.patient_adapter.PatientListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.patient_modal.PaitentProfile;
import com.ibt.niramaya.modal.patient_modal.PatientMainModal;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.patient.AddNewPatientActivity;
import com.ibt.niramaya.ui.activity.patient.PatientDetailActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;

public class PatientFragment extends BaseFragment implements View.OnClickListener {
    private View rootView;
    private ConnectionDetector cd;
    private PatientListAdapter patientListAdapter;
    private List<PaitentProfile> patientList = new ArrayList<>();
    private RecyclerView rvPatientList;
    private LinearLayout llAddPatient;
    private int position;
    private String invokeContact="";
    private String invokeVerificationType="";
    private String invokePatientName="";
    private String invokePatientId="";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_patient, container, false);
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        return rootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        init();


    }

    private void init() {
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);
        ImageView ivFilter = getActivity().findViewById(R.id.ivFilter);
        ivFilter.setVisibility(View.GONE);
        rvPatientList = rootView.findViewById(R.id.rvPatientList);
        llAddPatient = rootView.findViewById(R.id.llAddPatient);
        llAddPatient.setOnClickListener(this);


    }

    @Override
    public void onResume() {
        super.onResume();
        patientListApi();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.llPatient:
                position = (int) v.getTag();
                if (!patientList.get(position).getPatientId().equals("00")){
                    Intent intent = new Intent(mContext, PatientDetailActivity.class);
                    intent.putExtra("patientDetail", patientList.get(position));
                    startActivity(intent);
                }else {

                }
                break;
            case R.id.llAddPatient:
                startActivity(new Intent(mContext, AddNewPatientActivity.class));
                break;
        }
    }

    private void patientListApi() {
        if (cd.isNetworkAvailable()) {
            String strUserId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
            RetrofitService.getPatientList(new Dialog(mContext), retrofitApiClient.patientList(strUserId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    PatientMainModal mainModal = (PatientMainModal) result.body();
                    assert mainModal != null;


                    if (!mainModal.getError() && mainModal.getUser().getPaitentProfile() !=null) {
                        if (mainModal.getUser().getPaitentProfile().size()>0){
                            patientList = mainModal.getUser().getPaitentProfile();
                            String strPatientId = mainModal.getUser().getPaitentProfile().get(position).getPatientId();
                            AppPreference.setStringPreference(mContext, Constant.CURRENT_PATENT_ID, strPatientId);
                        }
                        llAddPatient.setVisibility(View.GONE);
                        rvPatientList.setVisibility(View.VISIBLE);

                        rvPatientList.setHasFixedSize(true);
                        rvPatientList.setLayoutManager(new GridLayoutManager(mContext, 3));
                        patientListAdapter = new PatientListAdapter(patientList, mContext, PatientFragment.this);
                        rvPatientList.setAdapter(patientListAdapter);
                        PaitentProfile patientProfile1 = new PaitentProfile();
                        patientProfile1.setPatientId("0");
                        patientProfile1.setPatientName("Add New Patient");
                        patientList.add(patientList.size(), patientProfile1);
                        PaitentProfile patientProfile2 = new PaitentProfile();
                        patientProfile2.setPatientId("00");
                        patientProfile2.setPatientName("Invoke Patient Profile");
                        patientList.add(patientList.size(), patientProfile2);
                        patientListAdapter.notifyDataSetChanged();

                        patientListAdapter.notifyDataSetChanged();
                        for (PaitentProfile pt : patientList){
                            if (pt.getPatientId().equals(strUserId)){
                                AppPreference.setStringPreference(mContext, Constant.USER_PROFILE_IMAGE, pt.getPatientProfilePicture());
                                AppPreference.setStringPreference(mContext, Constant.USER_NAME, pt.getPatientName());
                                AppPreference.setStringPreference(mContext, Constant.USER_CONTACT, pt.getPatientContact());
                                TextView txtPhone = Objects.requireNonNull(getActivity()).findViewById(R.id.txtPhone);
                                TextView txtName = getActivity().findViewById(R.id.txtName);
                                ImageView imgProfile = getActivity().findViewById(R.id.imgProfile);

                                txtPhone.setText(pt.getPatientContact());
                                txtName.setText(pt.getPatientName());
                                Glide.with(mContext)
                                        .load(pt.getPatientProfilePicture())
                                        .placeholder(R.drawable.ic_profile)
                                        .into(imgProfile);
                            }
                        }

                    //    Alerts.show(mContext, mainModal.getMessage());
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());

                        llAddPatient.setVisibility(View.VISIBLE);
                        rvPatientList.setVisibility(View.GONE);
                    }
                }
                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                    // Alerts.show(mContext, error);
                }
            });
        }
    }

}

package com.ibt.niramaya.ui.activity.hospital_ipd_detail_activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.ipd_test_report.TestList;
import com.ibt.niramaya.ui.activity.ViewImageAndFilesActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.BaseActivity;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PatientIpdReportDetailActivity extends BaseActivity implements View.OnClickListener {
    TestList testList;
    // Progress Dialog
    private ProgressDialog pDialog;
    public static final int progress_bar_type = 0;
    private LinearLayout llFoolView;
    private Bitmap bitmap;
    private String testName = "";

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_patient_reports);
        Toolbar toolbar = findViewById(R.id.toolbarReports);
        toolbar.setTitle("Report Detail");
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(v -> onBackPressed());

        Intent intent = getIntent();
        if (getIntent() != null) {
            testList = intent.getParcelableExtra("patientReportData");
            String name = testList.getPatientName();
            String doctorName = testList.getTestReferDoctor();
            testName = testList.getPathologyTestName();
            String textSample = testList.getTestSample();
            String testPresquistics = testList.getTestPrerequisites();

            ((TextView) findViewById(R.id.tvHospital)).setText(testList.getHospitalName());
            Glide.with(mContext).load(testList.getHospitalImage()).into(((ImageView) findViewById(R.id.ivHospital)));
            ((TextView) findViewById(R.id.patientName)).setText("Patient Name : " + name);
            ((TextView) findViewById(R.id.doctorName)).setText("Doctor Name : " + doctorName);
            ((TextView) findViewById(R.id.testName)).setText("Test Name : " + testName);
            ((TextView) findViewById(R.id.tvTestSample)).setText("Test : " + textSample);
            ((TextView) findViewById(R.id.tvTestSampleList)).setText("Samples : " + testList.getTestSampleList());
            ((TextView) findViewById(R.id.testPresquistics)).setText("Perquisites : " + testPresquistics);
            ((TextView) findViewById(R.id.tvTestDate)).setText("Sample Date : " + changeDateFormat(testList.getTestSampleDate()));
            ((TextView) findViewById(R.id.testSuggestion)).setText("Suggestion : " + testList.getIpdTestReportSuggestion());
            ((TextView) findViewById(R.id.testDescription)).setText("Test Description : " + testList.getIpdTestReportDescription());
            // Glide.with(mContext).load(testList.getTestStatusReportFile()).into(((ImageView) findViewById(R.id.ivImage)));

            String fileUrl = testList.getTestStatusReportFile();
            File file = new File(fileUrl);

            Button btnViewFile = findViewById(R.id.btnViewFile);
            if (testList.getIpdTestReportFileType().isEmpty()) {
                btnViewFile.setVisibility(View.GONE);
            } else {
                btnViewFile.setOnClickListener(view -> openFile(fileUrl, testList.getIpdTestReportFileType()));
            }
            findViewById(R.id.ivImage).setOnClickListener(v -> {
                viewReportImage(fileUrl);
            });
        }
        llFoolView = findViewById(R.id.llFoolView);


    }

    private void takeScreenshot(View v1) {

        Date now = new Date();
        android.text.format.DateFormat.format("dd-MM-yyyy", now);

        try {
            String mPath = Environment.getExternalStorageDirectory().toString() + "/" + testName + ".jpeg";

            v1 = llFoolView;//getWindow().getDecorView().getRootView()
            v1.setDrawingCacheEnabled(true);
            Bitmap bitmap = Bitmap.createBitmap(v1.getWidth(), v1.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            v1.draw(canvas);

            File imageFile = new File(mPath);

            FileOutputStream outputStream = new FileOutputStream(imageFile);
            int quality = 100;
            bitmap.compress(Bitmap.CompressFormat.JPEG, quality, outputStream);
            outputStream.flush();
            outputStream.close();

            //setting screenshot in imageview
            String filePath = imageFile.getPath();

            Bitmap ssbitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            //  iv.setImageBitmap(ssbitmap);
            Alerts.show(mContext, filePath);

        } catch (Throwable e) {
            Alerts.show(mContext, e.toString());
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_download, menu);
        return true;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_download:
                if (checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                    Log.v("TAG", "Permission is granted");
                    takeScreenshot(llFoolView);
                    return true;
                } else {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 123);
                }
                // new DownloadFileFromURL().execute(testList.getTestStatusReportFile());
                break;
        }
        return true;
    }

    /**
     * Showing Dialog
     */

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }

    private void viewReportImage(String imgUrl) {
        AlertDialog.Builder dialogBox = new AlertDialog.Builder(mContext);
        dialogBox.setCancelable(false);

        LayoutInflater li = LayoutInflater.from(mContext);
        @SuppressLint("InflateParams") final View view = li.inflate(R.layout.dialog_patient_report_image, null);
        dialogBox.setView(view);

        final AlertDialog alertDialog;
        alertDialog = dialogBox.create();
        alertDialog.show();
        Glide.with(mContext).load(imgUrl).into((ImageView) alertDialog.findViewById(R.id.imgReport));
        alertDialog.findViewById(R.id.imgClose).setOnClickListener(v -> alertDialog.dismiss());

    }

    /**
     * Background Async Task to download file
     */
    class DownloadFileFromURL extends AsyncTask<String, String, String> {

        /**
         * Before starting background thread Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            showDialog(progress_bar_type);
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();

                // this will be useful so that you can show a tipical 0-100%
                // progress bar
                int lenghtOfFile = conection.getContentLength();

                // download the file
                InputStream input = new BufferedInputStream(url.openStream(),
                        8192);

                // Output stream
                OutputStream output = new FileOutputStream(Environment
                        .getExternalStorageDirectory().toString()
                        + "/2011.kml");

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lenghtOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return null;
        }


        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task Dismiss the progress dialog
         **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);

        }

    }

    public String changeDateFormat(String time) {
        String inputPattern = "yyyy-MM-dd";
        String outputPattern = "dd/MM/yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern, Locale.ENGLISH);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern, Locale.ENGLISH);

        Date date = null;
        String str = null;
        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }


    @Override
    public void onClick(View v) {

    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void openFile(String url, String testReportFileType) {
        try {
            //  Uri uri = Uri.fromFile(url);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            if (testReportFileType.contains("doc") || testReportFileType.contains(".docx")) {
                activityIntent("file", url);
            } else if (testReportFileType.contains("pdf")) {
                activityIntent("file", url);
            } else if (testReportFileType.contains("ppt") || testReportFileType.contains(".pptx")) {
                // Powerpoint file
                activityIntent("file", url);
            } else if (testReportFileType.contains("xls") || testReportFileType.contains("xlsx")) {
                // Excel file
                activityIntent("file", url);
            } else if (testReportFileType.contains("jpg") || testReportFileType.contains("jpeg") || testReportFileType.contains("png")) {
                activityIntent("image", url);
            } else if (testReportFileType.contains("txt")) {
                // Text file
                activityIntent("file", url);
            }/* else if (url.toString().contains(".3gp") || url.toString().contains(".mpg") ||
                    url.toString().contains(".mpeg") || url.toString().contains(".mpe") || url.toString().contains(".mp4") || url.toString().contains(".avi")) {
                // Video files
                intent.setDataAndType(uri, "video/*");
            }else if (url.toString().contains(".zip")) {
                // ZIP file
                intent.setDataAndType(uri, "application/zip");
            } else if (url.toString().contains(".rar")) {
                // RAR file
                intent.setDataAndType(uri, "application/x-rar-compressed");
            } else if (url.toString().contains(".rtf")) {
                // RTF file
                intent.setDataAndType(uri, "application/rtf");
            } else if (url.toString().contains(".wav") || url.toString().contains(".mp3")) {
                // WAV audio file
                intent.setDataAndType(uri, "audio/x-wav");
            } else if (url.toString().contains(".gif")) {
                // GIF file
                activityIntent("file", uri);
            }
*/
        } catch (ActivityNotFoundException e) {
            Toast.makeText(mContext, "No application found which can open the file", Toast.LENGTH_SHORT).show();
        }
    }

    private void activityIntent(String type, String url) {
        startActivity(new Intent(mContext, ViewImageAndFilesActivity.class)
                .putExtra("type", type)
                .putExtra("url", url)
        );
    }

}

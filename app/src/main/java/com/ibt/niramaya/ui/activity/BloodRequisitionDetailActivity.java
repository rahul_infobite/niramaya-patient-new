package com.ibt.niramaya.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition;
import com.ibt.niramaya.utils.BaseActivity;

import de.hdodenhof.circleimageview.CircleImageView;

public class BloodRequisitionDetailActivity extends BaseActivity {
    BloodRequisition bloodRequisition ;

    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blood_requisition_detail);

        findViewById(R.id.ivBackPrescription).setOnClickListener(v -> finish());

        Intent intent = getIntent();
        if (getIntent() != null){
        bloodRequisition = intent.getParcelableExtra("bloodRequisition");

            ((EditText) findViewById(R.id.etProblemStatement)).setText(bloodRequisition.getProblemStatement());
            ((EditText) findViewById(R.id.etRequitisionDate)).setText(bloodRequisition.getRequisitionDob());
            ((EditText) findViewById(R.id.etPatientWeight)).setText(bloodRequisition.getPatientWeight()+"Kg");
            ((EditText) findViewById(R.id.etRequisitonType)).setText(bloodRequisition.getRequisitionType());
            ((EditText) findViewById(R.id.etWholeBlood)).setText(bloodRequisition.getWholeBlood());
            ((EditText) findViewById(R.id.etRcc)).setText(bloodRequisition.getRcc());
            ((EditText) findViewById(R.id.etPlateletsCount)).setText(bloodRequisition.getPlateletsConcentrate());
            ((EditText) findViewById(R.id.etCryoprecipitate)).setText(bloodRequisition.getCryoprecipitate());
            ((EditText) findViewById(R.id.etRequisitionUnit)).setText(bloodRequisition.getRequisitionUnit());
            ((EditText) findViewById(R.id.etRequisitionHistory)).setText(bloodRequisition.getRequisitionHistory());
            ((EditText) findViewById(R.id.etRequisitionDoctor)).setText(bloodRequisition.getRequisitionByDoctorName());
            
            if (bloodRequisition.getHospital() != null){
                ((TextView) findViewById(R.id.tv_hospital_name)).setText(bloodRequisition.getHospital().getHospitalName());
                ((TextView) findViewById(R.id.txtHospitalContact)).setText(bloodRequisition.getHospital().getHospitalContact());
                ((TextView) findViewById(R.id.hospitalFirstAdd)).setText(bloodRequisition.getHospital().getHospitalHouseNumber() + "," + bloodRequisition.getHospital().getHospitalStreetName());
                ((TextView) findViewById(R.id.hospitalCity)) .setText(bloodRequisition.getHospital().getHospitalCity());
                ((TextView) findViewById(R.id.hospitalState)).setText(bloodRequisition.getHospital().getHospitalState());
                ((TextView) findViewById(R.id.hospitalCountry)) .setText(bloodRequisition.getHospital().getHospitalCountry());
                ((TextView) findViewById(R.id.hospitalZipCode)).setText(bloodRequisition.getHospital().getHospitalZipcode());

                Glide.with(mContext).load(bloodRequisition.getHospital().getHospialLogo()).placeholder(R.drawable.ic_profile).into((CircleImageView) findViewById(R.id.hospitalImage));
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}

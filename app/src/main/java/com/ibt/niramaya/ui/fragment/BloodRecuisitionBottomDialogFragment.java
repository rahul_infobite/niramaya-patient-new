package com.ibt.niramaya.ui.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Nullable;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.BloodRequisitionAdapter;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisitonMainModal;
import com.ibt.niramaya.retrofit.RetrofitApiClient;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.util.ArrayList;

import retrofit2.Response;

public class BloodRecuisitionBottomDialogFragment extends BottomSheetDialogFragment implements View.OnClickListener {
    private View rootview;
    public RetrofitApiClient retrofitApiClient;
    public ConnectionDetector cd;
    public Context mContext;
    public Activity activity;
    //Blood requisition slip

    private RelativeLayout rlAddBloodRecuisition;
    private BloodRequisitionAdapter bloodRequisitionAdapter;
    private ArrayList<BloodRequisition> bloodRequisitions = new ArrayList<>();
    private TextView txtMessage;
    private int opdId;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.row_bottomsheet_blood, container, false);
        activity = getActivity();
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        Bundle bundle = getArguments();
        if (bundle != null) {
            opdId = bundle.getInt("opdId");
        }
        init();
        return rootview;
    }

    private void init() {
        rlAddBloodRecuisition = rootview.findViewById(R.id.rlAddBloodRecuisition);
        rlAddBloodRecuisition.setVisibility(View.VISIBLE);
        rlAddBloodRecuisition.setOnClickListener(this);
        txtMessage = rootview.findViewById(R.id.txtMessage);
        txtMessage.setVisibility(View.GONE);

        RecyclerView rvBloodRequisition = rootview.findViewById(R.id.rvBloodRequisition);
        rvBloodRequisition.setHasFixedSize(true);
        rvBloodRequisition.setLayoutManager(new LinearLayoutManager(mContext));
        bloodRequisitionAdapter = new BloodRequisitionAdapter(bloodRequisitions, mContext, this);
        rvBloodRequisition.setAdapter(bloodRequisitionAdapter);

        bloodRecuisitionApi();

    }

    private void bloodRecuisitionApi() {
        if (cd.isNetworkAvailable()) {
            String oId = String.valueOf(opdId);
            RetrofitService.selectBloodRequisition(new Dialog(mContext), retrofitApiClient.selectBloodRequisitionData("0", oId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    bloodRequisitions.clear();
                    BloodRequisitonMainModal mainModal = (BloodRequisitonMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getBloodRequisition().size() > 0) {
                            bloodRequisitions.addAll(mainModal.getBloodRequisition());
                            bloodRequisitionAdapter.notifyDataSetChanged();
                        } else {
                            rlAddBloodRecuisition.setVisibility(View.VISIBLE);
                            txtMessage.setVisibility(View.VISIBLE);
                            txtMessage.setText(mainModal.getMessage());
                        }
                    } else {
                        rlAddBloodRecuisition.setVisibility(View.VISIBLE);
                        txtMessage.setVisibility(View.VISIBLE);
                        txtMessage.setText(mainModal.getMessage());
                    }
                    bloodRequisitionAdapter.notifyDataSetChanged();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
   /*txtMessage.setVisibility(View.VISIBLE);
                    txtMessage.setText(error);*/
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {

        }

    }
}
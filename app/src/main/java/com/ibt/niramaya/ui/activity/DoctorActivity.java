package com.ibt.niramaya.ui.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.DoctorReviewRatingAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.doctor_opd_model.DoctorOpdData;
import com.ibt.niramaya.modal.doctor_opd_model.DoctorOpdDataModel;
import com.ibt.niramaya.modal.home.HospitalDatum;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.invoice_data.BookAppointmentActivityKt;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Response;

public class DoctorActivity extends BaseActivity implements View.OnClickListener {
    private RecyclerView rvdoctorAppointment;
    private DoctorReviewRatingAdapter reviewRatingAdapter;
    private List<String> reviewList = new ArrayList<>();
    private String doctorId;
    private String hospitalId;
    private DoctorOpdData doctorDetail;
    private TextView tvDoctorName, tvSpecialization, tvRateCount;
    private CircleImageView ivProfile;
    private HospitalDatum hospitalData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        doctorId = Objects.requireNonNull(getIntent().getExtras()).getString("DoctorId");
        hospitalId = getIntent().getExtras().getString("HospitalId");

        ivProfile = findViewById(R.id.ivProfile);

        fetchDoctorData();

        findViewById(R.id.ivBackImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        findViewById(R.id.btnBookAppointment).setOnClickListener(this);

    }

    private void fetchDoctorData() {
        String userId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
        if (cd.isNetworkAvailable()){
            RetrofitService.doctorDetail(new Dialog(mContext), retrofitApiClient.doctorOpd(
                    hospitalId, userId, "", doctorId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    DoctorOpdDataModel opdModel = (DoctorOpdDataModel) result.body();
                    assert opdModel != null;
                    if (!opdModel.getError()){
                        doctorDetail = opdModel.getDoctorData();
                        initViews();
                    }
                }

                @Override
                public void onResponseFailed(String error) {

                }
            });
        }
    }

    private void initViews() {
        StringBuilder specialization = new StringBuilder();
        int sCount = 0;
        tvDoctorName = findViewById(R.id.tvDoctorName);
        tvSpecialization = findViewById(R.id.tvSpecialization);
        tvRateCount = findViewById(R.id.tvRateCount);
        tvDoctorName.setText(doctorDetail.getName());
        for (int i = 0; i<doctorDetail.getDoctorSpecialization().size(); i++){
            if (sCount == 0){
                specialization = new StringBuilder(doctorDetail.getDoctorSpecialization().get(i).getSpecializationTitle().trim());
                sCount++;
            }else{
                specialization.append(", ").append(doctorDetail.getDoctorSpecialization().get(i).getSpecializationTitle().trim());
            }
        }

        Glide.with(mContext)
                .load(doctorDetail.getProfileImage())
                .placeholder(R.drawable.ic_profile)
                .into(ivProfile);

        tvSpecialization.setText(specialization);
        tvRateCount.setText(doctorDetail.getRating());
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBookAppointment :
                startActivity(new Intent(mContext, BookAppointmentActivityKt.class)
                        .putExtra("DoctorOpdData", doctorDetail));
            break;
        }
    }
}

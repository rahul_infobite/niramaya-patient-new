package com.ibt.niramaya.ui.fragment;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.BloodRequisitionAdapter;
import com.ibt.niramaya.adapter.PrescriptionListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisitonMainModal;
import com.ibt.niramaya.modal.doctor_refer_slip_modal.DoctorReeferSlipMainModal;
import com.ibt.niramaya.modal.prescription.OpdList;
import com.ibt.niramaya.modal.prescription.PrescritionListModel;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.activity.ViewInvoiceDetailActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import retrofit2.Response;

import static com.ibt.niramaya.ui.HomeActivity.imgSearch;
import static com.ibt.niramaya.ui.HomeActivity.imgSort;

public class PrescriptionsFragment extends BaseFragment implements View.OnClickListener {

    private List<OpdList> prescriptionList = new ArrayList<>();
    private ArrayList<BloodRequisition> bloodRequisitions = new ArrayList<>();
    private View rootView;
    private PrescriptionListAdapter prescriptionAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_prescription, container, false);
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        init();
        return rootView;
    }

    private void init() {
        imgSearch.setVisibility(View.GONE);
        imgSort.setVisibility(View.GONE);

        ImageView ivFilter = Objects.requireNonNull(getActivity()).findViewById(R.id.ivFilter);
        ivFilter.setVisibility(View.GONE);

        prescriptionListApi();
    }

    private void prescriptionListApi() {
        RecyclerView recyclerViewPrescription = rootView.findViewById(R.id.recyclerViewPrescription);

        fetchPrescriptionList(recyclerViewPrescription);
    }

    private void fetchPrescriptionList(final RecyclerView recyclerViewPrescription) {
        if (cd.isNetworkAvailable()) {
            String strUserId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
            String patientId = AppPreference.getStringPreference(mContext, Constant.CURRENT_PATENT_ID);
            RetrofitService.patientPrescriptionList(new Dialog(mContext), retrofitApiClient.patientPrescriptionList(strUserId, patientId
            ), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    PrescritionListModel mainModal = (PrescritionListModel) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        Alerts.show(mContext, mainModal.getMessage());
                        if (mainModal.getOpdList().size() > 0) {
                            prescriptionList = mainModal.getOpdList();
                            recyclerViewPrescription.setHasFixedSize(true);
                            recyclerViewPrescription.setLayoutManager(new LinearLayoutManager(mContext));
                            prescriptionAdapter = new PrescriptionListAdapter(prescriptionList, mContext, PrescriptionsFragment.this);
                            recyclerViewPrescription.setAdapter(prescriptionAdapter);
                            prescriptionAdapter.notifyDataSetChanged();
                        }
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
// Alerts.show(mContext, error);
                }
            });
        }
    }


    @Override
    public void onClick(View v) {
        int tag = 0;
        switch (v.getId()) {

            case R.id.txtOpen:
            case R.id.cardViewItem:
                tag = (int) v.getTag();
                Intent intent = new Intent(mContext, ViewInvoiceDetailActivity.class);
                intent.putExtra("from", "prescription");
                intent.putExtra("invoiceNumber", prescriptionList.get(tag).getOpdTitle());
                intent.putExtra("billLInk", prescriptionList.get(tag).getOpdSlip());
                startActivity(intent);
                break;
            case R.id.btnBloodSlip:
                tag = (int) v.getTag();
                bloodRecuisitionApi(prescriptionList.get(tag).getOpdId());
                break;
            case R.id.btnReferSlip:
                tag = (int) v.getTag();
                selectReferSleep(prescriptionList.get(tag).getOpdId());
                break;
        }
    }

    private void bloodRecuisitionApi(String opdId) {
        if (cd.isNetworkAvailable()) {
            RetrofitService.selectBloodRequisition(new Dialog(mContext), retrofitApiClient.selectBloodRequisitionData("0", opdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    bloodRequisitions.clear();
                    BloodRequisitonMainModal mainModal = (BloodRequisitonMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getBloodRequisition().size() > 0) {
                            bloodRequisitions = (ArrayList<BloodRequisition>) mainModal.getBloodRequisition();
                        }
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                    selectBloodRequisitionDialog(bloodRequisitions);
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }

    @SuppressLint("SetTextI18n")
    private void selectBloodRequisitionDialog(ArrayList<BloodRequisition> bloodRequisitions) {
        android.app.AlertDialog.Builder dialogBox = new android.app.AlertDialog.Builder(mContext);
        dialogBox.setCancelable(true);

        LayoutInflater li = LayoutInflater.from(mContext);
        @SuppressLint("InflateParams") final View dialogBoxView = li.inflate(R.layout.dialog_blood_slip_list, null);
        dialogBox.setView(dialogBoxView);

        AlertDialog bloodRequisitionDailog = dialogBox.create();
        Objects.requireNonNull(bloodRequisitionDailog.getWindow()).setBackgroundDrawableResource(android.R.color.transparent);


        if (bloodRequisitions != null || bloodRequisitions.size() == 0) {
            TextView tvMessage = dialogBoxView.findViewById(R.id.tvMessage);
            tvMessage.setVisibility(View.VISIBLE);
            tvMessage.setText("No Blood Requisition Slip");

        }

        RecyclerView rvBloodRequisition = dialogBoxView.findViewById(R.id.rvAmbulance);
        ((TextView) dialogBoxView.findViewById(R.id.tvTitle)).setText("Blood Requisition");
        rvBloodRequisition.setHasFixedSize(true);
        rvBloodRequisition.setLayoutManager(new LinearLayoutManager(mContext));
        BloodRequisitionAdapter bloodRequisitionAdapter = new BloodRequisitionAdapter(bloodRequisitions, mContext, this);
        rvBloodRequisition.setAdapter(bloodRequisitionAdapter);
        bloodRequisitionAdapter.notifyDataSetChanged();

        bloodRequisitionDailog.show();
    }

    private void selectReferSleep(String opdId) {
        if (cd.isNetworkAvailable()) {
            RetrofitService.selectPatientReffered(new Dialog(mContext), retrofitApiClient.refferedData("0", opdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    DoctorReeferSlipMainModal mainModal = (DoctorReeferSlipMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getReferSlip() != null) {
                            Intent intent = new Intent(mContext, ViewInvoiceDetailActivity.class);
                            intent.putExtra("billLInk", mainModal.getReferSlip().getReferSlip());
                            intent.putExtra("invoiceNumber", mainModal.getReferSlip().getPatientName());
                            //  selectReferDialog(mainModal);
                        } else {
                            Alerts.show(mContext, mainModal.getMessage());
                        }
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, error);
                }
            });
        }
    }

}

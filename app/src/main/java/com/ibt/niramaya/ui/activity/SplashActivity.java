package com.ibt.niramaya.ui.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.HomeActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class SplashActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(this, instanceIdResult -> {
            String newToken = instanceIdResult.getToken();
            Log.e("newToken", newToken);
            AppPreference.setStringPreference(mContext, Constant.FIREBASE_TOKEN, newToken);
            tokenApi(newToken);
        });
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.e("Firebase ", "Refreshed token: " + refreshedToken);

        Log.e("Login", String.valueOf(AppPreference.getBooleanPreference(mContext, Constant.Is_Login)));
    }

    private void tokenApi(String strToken) {
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String strId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
        if (cd.isNetworkAvailable()) {
            RetrofitService.getServerResponse(new Dialog(mContext), retrofitApiClient.userToken(android_id, strToken, strId, "1"), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ResponseBody mainModal = (ResponseBody) result.body();
                    try {
                        assert mainModal != null;
                        JSONObject jsonObject = new JSONObject(mainModal.string());
                        if (!jsonObject.getBoolean("error")) {
                            init();
                         //   Alerts.show(mContext, jsonObject.getString("message"));
                        } else {
                            Alerts.show(mContext, jsonObject.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }
    private void init() {
        final boolean isLogin = AppPreference.getBooleanPreference(mContext, Constant.Is_Login);

        new Handler().postDelayed(() -> {
            if (isLogin) {
                startActivity(new Intent(mContext, HomeActivity.class)
                .putExtra("from","otp"));
                finish();
            } else {
                Intent intent = new Intent(mContext, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                finish();
            }
        }, 3000);
    }

}

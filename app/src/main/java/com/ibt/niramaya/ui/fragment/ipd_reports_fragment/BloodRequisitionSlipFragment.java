package com.ibt.niramaya.ui.fragment.ipd_reports_fragment;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.BloodRequisitionAdapter;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisition;
import com.ibt.niramaya.modal.blood_requisition_slip_modal.BloodRequisitonMainModal;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Response;

public class BloodRequisitionSlipFragment extends BaseFragment implements View.OnClickListener {
    private View rootView;
    private BloodRequisitionAdapter bloodRequisitionAdapter;
    private ArrayList<BloodRequisition> bloodRequisitions = new ArrayList<>();
    private String ipdId;
    private TextView txtMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragement_blood_requisition, container, false);
        mContext = getContext();
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        Bundle bundle = getArguments();
        if (bundle != null) {
            ipdId = bundle.getString("ipdId");
        }

        txtMessage = rootView.findViewById(R.id.txtMessage);
        txtMessage.setVisibility(View.GONE);

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        txtMessage.setVisibility(View.GONE);
        bloodRecuisitionApi();
    }

    private void bloodRecuisitionApi() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.selectBloodRequisition(new Dialog(mContext), retrofitApiClient.selectBloodRequisitionData("1", ipdId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    bloodRequisitions.clear();
                    BloodRequisitonMainModal mainModal = (BloodRequisitonMainModal) result.body();
                    assert mainModal != null;
                    if (!mainModal.getError()) {
                        if (mainModal.getBloodRequisition().size() > 0) {
                            bloodRequisitions = (ArrayList<BloodRequisition>) mainModal.getBloodRequisition();
                            RecyclerView rvBloodRequisition = rootView.findViewById(R.id.rvBloodRequisition);
                            rvBloodRequisition.setHasFixedSize(true);
                            rvBloodRequisition.setLayoutManager(new LinearLayoutManager(mContext));
                            bloodRequisitionAdapter = new BloodRequisitionAdapter(bloodRequisitions, mContext, BloodRequisitionSlipFragment.this);
                            rvBloodRequisition.setAdapter(bloodRequisitionAdapter);
                            bloodRequisitionAdapter.notifyDataSetChanged();
                        } else {
                            txtMessage.setVisibility(View.VISIBLE);
                            txtMessage.setText(mainModal.getMessage());
                        }
                    } else {
                        txtMessage.setVisibility(View.VISIBLE);
                        txtMessage.setText(mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    txtMessage.setVisibility(View.VISIBLE);
                    txtMessage.setText(error);
                }
            });
        }
    }

    @Override
    public void onClick(View view) {

    }
}

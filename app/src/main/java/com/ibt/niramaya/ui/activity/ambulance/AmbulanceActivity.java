package com.ibt.niramaya.ui.activity.ambulance;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.ambulance.ChargeListAdapter;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.modal.ambulance.DriverLocation;
import com.ibt.niramaya.modal.ambulance.book_ambulance.ActiveAmbulanceModal;
import com.ibt.niramaya.modal.ambulance.driver_detail.Ambulance;
import com.ibt.niramaya.modal.ambulance.driver_detail.AmbulanceCharge;
import com.ibt.niramaya.modal.ambulance.driver_detail.AmbulanceDetailModel;
import com.ibt.niramaya.modal.driver.driver_list_modal.DriverList;
import com.ibt.niramaya.modal.driver.driver_list_modal.DriverMainModal;
import com.ibt.niramaya.modal.patient_modal.PaitentProfile;
import com.ibt.niramaya.modal.patient_modal.PatientMainModal;
import com.ibt.niramaya.retrofit.RetrofitApiClient;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.BaseActivity;
import com.ibt.niramaya.utils.GpsTracker;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Response;

public class AmbulanceActivity extends BaseActivity implements LocationListener,
        OnMapReadyCallback, RoutingListener, GoogleMap.OnMarkerClickListener {

    private RetrofitApiClient client;
    private DriverMainModal mainModal;
    private List<DriverList> driverListsUpdate = new ArrayList<>();
    private GoogleMap mMap;
    private static final String TAG = "TAG";
    private boolean isUp = false;
    private String bookingType = "1";

    private DatabaseReference mFirebaseDatabase;
    private FirebaseDatabase mFirebaseInstance;

    private String selectedDriverName = "";

    private double sLatitude = 0.0;
    private double sLongitude = 0.0;
    private double dLatitude = 0.0;
    private double dLongitude = 0.0;
    private double eLatitude = 0.0;
    private double eLongitude = 0.0;
    private long UPDATE_INTERVAL = 2000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private LocationRequest mLocationRequest;
    private static final int DEFAULT_ZOOM_LEVEL = 14;
    private LatLng latLng;
    private Marker mMarker;
    private ArrayList<DriverLocation> driverList;
    private ArrayList<DriverLocation> activeDriverList;
    private List<Polyline> polylines = new ArrayList<>();
    private static final int[] COLORS = new int[]{R.color.colorAccent};
    private Marker driverMarker;
    private boolean isMarkerRotating = false;
    private RelativeLayout cvDriverDetail;
    private String clickedDriverId;
    private TextView tvDriverName, tvDriverDistance, txtTitle;
    private ImageView ivCurrentLocation;
    private Button btnMore;
    private CoordinatorLayout clRoot;
    private BottomSheetBehavior sheetBehavior;
    private LinearLayout layoutBottomSheet;
    private double distance;
    private TextView tvDrop, tvPickUp;
    private ImageView ivCancel;
    private List<PaitentProfile> patientList = new ArrayList<>();
    private PaitentProfile selectedPatient;
    private Spinner spnPatient;

    private ChargeListAdapter chargeListAdapter;
    private ArrayList<com.ibt.niramaya.modal.ambulance.book_ambulance.Ambulance> ambulances = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ambulance);

        Toolbar toolbar = findViewById(R.id.toolbar);
        showHomeBackOnToolbar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        assert mapFragment != null;
        mapFragment.getMapAsync(this);

        //initRetrofit();

        init();
     //   initFirebaseDatabase();
     fetchActiveAmbulanceList();
    }

    @SuppressLint("SetTextI18n")
    private void init() {
        clRoot = findViewById(R.id.rlRoot);
        tvDrop = findViewById(R.id.tvDrop);
        tvPickUp = findViewById(R.id.tvPickUp);
        ivCancel = findViewById(R.id.ivCancel);
        ivCurrentLocation = findViewById(R.id.ivCurrentLocation);
        spnPatient = findViewById(R.id.spnPatient);

        getLatLong();
        tvPickUp.setText(myCurrentAddress(sLatitude, sLongitude));

        cvDriverDetail = findViewById(R.id.cvDriverDetail);
        tvDriverName = findViewById(R.id.tvDriverName);
        tvDriverDistance = findViewById(R.id.tvDriverDistance);
        txtTitle = findViewById(R.id.txtTitle);
        btnMore = findViewById(R.id.btnClose);

        ivCurrentLocation.setOnClickListener(v -> {
            getLatLong();
            ivCurrentLocation.setImageResource(R.drawable.ic_my_location_selected);
            tvPickUp.setText(myCurrentAddress(sLatitude, sLongitude));
            if (dLatitude > 0) {
                distance = calculateDistance(sLatitude, sLongitude, dLatitude, dLongitude);
                @SuppressLint("DefaultLocale") String strDistance = String.format("%.2f", distance);
                tvDriverName.setText("Distance : " + strDistance + "km");
            }
        });
        tvPickUp.setOnClickListener(v -> startActivityForResult(new Intent(mContext, SearchLocationActivity.class)
                .putExtra("FROM", "pickUp"), 117));
        tvDrop.setOnClickListener(v -> startActivityForResult(new Intent(mContext, SearchLocationActivity.class)
                .putExtra("FROM", "drop"), 119));
        ivCancel.setOnClickListener(v -> {
            tvDrop.setText("");
            eLatitude = 0.0;
            eLongitude = 0.0;
        });

        btnMore.setOnClickListener(v -> {
            /*startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                    .putExtra("DRIVER_ID", clickedDriverId));*/
            fetchAmbulanceDetail();
        });

        Places.initialize(getApplicationContext(), getResources().getString(R.string.google_key));
        AutocompleteSupportFragment autocompleteFragment = (AutocompleteSupportFragment)
                getSupportFragmentManager().findFragmentById(R.id.autocomplete_fragment);
        assert autocompleteFragment != null;
        autocompleteFragment.setPlaceFields(Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.LAT_LNG));

        autocompleteFragment.setOnPlaceSelectedListener(new PlaceSelectionListener() {
            @Override
            public void onPlaceSelected(Place place) {
                latLng = place.getLatLng();
                sLatitude = latLng.latitude;
                sLongitude = latLng.longitude;
                Log.e("lat : ", String.valueOf(sLatitude));
                Log.e("longi : ", String.valueOf(sLongitude));
                initFirebaseDatabase();
            }

            @Override
            public void onError(Status status) {
                Alerts.show(mContext, status.getStatusMessage());
            }
        });

        initPatientSpinner();
    }

    private void initFirebaseDatabase() {
        mFirebaseInstance = FirebaseDatabase.getInstance();
        mFirebaseDatabase = mFirebaseInstance.getReference("driver");
        mFirebaseDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                driverList = new ArrayList<>();
                activeDriverList = new ArrayList<>();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    DriverLocation driverLocation = postSnapshot.getValue(DriverLocation.class);
                    driverList.add(driverLocation);
                }
                if (mMap != null) {
                    mMap.clear();
                }

                for (int dl = 0; dl < ambulances.size(); dl++) {
                    for (int i = 0; i < driverList.size(); i++) {
                        if (ambulances.get(dl).getDriverId().equals(driverList.get(i).getDriverId())) {
                            if (driverList.get(i).isDriverStatus() &&
                                    calculateDistance(sLatitude, sLongitude, driverList.get(i).getDriverLat(),
                                            driverList.get(i).getDriverLong()) < 100.00) {
                                createMarker(driverList.get(i).getDriverLat(), driverList.get(i).getDriverLong(),
                                        driverList.get(i).getDriverName(), driverList.get(i).getDriverId());
                                activeDriverList.add(driverList.get(i));
                            }
                        }
                    }

                }

                addMarker(latLng);

                /*bound camera between these lat lng*/
                LatLngBounds.Builder builder = new LatLngBounds.Builder();
                for (DriverLocation dLoc : activeDriverList) {
                    builder.include(new LatLng(dLoc.getDriverLat(), dLoc.getDriverLong()));
                }
                builder.include(new LatLng(sLatitude, sLongitude));
                LatLngBounds bounds = builder.build();
                int padding = 100; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mMap.animateCamera(cu);
                mMap.moveCamera(cu);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }



    private void fetchActiveAmbulanceList() {
        if (cd.isNetworkAvailable()) {
            RetrofitService.activeAmbulanceList(new Dialog(mContext), retrofitApiClient.activeDriverAmbulance("0", "0"), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ActiveAmbulanceModal ambulanceModel = (ActiveAmbulanceModal) result.body();
                    assert ambulanceModel != null;
                    if (!ambulanceModel.getError()) {
                        if (ambulanceModel.getAmbulance().size() > 0) {
                            ambulances = (ArrayList<com.ibt.niramaya.modal.ambulance.book_ambulance.Ambulance>) ambulanceModel.getAmbulance();
                        }
                    } else {
                        Alerts.show(mContext, ambulanceModel.getMessage());
                    }
                    initFirebaseDatabase();
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Sever Error!!!");
                }
            });
        }

    }


    private void getLatLong() {
        GpsTracker gpsTracker = new GpsTracker(this);
        sLatitude = gpsTracker.getLatitude();
        sLongitude = gpsTracker.getLongitude();
        latLng = new LatLng(sLatitude, sLongitude);
        initFirebaseDatabase();
    }

    private void getAddressList(double aLat, double aLong) {
        // AppProgressDialog.show(dialog);
        Geocoder geocoder = new Geocoder(this, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(aLat, aLong, 1);
            if (addresses.size() > 0) {
                //  AppProgressDialog.hide(dialog);
            } else {
                // AppProgressDialog.show(dialog);
                new Handler().postDelayed(() -> getLatLong(), 3000);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        latLng = new LatLng(location.getLatitude(), location.getLongitude());

        // getDriverListDataApi();

        if (sLatitude > 0) {
            addMarker(latLng);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Alerts.show(this, "Please enable location permission...!!!");
            return;
        }

        getLatLong();
        if (latLng != null) {
            sLatitude = latLng.latitude;
            sLongitude = latLng.longitude;
        }
        if (sLatitude > 0) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
            mMap.animateCamera(cameraUpdate);
            addMarker(latLng);
        }
/*
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(true);*/

        LatLng indore = new LatLng(22.719568, 75.857727);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 15);
        mMap.animateCamera(cameraUpdate);
        addMarker(indore);

        mMap.setOnInfoWindowClickListener(arg0 -> {
            //DriverLocation dl = driverList.get(Integer.parseInt(arg0.getSnippet()));
            Alerts.show(mContext, arg0.getTitle() + " :=: " + arg0.getId() + " :=: ");
        });
        mMap.setOnMarkerClickListener(this);

    }

    private void addMarker(LatLng location) {
        if (mMap != null) {
            MarkerOptions mMarkerOption = new MarkerOptions();
            mMarkerOption.position(location);
            //mMarkerOption.title("My location");
            mMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_placeholder));
            removeMarker();
            mMarker = mMap.addMarker(mMarkerOption);

            /*driverList.add(new DriverLocation("1", "Driver 1", 22.72591965, 75.88166714));
            driverList.add(new DriverLocation("1", "Driver 2", 22.72370295, 75.88887691));
            driverList.add(new DriverLocation("1", "Driver 3", 22.7228321, 75.87831974));
            driverList.add(new DriverLocation("1", "Driver 4", 22.7301946, 75.88527203));
            driverList.add(new DriverLocation("2", "Driver 5", 22.71507333, 75.88278294));*/

            /*for(int i = 0 ; i < driverList.size() ; i++) {
                createMarker(driverList.get(i).getDriverLat(), driverList.get(i).getDriverLong(),
                        driverList.get(i).getDriverName(), driverList.get(i).getDriverName());
            }

            createRoute();*/

        }
    }

    private void removeMarker() {
        if (mMap != null && mMarker != null) {
            mMarker.remove();
        }
    }

    /*************************************************************
     *
     ************************************************************/
    protected Marker createMarker(double latitude, double longitude, String title, String snippet) {
        MarkerOptions dMarkerOption = new MarkerOptions();
        dMarkerOption.position(new LatLng(latitude, longitude));
        dMarkerOption.anchor(0.5f, 0.5f);
        dMarkerOption.title(title);
        dMarkerOption.snippet(snippet);
        dMarkerOption.icon(BitmapDescriptorFactory.fromResource(R.drawable.ambulance));

        return mMap.addMarker(dMarkerOption);
    }

    private void createRoute() {
        Routing routing = new Routing.Builder()
                .key("AIzaSyBvaYGedz5oMgLpYMF42wtJE8VIT28juM8")//NonRestrictedKey
                .travelMode(AbstractRouting.TravelMode.DRIVING)
                .withListener(this)
                .alternativeRoutes(false)
                .waypoints(new LatLng(driverList.get(0).getDriverLat(), driverList.get(0).getDriverLong()),
                        new LatLng(driverList.get(1).getDriverLat(), driverList.get(0).getDriverLong()))
                .build();
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {

    }

    @Override
    public void onRoutingStart() {

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int p1) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(driverList.get(0).getDriverLat(), driverList.get(0).getDriverLong()));
        CameraUpdate zoom = CameraUpdateFactory.zoomTo(16);

        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(driverList.get(0).getDriverLat(), driverList.get(0).getDriverLong()), 15);
        //mMap.animateCamera(cameraUpdate);
        mMap.moveCamera(cameraUpdate);


        if (polylines.size() > 0) {
            for (Polyline poly : polylines) {
                poly.remove();
            }
        }

        polylines = new ArrayList<>();
        //add route(s) to the map.
        for (int i = 0; i < route.size(); i++) {

            //In case of more than 5 alternative routes
            int colorIndex = i % COLORS.length;

            PolylineOptions polyOptions = new PolylineOptions();
            polyOptions.color(getResources().getColor(COLORS[colorIndex]));
            polyOptions.width(10 + i * 3);
            polyOptions.addAll(route.get(i).getPoints());
            Polyline polyline = mMap.addPolyline(polyOptions);
            polylines.add(polyline);

            Toast.makeText(getApplicationContext(), "Route " + (i + 1) + ": distance - " + route.get(i).getDistanceValue() + ": duration - " + route.get(i).getDurationValue(), Toast.LENGTH_SHORT).show();
        }

        // Start marker
        /*MarkerOptions options = new MarkerOptions();
        options.position(new LatLng(22.7301946, 75.88527203));
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin_placeholder));
        mMap.addMarker(options);*/

        // End marker
        /*options = new MarkerOptions();
        options.position(new LatLng(22.71507333, 75.88278294));
        options.icon(BitmapDescriptorFactory.fromResource(R.drawable.icf_hospital_location_pin));
        mMap.addMarker(options);*/
    }

    @Override
    public void onRoutingCancelled() {

    }

    /****************************************************************
     * Marker Clicked Event
     *****************************************************************/
    @SuppressLint("SetTextI18n")
    @Override
    public boolean onMarkerClick(Marker marker) {
        if (selectedPatient != null) {
            clickedDriverId = marker.getSnippet();
            for (DriverLocation clickedDriver : activeDriverList) {
                if (clickedDriver.getDriverId().equals(clickedDriverId)) {
                    cvDriverDetail.setVisibility(View.VISIBLE);
                    dLatitude = clickedDriver.getDriverLat();
                    dLongitude = clickedDriver.getDriverLong();
                    distance = calculateDistance(sLatitude, sLongitude, clickedDriver.getDriverLat(), clickedDriver.getDriverLong());
                    @SuppressLint("DefaultLocale") String strDistance = String.format("%.2f", distance);
                    /*Alerts.show(mContext, "Marker Clicked : "+marker.getTitle()
                            +"\nDriver Id : "+clickedDriverId
                            +"\nDriver Distance : "+strDistance+"KM");*/
                    selectedDriverName = marker.getTitle();
                    tvDriverName.setText("Distance : " + strDistance + "km");
                    tvDriverDistance.setText("Driver Name : " + marker.getTitle() + "(" + clickedDriverId + ")");
                }
            }
        } else {
            Alerts.showSnack(findViewById(R.id.rlRoot), mContext, "No patient selected!");
        }
        return true;
    }

    /********************************************************************
     *
     **********************************************************************/
    private double calculateDistance(double uLat, double uLong, double dLat, double dLong) {
        Location locationA = new Location("point A");
        locationA.setLatitude(uLat);
        locationA.setLongitude(uLong);
        Location locationB = new Location("point B");
        locationB.setLatitude(dLat);
        locationB.setLongitude(dLong);

        double distance = (locationA.distanceTo(locationB)) / 1000;
        return distance;
    }

    /**/
    private void fetchAmbulanceDetail() {
        if (cd.isNetworkAvailable()) {
            //Alerts.show(mContext, clickedDriverId);
            RetrofitService.ambulanceDetail(new Dialog(mContext), retrofitApiClient.ambulanceDetail(clickedDriverId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    AmbulanceDetailModel ambulanceModel = (AmbulanceDetailModel) result.body();
                    assert ambulanceModel != null;
                    Alerts.show(mContext, ambulanceModel.getMessage());
                    if (!ambulanceModel.getError()) {
                        openBottomSheet(ambulanceModel.getAmbulance());
                    }
                }

                @Override
                public void onResponseFailed(String error) {

                }
            });
        }
    }

    @SuppressLint({"SetTextI18n", "DefaultLocale"})
    private void openBottomSheet(Ambulance ambulance) {

        AlertDialog.Builder dialogBox = new AlertDialog.Builder(mContext);
        dialogBox.setCancelable(false);

        LayoutInflater li = LayoutInflater.from(mContext);
        final View dialogView = li.inflate(R.layout.dialog_ambulance_detail, null);
        dialogBox.setView(dialogView);
        final AlertDialog alertDialog = dialogBox.create();
        alertDialog.show();

        RadioGroup rgBookingType = dialogView.findViewById(R.id.rgBookingType);
        rgBookingType.setOnCheckedChangeListener((radioGroup, i) -> {
            RadioButton checkedRadioButton = radioGroup.findViewById(i);
            bookingType = checkedRadioButton.getText().toString();
            if (bookingType.equals("One Way Service")) {
                bookingType = "1";
            } else if (bookingType.equals("Two Way Service")) {
                bookingType = "0";
            }
        });


        RecyclerView rvAmbulance = dialogView.findViewById(R.id.rvAmbulance);
        Button btnCancel = dialogView.findViewById(R.id.btnCancel);
        Button btnBook = dialogView.findViewById(R.id.btnBook);
        TextView tvDriverName = dialogView.findViewById(R.id.tvDriverName);
        TextView tvDriverDistance = dialogView.findViewById(R.id.tvDriverDistance);
        TextView tvAmbulanceRegistrationNumber = dialogView.findViewById(R.id.tvAmbulanceRegistrationNumber);
        TextView tvHospitalName = dialogView.findViewById(R.id.tvHospitalName);

        //   distance = calculateDistance(sLatitude, sLongitude, dLatitude, dLongitude);

        btnCancel.setOnClickListener(v -> alertDialog.dismiss());
        btnBook.setOnClickListener(v -> {
            openAmbulanceDetail(alertDialog, ambulance);
            // Alerts.show(mContext, "In Process...");
            /*startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                    .putExtra("DRIVER_ID", clickedDriverId));*/
        });

        tvDriverName.setText("Driver Name : " + selectedDriverName);
        tvDriverDistance.setText("Distance : " + String.format("%.2f", distance) + "km");
        tvAmbulanceRegistrationNumber.setText("Ambulance No. : " + ambulance.getRtoNumber());
        tvHospitalName.setText("Hospital Name" + " : " + ambulance.getHospitalName());

        chargeListAdapter = new ChargeListAdapter(mContext, (ArrayList<AmbulanceCharge>) ambulance.getAmbulanceCharges());
        rvAmbulance.setLayoutManager(new LinearLayoutManager(mContext));
        rvAmbulance.setAdapter(chargeListAdapter);
        chargeListAdapter.notifyDataSetChanged();
    }

    private void openAmbulanceDetail(AlertDialog alertDialog, Ambulance ambulance) {
        if (selectedPatient != null) {
            String pId = selectedPatient.getPatientId();
            String pName = selectedPatient.getPatientName();
            String pAge = String.valueOf(getAge(selectedPatient.getPatientDateOfBirth()));
            String pNum = selectedPatient.getPatientContact();
            String pAddress = selectedPatient.getPatientHouseNumber() + ", " + selectedPatient.getPatientStreetName() + ", "
                    + selectedPatient.getPatientCity() + ", " + selectedPatient.getPatientState() + ", " + selectedPatient.getPatientCountry() + ", "
                    + selectedPatient.getPatientZipcode();
            if (cd.isNetworkAvailable()) {
                RetrofitService.getServerResponse(new Dialog(mContext), retrofitApiClient.bookAmbulance(ambulance.getAmbulanceId(),
                        ambulance.getHospitalId(), "", clickedDriverId, pId, pName, pAge, pNum, pAddress,
                        String.valueOf(sLatitude), String.valueOf(sLongitude), String.valueOf(eLatitude), String.valueOf(eLongitude), bookingType), new WebResponse() {
                    @Override
                    public void onResponseSuccess(Response<?> result) {
                        alertDialog.dismiss();
                        AppPreference.setBooleanPreference(mContext, Constant.PATIENT_AMBULANCE_BOOKING_STATUS, true);
                        AppPreference.setStringPreference(mContext, Constant.PATIENT_NAME_AMBULANCE_BOOKED, selectedPatient.getPatientId());
                        AppPreference.setStringPreference(mContext, Constant.PATIENT_BOOKING_LATITUDE, String.valueOf(sLatitude));
                        AppPreference.setStringPreference(mContext, Constant.PATIENT_BOOKING_LONGITUDE, String.valueOf(sLongitude));
                        startActivity(new Intent(mContext, AmbulanceDetailActivity.class)
                                .putExtra("From", "listing")
                                .putExtra("PatientId", selectedPatient.getPatientId())
                                .putExtra("Latitude", sLatitude)
                                .putExtra("Longitude", sLongitude));
                        finish();
                    }

                    @Override
                    public void onResponseFailed(String error) {
                        Alerts.show(mContext, "Server Error!!!");
                    }
                });
            }
        }
    }

    private String myCurrentAddress(Double latitude, Double longitude) {


       /* GpsTracker gpsTracker = new GpsTracker(mContext);
        double currentLat = gpsTracker.getLatitude();
        double currentLong = gpsTracker.getLongitude();*/

        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(mContext, Locale.getDefault());

        String address = "";

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL
            }

            /*Alerts.show(mContext, "Address : "+address+"\nCity : "+city+"\nState : "+state+"\nCountry : "+country+
                    "\nPostal Code : "+postalCode+"\nknown Name : "+knownName);*/

        } catch (IOException e) {
            e.printStackTrace();
        }

        return address;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != Activity.RESULT_CANCELED) {
            switch (requestCode) {
                case 117:
                    tvPickUp.setText(data.getStringExtra("ADDRESS_NAME"));
                    ivCurrentLocation.setImageResource(R.drawable.ic_my_location);
                    sLatitude = data.getDoubleExtra("LATITUDE", 0.0);
                    sLongitude = data.getDoubleExtra("LONGITUDE", 0.0);
                    distance = calculateDistance(sLatitude, sLongitude, dLatitude, dLongitude);
                    tvDriverName.setText("Distance : " + distance + "km");
                    String strDistance = String.format("%.2f", distance);
                    latLng = new LatLng(sLatitude, sLongitude);
                    initFirebaseDatabase();
                    //Alerts.show(mContext, "Latitude : "+sLatitude+"\nLongitude : "+sLongitude);
                    break;
                case 119:
                    tvDrop.setText(data.getStringExtra("ADDRESS_NAME"));
                    eLatitude = data.getDoubleExtra("LATITUDE", 0.0);
                    eLongitude = data.getDoubleExtra("LONGITUDE", 0.0);

                    distance = calculateDistance(sLatitude, sLongitude, eLatitude, eLongitude);

                    //     Alerts.show(mContext, "Latitude : "+eLatitude+"\nLongitude : "+eLongitude);
                    break;
            }
        }
    }

    private void initPatientSpinner() {
        if (cd.isNetworkAvailable()) {
            String strUserId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
            RetrofitService.getPatientList(new Dialog(mContext), retrofitApiClient.patientList(strUserId), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    PatientMainModal mainModal = (PatientMainModal) result.body();
                    if (mainModal != null) {
                        patientList = mainModal.getUser().getPaitentProfile();

                        PaitentProfile paitentProfile1 = new PaitentProfile();
                        paitentProfile1.setPatientId("0");
                        paitentProfile1.setPatientName("Select Patient");
                        patientList.add(0, paitentProfile1);

                        ArrayAdapter aa = new ArrayAdapter<>(mContext, R.layout.row_spinner_item_white, patientList);
                        //aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        //Setting the ArrayAdapter data on the Spinner
                        spnPatient.setAdapter(aa);
                        spnPatient.setOnItemSelectedListener(spinnerListener);
                    } else {
                        Alerts.show(mContext, mainModal.getMessage());
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }

    AdapterView.OnItemSelectedListener spinnerListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            if (position > 0) {
                selectedPatient = patientList.get(position);
            } else {
                selectedPatient = null;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }


    /***********************************************
     * Start Method block for calculating age
     **************************************************/

    private int getAge(String dobString) {

        Date date = null;
        //SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (date == null) return 0;

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month + 1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)) {
            age--;
        }
        return age;
    }
}

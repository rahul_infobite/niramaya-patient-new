package com.ibt.niramaya.ui.fragment.home_detail_fragment;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ibt.niramaya.R;
import com.ibt.niramaya.adapter.doctor_opd.DoctorOpdListAdapter;
import com.ibt.niramaya.modal.home.HospitalDatum;
import com.ibt.niramaya.modal.hospital_detail.DoctorDatum;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.ui.activity.invoice_data.BookAppointmentActivityKt;
import com.ibt.niramaya.utils.BaseFragment;
import com.ibt.niramaya.utils.ConnectionDetector;

import java.util.ArrayList;
import java.util.List;

public class DoctorFragment extends BaseFragment implements View.OnClickListener {

    private View rootView;
    private DoctorOpdListAdapter doctorAdapter;
    private RecyclerView rvDoctorOpdList;
    private List<DoctorDatum> doctorList = new ArrayList<>();
    private HospitalDatum hospitalData;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_doctor, container, false);
        mContext = getActivity();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();

        fetchDoctorList();
        return rootView;
    }

    private void fetchDoctorList() {

        assert getArguments() != null;
        doctorList = getArguments().getParcelableArrayList("DOCTORS");
        hospitalData = getArguments().getParcelable("Hospital");
        rvDoctorOpdList = rootView.findViewById(R.id.rvDoctorOpdList);
        if (doctorList.size()>0){
            doctorAdapter = new DoctorOpdListAdapter(doctorList, mContext, DoctorFragment.this, hospitalData);
            rvDoctorOpdList.setLayoutManager(new LinearLayoutManager(mContext));
            rvDoctorOpdList.setAdapter(doctorAdapter);
            doctorAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onClick(View v) {
        if (v.getId()== R.id.btnBookAppointment){
            int tag = (int) v.getTag();
            startActivity(new Intent(mContext, BookAppointmentActivityKt.class)
                    .putExtra("DoctorId", doctorList.get(tag).getDoctorId())
            .putExtra("HospitalId", hospitalData.getHospitalId()));
        }
    }
}

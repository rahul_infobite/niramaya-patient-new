package com.ibt.niramaya.firebase_service;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;

import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.ibt.niramaya.R;
import com.ibt.niramaya.constant.Constant;
import com.ibt.niramaya.retrofit.RetrofitApiClient;
import com.ibt.niramaya.retrofit.RetrofitService;
import com.ibt.niramaya.retrofit.WebResponse;
import com.ibt.niramaya.ui.HomeActivity;
import com.ibt.niramaya.ui.activity.SplashActivity;
import com.ibt.niramaya.ui.activity.ambulance.AmbulanceDetailActivity;
import com.ibt.niramaya.utils.Alerts;
import com.ibt.niramaya.utils.AppPreference;
import com.ibt.niramaya.utils.ConnectionDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Response;


public class MyFirebaseMessangingService extends FirebaseMessagingService {

    public Context mContext;
    public RetrofitApiClient retrofitApiClient;
    public ConnectionDetector cd;
    private static final String TAG = MyFirebaseMessangingService.class.getSimpleName();

    private NotificationUtils notificationUtils;

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        cd = new ConnectionDetector(mContext);
        retrofitApiClient = RetrofitService.getRetrofit();
        Log.e("Token", "...");

        if ((AppPreference.getStringPreference(mContext, Constant.FIREBASE_TOKEN)) == null || !(AppPreference.getStringPreference(mContext, Constant.FIREBASE_TOKEN)).isEmpty()) {
            tokenApi(refreshedToken);
        }
    }

    private void sendRegistrationToServer(String token) {
        // Add custom implementation, as needed.
        Log.e(TAG, "sendRegistrationToServer: " + token);
        if (!(AppPreference.getStringPreference(mContext, Constant.FIREBASE_TOKEN)).isEmpty()) {
            tokenApi(token);
        }
    }

    private void tokenApi(String strToken) {
        @SuppressLint("HardwareIds") String android_id = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
        String strId = AppPreference.getStringPreference(mContext, Constant.USER_ID);
        if (cd.isNetworkAvailable()) {
            RetrofitService.getServerResponse(new Dialog(mContext), retrofitApiClient.userToken(android_id, strToken, strId, "1"), new WebResponse() {
                @Override
                public void onResponseSuccess(Response<?> result) {
                    ResponseBody mainModal = (ResponseBody) result.body();
                    try {
                        assert mainModal != null;
                        JSONObject jsonObject = new JSONObject(mainModal.string());
                        if (!jsonObject.getBoolean("error")) {
                            Alerts.show(mContext, jsonObject.getString("message"));
                        } else {
                            Alerts.show(mContext, jsonObject.getString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onResponseFailed(String error) {
                    Alerts.show(mContext, "Server Error!!!");
                }
            });
        }
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.e(TAG, "From: " + remoteMessage.getFrom());
        super.onMessageReceived(remoteMessage);

        if (remoteMessage.getData().size() > 0) {
            try {
                JSONObject json = new JSONObject(remoteMessage.getData().toString());
                JSONObject data = new JSONObject(json.getJSONObject("data").toString());
                String title = "Niramaya Health";
                String body = (data.getString("body"));
                String clickAction = (data.getString("click_action"));
                String resultDigit = (data.getString("type"));
                sendNewNotification(title, body, clickAction, resultDigit, data.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void sendNewNotification(String title, String body, String clickAction, String payLoad, String rawData) {
        Intent intent = new Intent(this, SplashActivity.class);
        if ("1".equals(payLoad)) {
            intent = new Intent(this, AmbulanceDetailActivity.class);
            intent.putExtra("data", rawData);
        }else if ("2".equals(payLoad)) {
            intent = new Intent(this, HomeActivity.class);
            intent.putExtra("data", rawData);
        }
        //intent = new Intent(this, Class.forName(clickAction));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Bitmap mIcon = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.splash_image);

        String channelId = "1001";
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setLargeIcon(mIcon)/*Notification icon image*/
                        .setSmallIcon(R.drawable.ic_notifications)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        // Since android Oreo notification channel is needed.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

        /*revoce a brodcast when specific notification recieved*/
        if (payLoad.equals("1")) {
            Intent mIntent = new Intent("ambulanceMap");
            mIntent.putExtra("PayloadStatus", payLoad);
            LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent);
        }else if (payLoad.equals("2")){
            Intent mIntent = new Intent("appointment");
            mIntent.putExtra("PayloadStatus", payLoad);
            LocalBroadcastManager.getInstance(this).sendBroadcast(mIntent);
        }

    }

    /**Notification Data
    {
        "type_id": 8,
        "data": {
            "type_id": "8",
            "sound": "mySound",
            "icon": "http://infobitetechnology.tech/niramaya/dist/img/logo.png",
            "booking_type": "0",
            "body": "Activity Test",
            "click_action": "com.ibt.niramaya.ui.activity.ambulance.AmbulanceDetailActivity",
            "type": "1",
            "booking_type_id": "57"
        },
        "type": 1,
        "booking_type_id": 57,
        "booking_type": 0
    }*/
}

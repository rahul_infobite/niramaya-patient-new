package com.ibt.niramaya.interfaces;

import com.ibt.niramaya.modal.calander.DateOPD;
import com.ibt.niramaya.modal.calander.DayOPD;
import com.ibt.niramaya.modal.doctor_opd_model.DoctorSchedule;

public interface InitScheduleList {

    void initScheduleList(int i, String type, DoctorSchedule dOPD);
}
